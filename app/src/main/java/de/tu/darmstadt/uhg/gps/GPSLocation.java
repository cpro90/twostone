package de.tu.darmstadt.uhg.gps;

import de.tu.darmstadt.uhg.xml.ParsedFile;

/**
 * Repr�sentiert eine GPS Location
 * Beispiel: Der Herrngarten wird durch ein Rechteck mit den Eckkoordinaten topLeftCoordinate
 * und bottomRightCoordinate angen�hert. Daraus kann dann die fieldSize errechnet werden.
 * Dies dient dazu, dass das Programm wei�, wo sich der Spieler/die Spielerin GROB aufh�lt!
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
public class GPSLocation {
	
	String locationName;
	GPSPoint bottomLeftCoordinate;
	GPSPoint topRightCoordinate;
	GPSPoint fieldSize;
	ParsedFile content;
	boolean isCustomMap;
	

	public ParsedFile getContent() {
		return content;
	}
	public void setContent(ParsedFile content) {
		this.content = content;
	}
	public boolean isCustomMap() {
		return isCustomMap;
	}
	public void setCustomMap(boolean isCustomMap) {
		this.isCustomMap = isCustomMap;
	}
	
	/**WARNING: DEPRECATED! ONLY USABLE FOR TESTS**/
	public GPSLocation(String locationName, GPSPoint bottomLeftCoordinate,
			GPSPoint topRightCoordinate, boolean isCustomMap) {
		this.locationName = locationName;
		this.isCustomMap = isCustomMap;
		this.bottomLeftCoordinate = bottomLeftCoordinate;
		this.topRightCoordinate = topRightCoordinate;
		/**fieldSize berechnen aus den TopRight und BottomLeft Coordinates**/
		this.fieldSize = new GPSPoint(getTopRightCoordinate().getLatitude()-getBottomLeftCoordinate().getLatitude(),
				                      getTopRightCoordinate().getLongitude()-getBottomLeftCoordinate().getLongitude()); 
	
	}

	public GPSLocation(ParsedFile file) {
		this.content = file;
		this.locationName = file.getTitle();
		this.isCustomMap = file.isCustomMap();
		this.bottomLeftCoordinate = file.getBottomLeft();
		this.topRightCoordinate = file.getTopRight();
		/**fieldSize berechnen aus den TopRight und BottomLeft Coordinates**/
		this.fieldSize = new GPSPoint(getTopRightCoordinate().getLatitude()-getBottomLeftCoordinate().getLatitude(),
				                      getTopRightCoordinate().getLongitude()-getBottomLeftCoordinate().getLongitude()); 
	
	}
	public GPSPoint getFieldSize() {
		return fieldSize;
	}
	public void setFieldSize(GPSPoint fieldSize) {
		this.fieldSize = fieldSize;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public GPSPoint getBottomLeftCoordinate() {
		return bottomLeftCoordinate;
	}
	public void setBottomLeftCoordinate(GPSPoint topLeftCoordinate) {
		this.bottomLeftCoordinate = topLeftCoordinate;
	}
	public GPSPoint getTopRightCoordinate() {
		return topRightCoordinate;
	}
	public void setTopRightCoordinate(GPSPoint bottomRightCoordinate) {
		this.topRightCoordinate = bottomRightCoordinate;
	}
	
	
}
