package de.tu.darmstadt.uhg.adapter;

import java.util.ArrayList;

import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.MainActivity;
import de.tu.darmstadt.uhg.R;
import de.tu.darmstadt.uhg.game.SimpleEnemy;
import de.tu.darmstadt.uhg.game.Game;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/**
 * Setzt die Image Resourcen der Liste im opponentView entsprechend den
 * �bergebenen intelligence- und speedCode
 * 
 * @author Gerhard S�ckel yx68ijoq@stud.tu-darmstadt.de
 * 
 */
@SuppressWarnings("deprecation")
public class EinstellungAdapter extends ArrayAdapter<SimpleEnemy> {
	private final Context context;
	@SuppressWarnings("unused")
	private final ArrayList<SimpleEnemy> values;
	private View rowView;
	private int currentlyClicked = -1;

	public EinstellungAdapter(Context context, ArrayList<SimpleEnemy> values) {
		super(context, R.layout.highscore_list_item, values);
		this.context = context;
		this.values = values;
		initEnemys();
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = inflater.inflate(R.layout.einstellung_list_item, parent,
				false);
		final RelativeLayout opponentListRelativeInput = (RelativeLayout) rowView
				.findViewById(R.id.opponent_list_relative_input);
		final RelativeLayout opponentListRelativeStats = (RelativeLayout) rowView
				.findViewById(R.id.opponent_list_relative_stats_panel);
		final ImageView opponentListImageCharacter = (ImageView) rowView
				.findViewById(R.id.opponent_list_image_character);
		final TextView opponentListTextCharacterName = (TextView) rowView
				.findViewById(R.id.opponent_list_text_character_name);
		final ImageView opponentListImageIntelligenceCount = (ImageView) rowView
				.findViewById(R.id.opponent_list_image_intelligence_count);
		final TextView opponentListTextIntelligenceCount = (TextView) rowView
				.findViewById(R.id.opponent_list_text_intelligence_count);
		final ImageView opponentListImageSpeedCount = (ImageView) rowView
				.findViewById(R.id.opponent_list_image_speed_count);
		final TextView opponentListTextSpeedCount = (TextView) rowView
				.findViewById(R.id.opponent_list_text_speed_count);

		final SeekBar opponentIntelligenceSlider = (SeekBar) rowView
				.findViewById(R.id.opponentIntelligenceSlider);
		final SeekBar opponentSpeedSlider = (SeekBar) rowView
				.findViewById(R.id.opponentSpeedSlider);

		if (currentlyClicked == position) {
			opponentListRelativeStats.setVisibility(View.INVISIBLE);
			opponentListRelativeInput.setVisibility(View.VISIBLE);
		} else {
			opponentListRelativeStats.setVisibility(View.VISIBLE);
			opponentListRelativeInput.setVisibility(View.GONE);
		}

		// opponentIntelligenceSlider.setProgress(MainActivity.myGhosts.get(position).getIntelligence());
		// opponentIntelligenceCounter.setText(MainActivity.myGhosts.get(position).getIntelligence()
		// + 1 +"");
		//
		// opponentSpeedSlider.setProgress(MainActivity.myGhosts.get(position).getSpeed());
		// opponentSpeedCounter.setText(MainActivity.myGhosts.get(position).getSpeed()
		// + 1 +"");

		opponentListImageCharacter.setImageDrawable(getCharacterDrawable(
				position, true));
		opponentListTextCharacterName.setText(getCharacterText(position));

		opponentIntelligenceSlider.setTag(Integer.valueOf(position));
		opponentIntelligenceSlider
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar arg0) {
						int intelligenceValue = opponentIntelligenceSlider
								.getProgress() - 1;
						int position = (Integer) arg0.getTag();
						SimpleEnemy currentEnemy = MainActivity.myEnemies
								.get(position);
						currentEnemy.setIntelligence(intelligenceValue);
						if (intelligenceValue == -1) {
							opponentSpeedSlider.setProgress(0);
							currentEnemy.setSpeed(GlobalConstants.SPEED_OFF);
						} else if (opponentSpeedSlider.getProgress() == 0) {
							opponentSpeedSlider.setProgress(1);
							currentEnemy.setSpeed(GlobalConstants.SPEED_SNAIL);
						}
					}

					@Override
					public void onStartTrackingTouch(SeekBar arg0) {

					}

					@Override
					public void onProgressChanged(SeekBar arg0, int arg1,
							boolean arg2) {
						int intelligenceValue = opponentIntelligenceSlider
								.getProgress() - 1;
						int position = (Integer) opponentIntelligenceSlider
								.getTag();
						opponentListTextIntelligenceCount
								.setText((intelligenceValue + 1) + "");
						if (intelligenceValue == -1) {
							opponentListImageIntelligenceCount
									.setImageResource(R.drawable.icon_intelligence_inactive);
							opponentListTextIntelligenceCount
									.setTextColor(context.getResources()
											.getColor(
													R.color.twostone_text_grey));
							if (opponentSpeedSlider.getProgress() == 0) {
								opponentListImageCharacter
										.setImageDrawable(getCharacterDrawable(
												position, false));
								opponentListTextCharacterName
										.setTextColor(context
												.getResources()
												.getColor(
														R.color.twostone_text_grey));

							}
						}

						else {
							opponentListImageIntelligenceCount
									.setImageResource(R.drawable.icon_intelligence);
							opponentListTextIntelligenceCount
									.setTextColor(context
											.getResources()
											.getColor(
													R.color.twostone_text_green));
							opponentListImageCharacter
									.setImageDrawable(getCharacterDrawable(
											position, true));
							opponentListTextCharacterName
									.setTextColor(getCharacterColorRessource(position));

						}
					}
				});

		opponentSpeedSlider.setTag(Integer.valueOf(position));
		opponentSpeedSlider
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar arg0) {
						int speedValue = opponentSpeedSlider.getProgress() - 1;
						int position = (Integer) arg0.getTag();
						SimpleEnemy currentEnemy = MainActivity.myEnemies
								.get(position);
						switch (speedValue) {
						case -1:
							currentEnemy.setSpeed(GlobalConstants.SPEED_OFF);
							opponentIntelligenceSlider.setProgress(0);
							currentEnemy
									.setIntelligence(GlobalConstants.ENEMY_OFF);
							break;
						case 0:
							currentEnemy.setSpeed(GlobalConstants.SPEED_SNAIL);
							break;
						case 1:
							currentEnemy.setSpeed(GlobalConstants.SPEED_SLOW);
							break;
						case 2:
							currentEnemy
									.setSpeed(GlobalConstants.SPEED_WALKSPEED);
							break;
						case 3:
							currentEnemy.setSpeed(GlobalConstants.SPEED_MEDIUM);
							break;
						case 4:
							currentEnemy.setSpeed(GlobalConstants.SPEED_FAST);
							break;
						default:
							currentEnemy.setSpeed(GlobalConstants.SPEED_SNAIL);
							break;
						}
						if (speedValue >= 0
								&& opponentIntelligenceSlider.getProgress() == 0) {
							opponentIntelligenceSlider.setProgress(1);
							currentEnemy.setIntelligence(GlobalConstants.ENEMY_FOOL);
						}

					}

					@Override
					public void onStartTrackingTouch(SeekBar arg0) {

					}

					@Override
					public void onProgressChanged(SeekBar arg0, int arg1,
							boolean arg2) {
						int speedValue = opponentSpeedSlider.getProgress() - 1;
						int position = (Integer) opponentSpeedSlider.getTag();
						opponentListTextSpeedCount.setText((speedValue + 1)
								+ "");
						if (speedValue == -1) {
							opponentListImageSpeedCount
									.setImageResource(R.drawable.icon_opponent_speed_inactive);
							opponentListTextSpeedCount.setTextColor(context
									.getResources().getColor(
											R.color.twostone_text_grey));
							if (opponentIntelligenceSlider.getProgress() == 0) {
								opponentListImageCharacter
										.setImageDrawable(getCharacterDrawable(
												position, false));
								opponentListTextCharacterName
										.setTextColor(context
												.getResources()
												.getColor(
														R.color.twostone_text_grey));
							}
						}

						if (speedValue >= 0) {
							opponentListImageSpeedCount
									.setImageResource(R.drawable.icon_opponent_speed);
							opponentListTextSpeedCount.setTextColor(context
									.getResources().getColor(
											R.color.twostone_text_green));
							opponentListImageCharacter
									.setImageDrawable(getCharacterDrawable(
											position, true));
							opponentListTextCharacterName
									.setTextColor(getCharacterColorRessource(position));
						}
					}
				});
		opponentListImageCharacter.setTag(Integer.valueOf(position));
		opponentListImageCharacter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int position = (Integer) v.getTag();
				SimpleEnemy currentEnemy = MainActivity.myEnemies
						.get(position);
				if(currentEnemy.getIntelligence() == GlobalConstants.ENEMY_OFF){
					opponentSpeedSlider.setProgress(currentEnemy.getCharacter() + 1);
					opponentIntelligenceSlider.setProgress(currentEnemy.getCharacter() + 1);
					currentEnemy.setIntelligence(Game.encodeIntelligence(currentEnemy.getCharacter() + 1));
					currentEnemy.setSpeed(Game.encodeSpeed(currentEnemy.getCharacter() + 1));
				}
				else {
					opponentSpeedSlider.setProgress(0);
					opponentIntelligenceSlider.setProgress(0);
					currentEnemy.setIntelligence(GlobalConstants.ENEMY_OFF);
					currentEnemy.setSpeed(GlobalConstants.SPEED_OFF);
				}
			}
		});

		// ImageButton deleteOpponentButton = (ImageButton) rowView
		// .findViewById(R.id.deleteOpponentButton);
		// deleteOpponentButton.setTag(Integer.valueOf(position));
		// deleteOpponentButton.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// deleteGhostAt((Integer) v.getTag());
		// }
		// });

		// int intelligenceCode = Integer
		// .parseInt(values.get(position).split(",")[0]);
		// int speedCode = Integer.parseInt(values.get(position).split(",")[1]);
		if (MainActivity.myEnemies.get(position).getCharacter() == GlobalConstants.CHARACTER_NOT_ASSIGNED) {
			MainActivity.myEnemies.get(position).setCharacter(position);
		}
		int intelligenceCode = MainActivity.myEnemies.get(position)
				.getIntelligence();
		int speedCode = MainActivity.myEnemies.get(position).getSpeed();

		switch (intelligenceCode) {
		case GlobalConstants.ENEMY_OFF:
			opponentListTextIntelligenceCount.setText("0");
			opponentIntelligenceSlider.setProgress(0);
			break;
		case GlobalConstants.ENEMY_FOOL:
			opponentListTextIntelligenceCount.setText("1");
			opponentIntelligenceSlider.setProgress(1);
			break;
		case GlobalConstants.ENEMY_EASY:
			opponentListTextIntelligenceCount.setText("2");
			opponentIntelligenceSlider.setProgress(2);
			break;
		case GlobalConstants.ENEMY_NORMAL:
			opponentListTextIntelligenceCount.setText("3");
			opponentIntelligenceSlider.setProgress(3);
			break;
		case GlobalConstants.ENEMY_GOOD:
			opponentListTextIntelligenceCount.setText("4");
			opponentIntelligenceSlider.setProgress(4);
			break;
		case GlobalConstants.ENEMY_EXTREME:
			opponentListTextIntelligenceCount.setText("5");
			opponentIntelligenceSlider.setProgress(5);
			break;
		default:
			opponentListTextIntelligenceCount.setText("1");
			opponentIntelligenceSlider.setProgress(1);
			break;
		}

		switch (speedCode) {
		case GlobalConstants.SPEED_OFF:
			opponentListTextSpeedCount.setText("0");
			opponentSpeedSlider.setProgress(0);
			break;
		case GlobalConstants.SPEED_SNAIL:
			opponentListTextSpeedCount.setText("1");
			opponentSpeedSlider.setProgress(1);
			break;
		case GlobalConstants.SPEED_SLOW:
			opponentListTextSpeedCount.setText("2");
			opponentSpeedSlider.setProgress(2);
			break;
		case GlobalConstants.SPEED_WALKSPEED:
			opponentListTextSpeedCount.setText("3");
			opponentSpeedSlider.setProgress(3);
			break;
		case GlobalConstants.SPEED_MEDIUM:
			opponentListTextSpeedCount.setText("4");
			opponentSpeedSlider.setProgress(4);
			break;
		case GlobalConstants.SPEED_FAST:
			opponentListTextSpeedCount.setText("5");
			opponentSpeedSlider.setProgress(5);
			break;
		default:
			opponentListTextSpeedCount.setText("1");
			opponentSpeedSlider.setProgress(1);
			break;
		}
		if (intelligenceCode == GlobalConstants.ENEMY_OFF) {
			opponentListTextCharacterName.setTextColor(context.getResources()
					.getColor(R.color.twostone_text_grey));
		} else {
			opponentListTextCharacterName
					.setTextColor(getCharacterColorRessource(position));
		}

		return rowView;
	}

	private Drawable getCharacterDrawable(int position, boolean active) {
		int character = MainActivity.myEnemies.get(position).getCharacter();
		if (character == GlobalConstants.CHARACTER_NOT_ASSIGNED) {
			character = position;
		}
		switch (character) {
		case 0:
			if (active)
				return context.getResources().getDrawable(
						R.drawable.icon_hookey);
			return context.getResources().getDrawable(
					R.drawable.icon_hookey_inactive);
		case 1:
			if (active)
				return context.getResources().getDrawable(
						R.drawable.icon_spookey);
			return context.getResources().getDrawable(
					R.drawable.icon_spookey_inactive);
		case 2:
			if (active)
				return context.getResources().getDrawable(
						R.drawable.icon_rookey);
			return context.getResources().getDrawable(
					R.drawable.icon_rookey_inactive);
		case 3:
			if (active)
				return context.getResources().getDrawable(R.drawable.icon_loki);
			return context.getResources().getDrawable(
					R.drawable.icon_loki_inactive);
		case 4:
			if (active)
				return context.getResources().getDrawable(
						R.drawable.icon_bazargh);
			return context.getResources().getDrawable(
					R.drawable.icon_bazargh_inactive);
		default:
			if (active)
				return context.getResources().getDrawable(
						R.drawable.icon_bazargh);
			return context.getResources().getDrawable(
					R.drawable.icon_bazargh_inactive);
		}
	}

	private int getCharacterColorRessource(int position) {
		int character = MainActivity.myEnemies.get(position).getCharacter();
		if (character == GlobalConstants.CHARACTER_NOT_ASSIGNED) {
			character = position;
		}
		switch (character) {
		case 0:
			return context.getResources().getColor(R.color.twostone_hookey);
		case 1:
			return context.getResources().getColor(R.color.twostone_spookey);
		case 2:
			return context.getResources().getColor(R.color.twostone_rookey);
		case 3:
			return context.getResources().getColor(R.color.twostone_loki);
		case 4:
			return context.getResources().getColor(R.color.twostone_bazargh);
		default:
			return context.getResources().getColor(R.color.twostone_bazargh);
		}
	}

	private String getCharacterText(int position) {
		int character = MainActivity.myEnemies.get(position).getCharacter();
		if (character == GlobalConstants.CHARACTER_NOT_ASSIGNED) {
			character = position;
		}
		switch (character) {
		case 0:
			return context.getResources().getString(R.string.hookey);
		case 1:
			return context.getResources().getString(R.string.spookey);
		case 2:
			return context.getResources().getString(R.string.rookey);
		case 3:
			return context.getResources().getString(R.string.loki);
		case 4:
			return context.getResources().getString(R.string.bazargh);
		default:
			return context.getResources().getString(R.string.bazargh);
		}
	}

	
	public void activeGhost(int intelligenceCode, int speedCode){
		SimpleEnemy newEnemy = null;
		for(int i = 0; i < MainActivity.myEnemies.size(); i++){
			if(MainActivity.myEnemies.get(i).getIntelligence() == GlobalConstants.ENEMY_OFF){
				newEnemy = MainActivity.myEnemies.get(i);
				break;
			}
		}
		if(newEnemy != null){
		newEnemy.setIntelligence(intelligenceCode);
		newEnemy.setSpeed(speedCode);
		}
	}
	
	public int getActiveEnemyCount(){
		int active = 0;
		for(int i = 0; i < MainActivity.myEnemies.size(); i++){
			if(MainActivity.myEnemies.get(i).getIntelligence() != GlobalConstants.ENEMY_OFF) active++;
		}
		return active;
	}

	public void onItemClicked(int position) {
		if (currentlyClicked == position)
			currentlyClicked = -1;
		else
			currentlyClicked = position;
		notifyDataSetChanged();
	}

	private void initEnemys() {
		int missingGhostsCount = GlobalConstants.MAX_ENEMY_COUNT
				- MainActivity.myEnemies.size();
		for (int i = 0; i < missingGhostsCount; i++) {
			// Hinzuf�gen von DummyGhosts
			if(i < 1){
			int speedCode = Game.encodeSpeed(i+1);
			int intelligenceCode = Game.encodeIntelligence(i+1);
			MainActivity.myEnemies.add(new SimpleEnemy(intelligenceCode,
					speedCode));
			}
			else{
				int speedCode = GlobalConstants.SPEED_OFF;
				int intelligenceCode = GlobalConstants.ENEMY_OFF;
				MainActivity.myEnemies.add(new SimpleEnemy(intelligenceCode,
						speedCode));
			}
			notifyDataSetChanged();
		}
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		// final TextView opponentIntelligenceCounter = (TextView) rowView
		// .findViewById(R.id.opponentIntelligenceCounter);
		// final TextView opponentSpeedCounter = (TextView) rowView
		// .findViewById(R.id.opponentSpeedCounter);
		// opponentIntelligenceCounter.setText(mainActivity.myGhosts.get(pos).getIntelligence());
		// opponentSpeedCounter.setText(mainActivity.myGhosts.get(pos).getSpeed());
	}
}