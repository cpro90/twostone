package de.tu.darmstadt.uhg;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import de.tu.darmstadt.uhg.leveleditor.LevelEditorActivity;
import de.tu.darmstadt.uhg.leveleditor.ServerProgressing;

public class AsyncLoadingQuickplayDownloadMap extends
		AsyncTask<Void, Void, String> {

	Context context;
	MainActivity mainActivity;
	String mapTitle;

	public AsyncLoadingQuickplayDownloadMap(Context context, String mapTitle) {
		this.context = context;
		mainActivity = (MainActivity) context;
		this.mapTitle = mapTitle;
	}

	@Override
	protected String doInBackground(Void... params) {
		return ServerProgressing.downloadMap(mapTitle);
	}

	protected void onPostExecute(String result) {
		if(result != null){
		LevelEditorActivity.saveLevel(
				mainActivity,
				mapTitle, result);
		mainActivity.onQuickPlayMapSelected(mapTitle);
		} else{
			Toast.makeText(mainActivity, mainActivity.getResources().getString(R.string.noNearMaps), Toast.LENGTH_SHORT).show();
		}
	}

}
