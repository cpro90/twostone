package de.tu.darmstadt.uhg.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import de.tu.darmstadt.uhg.AsyncLoadingRatings;
import de.tu.darmstadt.uhg.MainActivity;
import de.tu.darmstadt.uhg.R;
import de.tu.darmstadt.uhg.game.LocalScore;
import de.tu.darmstadt.uhg.progressing.Rating;

/**
 * Klasse f�r die Men�auswahl der verschiedenen Spielfelder.
 * 
 * @author Chris Michel c.michel@stud.tu-darmstadt.de
 * 
 */
@SuppressWarnings("deprecation")
@SuppressLint("ViewHolder")
public class AvailableMapsArrayAdapter extends ArrayAdapter<String[]> implements
		Filterable {
	private final Context context;
	private ArrayList<String[]> values;
	public ArrayList<String[]> allValues;
	private LocalScore lscore;
	private HashMap<String, Rating> allRatingsHashMap;
	private boolean ratingsLoaded = false;
	private AsyncTask<Void, Void, Void> ratingLoader;
	
	private Drawable iconLocalGreen;
	private Drawable iconLocalWhite;
	private Drawable iconOnlineGreen;
	private Drawable iconOnlineWhite;
	private Drawable iconDistanceGreen;
	private Drawable iconDistanceYellow;
	private Drawable iconDistanceRed;
	private Drawable iconRatingGreen;
	private Drawable iconRatingGreenHalf;
	private Drawable iconRatingWhite;

	public AvailableMapsArrayAdapter(Context context,
			ArrayList<String[]> values, LocalScore lScore) {
		super(context, R.layout.available_maps_list_item, values);
		this.context = context;
		this.values = values;
		this.allValues = values;
		this.lscore = lScore;
		allRatingsHashMap = new HashMap<String, Rating>();
		
		iconLocalGreen = context.getResources().getDrawable(R.drawable.icon_local_green);
		iconLocalWhite = context.getResources().getDrawable(R.drawable.icon_local_white);
		iconOnlineGreen = context.getResources().getDrawable(R.drawable.icon_online_green);
		iconOnlineWhite = context.getResources().getDrawable(R.drawable.icon_online_white);
		iconDistanceGreen = context.getResources().getDrawable(R.drawable.icon_distance_green);
		iconDistanceYellow = context.getResources().getDrawable(R.drawable.icon_distance_yellow);
		iconDistanceRed = context.getResources().getDrawable(R.drawable.icon_distance_red);
		iconRatingGreen = context.getResources().getDrawable(R.drawable.icon_rating_green);
		iconRatingGreenHalf = context.getResources().getDrawable(R.drawable.icon_rating_green_half);
		iconRatingWhite = context.getResources().getDrawable(R.drawable.icon_rating_white);
	}

	@SuppressLint("DefaultLocale")
	@Override
	public Filter getFilter() {

		Filter filter = new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				values = (ArrayList<String[]>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				values = allValues;
				FilterResults results = new FilterResults();
				ArrayList<String[]> FilteredArrayNames = new ArrayList<String[]>();

				constraint = constraint.toString().toLowerCase();
				for (int i = 0; i < values.size(); i++) {
					String dataNames = values.get(i)[0];
					if (dataNames.toLowerCase().contains(constraint.toString())) {
						FilteredArrayNames.add(new String[] { values.get(i)[0],
								values.get(i)[1], values.get(i)[2],
								values.get(i)[3] });
						Log.d("UHG", values.get(i)[0] + " <> "
								+ values.get(i)[1] + " <> " + values.get(i)[2]
								+ " <> " + values.get(i)[3] + " <FIN>");
					}
				}

				results.count = FilteredArrayNames.size();
				results.values = FilteredArrayNames;

				return results;
			}
		};

		return filter;
	}

	@Override
	public int getCount() {
		return values.size();
	}

	public int getPosition(String mapName) {
		for (int i = 0; i < values.size(); i++) {
			if (values.get(i)[0].equals(mapName))
				return i;
		}
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.available_maps_list_item,
				parent, false);
		RelativeLayout frame = (RelativeLayout) rowView
				.findViewById(R.id.AvailableMapRelativeFrame);
		TextView mapNameTextView = (TextView) rowView
				.findViewById(R.id.AvailableMapName);
		TextView mapDistance = (TextView) rowView
				.findViewById(R.id.AvailableMapDistanceText);
		TextView mapInfo = (TextView) rowView
				.findViewById(R.id.AvailableMapInfo);
		ImageView mapLabel = (ImageView) rowView
				.findViewById(R.id.AvailableMapDistanceIcon);
		ImageView onlineLabel = (ImageView) rowView
				.findViewById(R.id.IsOnlineLabels);
		ImageView localLabel = (ImageView) rowView
				.findViewById(R.id.IsLocalLabels);
		TextView bestScore = (TextView) rowView
				.findViewById(R.id.AvailableMapBestText);
		TextView length = (TextView) rowView
				.findViewById(R.id.AvailableMapLengthText);
		TextView downloads = (TextView) rowView
				.findViewById(R.id.AvailableMapDownloadsText);

		TextView mapRatingCount = (TextView) rowView
				.findViewById(R.id.AvailableMapRatingCount);
		TextView mapCommentCount = (TextView) rowView
				.findViewById(R.id.AvailableMapCommentCount);

		ImageView ratingStar1 = (ImageView) rowView
				.findViewById(R.id.AvailableMapRatingStar1);
		ImageView ratingStar2 = (ImageView) rowView
				.findViewById(R.id.AvailableMapRatingStar2);
		ImageView ratingStar3 = (ImageView) rowView
				.findViewById(R.id.AvailableMapRatingStar3);
		ImageView ratingStar4 = (ImageView) rowView
				.findViewById(R.id.AvailableMapRatingStar4);
		ImageView ratingStar5 = (ImageView) rowView
				.findViewById(R.id.AvailableMapRatingStar5);

		if (values.get(position) == null)
			return convertView;
		if (values.get(position).length < 1)
			return convertView;
		
		final String mapName = values.get(position)[0];
		
		mapNameTextView.setText(mapName);
		mapInfo.setText(values.get(position)[2]);
		if (values.get(position).length > 4 && values.get(position)[4] != null) {
			length.setText(context.getResources().getString(R.string.distance)
					+ ": " + values.get(position)[4]);
		}
		//if (values.get(position).length > 5 && values.get(position)[5] != null) {
		//	downloads.setText("Downloads: " + values.get(position)[5]);
		//}
		
		int score = lscore.getScoreFromName(mapName);
		if (score != 0)
			bestScore.setText("Highscore: " + score);
		else
			bestScore.setText("Highscore: n/a");

		// Anzeige, ob online

		if (MainActivity.levelExists(mapName)) {
			localLabel.setImageDrawable(iconLocalGreen);
			if (Double.parseDouble(values.get(position)[1]) < 500 && Double.parseDouble(values.get(position)[1]) > 0)
				rowView.setBackgroundResource(R.color.twostone_blue);
		} else {
			localLabel.setImageDrawable(iconLocalWhite);
		}

		if (values.get(position)[3].equalsIgnoreCase("false")) {
			onlineLabel.setImageDrawable(iconOnlineWhite);
		} else {
			onlineLabel.setImageDrawable(iconOnlineGreen);
			// if (values.get(position)[3].equalsIgnoreCase("downloaded_false"))
			// {
			// }
			// if (values.get(position)[3].equalsIgnoreCase("downloaded_true"))
			// {
			// }
		}

		// Entfernungen berechnen
		double dist = Double.parseDouble(values.get(position)[1]);

		// Einf�rben der aktuellen Karte
		if (context instanceof MainActivity) {
			if ((mapName)
					.equalsIgnoreCase(MainActivity.choosenLocationFromList)) {
				mapInfo.setVisibility(View.VISIBLE);
				frame.setVisibility(View.VISIBLE);

				// if (dist < MIN_DISTANCE) {
				// rowView.setBackgroundColor(0xD082A0D0);
				// } else {
				// rowView.setBackgroundColor(0xA04280A0);
				//
				// }
			}
		}

		if (dist < 0)
			mapDistance.setText("-----");
		else if (dist < 1000)
			mapDistance.setText(values.get(position)[1] + "m");
		else if (dist < 100000)
			mapDistance.setText(((double) Math.round(dist / 100)) / 10 + "km");
		else
			mapDistance.setText(Math.round(dist / 1000) + "km");

		if (dist < 0)
			mapLabel.setImageDrawable(iconDistanceRed);
		else if (dist < 500)
			mapLabel.setImageDrawable(iconDistanceGreen);
		else if (dist < 1000)
			mapLabel.setImageDrawable(iconDistanceYellow);
		else if (dist < 2500)
			mapLabel.setImageDrawable(iconDistanceYellow);
		else if (dist < 10000)
			mapLabel.setImageDrawable(iconDistanceRed);
		else
			mapLabel.setImageDrawable(iconDistanceRed);

		// int mapRating = (int)
		// Math.round(MapRating.getMapRating(mapname));
		// getMapRating is causing lags //FIXED 24/01/2016 by Chris  // Modified by Gerhard
		Rating rating;
		double mapRating;
		if(ratingsLoaded && allRatingsHashMap.containsKey(mapName)){
			rating = allRatingsHashMap.get(mapName);
		}
		else{
			rating = new Rating(mapName, 0, 0, 0);
		}
		
		mapRating = rating.getTotalRating();

		mapRatingCount.setText(mapRating + " rating");
		
		if (rating.getCommentAmount() != 1)
			mapCommentCount.setText(rating.getCommentAmount() + " comments");
		else
			mapCommentCount.setText(rating.getCommentAmount() + " comment");

		if (rating.getDownloadAmount() != 1)
			downloads.setText(rating.getDownloadAmount() + " Downloads");
		else
			downloads.setText(rating.getCommentAmount() + " Download");
		
		if (mapRating < 0.25) 
			ratingStar1.setImageDrawable(iconRatingWhite);		
		if (mapRating >= 0.25)
			ratingStar1.setImageDrawable(iconRatingGreenHalf);
		if (mapRating >= 0.75)
			ratingStar1.setImageDrawable(iconRatingGreen);
		if (mapRating >= 1.25)
			ratingStar2.setImageDrawable(iconRatingGreenHalf);
		if (mapRating >= 1.75)
			ratingStar2.setImageDrawable(iconRatingGreen);
		if (mapRating >= 2.25)
			ratingStar3.setImageDrawable(iconRatingGreenHalf);
		if (mapRating >= 2.75)
			ratingStar3.setImageDrawable(iconRatingGreen);
		if (mapRating >= 3.25)
			ratingStar4.setImageDrawable(iconRatingGreenHalf);
		if (mapRating >= 3.75)
			ratingStar4.setImageDrawable(iconRatingGreen);
		if (mapRating >= 4.25)
			ratingStar5.setImageDrawable(iconRatingGreenHalf);
		if (mapRating >= 4.75)
			ratingStar5.setImageDrawable(iconRatingGreen);
		
		return rowView;
	}
	
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		Log.d("+++++++++++++", "notifyDataSetChanged");
		loadRatings();
	}
	
	public HashMap<String, Rating> getAllRatingsHashMap(){
		return allRatingsHashMap;
	}
	
	public void onRatingsLoaded(){
		ratingsLoaded = true;
		Log.d("+++++++++++++", "onRatingsLoaded");
	}
	
	public void invalidateRatings(){
		Log.d("+++++++++++++", "invalidateRatings");
		allRatingsHashMap.clear();
		ratingsLoaded = false;
		loadRatings();
	}
	
	private void loadRatings(){
		if(ratingLoader == null || !ratingLoader.getStatus().equals(AsyncTask.Status.RUNNING))
			ratingLoader = new AsyncLoadingRatings(context, this).execute();
	}
}