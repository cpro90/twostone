package de.tu.darmstadt.uhg.progressing;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.MainActivity;

import android.os.StrictMode;
import android.util.Log;

public class MapRating {
	
	private static ArrayList<Rating> indizes = new ArrayList<Rating>();
	
	/**
	 * Add a map to the indizes list
	 * @param mapName
	 */
	public static void indicate(Rating indicator) {
		indizes.add(indicator);
	}
	
	/**
	 * Returns the Rating, if a map is Indicated
	 * @param mapName
	 */
	private static Rating getIndicated(String mapName) {
		for (int i = 0; i < indizes.size(); i++) {
			if (indizes.get(i).getMapName() == mapName)
				return indizes.get(i);
		}
		return null;
	}
	
	/**
	 * Returns the Rating, if a map is Indicated
	 * @param mapName
	 */
	public static void deleteIndicated(String mapName) {
		for (int i = 0; i < indizes.size(); i++) {
			if (indizes.get(i).getMapName() == mapName) {
				indizes.remove(i);
				return;
			}
		}
	}
	
	
	public static void rateMap (String mapName, int rating, String comment) {
	   	SQLQuery query = new SQLQuery();
	   	query.addPair("username", MainActivity.player.getUsername());
	   	query.addPair("mapname", mapName);
	   	query.addPair("rating", rating + "");
	   	query.addPair("comment", comment);
 	    query.executeSQL(GlobalConstants.PHP_RATE_URL);
 	    deleteIndicated(mapName);
	}
	
	public static void deleteMapRating (String mapName, int rating) {
	   	SQLQuery query = new SQLQuery();
	   	query.addPair("username", MainActivity.player.getUsername());
	   	query.addPair("mapname", mapName);
	   	query.addPair("rating", rating + "");
 	    query.executeSQL(GlobalConstants.PHP_DELETE_RATING_URL);
	}

	public static int getUsersMapRating (String mapName) {

	   	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	  	 StrictMode.setThreadPolicy(policy); 
	  	
	  	 SQLQuery query = new SQLQuery();
	  	 query.addPair("username", MainActivity.player.getUsername());
	  	 query.addPair("mapname", mapName);
	  	 
	  	 String result = "";
			 JSONObject json_data;
			 
			 if (!query.executeSQL(GlobalConstants.PHP_USER_RATING_URL)) return 0;
			 
			 try {
				 	 BufferedReader reader = new BufferedReader(new InputStreamReader(query.getReceivedStream(),"iso-8859-1"),8);
				 	 StringBuilder sb = new StringBuilder();
				 	 String line = null;
				 	 while ((line = reader.readLine()) != null) {
				 		 sb.append(line + "n");
				 	 } 
				 	 query.getReceivedStream().close();
				 	 result=sb.toString();
				 	 }	catch(Exception e)	{
				 		 Log.e("log_tag", "Error converting result "+e.toString());
				 		 return 0;
				 	 }
			 try {
				 	 JSONArray jArray = new JSONArray(result);
				 	 for(int i=0;i<jArray.length();i++){
					 	 json_data = jArray.getJSONObject(i);
					 	 if (json_data.equals(null))
					 		 return 0;
					 	 result = (String) json_data.get("rating");
				 	 } 
				 }
			     catch(JSONException e){
				 		 Log.e("log_tag", "Error parsing data "+e.toString());
				 		 return 0;
			 }
			 int resout = 0;
			 try {
			 	resout = Integer.parseInt(result);
			 } catch (Exception e) {}
			 
			 return (resout);
	}
	public static String getUsersComment (String mapName) {

	   	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	  	 StrictMode.setThreadPolicy(policy); 
	  	
	  	 SQLQuery query = new SQLQuery();
	  	 query.addPair("username", MainActivity.player.getUsername());
	  	 query.addPair("mapname", mapName);
	  	 
	  	 String result = "";
			 JSONObject json_data;
			 
			 if (!query.executeSQL(GlobalConstants.PHP_USER_COMMENT_URL)) return "";
			 
			 try {
				 	 BufferedReader reader = new BufferedReader(new InputStreamReader(query.getReceivedStream(),"iso-8859-1"),8);
				 	 StringBuilder sb = new StringBuilder();
				 	 String line = null;
				 	 while ((line = reader.readLine()) != null) {
				 		 sb.append(line + "n");
				 	 } 
				 	 query.getReceivedStream().close();
				 	 result=sb.toString();
				 	 }	catch(Exception e)	{
				 		 Log.e("log_tag", "Error converting result "+e.toString());
				 		 return "";
				 	 }
			 try {
				 	 JSONArray jArray = new JSONArray(result);
				 	 for(int i=0;i<jArray.length();i++){
					 	 json_data = jArray.getJSONObject(i);
					 	 try {
					 	 result = (String) json_data.get("comment");
					 	 } catch (Exception e) {
					 		 return "";
					 	 }
				 	 } 
				 }
			     catch(JSONException e){
				 		 Log.e("log_tag", "Error parsing data "+e.toString());
				 		 return "";
			 }

			 return result;

	}

	
	/**
	* Returns the global Rating and comment Amount of a map and indicates it
	* @param mapName name of the map
	* @return
	 */
	public static Rating getMapPreferences(String mapName) {
			
		Rating isAvailable = getIndicated(mapName);
		if (isAvailable != null)
			return isAvailable;
		
		Rating fin = new Rating (mapName, getTotalMapRating(mapName), (int) getTotalCommentAmount(mapName), (int) getTotalDownloadAmount(mapName));
		indicate(fin);
		return fin;
					
	}
				
				
	/**
	 * Returns the global average map rating
	 * @param mapName name of the map
	 * @return
	 */
	private static double getTotalMapRating (String mapName) {
	
	   	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	  	 StrictMode.setThreadPolicy(policy); 
	  	
	  	 SQLQuery query = new SQLQuery();
	  	 query.addPair("mapname", mapName);
	  	 
	  	 String result = "";
			 JSONObject json_data;
			 double res = 0.0;
			 
			 if (!query.executeSQL(GlobalConstants.PHP_MAP_RATING_ALL_URL)) return 0;
			 
			 try {
				 	 BufferedReader reader = new BufferedReader(new InputStreamReader(query.getReceivedStream(),"iso-8859-1"),8);
				 	 StringBuilder sb = new StringBuilder();
				 	 String line = null;
				 	 while ((line = reader.readLine()) != null) {
				 		 sb.append(line + "n");
				 	 } 
				 	 query.getReceivedStream().close();
				 	 result=sb.toString();
					 Log.d("UHG", result);
				 	 }	catch(Exception e)	{
				 		 Log.e("log_tag", "Error converting result "+e.toString());
				 		 return 0;
				 	 }
			 try {
				 	 JSONArray jArray = new JSONArray(result);
				 	 for(int i=0;i<jArray.length();i++){
					 	 json_data = jArray.getJSONObject(i);
					 	 if (json_data.equals(null))
					 		 return 0;
					 	 res = (double) json_data.getDouble("avgrate");
				 	 } 
				 }
			     catch(JSONException e){
				 		 Log.e("log_tag", "Error parsing data "+e.toString());
				 		 return 0;
			 }
			 
			 return (((double) Math.round(res*10)/10)); //Auf eine Dezimalstelle gerundet ausgeben
	}
	
	/**
	 * Returns the global comment amount
	 * @param mapName name of the map
	 * @return
	 */
	private static double getTotalCommentAmount (String mapName) {

	   	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	  	 StrictMode.setThreadPolicy(policy); 
	  	
	  	 SQLQuery query = new SQLQuery();
	  	 query.addPair("mapname", mapName);
	  	 
	  	 String result = "";
			 JSONObject json_data;
			 double res = 0.0;
			 
			 if (!query.executeSQL(GlobalConstants.PHP_MAP_COMMENTS_ALL_URL)) return 0;
			 
			 try {
				 	 BufferedReader reader = new BufferedReader(new InputStreamReader(query.getReceivedStream(),"iso-8859-1"),8);
				 	 StringBuilder sb = new StringBuilder();
				 	 String line = null;
				 	 while ((line = reader.readLine()) != null) {
				 		 sb.append(line + "n");
				 	 } 
				 	 query.getReceivedStream().close();
				 	 result=sb.toString();
					 Log.d("UHG", result);
				 	 }	catch(Exception e)	{
				 		 Log.e("log_tag", "Error converting result "+e.toString());
				 		 return 0;
				 	 }
			 try {
				 	 JSONArray jArray = new JSONArray(result);
				 	 for(int i=0;i<jArray.length();i++){
					 	 json_data = jArray.getJSONObject(i);
					 	 if (json_data.equals(null))
					 		 return 0;
					 	 res = (double) json_data.getDouble("comments");
				 	 } 
				 }
			     catch(JSONException e){
				 		 Log.e("log_tag", "Error parsing data "+e.toString());
				 		 return 0;
			 }
			 
			 return (res);
	}
	
	/**
	 * Returns the global download amount
	 * @param mapName name of the map
	 * @return
	 */
	private static double getTotalDownloadAmount (String mapName) {

	   	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	  	 StrictMode.setThreadPolicy(policy); 
	  	
	  	 SQLQuery query = new SQLQuery();
	  	 query.addPair("mapname", mapName);
	  	 
	  	 String result = "";
			 JSONObject json_data;
			 double res = 0.0;
			 
			 if (!query.executeSQL(GlobalConstants.PHP_MAP_DOWNLOADS_URL)) return 0;
			 
			 try {
				 	 BufferedReader reader = new BufferedReader(new InputStreamReader(query.getReceivedStream(),"iso-8859-1"),8);
				 	 StringBuilder sb = new StringBuilder();
				 	 String line = null;
				 	 while ((line = reader.readLine()) != null) {
				 		 sb.append(line + "n");
				 	 } 
				 	 query.getReceivedStream().close();
				 	 result=sb.toString();
					 Log.d("UHG", result);
				 	 }	catch(Exception e)	{
				 		 Log.e("log_tag", "Error converting result "+e.toString());
				 		 return 0;
				 	 }
			 try {
				 	 JSONArray jArray = new JSONArray(result);
				 	 for(int i=0;i<jArray.length();i++){
					 	 json_data = jArray.getJSONObject(i);
					 	 if (json_data.equals(null))
					 		 return 0;
					 	 res = (double) json_data.getDouble("downloads");
				 	 } 
				 }
			     catch(JSONException e){
				 		 Log.e("log_tag", "Error parsing data "+e.toString());
				 		 return 0;
			 }
			 
			 return (res);
	}
	

}
