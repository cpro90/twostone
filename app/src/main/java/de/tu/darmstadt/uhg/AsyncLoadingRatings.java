package de.tu.darmstadt.uhg;

import java.util.ArrayList;
import java.util.HashMap;

import de.tu.darmstadt.uhg.adapter.AvailableMapsArrayAdapter;
import de.tu.darmstadt.uhg.progressing.MapRating;
import de.tu.darmstadt.uhg.progressing.Rating;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class AsyncLoadingRatings extends AsyncTask<Void, Void, Void>{
	
	Context context;
	MainActivity mainActivity;
	AvailableMapsArrayAdapter availableMapsAdapter;
	ArrayList<String[]> maps;	
	HashMap<String, Rating> allRatingsHashMap;
	
	public AsyncLoadingRatings(Context context, AvailableMapsArrayAdapter mapsAdapter) {
		this.context = context;
		mainActivity = (MainActivity) context;
		availableMapsAdapter = mapsAdapter;
		allRatingsHashMap = availableMapsAdapter.getAllRatingsHashMap();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		maps = availableMapsAdapter.allValues;
		for(int i = 0; i < maps.size(); i++){
			String mapName = maps.get(i)[0];
			if(!allRatingsHashMap.containsKey(mapName)){
				allRatingsHashMap.put(mapName, MapRating.getMapPreferences(mapName));
				Log.d("+++++++++++++", "loading: " + mapName);
			}
		}

		
		return null;
	}
	
	protected void onPostExecute(Void result) {
		availableMapsAdapter.onRatingsLoaded();
	}

}
