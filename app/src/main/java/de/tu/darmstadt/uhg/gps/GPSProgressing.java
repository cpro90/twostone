package de.tu.darmstadt.uhg.gps;

import java.util.ArrayList;

import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.Player;


/**
 * Diese Klasse enth�lt weiterf�hrende Methoden zur GPS Datenverarbeitung.
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 *
 */
public class GPSProgressing {

/**Korrigiert die GPS Koordinate, sodass der Punkt auf den Path gemappt wird,
 * somit wird vermieden, dass fehlerhafte GPS Daten auf der Karte angezeigt werden.
 * Beim Prinz-Georgs-Garten kann z.B. der Spieler nicht auf einer Wiese stehen
 * (Kann nat�rlich schon, aber das gibt �rger)
 */
public static GPSPoint mapPointToPath(Player player, GPSPath path, double radius) {
	
	//Spielerposition filtern
	GPSPoint point = player.getPosition().copy();
	
	//Die n�chsten Punkte in Spielern�he suchen
	ArrayList<GPSPathPoint> pointsInRadius = getAllPointsWithinRadius(path, point, radius);
	
	//Wenn zu wenige Punkte im Umkreis, kann nicht korrigiert werden
	if (pointsInRadius.size() < 2) return point.copy();
	
	GPSPathPoint pointA;
	GPSPathPoint pointB;
	GPSPoint newPoint;
	GPSPoint bestPoint = point.copy(); //Standard
	double bestDistance = GlobalConstants.GPS_INFINITE;
	
	/**Start des Algoritmus zum Pathmapping**/
	for (int i = 0; i < pointsInRadius.size(); i++) {
		pointA = pointsInRadius.get(i);
		//Hier: noch checken, if getLeadToPoints() in der Menge von pointsInRadius ist => Performance+
		for (int j = 0; j < pointA.getLeadToPoints().size(); j++) {
			pointB = pointA.getLeadToPoints().get(j);
			newPoint = orthogonalProjection(point, pointA, pointB);
			if (getWayDifference(newPoint, point) < bestDistance) {
				bestDistance = getWayDifference(newPoint, point);
				bestPoint = newPoint.copy();
				player.setBetweenPoints(pointA, pointB);
			}
		}
	}
	
	return bestPoint.copy();
}

/**Gibt eine ArrayList mit allen Punkten im Umkreis zur�ck**/
private static ArrayList<GPSPathPoint> getAllPointsWithinRadius(GPSPath path, GPSPoint point, double radius) {
	ArrayList<GPSPathPoint> list = new ArrayList<GPSPathPoint>();
	for (int i = 0; i < path.size(); i++) {
		if (path.getPoint(i).getPoint().equalsWithTolerance(point, radius)) 
			list.add(path.getPoint(i)); 
	}
	return list;
}

/**F�hrt eine Orthogonalprojektion eines Punktes zwischen 2 Punkte aus**/
private static GPSPoint orthogonalProjection(GPSPoint point, GPSPathPoint pointA, GPSPathPoint pointB) {
	
	//Zuerst den Geradenvektor bestimmen
	double x1 = pointB.getPoint().getLatitude() - pointA.getPoint().getLatitude();
	double y1 = pointB.getPoint().getLongitude() - pointA.getPoint().getLongitude();
	double wayX = x1;
	double wayY = y1;
	
	//Daraus kann der Orthogonalvektor bestimmt werden
	double x2 = -y1;
	double y2 = x1;
	
	//Gleichsetzen
	double difX = pointA.getPoint().getLatitude()-point.getLatitude();;
	double difY = pointA.getPoint().getLongitude()-point.getLongitude();
	//m*x2 - l*x1 = difX;
	//m*y2 - l*y2 = difY;
	
	//GAUSS-ELIMINATION
	//I == II
	double scalar = y2/x2;
	x2 *= scalar;
	x1 *= scalar;
	difX *= scalar;
	
	//II-I
	x1 -= y1;
	difX -= difY;
	
    //difY = lambda
    difX /= -x1;
    
    double newX = pointA.getPoint().getLatitude()+wayX*difX;
    double newY = pointA.getPoint().getLongitude()+wayY*difX;
    
	//Abfrage, ob Punkt zwischen zwei Punkten sein kann!
	if (((newX < pointA.getPoint().getLatitude()) &&
			(newX < pointB.getPoint().getLatitude())) ||
			((newX > pointA.getPoint().getLatitude()) &&
		    (newX > pointB.getPoint().getLatitude()))){
				if (getWayDifference(new GPSPoint(newX,newY),pointA.getPoint()) < getWayDifference(point,pointB.getPoint())) {
					return pointA.getPoint().copy();
				} else {
					return pointB.getPoint().copy();			
				}
	}
	
	return new GPSPoint(newX, newY);	
}


/**Gibt true zur�ck, wenn der Player sich "sp�rbar" bewegt hat, d.h. den Toleranzbereich �berschreitet**/
public static boolean checkMovementTolerance(GPSPoint a, GPSPoint b) {
	return (Math.abs(a.getLatitude()-b.getLatitude()) >= GlobalConstants.TOLERANCEMOVEMENT) &&
			(Math.abs(a.getLongitude()-b.getLongitude()) >= GlobalConstants.TOLERANCEMOVEMENT);
}

/**Gibt den Punkt in der Mitte zwischen zwei Punkten zur�ck**/
public static GPSPoint getCenterPoint(GPSPoint a, GPSPoint b) {
	return new GPSPoint ((a.getLatitude()+b.getLatitude())/2,(a.getLongitude()+b.getLongitude())/2);
}

	/**Returns true if GPSKoordinate is within Tolerance**/
	public static boolean checkPositionTolerance (GPSPoint a, GPSPoint b) {
		return (getWayDifference(a,b) <= GlobalConstants.TOLERANCEPOSITION);
	}
	
	/**Returns difference way between to GPS Points in meter**/
	public static double getWayDifference(GPSPoint a, GPSPoint b) {
		 
		double lat = Math.toRadians(b.latitude - a.latitude);
		double lon = Math.toRadians(b.longitude - a.longitude);
		
		double x = Math.sin(lat / 2) * Math.sin(lat / 2) + Math.cos(Math.toRadians(a.latitude)) * Math.cos(Math.toRadians(b.latitude)) * Math.sin(lon / 2) * Math.sin(lon / 2);
		double c = 2 * Math.atan2(Math.sqrt(x), Math.sqrt(1 - x));
		double d = GlobalConstants.EARTH_RADIUS * c;
		return Math.abs(d);
	}

	/**Returns Time difference time between to GPS Points in seconds**/
	public static double getTimeDifference(GPSPoint a, GPSPoint b) {
		return Math.abs(a.getRecordedTime()-b.getRecordedTime())/1000;
	}

	/**Returns speed between to GPS Points in m/s**/
	public static double getSpeed(GPSPoint a, GPSPoint b) {
		 double way = getWayDifference(a, b);
		 double time = getTimeDifference(a, b);
		 return (way/time);
	}
	
}
