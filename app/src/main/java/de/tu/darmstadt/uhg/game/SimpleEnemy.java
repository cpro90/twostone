package de.tu.darmstadt.uhg.game;

import de.tu.darmstadt.uhg.GlobalConstants;

/**
 * Repr�sentiert einen einfachen Geist, der noch nicht aufs Spielfeld geschickt
 * wurde. F�r die Voreinstellungen der Intelligenz und Geschwindigkeit der
 * Geister gedacht.
 * 
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
public class SimpleEnemy {

	int intelligence;
	int speed;
	int character = GlobalConstants.CHARACTER_NOT_ASSIGNED;

	public int getIntelligence() {
		return intelligence;
	}

	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getCharacter() {
		return character;
	}

	public void setCharacter(int character) {
		this.character = character;
	}

	public SimpleEnemy(int intelligence, int speed) {
		this.intelligence = intelligence;
		this.speed = speed;
	}

}
