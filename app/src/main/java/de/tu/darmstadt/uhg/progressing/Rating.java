package de.tu.darmstadt.uhg.progressing;

public class Rating {
	String mapName;
	double totalRating;
	int commentAmount;
	int downloadAmount;
	
	public Rating(String mapName, double totalRating, int commentAmount, int downloadAmount) {
		super();
		this.mapName = mapName;
		this.totalRating = totalRating;
		this.commentAmount = commentAmount;
		this.downloadAmount = commentAmount;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public double getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(double totalRating) {
		this.totalRating = totalRating;
	}

	public int getCommentAmount() {
		return commentAmount;
	}

	public void setCommentAmount(int commentAmount) {
		this.commentAmount = commentAmount;
	}
	
	public int getDownloadAmount() {
		return commentAmount;
	}

	public void setDownloadAmount(int downloadAmount) {
		this.downloadAmount = downloadAmount;
	}
	
	
}
