package de.tu.darmstadt.uhg.gps;

import java.util.ArrayList;

import de.tu.darmstadt.uhg.leveleditor.Connection;

/**
 * Diese Klasse wurde f�r den Open-Street-Map-View angelegt. Zu einem GPSPoint
 * werden zus�tzlich ein Titel und eine Beschreibung gespeichert. So k�nnen
 * sp�ter Marker verwendet werden.
 * 
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
public class GPSPointText {
	GPSPoint point;
	String title;
	String description;
	double size;
	ArrayList<Connection> connections;
	
	public ArrayList<Connection> getConnections() {
		return connections;
	}

	public void setConnections(ArrayList<Connection> connections) {
		this.connections = connections;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	/**Gibt die GPS Koordinate zur�ck**/
	public GPSPoint getPoint() {
		return point;
	}

	/**Setzt die GPS Koordinate**/
	public void setPoint(GPSPoint point) {
		this.point = point;
	}

	/**Gibt den Titel zur�ck**/
	public String getTitle() {
		return title;
	}

	/**Setzt den Titel**/
	public void setTitle(String title) {
		this.title = title;
	}

	/**Gibt den Beschreibungstext zur�ck**/
	public String getDescription() {
		return description;
	}

	/**Setzt den Beschreibungstext**/
	public void setDescription(String description) {
		this.description = description;
	}

	public GPSPointText(GPSPoint point, String title, String description, double size, ArrayList<Connection> c) {
		super();
		this.point = point;
		this.title = title;
		this.description = description;
		this.size = size;
		this.connections = c;
	}

}
