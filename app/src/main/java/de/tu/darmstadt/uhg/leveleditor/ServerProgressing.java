package de.tu.darmstadt.uhg.leveleditor;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.progressing.MapRating;
import de.tu.darmstadt.uhg.progressing.SQLQuery;
import de.tu.darmstadt.uhg.xml.ParsedFile;
import de.tu.darmstadt.uhg.xml.ShortParsedFile;

/**
 * Schnittstelle f�r die Kommunikation zum Server.
 * Diese Klasse ist Bestandteil der Bachelorarbeit 
 * "Erzeugen von Spielfeldern f�r Standortbasierte Bewegungsspiele"
 * @author Chris Michel
 */
public class ServerProgressing {
	
	/**L�dt eine Karte auf den Server hoch**/
	public static boolean uploadMap(String username, ParsedFile map) {
		 SQLQuery query = new SQLQuery();
    	 query.addPair("user", username);   
    	 query.addPair("mapName", SQLQuery.cleanString(map.getTitle()));    	
    	 query.addPair("mapDescription", SQLQuery.cleanString(map.getDescription())); 
    	 double lat = (map.getBottomLeft().getLatitude() + 
    			 map.getTopRight().getLatitude())/2;
    	 double lon = (map.getBottomLeft().getLongitude() + 
    			 map.getTopRight().getLongitude())/2;
    	 query.addPair("pos", lat + "," + lon); 
    	 query.addPair("content", map.makeXMLContent());
		 
		 return (query.executeSQL(GlobalConstants.PHP_UPLOAD_FILE_URL));
	}
	
	/**Holt sich alle Online Maps als Vorschau (ohne Inhalt)*/
	  public static ArrayList<ShortParsedFile> getAllOnlineMaps() {
	  	
		     SQLQuery query = new SQLQuery();
			 ArrayList<ShortParsedFile> results = new ArrayList<ShortParsedFile>();
			 if(!query.executeSQL(GlobalConstants.PHP_GETALL_FILE_URL)) {
				 return results;
			 }
			 JSONObject json_data;
			 String result = query.querying();
			 
			 try{
				 	 JSONArray jArray = new JSONArray(result);
				 	 for(int i=0;i<jArray.length();i++){
				 	 json_data = jArray.getJSONObject(i);
				 	 results.add(
				 			 new ShortParsedFile(json_data.get("mapName") + "", 
					 				json_data.get("mapDescription") + "", 
					 				json_data.get("pos") + "", 
					 				json_data.get("user") + ""));
				 	 } 
				}
				catch(JSONException e){
				       Log.e("UHG", "Error parsing data "+e.toString());
				}
			 return results;
		}

	  /**Testet, ob die Karte bereits auf dem Server existiert*/
	  public static boolean onlineMapExists(String mapName) {
	  	
		     SQLQuery query = new SQLQuery();
	    	 query.addPair("mapName", SQLQuery.cleanString(mapName));  
			 if(!query.executeSQL(GlobalConstants.PHP_MAPEXISTS_URL)) {
				 return true;
			 }
			 
			 String result = query.returnSingle("mapName", query.querying());			 
			 return (result.equalsIgnoreCase(mapName));
		}
	  

	  /**Testet, ob die Karte der angegebenen Person geh�rt*/
	  public static boolean isMapOwner (String owner, String mapName) {
	  	
		     SQLQuery query = new SQLQuery();
	    	 query.addPair("mapName", SQLQuery.cleanString(mapName));  
	    	 query.addPair("user", SQLQuery.cleanString(owner));  
			 if(!query.executeSQL(GlobalConstants.PHP_MAPOWNER_URL)) {
				 return false;
			 }	
			 String result = query.returnSingle("mapName", query.querying());
		     return (result.equalsIgnoreCase(mapName));
		}


	  /**L�scht die Karte vom Server*/
	  public static void deleteMapFromServer (String owner, String mapName) {
	  	
		     SQLQuery query = new SQLQuery();
	    	 query.addPair("mapName", SQLQuery.cleanString(mapName));
	    	 query.addPair("user", SQLQuery.cleanString(owner));
			 query.executeSQL(GlobalConstants.PHP_MAPDELETE_URL);
		}

	  /**L�dt den Inhalt einer Karte vom Server*/
	  public static String downloadMap(String mapName) {
	  	
		     SQLQuery query = new SQLQuery();
	    	 query.addPair("mapName", SQLQuery.cleanString(mapName));   
			 if(!query.executeSQL(GlobalConstants.PHP_DOWNLOAD_FILE_URL)) {
				 return "";
			 }
			 MapRating.deleteIndicated(mapName);
			 
			 String result = query.returnSingle("content", query.querying());
			 return result;
		}
	  
}
