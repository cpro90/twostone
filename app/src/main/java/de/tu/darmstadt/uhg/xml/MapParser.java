package de.tu.darmstadt.uhg.xml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import de.tu.darmstadt.uhg.gps.GPSPath;
import de.tu.darmstadt.uhg.gps.GPSPoint;


/**
 * Diese Klasse ist f�r das parsen einer MapName.xml Datei zust�ndig.
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
public class MapParser {

	GPSPath path;	
	String completeInput;
	
	public String getCompleteInput() {
		return completeInput;
	}


	public void setCompleteInput(String completeInput) {
		this.completeInput = completeInput;
	}


	public MapParser(GPSPath path) {
		this.path = path;
	}

	
	/**Parst eine komplette XML-File**/
	public MapParser (InputStream file, GPSPath path) {
		this.path = path;
		try {
	        BufferedReader buffer = new BufferedReader(new InputStreamReader(file));
	        String out = "";
	        String line;
	        while ((line = buffer.readLine()) != null) {
	        	out += line;
	        }
	        this.completeInput = out;
		       parseAllPointTags(out);
		       parseAllConnections(out);
		       buffer.close();		
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	/**Parst alle Connections einer XML-File**/
	public void parseAllConnections(String input) {
		while (input.length() > 0)
			input = parseConnectionTag(input);
	}

	/**Parst alle Points einer XML-File**/
	public void parseAllPointTags(String input) {
		while (input.length() > 0)
			input = parsePointTag(input);
	}

	/**Parst eine Connection und verbindet 2 Pfadpunkte (int, int)**/
	public String parseConnectionTag(String input) {
		if (input.length() < 1) return "";
		ParsedEntry path = parseTag(input,"connect");
		parseConnection(path.getParsedText());
		return path.getRestString();
	}
	
	/**Parst einen Punkt und f�gt dessen Lat und Lon dem Path hinzu und gibt den Reststring zur�ck**/
	public String parsePointTag(String input) {
		if (input.length() < 1) return "";
		ParsedEntry point = parseTag(input,"point");
		parseGPSPathPoint(point.getParsedText());
		return point.getRestString();
	}
	

	/**Parst Latitude und Longitude eines PathPoints im String und f�gt diesen dem Path hinzu**/
	public void parseGPSPathPoint(String input) {
		if (input.length() < 1) return;
		ParsedEntry lat = parseTag(input,"lat");
		ParsedEntry lon = parseTag(input,"lon");
		ParsedEntry inactive = parseTag(input,"inactive");
		ParsedEntry name = parseTag(input, "name");
		ParsedEntry startFlag = parseTag(input, "start");
		ParsedEntry ghostIndex = parseTag(input, "ghostIndex");
		GPSPoint p = new GPSPoint(Double.parseDouble(lat.getParsedText()),
				Double.parseDouble(lon.getParsedText()));
		path.addPoint(p, 
				name.getParsedText(), 
				startFlag.getParsedText(), 
				ghostIndex.getParsedText(), 
				inactive.getParsedText());		
	}

	/**Parst Latitude und Longitude eines GPSPoints im String**/
	public GPSPoint parseGPSPoint(String input) {
		if (input.length() < 1) return new GPSPoint(0,0);
		ParsedEntry lat = parseTag(input,"lat");
		ParsedEntry lon = parseTag(input,"lon");
		return new GPSPoint(Double.parseDouble(lat.getParsedText()),
				Double.parseDouble(lon.getParsedText()));
	}
	
	/**Parst den Integer im String**/
	public int parseInteger(String input) {
		if (input == "") return -1;
		return Integer.parseInt(input);
	}

	/**Parst ein boolean im String
	 * true, when String="true"
	 * false, when String!="true"**/
	public boolean parseBoolean(String input) {
		return (input.equalsIgnoreCase("true"));
	}

	/**Parst Start und Endknoten im String und f�gt diesen dem Path hinzu**/
	public void parseConnection(String input) {
		if (input.length() < 1) return;
		ParsedEntry a = parseTag(input,"source");
		ParsedEntry b = parseTag(input,"target");
		int Avalue = Integer.parseInt(a.getParsedText());
		int Bvalue = Integer.parseInt(b.getParsedText());
		path.connect(Avalue, Bvalue);		
	}
	
	/**Parst einen Tag
	 * z.B. parseTag("<tag>Hallo</tag>","tag") = "Hallo"*/
	public static ParsedEntry parseTag(String input, String tag) {
		
        String tagStart = "<" + tag + ">";
        String tagEnd = "</" + tag + ">";
        
        int start = input.indexOf(tagStart) + tagStart.length();
        int end = input.indexOf(tagEnd);
        
        if ((start == -1) || (end == -1)) return new ParsedEntry("","");
        
        return new ParsedEntry(input.substring(start, end), input.substring(end+tagEnd.length()));
        
	}

	/**Parst einen Tag und gibt den Inhalt direkt wieder
	 * z.B. parseTag("tag") = "Hallo", sofern Input der letzten File = "<tag>Hallo</tag>"*/
	public String getTag(String tag) {		
        return parseTag(completeInput, tag).getParsedText();        
	}

	/**Parst einen Tag und gibt den Inhalt direkt wieder
	 * z.B. parseTag("<tag>Hallo</tag>","tag") = "Hallo"*/
	public static String getTag(String input, String tag) {		
        return parseTag(input, tag).getParsedText();        
	}
	
	
}
