package de.tu.darmstadt.uhg.game;

import java.util.ArrayList;

import android.graphics.Bitmap;

public class Animation {
	ArrayList<Bitmap> bitmaps;
	int animationCounter;
	boolean finished;

	public Animation() {
		super();
		this.bitmaps = new ArrayList<Bitmap>();
		this.finished = false;
		animationCounter = 0;
	}
	
	public Bitmap getNextAnimationPicture() {
		animationCounter++;
		if (animationCounter >= bitmaps.size())
			animationCounter = 0;
		
		finished = (animationCounter >= bitmaps.size()-1);
				
		if (bitmaps.size() < 1)
			return null;
		
		return bitmaps.get(animationCounter);
	}
	
	public boolean finishedAnimation() {
		return finished;
	}
	
	public void addAnimationPicture(Bitmap bitmap) {
		bitmaps.add(bitmap);
	}

		
	
	

}
