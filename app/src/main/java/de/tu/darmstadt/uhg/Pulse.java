package de.tu.darmstadt.uhg;

public class Pulse {
	public static int pulse;

	public static int getPulse() {
		return pulse;
	}

	public static void setPulse(int pulse) {
		Pulse.pulse = pulse;
	}
}
