package de.tu.darmstadt.uhg.leveleditor;

import android.support.v4.app.FragmentActivity;

/**
 * NOT IN USE ANYMORE. USE MapFragment INSTEAD
 * Klasse zur Darstellung aller insgesamt verf�gbaren Karten online und offline.
 * Diese Klasse ist Bestandteil der Bachelorarbeit 
 * "Erzeugen von Spielfeldern f�r Standortbasierte Bewegungsspiele"
 * @author Chris Michel
 */
public class MapActivity extends FragmentActivity  {

//	static GoogleMap map;
//	static ArrayList<Circle> circleArray; 
//	boolean initial = true;
//	static ArrayList<ShortParsedFile> onlineMaps; 
//	public static ArrayList<GPSPointText> points = new ArrayList<GPSPointText>();
//	static ArrayList<Marker> markers = new ArrayList<Marker>();
//	static String selectedMarker;
//	static boolean selectedMarkerOnline;
//	static boolean SHOW_ONLINE_MAPS = true;
//	
//	
//	/**Gibt die Position des Spielfeldes zur�ck**/
//	public static GPSPoint getMarkerPosition (String title) {
//		for (int i = 0; i < points.size(); i++) {
//			if (points.get(i).getTitle().equalsIgnoreCase(title))
//				return points.get(i).getPoint();
//		}
//		return new GPSPoint(0, 0);		
//	}
//	
//	/**
//	 * Generates a marker on the overlay
//	 * @param pos GPS Position of the marker
//	 * @param title Title of the marker
//	 * @param description Description of the marker
//	 */
//	public static void generateMarker(GPSPoint pos, String title, String description, double size, ArrayList<Connection> c) {
//		points.add(new GPSPointText(pos, title, description, size, c));
//	}	
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//				WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		
//		 super.onCreate(savedInstanceState);
//		 setContentView(R.layout.activity_map);  
//
//		onlineMaps = ServerProgressing.getAllOnlineMaps();
//			
//		 SupportMapFragment mapFragment = 
//				 (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.locationMap);		 
//		map = mapFragment.getMap();		
//		map.getUiSettings().setZoomControlsEnabled(false);
//		map.setMyLocationEnabled(true);
//		drawAllMarkers();
//		//createAllCircles();
//		drawAllMapShapes();
//
//	     
//		map.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
//			
//			@Override
//			public void onMyLocationChange(Location location) {
//				//drawAllCircles(location);
//							
//			}
//		});
//	     map.setOnMarkerClickListener(new OnMarkerClickListener() {
//			
//			@Override
//			public boolean onMarkerClick(Marker marker) {
//				TextView viewName = (TextView) findViewById(R.id.theLocationName);
//				TextView viewInfo = (TextView) findViewById(R.id.theLocationInfo);
//				viewName.setText(marker.getTitle());
//				viewInfo.setText(marker.getSnippet());			
//				RelativeLayout view = (RelativeLayout) findViewById(R.id.theLocationView);
//				view.setVisibility(RelativeLayout.VISIBLE);	
//				ImageView button = (ImageView) findViewById(R.id.theLocationSelector);
//				if (MainActivity.levelExists(marker.getTitle())) {
//					//LOCALE
//					button.setImageResource(R.drawable.play);
//					selectedMarkerOnline = false;
//				} else {
//					//ONLINE
//					button.setImageResource(R.drawable.upload_map);
//					selectedMarkerOnline = true;
//				}
//				selectedMarker = marker.getTitle();
//				return false;
//			}
//		});
//	     
//
//		ImageView playButton = (ImageView) findViewById(R.id.theLocationSelector);
//		playButton.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				if (selectedMarker != "") {
//					if (selectedMarkerOnline) {
//													
//						AlertDialog.Builder builder = new AlertDialog.Builder(
//								MapActivity.this);
//						
//						builder.setMessage(
//								getResources().getString(
//										R.string.downloadMap))
//								.setTitle(
//										getResources().getString(
//												R.string.downloadMapTitle));
//
//						builder.setPositiveButton(
//								getResources().getString(R.string.ok),
//								new DialogInterface.OnClickListener() {
//									public void onClick(DialogInterface dialog, int id) {
//										String content = ServerProgressing.downloadMap(selectedMarker);
//										LevelEditorActivity.saveLevel(MapActivity.this, selectedMarker, content);
//										MainActivity.updateAllAvailableMaps(MapActivity.this);
//										refreshAll();
//										
//									}
//								});
//
//						builder.setNegativeButton(
//								getResources().getString(R.string.cancel),
//								new DialogInterface.OnClickListener() {
//									public void onClick(DialogInterface dialog,
//											int id) {
//										//Do nothing
//									}
//								});
//
//						Dialog dialog = builder.create();
//						dialog.show();
//						
//					} else {
//						MainActivity.selectLevelByName(selectedMarker, MapActivity.this);
//						MainActivity.updateAllAvailableMaps(MapActivity.this);
//						MapActivity.this.finish();
//					}
//				}
//			}
//		});
//	}
//	
//	/**Draw all the shapes of the level to the map**/
//	private void drawAllMapShapes() {		
//		for (int i = 0; i < points.size(); i++) {
//			Random rnd = new Random(); 
//			int color = Color.argb(172, rnd.nextInt(128), rnd.nextInt(128), rnd.nextInt(128));   
//			ArrayList<Connection> connectionList = points.get(i).getConnections();
//			for (int j = 0; j < connectionList.size(); j++) {
//				Connection current = connectionList.get(j);
//				for (int k = 0; k < current.getConnectsTo().size(); k++) {
//					drawLineOnMap(current, current.getConnectsTo().get(k), color);
//				}
//			}
//		}
//	}
//	
//	
//	/**Draw a Line between two Connections on the Map**/
//	public void drawLineOnMap(Connection from, Connection to, int color) {
//		PolylineOptions line=
//			      new PolylineOptions().add(new LatLng(from.getPos().latitude,
//			    		  							   from.getPos().longitude),
//			    		  					new LatLng(to.getPos().latitude,
//					    		  					   to.getPos().longitude))
//			                           .width(10).color(color);
//		map.addPolyline(line);
//	}
//	
//	
//	/**Draw all Markers to the Overview**/
//	private void drawAllMarkers() {
//	     //ONLINE MAPS
//	     if (SHOW_ONLINE_MAPS) {
//		     for (int i = 0; i < onlineMaps.size(); i++) {
//		    	 if (!MainActivity.levelExists(onlineMaps.get(i).getTitle())) {
//		    		 GPSPoint p = new GPSPoint(onlineMaps.get(i).getPos());
//			    	 LatLng g = new LatLng(p.getLatitude(), p.getLongitude());
//				     MarkerOptions marker = new MarkerOptions();
//				     marker.position(g);
//				     marker.title(onlineMaps.get(i).getTitle());
//				     marker.snippet(onlineMaps.get(i).getDescription());
//				     marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_blue));
//				     markers.add(map.addMarker(marker));
//		    	 }
//		     }
//	     }
//	     
//		//OFFLINE MAPS
//	     for (int i = 0; i < points.size(); i++) {
//	    	  LatLng g = new LatLng(points.get(i).getPoint().getLatitude(),
//			    		 points.get(i).getPoint().getLongitude());
//			     MarkerOptions marker = new MarkerOptions();
//			     marker.position(g);
//			     marker.title(points.get(i).getTitle());
//			     marker.snippet(points.get(i).getDescription());	
//			     marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_green));		     
//			     markers.add(map.addMarker(marker));
//	     }
//	    
//	}
//	
//	/**Refresh all Markers and Shapes. Usefull after Downloads**/
//	private void refreshAll() {
//		map.clear();
//		drawAllMarkers();
//		drawAllMapShapes();
//		selectedMarker = "";
//		RelativeLayout view = (RelativeLayout) findViewById(R.id.theLocationView);
//		view.setVisibility(RelativeLayout.INVISIBLE);	
//	}
//	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.map, menu);
//		
//		MenuItem abort = menu.findItem(R.id.abortMap);
//		abort.setOnMenuItemClickListener(new OnMenuItemClickListener() {
//			@Override
//			public boolean onMenuItemClick(MenuItem arg0) {
//				finish();
//				return true;
//			}
//		});
//		
//		return true;
//	}

}
