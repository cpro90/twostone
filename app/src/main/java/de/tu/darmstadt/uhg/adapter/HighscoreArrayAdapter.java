package de.tu.darmstadt.uhg.adapter;

import java.util.ArrayList;

import de.tu.darmstadt.uhg.MainActivity;
import de.tu.darmstadt.uhg.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * F�llt die Zeilen des Highscores mit Position, Name und Punkte, den �bergebenen Daten entsprechend.
 * @author Gerhard S�ckel yx68ijoq@stud.tu-darmstadt.de
 *
 */
public class HighscoreArrayAdapter extends ArrayAdapter<String> {
  private final Context context;
  private final ArrayList<String> values;

  public HighscoreArrayAdapter(Context context, ArrayList<String> values) {
    super(context, R.layout.highscore_list_item, values);
    this.context = context;
    this.values = values;
  }

  @SuppressLint("ViewHolder") @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View rowView = inflater.inflate(R.layout.highscore_list_item, parent, false);
    TextView highscoreNr = (TextView) rowView.findViewById(R.id.HighscoreNr);
    TextView highscoreName = (TextView) rowView.findViewById(R.id.HighscoreName);
    TextView highscorePoints = (TextView) rowView.findViewById(R.id.HighscorePoints);
    if(values.get(position).split(" ")[1].equals(MainActivity.player.getUsername())){
    	highscoreNr.setTextColor(Color.parseColor("#C97500"));
    	highscoreName.setTextColor(Color.parseColor("#C97500"));
    	highscorePoints.setTextColor(Color.parseColor("#C97500"));
    }
    highscoreNr.setText((position + 1) + ".");
    highscoreName.setText(values.get(position).split(" ")[1]);
    String p = values.get(position).split(" ")[0];
    highscorePoints.setText(p + " m");

    return rowView;
  }
} 