package de.tu.darmstadt.uhg.xml;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewFiraSansLight extends TextView {
	
	static Typeface firaSansLight;

	public TextViewFiraSansLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypefaceFiraSansLight();
    }

   public TextViewFiraSansLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypefaceFiraSansLight();
    }

   public TextViewFiraSansLight(Context context) {
        super(context);
        setTypefaceFiraSansLight();
   }
   
   private void setTypefaceFiraSansLight(){
	   if(firaSansLight == null) firaSansLight = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "FiraSans-Light.ttf");
	   setTypeface(firaSansLight);
   }

}
