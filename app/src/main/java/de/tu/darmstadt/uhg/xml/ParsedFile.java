package de.tu.darmstadt.uhg.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import android.content.Context;
import android.widget.Toast;
import de.tu.darmstadt.uhg.gps.GPSPath;
import de.tu.darmstadt.uhg.gps.GPSPathPoint;
import de.tu.darmstadt.uhg.gps.GPSProgressing;
import de.tu.darmstadt.uhg.MapFragment;
import de.tu.darmstadt.uhg.R;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.leveleditor.Connection;

/**
 * Diese Klasse repr�sentiert eine vollst�ndig geparste XML Datei
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 *
 */
public class ParsedFile {
	String title;
	GPSPoint bottomLeft;
	GPSPoint topRight;
	GPSPoint marker;
	GPSPath path;
	InputStream file;
	String description;
	String savePath;
	int fileResource;
	int imageResource;
	boolean isCustomMap;
	boolean boundOnWays;
	boolean isOnline;
	int maxGhosts;
	int maxTime;
	long distance;
	static double OFFSET = 0.001d;

	
	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public boolean isOnline() {
		return isOnline;
	}

	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}
	
	public boolean isBoundOnWays() {
		return boundOnWays;
	}

	public void setBoundOnWays(boolean boundOnWays) {
		this.boundOnWays = boundOnWays;
	}
	
	public int getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(int maxTime) {
		this.maxTime = maxTime;
	}

	public boolean isCustomMap() {
		return isCustomMap;
	}

	public void setCustomMap(boolean isCustomMap) {
		this.isCustomMap = isCustomMap;
	}

	public int getMaxGhosts() {
		return maxGhosts;
	}

	public void setMaxGhosts(int maxGhosts) {
		this.maxGhosts = maxGhosts;
	}
	
	public int getFileResource() {
		return fileResource;
	}

	public void setFileResource(int resource) {
		this.fileResource = resource;
	}
	
	public int getImageResource() {
		return imageResource;
	}

	public void setImageResource(int resource) {
		this.imageResource = resource;
	}

	/**
	public ParsedFile(Context context, int fileResource, int imageResource, boolean isCustomMap) {
		this.isOnline = true;
		this.fileResource = fileResource;
		this.imageResource = imageResource;
		this.file = context.getResources().openRawResource(fileResource);
		this.isCustomMap = isCustomMap;
		parseAll("");
		MapActivity.generateMarker(
				this.marker, 
				this.title, 
				this.description,
				GPSProgressing.getWayDifference(
						this.getBottomLeft(),
						this.getTopRight())
				);
	}**/

	
	/**parsing for assets
	public ParsedFile(Context context, String filePath) {
		try {
			this.isOnline = true;
			this.file = context.getAssets().open("maps/" + filePath);
			this.isCustomMap = true; //eigentlich nicht, aber alles wurde umgestellt!
			parseAll(filePath.substring(0, filePath.length()-4));
			MapActivity.generateMarker(
					this.marker, 
					this.title, 
					this.description,
					GPSProgressing.getWayDifference(
							this.getBottomLeft(),
							this.getTopRight()));
		} catch (IOException e) {
			//Fehler beim Finden der Datei, kann per Definition nicht auftreten
		}
	}**/

	/**parsing for own maps**/
	public ParsedFile(String filePath) {
		try {
			this.isOnline = false;
			this.file = 
					new FileInputStream(new File(filePath));
			this.isCustomMap = true;
			parseAll(filePath.substring(0, filePath.length()-4));
			this.savePath = filePath;
			MapFragment.generateMarker(
					this.marker, 
					this.title, 
					this.description,
					GPSProgressing.getWayDifference(
							this.getBottomLeft(),
							this.getTopRight()),
					Connection.GPSPathToConnectionArray(this.path));
		} catch (IOException e) {
			//Fehler beim Finden der Datei, kann per Definition nicht auftreten
		}
	}
	
	public void save(Context context) {
		if (savePath == null) {
			Toast.makeText(
					context.getApplicationContext(),
					context.getResources().getString(R.string.cannotUpload),
					Toast.LENGTH_SHORT).show();
		} else {
			File outputFile = new File(savePath);
			try {
				if (!outputFile.exists()) {
						outputFile.createNewFile();
				}
				FileWriter output = new FileWriter(outputFile);
				output.write(this.makeXMLContent());
				output.close();			
			} catch (IOException e) {
				Toast.makeText(
						context.getApplicationContext(),
						context.getResources().getString(R.string.savingError),
						Toast.LENGTH_SHORT).show();
			
			}
		}
	}
	
	public ParsedFile(String title, String description, GPSPoint bottomLeft, GPSPoint topRight, GPSPoint marker, int maxGhosts, long distance, int maxTime, GPSPath path) {
		this.title = title;
		this.description = description;
		this.bottomLeft = bottomLeft;
		this.topRight = topRight;
		this.marker = marker;
		this.path = path;
		this.maxTime = maxTime;
		this.maxGhosts = maxGhosts;
		this.distance = distance;
		this.isOnline = false;
	}
	
	/**Baut aus einer geparsten Datei wieder eine XML Datei**/
	public String makeXMLContent() {
		OFFSET = 0;
		
		if (maxTime < 60)
			maxTime = 60;
		
		String output = "";
		output += "<map>\n";
		output += "   <setup>\n";
		output += "      <title>" + title + "</title>\n";
		output += "      <description>\n";
		output += "         <en>"+ description + "</en>\n";
		output += "      </description>\n";
		output += "      <online>" + isOnline + "</online>\n";
		output += "      <bottomLeft>\n";
		output += "         <lat>" + (bottomLeft.getLatitude()-OFFSET) + "</lat>\n";
		output += "         <lon>" + (bottomLeft.getLongitude()-OFFSET) + "</lon>\n";
		output += "      </bottomLeft>\n";
		output += "      <topRight>\n";
		output += "         <lat>" + (topRight.getLatitude()+OFFSET) + "</lat>\n";
		output += "         <lon>" + (topRight.getLongitude()+OFFSET) + "</lon>\n";
		output += "      </topRight>\n";
		output += "      <marker>\n";
		output += "         <lat>" + marker.getLatitude() + "</lat>\n";
		output += "         <lon>" + marker.getLongitude() + "</lon>\n";
		output += "      </marker>\n";
		output += "      <maxGhosts>" + maxGhosts + "</maxGhosts>\n";
		output += "      <size>" + distance + "</size>\n";
		output += "      <maxTime>" + maxTime + "</maxTime>\n";
		output += "      <boundOnWays>false</boundOnWays>\n";
		output += "   </setup>\n";
		output += "   <path>\n";
		for (int i = 0; i < path.size(); i++) {
			output += "      <point>\n";
			output += "         <lat>" + path.getPoint(i).getPoint().getLatitude() + "</lat>\n";
			output += "         <lon>" + path.getPoint(i).getPoint().getLongitude() + "</lon>\n";
			output += "      </point>\n";
		}

		for (int i = 0; i < path.size(); i++) {
			for (int j = 0; j < path.getPoint(i).getLeadToPoints().size(); j++) {
				int k = getLeadingPointNr(path, path.getPoint(i).getLeadToPoints().get(j));
				output += "      <connect>\n";
				output += "         <source>" + i + "</source>\n";
				output += "         <target>" + k + "</target>\n";
				output += "      </connect>\n";
			}			
		}
		output += "   </path>\n";
		output += "</map>\n";
		return output;		
	}
	
	/**Sucht den Index des gesuchten Punktes, -1 wenn nicht gefunden**/
	public static int getLeadingPointNr(GPSPath path, GPSPathPoint point) {
		for (int i = 0; i < path.size(); i++) {
			if (point.equals(path.getPoint(i))) return i;
		}
		return -1;
	}

	/**Gibt den Titel zur�ck**/
	public String getTitle() {
		return title;
	}

	/**Parst die komplette Datei.**/
	public void parseAll(String title) {
		path = new GPSPath();
	    MapParser mp = new MapParser(file, path);
	    this.title = mp.getTag("title");
	    //Locale auslesen und schauen, ob eine Beschreibung in der Sprache des Spielers verf�gbar ist
	    description = MapParser.getTag(mp.getTag("description"), (Locale.getDefault() + "").substring(0,2));
	    if (description == "")
	    	//Spieler-Sprache nicht verf�gbar => w�hle Englisch!
		    description = MapParser.getTag(mp.getTag("description"), "en");
	    bottomLeft = mp.parseGPSPoint(mp.getTag("bottomLeft"));
	    topRight = mp.parseGPSPoint(mp.getTag("topRight"));
	    marker = mp.parseGPSPoint(mp.getTag("marker"));
	    boundOnWays = mp.parseBoolean(mp.getTag("boundOnWays"));
	    isOnline = mp.parseBoolean(mp.getTag("online"));
	    maxGhosts = mp.parseInteger(mp.getTag("maxGhosts"));
	    distance = mp.parseInteger(mp.getTag("size"));
	    if (maxGhosts < 0) 
	    	maxGhosts = 16;
	    maxTime = mp.parseInteger(mp.getTag("maxTime"));
	    if (maxTime < 0) 
	    	maxTime = 3600;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public GPSPoint getBottomLeft() {
		return bottomLeft;
	}

	public void setBottomLeft(GPSPoint bottomLeft) {
		this.bottomLeft = bottomLeft;
	}

	public GPSPoint getTopRight() {
		return topRight;
	}

	public void setTopRight(GPSPoint topRight) {
		this.topRight = topRight;
	}

	public GPSPath getPath() {
		return path;
	}

	public void setPath(GPSPath path) {
		this.path = path;
	}
	
	
}
