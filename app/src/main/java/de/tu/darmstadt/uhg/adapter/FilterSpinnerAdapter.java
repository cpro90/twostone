package de.tu.darmstadt.uhg.adapter;

import de.tu.darmstadt.uhg.MainActivity;
import de.tu.darmstadt.uhg.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class FilterSpinnerAdapter extends ArrayAdapter<String>{
	
	String[] values;
	Context context;
	MainActivity mainActivity;
	
	public FilterSpinnerAdapter(Context context, String[] values) {
		super(context, R.layout.filter_spinner_header, values);
		this.values = values;
		this.context = context;
		mainActivity = (MainActivity) context;
	}
	
	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// View listFilterGenreItem =
		// inflater.inflate(R.layout.list_filter_genre_item, parent, false);
		View filterSpinnerHeader = inflater.inflate(
				R.layout.filter_spinner_header, parent, false);
		TextView filterHeaderText = (TextView) filterSpinnerHeader.findViewById(R.id.filter_header_text);
		filterHeaderText.setText(values[position]);
		
		
		
		return filterSpinnerHeader;
		
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View filterSpinnerItem = View.inflate(getContext(),
				R.layout.filter_spinner_item, null);
		
		TextView filterItemText = (TextView) filterSpinnerItem.findViewById(R.id.filter_item_text);
		filterItemText.setText(values[position]);
		
		return filterSpinnerItem;
	}

}
