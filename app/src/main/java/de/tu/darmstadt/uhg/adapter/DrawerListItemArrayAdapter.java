package de.tu.darmstadt.uhg.adapter;

import de.tu.darmstadt.uhg.MainActivity;
import de.tu.darmstadt.uhg.R;
import de.tu.darmstadt.uhg.UserSettings;
import de.tu.darmstadt.uhg.progressing.BitmapProgressing;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class DrawerListItemArrayAdapter extends ArrayAdapter<String>{
	
	private final Context context;
	private final String[] values;

	public DrawerListItemArrayAdapter(Context context, String[] values) {
		super(context, R.layout.drawer_list_item, values);
		this.values = values;
		this.context = context;		
	}
	
	@SuppressLint("ViewHolder") @Override
	  public View getView(int position, View view, ViewGroup parent){
		LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView;
		if(values[position].equalsIgnoreCase("header")){
			rowView = inflater.inflate(R.layout.drawer_list_header, parent, false);
		}
		else {
			rowView = inflater.inflate(R.layout.drawer_list_item, parent, false);
		}
		    ImageView itemProfilePicture = (ImageView) rowView.findViewById(R.id.listItemProfilePicture); //TODO: Set Profile Picture
		    TextView itemText = (TextView) rowView.findViewById(R.id.listItemText);
		    
		    if(values[position].equalsIgnoreCase("header")){
		    	
		    	//Crop Profile Picture
		    	BitmapDrawable draw = (BitmapDrawable) itemProfilePicture.getDrawable();
		    	Bitmap profilePicture = draw.getBitmap();

		    	if ((MainActivity.player != null) && (UserSettings.userData != null) && (UserSettings.userData.getPicture() != null))
		    		profilePicture = UserSettings.userData.getPicture();
		    	else 
		    		profilePicture = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
		    	
		    	profilePicture = BitmapProgressing.getRoundedShape(profilePicture);
		    	itemProfilePicture.setImageBitmap(profilePicture);
		    	
		    	//Set the menu entries
		    	itemProfilePicture.setVisibility(ImageView.VISIBLE);
		    	
		    	if((MainActivity.player != null) && (UserSettings.userData != null)){
//		    	headerText.setText(MainActivity.player.getRank(MainActivity.userData) + " " + MainActivity.player.getUsername()); //TODO: More Ranks
		    		itemText.setText(MainActivity.player.getUsername());
		    	}
		    }
		    else if(values[position].equalsIgnoreCase("level overview")){
		    	itemText.setBackgroundResource(R.drawable.bg_button_green);
		    	itemText.setTextColor(context.getResources().getColorStateList(R.color.color_button_text_white));
		    	itemText.setText(context.getResources().getString(R.string.levelOverview));
		    	ViewGroup.LayoutParams params = itemText.getLayoutParams();
		    	params.height = params.height * 2;
		    	itemText.setLayoutParams(params);
		    }
		    else if(values[position].equalsIgnoreCase("create level")){
		    	itemText.setBackgroundResource(R.drawable.bg_button_green);
		    	itemText.setTextColor(context.getResources().getColorStateList(R.color.color_button_text_white));
		    	itemText.setText(context.getResources().getString(R.string.createNewLevel));
		    	ViewGroup.LayoutParams params = itemText.getLayoutParams();
		    	params.height = params.height * 2;
		    	itemText.setLayoutParams(params);
		    }
		    else if(values[position].equalsIgnoreCase("chat")){
		    	itemText.setText(context.getResources().getString(R.string.chat));
		    }
		    else if(values[position].equalsIgnoreCase("profile")){
		    	itemText.setText(context.getResources().getString(R.string.profile));
		    }
		    else if(values[position].equalsIgnoreCase("highscore")){
		    	itemText.setText(context.getResources().getString(R.string.highscore));
		    }
		    else if(values[position].equalsIgnoreCase("change user")){
		    	itemText.setText(context.getResources().getString(R.string.logout));
		    }
		    else if(values[position].equalsIgnoreCase("about")){
		    	itemText.setText(context.getResources().getString(R.string.about));
		    }
		    else if(values[position].equalsIgnoreCase("exit")){
		    	itemText.setText(context.getResources().getString(R.string.exit));
		    }
		    else if(values[position].equalsIgnoreCase("home")){
		    	itemText.setText(context.getResources().getString(R.string.home));
		    }
		    else if(values[position].equalsIgnoreCase("options")){	
		    	itemText.setText(context.getResources().getString(R.string.options));
		    }
		    else if(values[position].equalsIgnoreCase("help")){
		    	itemText.setText(context.getResources().getString(R.string.help));
		    }
		    
		    
		    return rowView;
	}

}
