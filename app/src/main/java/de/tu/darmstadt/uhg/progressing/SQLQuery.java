package de.tu.darmstadt.uhg.progressing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.tu.darmstadt.uhg.GlobalConstants;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.util.Log;

/**
 * Diese Klasse repr�sentiert eine SQL Query.
 * Mit dieser k�nnen SQL-Anfragen an den Server geschickt werden.
 * Die R�ckgabe von Anfragen ist im receivedStream (InputStream) gespeichert.
 * 
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
@SuppressWarnings("deprecation")
public class SQLQuery {
	private ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	private InputStream receivedStream;

	public SQLQuery() {
		super();
	}

	public String querying () {
		 try {
			 BufferedReader reader = new BufferedReader(new InputStreamReader(this.getReceivedStream(),"iso-8859-1"),8);
			 StringBuilder sb = new StringBuilder();
			 String line = null;
			 while ((line = reader.readLine()) != null) {
				 sb.append(line + "n");
			 } 
			 this.getReceivedStream().close();
			 return sb.toString();
		} catch (IOException e) {
			 e.printStackTrace();
		}
		return "";
	}

	public String returnSingle (String param, String returnedString) {
		String result = "";
		try{
			JSONObject json_data;
			JSONArray jArray = new JSONArray(returnedString);
			for(int i=0;i<jArray.length();i++){
			 	 json_data = jArray.getJSONObject(i);
			 	 result = json_data.get(param) + "";
			} 
		} catch(JSONException e){
			       Log.e("UHG", "Error parsing data "+e.toString());	
		}
		return result;
	}
	
	 
	 
	 
	/**Gibt das NameValue-Paar zur�ck**/
	public ArrayList<NameValuePair> getNameValuePairs() {
		return nameValuePairs;
	}

	/**Setzt das NameValue-Paar**/
	public void setNameValuePairs(ArrayList<NameValuePair> nameValuePairs) {
		this.nameValuePairs = nameValuePairs;
	}
	
	/**Entfernt ung�ltige Zeichen aus dem String**/
	public static String cleanString (String message) {
		 message = message.replace("�", "ae");
		 message = message.replace("�", "Ae");
		 message = message.replace("�", "oe");
		 message = message.replace("�", "Oe");
		 message = message.replace("�", "ue");
		 message = message.replace("�", "Ue");
		 message = message.replace("�", "ss");
		 return message;
	}

	/**F�gt eine Paarung den bereits vorhandenen Paarungen hinzu**/
	public void addPair(String name, String value) {
		nameValuePairs.add(new BasicNameValuePair(name, value));
	}
	
	/**Execute SQL Query on Server.
	 * Returns true if SQL Query was progressed**/
	public boolean executeSQL(String fileName) {

	    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	   	StrictMode.setThreadPolicy(policy); 
	   	
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(GlobalConstants.SERVER_HTTP_URL + fileName);
			Log.d("UHG", "Established: " + GlobalConstants.SERVER_HTTP_URL + fileName);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			this.receivedStream = response.getEntity().getContent();
		} catch(Exception e){
			Log.e("log_tag", "Error in http connection." + e.toString());
			Log.d("UHG", "FAILED: " + e.toString());
			return false;
		}
		return true;
	}
	
	/**Execute SQL Query on external Server.
	 * Returns true if SQL Query was progressed**/
	public boolean executeExternalSQL(String fileName) {

	    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	   	StrictMode.setThreadPolicy(policy); 
	   	
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(fileName);
			Log.d("UHG", "Established external: " + fileName);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			this.receivedStream = response.getEntity().getContent();
		} catch(Exception e){
			Log.e("log_tag", "Error in http connection." + e.toString());
			Log.d("UHG", "FAILED: " + e.toString());
			return false;
		}
		return true;
	}

	/**Gibt den empfangenen Stream zur�ck**/
	public InputStream getReceivedStream() {
		return receivedStream;
	}

	/**Setzt den empfangenen Stream**/
	public void setReceivedStream(InputStream receivedStream) {
		this.receivedStream = receivedStream;
	}

	/**Wandelt das Zeitformat ins mysql-Zeitformat um**/
	@SuppressLint("SimpleDateFormat")
	public static String convertDateTimeFormat(String cDT) {
		try {
			SimpleDateFormat fmt = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			Date inputDate;
			inputDate = fmt.parse(cDT);
			fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			cDT = fmt.format(inputDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		return cDT;
	}
}
