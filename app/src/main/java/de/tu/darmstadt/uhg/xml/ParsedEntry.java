package de.tu.darmstadt.uhg.xml;

/**
 * Repräsentiert einen geparsten Eintrag, der das geparste Wort und den restlichen Text der
 * noch folgt enthält.
 * input: <"A"><"B"><"C"> => parsedText='A', restString='<"B"><"C">'
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de 
 */
public class ParsedEntry {

	String parsedText;
	String restString;
	
	public ParsedEntry(String parsedText, String restString) {
		super();
		this.parsedText = parsedText;
		this.restString = restString;
	}
	
	public String getParsedText() {
		return parsedText;
	}
	public void setParsedText(String parsedText) {
		this.parsedText = parsedText;
	}
	public String getRestString() {
		return restString;
	}
	public void setRestString(String restString) {
		this.restString = restString;
	}
	
	
	
}
