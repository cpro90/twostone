package de.tu.darmstadt.uhg.xml;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextFiraSansLight extends EditText {
	
	static Typeface firaSansLight;
	
	public EditTextFiraSansLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypefaceFiraSansLight();
    }

   public EditTextFiraSansLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypefaceFiraSansLight();
    }

   public EditTextFiraSansLight(Context context) {
        super(context);
        setTypefaceFiraSansLight();
   }
   
   private void setTypefaceFiraSansLight(){
	   if(firaSansLight == null) firaSansLight = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "FiraSans-Light.ttf");
	   setTypeface(firaSansLight);
   }

}
