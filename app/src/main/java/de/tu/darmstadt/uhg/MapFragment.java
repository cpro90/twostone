package de.tu.darmstadt.uhg;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Random;

import de.tu.darmstadt.uhg.adapter.AvailableMapsArrayAdapter;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.gps.GPSPointText;
import de.tu.darmstadt.uhg.leveleditor.Connection;
import de.tu.darmstadt.uhg.xml.ShortParsedFile;

public class MapFragment extends Fragment implements OnMapReadyCallback {

	private MainActivity mainActivity;
	static GoogleMap map;
	static ArrayList<Circle> circleArray;
	boolean initial = true;
	public static ArrayList<ShortParsedFile> onlineMaps;
	public static ArrayList<GPSPointText> points = new ArrayList<GPSPointText>();
	static ArrayList<Marker> markers = new ArrayList<Marker>();
	static String selectedMarker;
	static boolean selectedMarkerOnline;


	@Override
	public void onMapReady(GoogleMap mapVar) {
		map = mapVar;

		map.getUiSettings().setZoomControlsEnabled(true);
		map.setMyLocationEnabled(true);
		drawAllMarkers();
		// createAllCircles();
		drawAllMapShapes();

		map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

			@Override
			public void onMyLocationChange(Location location) {
				// drawAllCircles(location);

			}
		});
		map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker marker) {
				// TextView viewName = (TextView)
				// fragmentView.findViewById(R.id.theLocationName);
				// TextView viewInfo = (TextView)
				// fragmentView.findViewById(R.id.theLocationInfo);
				// viewName.setText(marker.getTitle());
				// viewInfo.setText(marker.getSnippet());
				// RelativeLayout view = (RelativeLayout)
				// fragmentView.findViewById(R.id.theLocationView);
				// view.setVisibility(RelativeLayout.VISIBLE);
				// ImageView button = (ImageView)
				// fragmentView.findViewById(R.id.theLocationSelector);
				// if (MainActivity.levelExists(marker.getTitle())) {
				// //LOCALE
				// button.setImageResource(R.drawable.play);
				// selectedMarkerOnline = false;
				// } else {
				// //ONLINE
				// button.setImageResource(R.drawable.upload_map);
				// selectedMarkerOnline = true;
				// }
				// selectedMarker = marker.getTitle();

				mainActivity.selectLevelByName(marker.getTitle(), mainActivity);

				refreshAll();

				return true;
			}
		});

		map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

			@Override
			public void onMapClick(LatLng arg0) {
				mainActivity.findViewById(R.id.availableMapsSelectedMapPanel)
						.setVisibility(View.INVISIBLE);
			}
		});

		// ImageView playButton = (ImageView)
		// fragmentView.findViewById(R.id.theLocationSelector);
		// playButton.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// if (selectedMarker != "") {
		// if (selectedMarkerOnline) {
		//
		// AlertDialog.Builder builder = new AlertDialog.Builder(
		// mainActivity);
		//
		// builder.setMessage(
		// getResources().getString(
		// R.string.downloadMap))
		// .setTitle(
		// getResources().getString(
		// R.string.downloadMapTitle));
		//
		// builder.setPositiveButton(
		// getResources().getString(R.string.ok),
		// new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog, int id) {
		// String content = ServerProgressing.downloadMap(selectedMarker);
		// LevelEditorActivity.saveLevel(mainActivity, selectedMarker, content);
		// MainActivity.updateAllAvailableMaps(mainActivity);
		// refreshAll();
		//
		// }
		// });
		//
		// builder.setNegativeButton(
		// getResources().getString(R.string.cancel),
		// new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog,
		// int id) {
		// //Do nothing
		// }
		// });
		//
		// Dialog dialog = builder.create();
		// dialog.show();
		//
		// } else {
		// mainActivity.selectLevelByName(selectedMarker, mainActivity);
		// }
		// }
		// }
		// });

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		final View fragmentView = inflater.inflate(R.layout.activity_map,
				container, false);
		SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager()
				.findFragmentById(R.id.locationMap));
		mapFragment.getMapAsync(this);

		return fragmentView;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mainActivity = (MainActivity) getContext();
//		onlineMaps = ServerProgressing.getAllOnlineMaps();
		onlineMaps = new ArrayList<ShortParsedFile>();

	}

	/** Gibt die Position des Spielfeldes zur�ck **/
	public static GPSPoint getMarkerPosition(String title) {
		for (int i = 0; i < points.size(); i++) {
			if (points.get(i).getTitle().equalsIgnoreCase(title))
				return points.get(i).getPoint();
		}
		return new GPSPoint(0, 0);
	}

	/**
	 * Generates a marker on the overlay
	 * 
	 * @param pos
	 *            GPS Position of the marker
	 * @param title
	 *            Title of the marker
	 * @param description
	 *            Description of the marker
	 */
	public static void generateMarker(GPSPoint pos, String title,
			String description, double size, ArrayList<Connection> c) {
		points.add(new GPSPointText(pos, title, description, size, c));
	}

	/** Draw all the shapes of the level to the map **/
	private void drawAllMapShapes() {
		for (int i = 0; i < points.size(); i++) {
			Random rnd = new Random();
			int color = Color.argb(172, rnd.nextInt(128), rnd.nextInt(128),
					rnd.nextInt(128));
			ArrayList<Connection> connectionList = points.get(i)
					.getConnections();
			for (int j = 0; j < connectionList.size(); j++) {
				Connection current = connectionList.get(j);
				for (int k = 0; k < current.getConnectsTo().size(); k++) {
					drawLineOnMap(current, current.getConnectsTo().get(k),
							color);
				}
			}
		}
	}

	/** Draw a Line between two Connections on the Map **/
	public void drawLineOnMap(Connection from, Connection to, int color) {
		PolylineOptions line = new PolylineOptions()
				.add(new LatLng(from.getPos().latitude, from.getPos().longitude),
						new LatLng(to.getPos().latitude, to.getPos().longitude))
				.width(10).color(color);
		map.addPolyline(line);
	}

	/** Draw all Markers to the Overview **/
	private void drawAllMarkers() {
		// ONLINE MAPS
		if ((MainActivity.filterSelection.equals(GlobalConstants.FILTER_MAP_ALL) || MainActivity.filterSelection
						.equals(GlobalConstants.FILTER_MAP_ONLINE))) {
			for (int i = 0; i < onlineMaps.size(); i++) {
				if (!MainActivity.levelExists(onlineMaps.get(i).getTitle())) {
					GPSPoint p = new GPSPoint(onlineMaps.get(i).getPos());
					LatLng g = new LatLng(p.getLatitude(), p.getLongitude());
					MarkerOptions marker = new MarkerOptions();
					marker.position(g);
					marker.title(onlineMaps.get(i).getTitle());
					marker.snippet(onlineMaps.get(i).getDescription());
					if (!marker.getTitle().equals("")) {
						if (marker.getTitle().equals(
								MainActivity.choosenLocationFromList)) {
							marker.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.icon_map_online_active));
						} else {
							marker.icon(BitmapDescriptorFactory
									.fromResource(R.drawable.icon_map_online));
						}
					}
					markers.add(map.addMarker(marker));
				}
			}
		}

		// OFFLINE MAPS
		if ((MainActivity.filterSelection.equals(GlobalConstants.FILTER_MAP_ALL) || MainActivity.filterSelection
				.equals(GlobalConstants.FILTER_MAP_LOCAL))) {
			for (int i = 0; i < points.size(); i++) {
				LatLng g = new LatLng(points.get(i).getPoint().getLatitude(),
						points.get(i).getPoint().getLongitude());
				MarkerOptions marker = new MarkerOptions();
				marker.position(g);
				marker.title(points.get(i).getTitle());
				marker.snippet(points.get(i).getDescription());
				if (marker.getTitle().equals(
						MainActivity.choosenLocationFromList)) {
					marker.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.icon_map_local_active));
				} else {
					marker.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.icon_map_local));
				}
				markers.add(map.addMarker(marker));
			}
		}

	}

	/** Refresh all Markers and Shapes. Usefull after Downloads **/
	public void refreshAll() {
		map.clear();
		drawAllMarkers();
		drawAllMapShapes();
		selectedMarker = "";
		RelativeLayout view = (RelativeLayout) mainActivity
				.findViewById(R.id.theLocationView);
		view.setVisibility(RelativeLayout.INVISIBLE);
		refreshSelectedMap();
	}

	private void refreshSelectedMap() {
		if (MainActivity.levelExists(MainActivity.choosenLocationFromList)
				&& !MainActivity.choosenLocationFromList.equals("")) {

			RelativeLayout availableMapsSelectedMapPanel = (RelativeLayout) mainActivity
					.findViewById(R.id.availableMapsSelectedMapPanel);
			availableMapsSelectedMapPanel.setVisibility(View.VISIBLE);
			AvailableMapsArrayAdapter mapsAdapter = (AvailableMapsArrayAdapter) MainActivity.availableMapsList
					.getAdapter();
			availableMapsSelectedMapPanel.removeAllViews();
			availableMapsSelectedMapPanel.addView(mapsAdapter.getView(
					mapsAdapter
							.getPosition(MainActivity.choosenLocationFromList),
					null, availableMapsSelectedMapPanel),
					new RelativeLayout.LayoutParams(
							RelativeLayout.LayoutParams.MATCH_PARENT,
							RelativeLayout.LayoutParams.WRAP_CONTENT));
		} else {
			mainActivity.findViewById(R.id.availableMapsSelectedMapPanel)
					.setVisibility(View.INVISIBLE);
		}

	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// getMenuInflater().inflate(R.menu.map, menu);
	//
	// MenuItem abort = menu.findItem(R.id.abortMap);
	// abort.setOnMenuItemClickListener(new OnMenuItemClickListener() {
	// @Override
	// public boolean onMenuItemClick(MenuItem arg0) {
	// finish();
	// return true;
	// }
	// });
	//
	// return true;
	// }

}
