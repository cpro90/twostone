package de.tu.darmstadt.uhg.game;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.MainActivity;
import de.tu.darmstadt.uhg.Map;
import de.tu.darmstadt.uhg.R;
import de.tu.darmstadt.uhg.UserSettings;
import de.tu.darmstadt.uhg.adapter.DrawerListItemArrayAdapter;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.gps.GPSProgressing;

@SuppressWarnings("deprecation")
public class PlayActivity extends FragmentActivity implements OnMapReadyCallback {

	static Map map;
	static GoogleMap playground;
	static Game game;
	ArrayList<GroundOverlay> stoneOverlay;
	ArrayList<GroundOverlay> enemyOverlay;
	static GroundOverlay twostoneOverlay;
	public static GPSPoint curPos;
	static boolean blocked = false;
	
	ArrayList<Animation> enemyAnimations;
	Animation twostoneEatAnimation;
	Animation twostoneMoveAnimation;
	Animation stoneAnimation;
	Bitmap stoneImageAR;
	Bitmap stoneImageARPerformance;
	
	long startTime;
	long endTime;
	String startDate;
	String endDate;
	static MainActivity main;
	double currentTime; // ADAPTATION: Vergangene Zeit seit Spielbeginn
	float zoomLevel = 18.0f;
	boolean userScreenVisible;
	public static boolean active = false;
	private ListView drawerListView;
	private DrawerLayout drawerLayout;

	public FrameLayout previewFrame;
	public ImageView cameraForeground;
	public ImageView secondScreen;
	
	public static boolean USE_CARDBOARD = false;
	
	public static MainActivity getMain() {
		return main;
	}

	public static void setMain(MainActivity main) {
		PlayActivity.main = main;
	}

	public Game getGame() {
		return game;
	}

	public static void setGame(Game game) {
		PlayActivity.game = game;
	}

	public static Map getMap() {
		return map;
	}

	public static void setMap(Map map) {
		PlayActivity.map = map;
	}

	public static boolean GPSflag = false;

	public static void instantiateGPS(LocationManager locman) {
		LocationManager locationManager = locman;
		LocationListener locationListener = new LocationListener() {

			public void onLocationChanged(Location location) { // ADAPTATION:
																// Hier wird die
																// Position des
																// Spielers
																// abgefragt
				// Set Flag
				if (!GPSflag)
					GPSflag = true;

				// SaveDistance
				if (active && (curPos != null)
						&& (UserSettings.userData != null)) {
					
					double wd = GPSProgressing.getWayDifference(
							curPos,
							new GPSPoint(location.getLatitude(),
									location.getLongitude()));
					
					//Bei Spr�ngen von mehr als 100m vermutlich Cheating? Fahrend im Auto?
					if (wd < 100)								
						UserSettings.userData
								.addDistance(wd);
				}
				
				curPos = new GPSPoint(location.getLatitude(),
						location.getLongitude());
				
				MainActivity.player.setPosition(curPos);
				MainActivity.augmentedReality.setLocation(curPos);
				
				//Update Player on Map
				if (twostoneOverlay != null)
					twostoneOverlay.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
				
			}

			@Override
			public void onProviderDisabled(String arg0) {
			}

			@Override
			public void onProviderEnabled(String arg0) {
			}

			@Override
			public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
			}
		};

		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, GlobalConstants.REDRAW_TIME, 0,
					locationListener);
		} 
	}


	//HOTFIXES!
	int scounter = 10;
	public void duplicateScreen() {

		scounter--;
		if (scounter < 0) 
			scounter = 10;
		if (scounter > 0)
			return;
		
		SnapshotReadyCallback callback = new SnapshotReadyCallback() {
	        Bitmap bitmap;
	
	        @Override
	        public void onSnapshotReady(Bitmap snapshot) {
	            bitmap = snapshot;
	            secondScreen.setImageBitmap(bitmap);
	            try {
	            	
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
		};

	    playground.snapshot(callback);
	}

	@Override
	public void onMapReady(GoogleMap mapVar) {
        playground = mapVar;

        if (playground == null) {
            // Google Play Services veraltet oder nicht vorhanden! ABBRUCH!
            AlertDialog alertDialog = new AlertDialog.Builder(PlayActivity.this)
                    .create();
            alertDialog.setTitle(getResources().getString(
                    R.string.mapsTooOldTitle));
            alertDialog.setMessage(getResources()
                    .getString(R.string.mapsTooOld));
            alertDialog.show();
            finish();
            return;
        }

        if (USE_CARDBOARD) {
            //CARDBOARD? Use Landscape Orientation for displaying two screens parallel
            secondScreen = (ImageView) findViewById(R.id.cardboardScreen);
            secondScreen.setVisibility(ImageView.VISIBLE);
            RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) mapFragment.getView().getLayoutParams();
            RelativeLayout.LayoutParams lp2 = (RelativeLayout.LayoutParams) secondScreen.getLayoutParams();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            lp1.width = displayMetrics.widthPixels/2;
            lp2.width = displayMetrics.widthPixels/2;
            mapFragment.getView().setLayoutParams(lp1);
            secondScreen.setLayoutParams(lp2);
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }


        /** Begin new Layout Area **/

        String[] drawerStrings;

        final ImageButton drawerButton = (ImageButton) findViewById(R.id.drawerButton_play);
        DrawerLayout.DrawerListener drawerListener;
        drawerStrings = getResources().getStringArray(R.array.drawerListPause);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_play);
        drawerListView = (ListView) findViewById(R.id.drawerListView_play);
        SeekBar difficultySeekDfficulty = (SeekBar) findViewById(R.id.difficulty_seek_difficulty);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) drawerListView
                .getLayoutParams();
        params.width = (int) (width * 0.95f);
        drawerListView.setLayoutParams(params);

        drawerListView.setAdapter(new DrawerListItemArrayAdapter(this,
                drawerStrings));
        drawerListener = new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerStateChanged(int arg0) {
            }

            @Override
            public void onDrawerSlide(View arg0, float arg1) {
            }

            @Override
            public void onDrawerOpened(View arg0) {
                drawerButton.setImageDrawable(getResources().getDrawable(
                        R.drawable.button_drawer_active));
            }

            @Override
            public void onDrawerClosed(View arg0) {
                drawerButton.setImageDrawable(getResources().getDrawable(
                        R.drawable.button_drawer));
            }
        };
        drawerLayout.setDrawerListener(drawerListener);

        drawerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!drawerLayout.isDrawerOpen(drawerListView)) {
                    drawerLayout.openDrawer(drawerListView);
                }
            }
        });

        drawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String screen = drawerListView.getItemAtPosition(position)
                        .toString();

                if (screen.equalsIgnoreCase("home")) {
                    setResult(GlobalConstants.DRAWER_HOME);
                    quitGame();
                }
                if (screen.equalsIgnoreCase("exit")) {
                    setResult(GlobalConstants.DRAWER_EXIT);
                    quitGame();
                }
                // drawerListView.setItemChecked(position, true);
                drawerLayout.closeDrawer(drawerListView);
            }
        });

        difficultySeekDfficulty.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                int intelligence = GlobalConstants.ENEMY_FOOL;
                int speed = GlobalConstants.SPEED_SNAIL;
                switch (progress) {
                    case 1:
                        intelligence = GlobalConstants.ENEMY_EASY;
                        speed = GlobalConstants.SPEED_SLOW;
                        break;
                    case 2:
                        intelligence = GlobalConstants.ENEMY_NORMAL;
                        speed = GlobalConstants.SPEED_WALKSPEED;
                        break;
                    case 3:
                        intelligence = GlobalConstants.ENEMY_GOOD;
                        speed = GlobalConstants.SPEED_MEDIUM;
                        break;
                    case 4:
                        intelligence = GlobalConstants.ENEMY_EXTREME;
                        speed = GlobalConstants.SPEED_FAST;
                        break;
                    default:
                        intelligence = GlobalConstants.ENEMY_FOOL;
                        speed = GlobalConstants.SPEED_SNAIL;
                        break;
                }
                for (Enemy e : game.getEnemies()) {
                    e.setIntelligence(intelligence);
                    e.setSpeed(speed);
                }
                TextView difficultyText = (TextView) findViewById(R.id.difficulty_text_difficulty);
                difficultyText.setText((intelligence+1)+"");
                showGhostAngry(intelligence);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                TextView difficultyText = (TextView) findViewById(R.id.difficulty_text_difficulty);
                difficultyText.setText((progress+1)+"");
                showGhostAngry(progress);
            }
        });


        /** new Layout Area END **/

        currentTime = 0;
        userScreenVisible = false;

        //ALLE ANIMATIONEN KREIEREN
        generateAllAnimations();

        // ANIMATIONEN STONES FOR AR
        Bitmap singleStone = (BitmapFactory.decodeResource(getResources(), R.drawable.stein_01));
        stoneImageAR = Bitmap.createScaledBitmap(singleStone,
                singleStone.getWidth() / 2,
                singleStone.getHeight() / 2, false);
        stoneImageARPerformance = Bitmap.createScaledBitmap(singleStone,
                singleStone.getWidth() / 3,
                singleStone.getHeight() / 3, false);

        drawAllWays();


        initPlayerOverlay(curPos, twostoneMoveAnimation.getNextAnimationPicture());

        stoneOverlay = new ArrayList<GroundOverlay>();
        for (int i = 0; i < game.eatStones().size(); i++)
            addToStoneOverlay(game.eatStones().get(i).getPosition());

        enemyOverlay = new ArrayList<GroundOverlay>();
        for (int i = 0; i < game.getEnemies().size(); i++)
            addToEnemyOverlay(game.getEnemies().get(i).getPosition(), i);

        if (MainActivity.currentPosition != null)
            curPos = MainActivity.currentPosition;

        playground
                .setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                    @Override
                    public void onMyLocationChange(Location arg0) {
                        curPos = new GPSPoint(arg0.getLatitude(), arg0
                                .getLongitude());
                        MainActivity.player.setPosition(curPos);
                    }
                });

        playground.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng arg0) {
                clickedOnMap();
            }
        });

        playground.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng arg0) {
                longClickedOnMap();
            }
        });

        updateCamera(new LatLng(curPos.getLatitude(), curPos.getLongitude()),
                0.0f, MainActivity.getDirection(), zoomLevel);


        GPSPoint a2 = map.getLocation().getBottomLeftCoordinate();
        a2.substract(new GPSPoint(0.1, 0.1));
        GPSPoint b2 = map.getLocation().getTopRightCoordinate();
        b2.add(new GPSPoint(0.1, 0.1));
        LatLng a = a2.toLatLng();
        LatLng b = b2.toLatLng();
        LatLngBounds bounds = new LatLngBounds(a, b);

        BitmapDescriptor background = BitmapDescriptorFactory
                .fromResource(R.drawable.background_playground);
        playground.addGroundOverlay(new GroundOverlayOptions()
                .image(background).positionFromBounds(bounds)
                .transparency(0.1f));

        playground.setMapType(GoogleMap.MAP_TYPE_NONE);
        playground.setMyLocationEnabled(false);
        playground.getUiSettings().setZoomControlsEnabled(false);
        playground.getUiSettings().setCompassEnabled(false);
        playground.getUiSettings().setRotateGesturesEnabled(false);
        playground.getUiSettings().setScrollGesturesEnabled(false);
        playground.getUiSettings().setTiltGesturesEnabled(false);
        playground.getUiSettings().setZoomGesturesEnabled(true);
        playground.getUiSettings().setMyLocationButtonEnabled(false);

        playground.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                if (position.zoom > GlobalConstants.MAX_ZOOM)
                    playground.animateCamera(CameraUpdateFactory
                            .zoomTo(GlobalConstants.MAX_ZOOM));
                if (position.zoom < GlobalConstants.MIN_ZOOM)
                    playground.animateCamera(CameraUpdateFactory
                            .zoomTo(GlobalConstants.MIN_ZOOM));
            }
        });
        createGameTimer();
        initCamera();

        // AR SPECIFIC PART
        MainActivity.augmentedReality.setPlayActivity(this);
        cameraForeground = (ImageView) findViewById(R.id.cameraForeground);
        MainActivity.augmentedReality.setCameraForeground(cameraForeground);
        previewFrame = (FrameLayout) findViewById(R.id.mapCamera);

        cameraForeground.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                longClickedOnMap();
                return false;
            }
        });
        if (UserSettings.cameraEnabled) {
            if (MainActivity.augmentedReality.initCamera()) {
                MainActivity.camera = MainActivity.augmentedReality.getCamera();
                previewFrame.removeAllViews();
                previewFrame.addView(MainActivity.augmentedReality
                        .getCameraSurfacePreview());
                MainActivity.augmentedReality.setCameraEnabled(true);
                MainActivity.augmentedReality.switchToAR();
            } else {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.noCamera),
                        Toast.LENGTH_LONG).show();
            }

        } else {
            MainActivity.augmentedReality.setCameraEnabled(false);
            if (MainActivity.camera != null) {
                FrameLayout previewFrame = (FrameLayout) findViewById(R.id.mapCamera);
                previewFrame.removeAllViews();
                MainActivity.augmentedReality.releaseCamera();
                MainActivity.augmentedReality.switchToMap();
                MainActivity.camera = null;
            }
        }


    }
    SupportMapFragment mapFragment;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		active = true;
		startTime = System.currentTimeMillis();
		startDate = Calendar.getInstance().getTime().toString();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_play);

		mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.playground);
		mapFragment.getMapAsync(this);


	}

	private void generateAllAnimations() {

		//ANIMATIONEN PLAYER
		twostoneEatAnimation = new Animation();
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te1));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te2));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te3));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te4));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te5));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te6));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te7));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te8));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te9));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te10));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te11));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te12));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te13));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te14));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te15));
		twostoneEatAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.te16));
		
		twostoneMoveAnimation = new Animation();
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm1));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm2));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm3));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm4));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm5));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm6));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm7));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm8));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm9));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm10));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm11));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm12));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm13));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm14));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm15));
		twostoneMoveAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(),R.drawable.tm16));
		
		
		// ANIMATIONEN ENEMY
		enemyAnimations = new ArrayList<Animation>();
		
		Animation hookeyAnimation = new Animation();
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey1));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey2));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey3));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey4));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey5));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey6));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey7));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey8));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey9));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey10));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey11));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey12));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey13));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey14));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey15));
		hookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.hookey16));
		enemyAnimations.add(hookeyAnimation);	
		
		Animation spookeyAnimation = new Animation();
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey1));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey2));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey3));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey4));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey5));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey6));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey7));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey8));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey9));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey10));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey11));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey12));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey13));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey14));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey15));
		spookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.spookey16));
		enemyAnimations.add(spookeyAnimation);
		
		Animation rookeyAnimation = new Animation();
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey1));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey2));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey3));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey4));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey5));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey6));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey7));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey8));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey9));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey10));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey11));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey12));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey13));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey14));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey15));
		rookeyAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.rookey16));
		enemyAnimations.add(rookeyAnimation);
		
		Animation lokiAnimation = new Animation();
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki1));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki2));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki3));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki4));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki5));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki6));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki7));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki8));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki9));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki10));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki11));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki12));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki13));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki14));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki15));
		lokiAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.loki16));
		enemyAnimations.add(lokiAnimation);
		
		Animation bazarghAnimation = new Animation();
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh1));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh2));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh3));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh4));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh5));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh6));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh7));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh8));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh9));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh10));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh11));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh12));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh13));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh14));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh15));
		bazarghAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bazargh16));
		enemyAnimations.add(bazarghAnimation);
		
		stoneAnimation = new Animation();
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_01));
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_02));
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_03));
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_04));
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_05));
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_06));
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_07));
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_08));
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_09));
		stoneAnimation.addAnimationPicture(BitmapFactory.decodeResource(getResources(), R.drawable.stein_10));		
	}

	private static void drawAllWays() {
		if (map.getPath() == null)
			return; // DEBUG PURPOSE
		for (int i = 0; i < map.getPath().size(); i++) {
			LatLng from = map.getPath().getPoint(i).getPoint().toLatLng();
			for (int j = 0; j < map.getPath().getPoint(i).getLeadToPoints()
					.size(); j++) {
				LatLng to = map.getPath().getPoint(i).getLeadToPoints().get(j)
						.getPoint().toLatLng();
				drawLineOnMap(from, to);
			}
		}
	}

	/** Zur�ck-Button gedr�ckt **/
	@Override
	public void onBackPressed() {

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getResources().getString(R.string.quitGame))
				.setTitle(getResources().getString(R.string.quitGameTitle));

		builder.setPositiveButton(getResources().getString(R.string.mainMenu),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						setResult(GlobalConstants.DRAWER_HOME);
						quitGame();
					}
				});

		builder.setNegativeButton(getResources().getString(R.string.playing),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// DO NOTHING!
					}
				});

		Dialog dialog = builder.create();
		dialog.show();
		
		
	}

	private void quitGame() {
		MainActivity.t.cancel();
		active = false;
		cameraFinish();
		this.finish();
		playground.clear();
		playground = null;
		System.gc();
	}

	private static void drawLineOnMap(LatLng from, LatLng to) {
		PolylineOptions line = new PolylineOptions().add(from, to)
				.width(GlobalConstants.LINE_WIDTH).color(GlobalConstants.LINE_COLOR);
		playground.addPolyline(line);
	}


	//MAIN GAME TIME
	private void createGameTimer() {
		MainActivity.t = new Timer();
		MainActivity.tTask = new TimerTask() {
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						MainActivity.augmentedReality.redraw();
						game.updateAllEnemies();

						// SET CURRENT POINTS
						currentTime += GlobalConstants.REDRAW_TIME;
						game.setPointsPerStone(Math
								.round((1 - ((currentTime / 1000) / (map
										.getMaxTime()))) * game.getDifficulty()) + 1);

						// ANIMATIONEN
						animatePlayerMoving();
						
						for (int i = 0; i < game.getEnemies().size(); i++) {
							GPSPoint p = game.getEnemies().get(i).getPosition();
							try {

								enemyOverlay.get(i).setImage(
									BitmapDescriptorFactory.fromBitmap(enemyAnimations.get(game.getEnemies().get(i).getCharacter()).getNextAnimationPicture()));
								
								enemyOverlay.get(i).setBearing(MainActivity.getDirection());	

								enemyOverlay.get(i).setPosition(
										new LatLng(p.getLatitude(), p
												.getLongitude()));

								MainActivity.augmentedReality.drawOnOverlay(
										enemyAnimations.get(game.getEnemies().get(i).getCharacter()).getNextAnimationPicture(), p,
										curPos, true, true);
							} catch (Exception e) {
							}
							
							
						}

						//Only in AR Mode
						for (int i = 0; i < game.eatStones().size(); i++) {
							GPSPoint p = game.eatStones().get(i)
									.getPosition();
							MainActivity.augmentedReality.drawOnOverlay(
									stoneImageARPerformance, p, false);

						}

						// ABSORB STONES?
						int pst = game.getStoneAt(curPos);
						if (pst > -1) {
							animatePlayerEating();
							GroundOverlay del = stoneOverlay.get(pst);
							del.setPosition(new LatLng(0, 0));
							del.remove();
							stoneOverlay.remove(pst);
							if (game.eatStoneAt(curPos, true, pst)) {
								// WON
								finishGame(GAME_INDICATOR_WON);
							}
						}
						;

						// VIBRATE?
						if (game.enemyIsInNearOfPlayer(curPos)) {
							// GHOST IS NEAR, VIBRATE!
							MainActivity.augmentedReality.vibrateEnemy(curPos,
									game.getEnemy(), 1000);
						}


						//CARDBOARD
						if (USE_CARDBOARD) {
							//UPDATE SECOND SCREEN
							duplicateScreen();
						}
						
						
						// TIMEOUT?
						if ((currentTime / 1000) > map.getMaxTime()) {							
							// GAME LOST, TIME OUT
							finishGame(GAME_INDICATOR_TIME);
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.timeOut),
									Toast.LENGTH_SHORT).show();
						}

						// GHOSTS EATS PLAYER?
						if (game.didEnemyDevoursPlayer(curPos)) {
							// GAME LOST, GHOST ATE THE PLAYER
							finishGame(GAME_INDICATOR_MONSTER);
							Toast.makeText(
									getApplicationContext(),
									getResources().getString(
											R.string.ghostAtePlayer),
									Toast.LENGTH_SHORT).show();

						}

						redrawTopMenu();
						float angleLevel = MainActivity.getDirection();
						if(!UserSettings.rotationEnabled){
							angleLevel = 0;
						} else if(UserSettings.rotation_type_GPS){
							angleLevel = MainActivity.getDirectionGPS();
						}
						updateCamera(
								new LatLng(curPos.getLatitude(),
										curPos.getLongitude()), 0.0f,
										angleLevel, 0.0f);

					}
				});
			}
		};

		MainActivity.t.schedule(MainActivity.tTask, GlobalConstants.REDRAW_TIME, GlobalConstants.REDRAW_TIME);
	}


	//TwoStone Eats
	private void animatePlayerEating() {	
		blocked = true;
		final Timer bTimer= new Timer();
  		TimerTask bTimerTask = new TimerTask() {
  			public void run() {
  				runOnUiThread(new Runnable() {
  					@Override
  					public void run() {
  						twostoneOverlay.setImage(BitmapDescriptorFactory.fromBitmap(twostoneEatAnimation.getNextAnimationPicture()));
  						twostoneOverlay.setBearing(MainActivity.getDirection());	
  						if (twostoneEatAnimation.finishedAnimation()) {
  							blocked = false;
  							bTimer.cancel();
  						}
  					}
  				});
  			}
  		};
  		bTimer.schedule(bTimerTask, 0, GlobalConstants.REDRAW_TIME);	  
  		
	}
	//TwoStone Moves
	private void animatePlayerMoving() {
		if (!blocked) {
			twostoneOverlay.setImage(BitmapDescriptorFactory.fromBitmap(twostoneMoveAnimation.getNextAnimationPicture()));		
			twostoneOverlay.setBearing(MainActivity.getDirection());	
		}
	}
	
	/** Updated die Punktetafel im Spiel **/
	private void redrawTopMenu() {
		final TextView topViewStoneText = (TextView) findViewById(R.id.playPointEnergyCount); 
		final TextView topViewTimeText = (TextView) findViewById(R.id.playPointTimeCount); 
		final TextView topViewPointsText = (TextView) findViewById(R.id.playPointPoints); 
		topViewStoneText.setText("" + game.getRemainingStoneCount());
		topViewTimeText.setText("" + Math.round(map.getMaxTime() - (currentTime / 1000)));
		topViewPointsText.setText("" + game.getPoints() + " ");
	}

	/** Bei click on Map die ZoomControl anzeigen/ausblenden **/
	private void clickedOnMap() {
		/**
		 * userScreenVisible = !userScreenVisible; ZoomControls zC =
		 * (ZoomControls) findViewById(R.id.zoomControlPlay); if
		 * (userScreenVisible) { zC.setVisibility(FrameLayout.VISIBLE); } else {
		 * zC.setVisibility(FrameLayout.INVISIBLE); }
		 **/
	}

	/** Bei longclick on Map = Men� aufrufen **/
	private void longClickedOnMap() {
		openOptionsMenu();
	}

    protected void cameraFinish() {
		if (MainActivity.camera != null) {
			MainActivity.camera.release(); // release the camera for other
											// applications
			MainActivity.camera = null;
		}
	}

    public static final int GAME_INDICATOR_WON = 1;
    public static final int GAME_INDICATOR_TIME = 2;
    public static final int GAME_INDICATOR_MONSTER = 3;
    
	protected void finishGame(int gameIndicator) {
		active = false;
		endTime = System.currentTimeMillis();
		endDate = Calendar.getInstance().getTime().toString();
		MainActivity.t.cancel();
		cameraFinish();
		game.setTimePlayed(endTime - startTime);
		game.setStartDate(startDate);
		game.setEndDate(endDate);
		main.finishGame(MainActivity.player, game);

		int maxScore = MainActivity.localScore.getScoreFromName(map.getName());
		if (game.getPoints() > maxScore)
			MainActivity.localScore.setScore(map.getName(), game.getPoints());
		main.saveLocalScores();

		if (gameIndicator == GAME_INDICATOR_WON) {
			main.showScreen((RelativeLayout) main.findViewById(R.id.gameWonView), false);
		}
		if (gameIndicator == GAME_INDICATOR_TIME) {
			main.showScreen((RelativeLayout) main.findViewById(R.id.timeOverView), false);
		}
		if (gameIndicator == GAME_INDICATOR_MONSTER) {
			main.showScreen((RelativeLayout) main.findViewById(R.id.gameOverView), false);
		}
		
		
		finish();
	}

	protected MarkerOptions createNewMarker(LatLng pos, int res) {
		MarkerOptions marker = new MarkerOptions();
		marker.position(pos);
		marker.snippet("");
		marker.anchor(0.5f, 0.5f);
		marker.icon(BitmapDescriptorFactory.fromResource(res));
		marker.title("");
		return marker;
	}

	/** Setzt die Kamera auf die aktuelle Position **/
	private void updateCamera(LatLng loc, float tiltLevel, float angleLevel,
			float zoomLvl) {
		
		// DEBUG PURPOSE
		if ((playground == null) || (playground.getCameraPosition() == null))
			return;

		if (zoomLvl != 0)
			zoomLevel = zoomLvl;
		else
			zoomLevel = playground.getCameraPosition().zoom;
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(new LatLng(loc.latitude, loc.longitude))
				.bearing(angleLevel).zoom(zoomLevel).tilt(tiltLevel).build();

		CameraUpdate cameraUpdate = CameraUpdateFactory
				.newCameraPosition(cameraPosition);
		playground.moveCamera(cameraUpdate);
	}

	/** Setzt die Kamera auf die aktuelle Position **/
	private void initCamera() {
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(new LatLng(curPos.getLatitude(), curPos.getLongitude()))
				.zoom(zoomLevel).build();

		CameraUpdate cameraUpdate = CameraUpdateFactory
				.newCameraPosition(cameraPosition);
		playground.moveCamera(cameraUpdate);
	}

	protected void addToStoneOverlay(GPSPoint gpsPoint) {

		// Calc Boundaries
		double size = GlobalConstants.STONE_IMAGE_SIZE;
		GPSPoint sizeP = new GPSPoint(size, size * GlobalConstants.IMAGE_SCALE_FACTOR);
		GPSPoint a2 = gpsPoint.copy();
		a2.substract(sizeP);
		GPSPoint b2 = gpsPoint.copy();
		b2.add(sizeP);
		LatLng a = a2.toLatLng();
		LatLng b = b2.toLatLng();
		LatLngBounds bounds = new LatLngBounds(a, b);

		stoneOverlay.add(playground.addGroundOverlay(new GroundOverlayOptions()
				.image(BitmapDescriptorFactory.fromBitmap(stoneAnimation.getNextAnimationPicture()))
				.positionFromBounds(bounds).anchor(0.5f, 0.5f).zIndex(0.5f)
				.transparency(0.0f)));
	}

	protected void addToEnemyOverlay(GPSPoint gpsPoint, int enemy) {
		LatLng pos = new LatLng(gpsPoint.getLatitude(), gpsPoint.getLongitude());
		LatLng a = new LatLng(pos.latitude - GlobalConstants.ENEMY_IMAGE_OFFSET, pos.longitude
				- GlobalConstants.ENEMY_IMAGE_OFFSET * GlobalConstants.IMAGE_SCALE_FACTOR);
		LatLng b = new LatLng(pos.latitude + GlobalConstants.ENEMY_IMAGE_OFFSET, pos.longitude
				+ GlobalConstants.ENEMY_IMAGE_OFFSET * GlobalConstants.IMAGE_SCALE_FACTOR);

		enemyOverlay.add(playground.addGroundOverlay(new GroundOverlayOptions()
				.image(BitmapDescriptorFactory.fromBitmap(enemyAnimations.get(enemy).getNextAnimationPicture())) //TODO:
				.positionFromBounds(new LatLngBounds(a, b)).anchor(0.5f, 0.5f)
				.bearing(1.0f).zIndex(0.7f).transparency(0.0f)));
	}
	

	protected void initPlayerOverlay(GPSPoint gpsPoint, Bitmap bitmap) {

		double size = GlobalConstants.PLAYER_IMAGE_SIZE;
		GPSPoint sizeP = new GPSPoint(size, size * GlobalConstants.IMAGE_SCALE_FACTOR);
		GPSPoint a2 = gpsPoint.copy();
		a2.substract(sizeP);
		GPSPoint b2 = gpsPoint.copy();
		b2.add(sizeP);
		LatLng a = a2.toLatLng();
		LatLng b = b2.toLatLng();
		LatLngBounds bounds = new LatLngBounds(a, b);
		
		twostoneOverlay = playground.addGroundOverlay(new GroundOverlayOptions()
				.image(BitmapDescriptorFactory.fromBitmap(bitmap))
				.positionFromBounds(bounds).anchor(0.5f, 0.5f).zIndex(1.0f)
				.bearing(1.0f).zIndex(1.0f).transparency(0.0f));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.play, menu);

		MenuItem exitGame = menu.findItem(R.id.exitGame);
		exitGame.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem arg0) {
				quitGame();
				return true;
			}
		});

		MenuItem switchView = menu.findItem(R.id.switchView);
		switchView.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem arg0) {
				MainActivity.augmentedReality.switchView();
				return true;
			}
		});

		return true;
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
	    int action = event.getAction();
	    int keyCode = event.getKeyCode();
	        switch (keyCode) {
	        case KeyEvent.KEYCODE_VOLUME_UP:
	            if (action == KeyEvent.ACTION_DOWN) {
	            	int setted = 1;
	                for (Enemy e : game.getEnemies()) {
	                	
	                	switch (e.getSpeed()) {
		                	case GlobalConstants.SPEED_SNAIL:
	                			e.setSpeed(GlobalConstants.SPEED_SLOW);
	                			break;
		                	case GlobalConstants.SPEED_SLOW:
	                			e.setSpeed(GlobalConstants.SPEED_WALKSPEED);
	                			break;
		                	case GlobalConstants.SPEED_WALKSPEED:
	                			e.setSpeed(GlobalConstants.SPEED_MEDIUM);
	                			break;
		                	case GlobalConstants.SPEED_MEDIUM:
	                			e.setSpeed(GlobalConstants.SPEED_FAST);
	                			break;
	                	}
	                		
	                	switch (e.getIntelligence()) {
                		case GlobalConstants.ENEMY_FOOL:
                			e.setIntelligence(GlobalConstants.ENEMY_EASY);
                			break;
                		case GlobalConstants.ENEMY_EASY:
                			e.setIntelligence(GlobalConstants.ENEMY_NORMAL);
                			break;
                		case GlobalConstants.ENEMY_NORMAL:
                			e.setIntelligence(GlobalConstants.ENEMY_GOOD);
                			break;
                		case GlobalConstants.ENEMY_GOOD:
                			e.setIntelligence(GlobalConstants.ENEMY_EXTREME);
                			break;
	                	}
	                	
	                	setted = e.getIntelligence();
	                }	              
	                SeekBar difficultySeek = (SeekBar) findViewById(R.id.difficulty_seek_difficulty);
	                difficultySeek.setProgress(setted);
	                TextView difficultyText = (TextView) findViewById(R.id.difficulty_text_difficulty);
	                difficultyText.setText((setted+1)+"");
	                showGhostAngry(setted);
	            }
	            return true;
	            
	        case KeyEvent.KEYCODE_VOLUME_DOWN:
	            if (action == KeyEvent.ACTION_DOWN) {
	            	int setted = 1;
	                for (Enemy e : game.getEnemies()) {

	                	switch (e.getSpeed()) {
		                	case GlobalConstants.SPEED_SLOW:
	                			e.setSpeed(GlobalConstants.SPEED_SNAIL);
	                			break;
		                	case GlobalConstants.SPEED_WALKSPEED:
	                			e.setSpeed(GlobalConstants.SPEED_SLOW);
	                			break;
		                	case GlobalConstants.SPEED_MEDIUM:
	                			e.setSpeed(GlobalConstants.SPEED_WALKSPEED);
	                			break;
		                	case GlobalConstants.SPEED_FAST:
	                			e.setSpeed(GlobalConstants.SPEED_MEDIUM);
	                			break;
	                	}
	                		
	                	switch (e.getIntelligence()) {
                		case GlobalConstants.ENEMY_EASY:
                			e.setIntelligence(GlobalConstants.ENEMY_FOOL);
                			break;
                		case GlobalConstants.ENEMY_NORMAL:
                			e.setIntelligence(GlobalConstants.ENEMY_EASY);
                			break;
                		case GlobalConstants.ENEMY_GOOD:
                			e.setIntelligence(GlobalConstants.ENEMY_NORMAL);
                			break;
                		case GlobalConstants.ENEMY_EXTREME:
                			e.setIntelligence(GlobalConstants.ENEMY_GOOD);
                			break;
	                	}
	                	
	                	setted = e.getIntelligence();
	                }	            
	                SeekBar difficultySeek = (SeekBar) findViewById(R.id.difficulty_seek_difficulty);
	                difficultySeek.setProgress(setted);
	                TextView difficultyText = (TextView) findViewById(R.id.difficulty_text_difficulty);
	                difficultyText.setText((setted+1)+"");
	                showGhostAngry(setted);
	            }
	            return true;
	        default:
	            return super.dispatchKeyEvent(event);
	        }
	    }

    static Timer t = new Timer();
	private void showGhostAngry(int intelligence) {
		final RelativeLayout r = (RelativeLayout) findViewById(R.id.difficultyPanel);
		final ImageView ghost = (ImageView) findViewById(R.id.difficultyGhost);
		
		ghost.setImageResource(R.drawable.s5);
		if (intelligence == GlobalConstants.ENEMY_FOOL)
			ghost.setImageResource(R.drawable.s1);
		if (intelligence == GlobalConstants.ENEMY_EASY)
			ghost.setImageResource(R.drawable.s2);
		if (intelligence == GlobalConstants.ENEMY_NORMAL)
			ghost.setImageResource(R.drawable.s3);
		if (intelligence == GlobalConstants.ENEMY_GOOD)
			ghost.setImageResource(R.drawable.s4);
			
		r.setVisibility(RelativeLayout.VISIBLE);
		
		if (t != null) {
			t.cancel();
			t = null;
		}
		
		t = new Timer();
		TimerTask vanish = new TimerTask (){
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						t.cancel();
						t = null;
						r.setVisibility(RelativeLayout.INVISIBLE);						
					}
				});
			}
		};
		
		t.schedule(vanish, 5000);		
	}
	
}
