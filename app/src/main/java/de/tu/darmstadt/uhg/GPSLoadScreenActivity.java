package de.tu.darmstadt.uhg;

import java.util.Timer;
import java.util.TimerTask;


import de.tu.darmstadt.uhg.game.PlayActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class GPSLoadScreenActivity extends FragmentActivity {

	private Timer t;
	private boolean runOnce = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_gpsload_screen);		
			
		t = new Timer();

		final ImageView l2 = (ImageView) findViewById(R.id.load_step2);
		final ImageView l3 = (ImageView) findViewById(R.id.load_step3);
		
		final Button start = (Button) findViewById(R.id.start);
		start.setVisibility(Button.INVISIBLE);
		start.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				//Start Game?!
				t.cancel();
				Intent playIntent = new Intent(GPSLoadScreenActivity.this,
						PlayActivity.class);
				startActivityForResult(playIntent, GlobalConstants.ACTIVITY_PLAY); //Let's start the Game
//				finish(); // Au revoir to this screen
				
			}
		});
		
		PlayActivity.instantiateGPS((LocationManager) getSystemService(Context.LOCATION_SERVICE));


		final Vibrator vibration = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);		
		
		TimerTask tTask = new TimerTask() {
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						l2.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.gps));
						
						if ((PlayActivity.GPSflag) && (!runOnce)) {
							//Got GPS Signal! 
							runOnce = true;
							start.setVisibility(ImageButton.VISIBLE);
							l3.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ready));
							vibration.vibrate(300);
						}
												
					}
				});
			}
		};
		
			
		//Going to check every x milliseconds if GPS is now available and update satellite image!
		t.schedule(tTask, 300, 300);
		
	}

	/** Zur�ck-Button gedr�ckt **/
	@Override
	public void onBackPressed() {
		//TODO: Dismiss GPS finding
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gpsload_screen, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		setResult(resultCode);
		finish();
	}
}
