package de.tu.darmstadt.uhg.progressing;

import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.Map;
import de.tu.darmstadt.uhg.Player;
import android.graphics.Matrix;
import android.graphics.Point;
import android.widget.ImageView;

/**
 * Enth�lt alle relevanten Methoden zur Richtungsverarbeitung
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 *
 */
public class DirectionProgressing {

	static public int ROTATION_OFFSET = 0; //Bildschirm nach oben drehen!
	static public boolean enabledRotation = true;
	static private double lastAngle = 0;

	/**Turns rotation off**/
	static public void disableRotation() {
		enabledRotation = false;
	}
	
	/**Turns rotation on**/
	static public void enableRotation() {
		enabledRotation = true;
	}
	
	/**RETURNS THE DIRECTION**/
	static public int getSkyDirection(float direction) {
		if ((direction >= 45) && (direction < 135)) return GlobalConstants.EAST;
		if ((direction >= 135) && (direction < 225)) return GlobalConstants.SOUTH;
		if ((direction >= 225) && (direction < 315)) return GlobalConstants.WEST;
		return GlobalConstants.NORTH;
	}
	
	/**Gibt als String aus, welche Richtung gerade angezeigt wird**/
	static public String directionToString(int i) {
		if (i == GlobalConstants.EAST) return "East";
		if (i == GlobalConstants.SOUTH) return "South";
		if (i == GlobalConstants.WEST) return "West";
		return "North";
	}
	
	
	/**
	 * Dreht das Image
	 * @param img ImageView
	 * @param angle Winkel, gibt an, wie weit gedreht werden soll
	 * @param map Karte, auf der gedreht wird
	 * @param player Spielerposition, um die gedreht wird
	 * @param last true, wenn letzter Winkel gespeichert werden soll
	 */
	static public void rotateImage(ImageView img, float angle, Map map, Player player, boolean last) {		
		
		//ROTATION OFFSET BEHANDLEN
		if (ROTATION_OFFSET < 360) {
			angle = angle + ROTATION_OFFSET;
			if (angle > 360) angle -= 360;
		}
		
		//Gegen ruckeln => Toleranz!
		if ((Math.abs(angle-getLastAngle()) >= GlobalConstants.TOLERANCE_DIRECTION)
		 && (enabledRotation)) {
				//First Redo last Step
				if (last) setLastAngle(angle);
				img.getImageMatrix().reset();
		 		
				Point p = map.GPSPointToPixelPoint(player.getPosition());	
				
				float offX = p.x; 
				float offY = p.y;
				
				Matrix matrix = new Matrix();
			 	matrix.postRotate(angle, offX, offY); 
			 	img.setImageMatrix(matrix);  
			 	
			 	//GHOSTS DREHEN
			 	//BREAD DREHEN
			 				 				 	
		}
	}

	/**
	 * Gibt den letzten Winkel zur�ck
	 */
	public static double getLastAngle() {
		return lastAngle;
	}

	/**
	 * Setzt den letzten Winkel
	 */
	public static void setLastAngle(double lastAngle) {
		DirectionProgressing.lastAngle = lastAngle;
	}
	
}
