package de.tu.darmstadt.uhg.gps;

import java.util.ArrayList;

/**
 * Diese Klasse repräsentiert einen Knoten auf einer Karte
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
public class GPSPathPoint {
	
	private GPSPoint point;
	public String description;
	public ArrayList<GPSPathPoint> leadToPoints;
	public ArrayList<GPSPathPoint> cameFromPoints;
	public boolean isStartPoint;
	public int ghostIndex;
	public boolean isInactivePath;
	
	public boolean isInactivePath() {
		return isInactivePath;
	}

	public void setInactivePath(boolean isInactivePath) {
		this.isInactivePath = isInactivePath;
	}

	public int getGhostIndex() {
		return ghostIndex;
	}

	public void setGhostIndex(int ghostIndex) {
		this.ghostIndex = ghostIndex;
	}

	public boolean isStartPoint() {
		return isStartPoint;
	}

	public void setStartPoint(boolean isStartPoint) {
		this.isStartPoint = isStartPoint;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public GPSPathPoint(GPSPoint point) {
		this.point = point;
		this.leadToPoints = new ArrayList<GPSPathPoint>();
		this.cameFromPoints = new ArrayList<GPSPathPoint>();
		this.description = "";
		this.isStartPoint = false;
		this.ghostIndex = -1;
		this.isInactivePath = false;
	}
	
	public GPSPathPoint(GPSPoint point, String description, boolean startTag, int ghostIndex, boolean isInactivePath) {
		this.point = point;
		this.leadToPoints = new ArrayList<GPSPathPoint>();
		this.cameFromPoints = new ArrayList<GPSPathPoint>();
		this.description = description;
		this.isStartPoint = startTag;
		this.ghostIndex = ghostIndex;
		this.isInactivePath = isInactivePath;
	}

	public GPSPoint getPoint() {
		return point;
	}

	public void setPoint(GPSPoint point) {
		this.point = point;
	}

	public ArrayList<GPSPathPoint> getLeadToPoints() {
		return leadToPoints;
	}

	public void setLeadToPoints(ArrayList<GPSPathPoint> leadToPoints) {
		this.leadToPoints = leadToPoints;
	}

	public ArrayList<GPSPathPoint> getCameFromPoints() {
		return cameFromPoints;
	}

	public void setCameFromPoints(ArrayList<GPSPathPoint> cameFromPoints) {
		this.cameFromPoints = cameFromPoints;
	}

	public void addToCameFromPoints(GPSPathPoint newPoint) {
		this.cameFromPoints.add(newPoint);
	}

	public void addToLeadToPoints(GPSPathPoint newPoint) {
		this.leadToPoints.add(newPoint);
	}
	
	

}
