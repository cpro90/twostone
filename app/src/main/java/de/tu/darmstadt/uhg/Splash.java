package de.tu.darmstadt.uhg;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Splash extends Activity implements AsyncLoadingCallback {

    private final int SPLASH_DISPLAY_LENGHT = 2500;
    private MediaPlayer music;
    private boolean loadingDone = false;
    private boolean animationDone = false;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
        setContentView(R.layout.activity_splash);

		ImageView myImageView= (ImageView)findViewById(R.id.logo_tud);
		Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fader);
		myImageView.startAnimation(myFadeInAnimation);

		myImageView= (ImageView)findViewById(R.id.logo_kom);
		myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fader);
		myImageView.startAnimation(myFadeInAnimation);
		
		ImageView splashImageLoading1Done= (ImageView)findViewById(R.id.splash_image_loading_1_done);
		ImageView splashImageLoading2Done= (ImageView)findViewById(R.id.splash_image_loading_2_done);
		ImageView splashImageLoading3Done= (ImageView)findViewById(R.id.splash_image_loading_3_done);
		
		splashImageLoading1Done.animate().alpha(1f).setDuration(SPLASH_DISPLAY_LENGHT/3).setStartDelay(0)
		.setListener(null);
		splashImageLoading2Done.animate().alpha(1f).setDuration(SPLASH_DISPLAY_LENGHT/3).setStartDelay(SPLASH_DISPLAY_LENGHT/3)
		.setListener(null);
		splashImageLoading3Done.animate().alpha(1f).setDuration(SPLASH_DISPLAY_LENGHT/3).setStartDelay(2*SPLASH_DISPLAY_LENGHT/3)
		.setListener(null);
       
//        VideoView view = (VideoView)findViewById(R.id.splashVideo);
        //String path = "android.resource://" + getPackageName() + "/" + R.raw.splash_screen;
//        view.setVideoURI(Uri.parse(path));
//        view.start();
        
        
        new Handler().postDelayed(new Runnable(){
        	
            @Override
            public void run() {
            	animationDone = true;
                checkStart();
            }
        }, SPLASH_DISPLAY_LENGHT);
        

        //TODO:
        //PUT SOMETHING THAT HAVE TO BE LOADED ON SPLASH SCREEN HERE:
        UserSettings.context = this;
        UserSettings.loadUserData(this);
       
        
        
        
		music = MediaPlayer.create(this, R.raw.nac);
		music.start();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}

	@Override
	public void loadingDone() {
		loadingDone = true;
		checkStart();
	}
	
	private void checkStart(){
		if(loadingDone && animationDone){
			final Intent mainIntent = new Intent(Splash.this, MainActivity.class);
			mainIntent.putExtras(getIntent());
			Splash.this.startActivity(mainIntent);
            Splash.this.finish();
		}
	}
	
	

}
