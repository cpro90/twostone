package de.tu.darmstadt.uhg.game;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.tu.darmstadt.uhg.CameraSurfacePreview;
import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.MainActivity;
import de.tu.darmstadt.uhg.R;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.progressing.BitmapProgressing;

@SuppressWarnings("deprecation")
public class AugmentedReality {

	MainActivity mainActivity;
	PlayActivity playActivity;

	public void setPlayActivity(PlayActivity playActivity) {
		this.playActivity = playActivity;
	}

	public static float sensorRotationOffset = 0.0f;

	private boolean augmentedRealityReady = false; // true, falls Kamera und
													// Rotationsensor bereit
	private boolean cameraReady = false; // true, falls Kamera Initialisiert
											// wurde
	private boolean cameraEnabled = false; // true, falls Kamera in
											// Einstellungen aktiviert ist
	private boolean rotationVectorSensorReady = false; // true, falls
														// Rotationsensor bereit
	private SensorManager sensorManager;
	private Sensor rotationVectorSensor; // Sensor zur Bestimmung der
											// Ausrichtung
	// private Sensor accelerometer; // nicht umgesetzt
	// private Sensor magneticField; // nicht umgesetzt
	private Vibrator vibration; // Vibration f�r nahe Geister
	float[] accelerometerValues; // nicht umgesetzt
	float[] magneticFieldValues; // nicht umgesetzt
	float[] getOrientationVectorValues = new float[3]; // Werte zur Ausrichtung
														// des Smartphones
														// (urspr�ngliches
														// Koordinatensystem)
	float[] getOrientationVectorRemapedValues = new float[3]; // Werte zur
																// Ausrichtung
																// des
																// SMartphones
																// (Koordinatensystem
																// bei nach
																// vorne
																// gerichteter
																// Kamera)
	double distance = 1; // Distanz zu einem virtuellen Objekt
	private boolean currentViewType = false; // Aktuell angezeigte Ansicht (true
												// = AR, false = Map)
	private boolean switchViewEnabled = true; // Automatischer Wechsel der
												// Ansicht in EInstellungen
												// aktiviert
	GPSPoint location = new GPSPoint(0, 0); // GPS Position des Smartphones
	// float[] getOrientationValues = new float[3];
	Camera camera; // R�ckseitenkamera
	private Camera.Parameters cameraParameters;
	private float cameraHorizontalViewAngle;
	CameraSurfacePreview cameraSurface; // Preview-Fenster zur Ausgabe des
										// Kameraildes
	private Bitmap augmentedBitmap; // Bitmap auf dem die virtuellen Objekte
									// gezeichnet werden
	private Canvas augmentedCanvas; // Canvas zum Zeichen der virtuellen Objekte
	private boolean bitmapChanged = false;

	/*
	 * ============================================ Begin SensorsView test area
	 * global variables (Debugging Only)
	 * ============================================
	 */

	public static float imageHeightOffset = 1.0f;
	public static float imageWidthOffset = 1.0f;
	private Bitmap testDotImage;

	// private TextView getOrientationVector0;
	// private TextView getOrientationVector1;
	// private TextView getOrientationVector2;
	private TextView getOrientationRemaped0;
	private TextView getOrientationRemaped1;
	private TextView getOrientationRemaped2;
	private TextView correctedRotation;
	private TextView calcRotation;
	private TextView distanceTextView;
	private TextView locationLatView;
	private TextView locationLongView;
	private EditText playerLat;
	private EditText playerLong;
	private EditText targetLat;
	private EditText targetLong;
	private EditText editImageHeightOffset;
	private EditText editImageWidthOffset;
	private EditText editRotationOffset;
	private Button setPlayer;
	private Button setTarget;
	private Button setOffset;
	private Button setRotationOffset;
	// private GPSPoint a = new GPSPoint(49.897217, 8.850655); // dieburg zimmer
	// private GPSPoint b = new GPSPoint(49.897133, 8.850482);
	private GPSPoint a = new GPSPoint(49.897178, 8.850712); // dieburg
															// Wohnzimmer
	private GPSPoint b = new GPSPoint(49.897364, 8.850712);
	// private GPSPoint a = new GPSPoint(49.869138, 8.648455);
	// private GPSPoint b = new GPSPoint(49.868554, 8.650043); // Darmstadt
	// Theater

	private ImageView cameraForeground;

	/*
	 * =========================== SensorsView test area global variables END
	 * ===========================
	 */

	public AugmentedReality(MainActivity context) {
		this.mainActivity = context;
		sensorManager = (SensorManager) context
				.getSystemService(Context.SENSOR_SERVICE);
		vibration = (Vibrator) mainActivity
				.getSystemService(Context.VIBRATOR_SERVICE);
		if (!initRotationVectorSensor()) {
			Toast.makeText(
					mainActivity,
					mainActivity.getResources().getString(
							R.string.noRotationVectorSensor), Toast.LENGTH_LONG)
					.show();
		}
		if (mainActivity.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)
				&& rotationVectorSensor != null) {
		}

		// initAccelerometer();
		// initMagneticFieldSensor();

		/*
		 * ============================================= Begin SensorsView test
		 * area (Debugging Only) ============================================
		 */
		// getOrientationVector0 = (TextView) mainActivity
		// .findViewById(R.id.getOrientationRotation0);
		// getOrientationVector1 = (TextView) mainActivity
		// .findViewById(R.id.getOrientationRotation1);
		// getOrientationVector2 = (TextView) mainActivity
		// .findViewById(R.id.getOrientationRotation2);
		getOrientationRemaped0 = (TextView) mainActivity
				.findViewById(R.id.getOrientationRemaped0);
		getOrientationRemaped1 = (TextView) mainActivity
				.findViewById(R.id.getOrientationRemaped1);
		getOrientationRemaped2 = (TextView) mainActivity
				.findViewById(R.id.getOrientationRemaped2);
		correctedRotation = (TextView) mainActivity.findViewById(R.id.ratio);
		calcRotation = (TextView) mainActivity.findViewById(R.id.calcRotation);
		distanceTextView = (TextView) mainActivity.findViewById(R.id.distance);
		locationLatView = (TextView) mainActivity
				.findViewById(R.id.locationLat);
		locationLongView = (TextView) mainActivity
				.findViewById(R.id.locationLong);

		playerLat = (EditText) mainActivity.findViewById(R.id.PlayerLat);
		playerLong = (EditText) mainActivity.findViewById(R.id.PlayerLong);
		targetLat = (EditText) mainActivity.findViewById(R.id.TargetLat);
		targetLong = (EditText) mainActivity.findViewById(R.id.TargetLong);
		editImageHeightOffset = (EditText) mainActivity
				.findViewById(R.id.imageHeightOffset);
		editImageWidthOffset = (EditText) mainActivity
				.findViewById(R.id.imageWidthOffset);
		editRotationOffset = (EditText) mainActivity
				.findViewById(R.id.rotationOffset);
		setPlayer = (Button) mainActivity.findViewById(R.id.AcceptPlayer);
		setTarget = (Button) mainActivity.findViewById(R.id.AcceptTarget);
		setOffset = (Button) mainActivity.findViewById(R.id.AcceptOffset);
		setRotationOffset = (Button) mainActivity
				.findViewById(R.id.AcceptRotationOffset);

		playerLat.setText(a.getLatitude() + "");
		playerLong.setText(a.getLongitude() + "");
		targetLat.setText(b.getLatitude() + "");
		targetLong.setText(b.getLongitude() + "");
		editImageHeightOffset.setText(imageHeightOffset + "");
		editImageWidthOffset.setText(imageWidthOffset + "");
		editImageWidthOffset.setText(imageWidthOffset + "");
		editRotationOffset.setText(sensorRotationOffset + "");

		setPlayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!playerLat.getText().toString().equals("")) {
					a.setLatitude(Float.parseFloat(playerLat.getText()
							.toString()));
				} else {
					a.setLatitude(location.getLatitude());
				}
				if (!playerLong.getText().toString().equals("")) {
					a.setLongitude(Float.parseFloat(playerLong.getText()
							.toString()));
				} else {
					a.setLongitude(location.getLongitude());
				}
			}
		});

		setTarget.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!targetLat.getText().toString().equals("")) {
					b.setLatitude(Float.parseFloat(targetLat.getText()
							.toString()));
				} else {
					b.setLatitude(location.getLatitude());
				}
				if (!targetLong.getText().toString().equals("")) {
					b.setLongitude(Float.parseFloat(targetLong.getText()
							.toString()));
				} else {
					b.setLongitude(location.getLongitude());
				}
			}
		});

		setOffset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setImageHeightOffset(Float.parseFloat(editImageHeightOffset
						.getText().toString()));
				setImageWidthOffset(Float.parseFloat(editImageWidthOffset
						.getText().toString()));
			}
		});

		setRotationOffset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sensorRotationOffset = Float.parseFloat(editRotationOffset
						.getText().toString());
			}
		});

		augmentedBitmap = BitmapProgressing.convertToMutable(BitmapFactory
				.decodeResource(mainActivity.getResources(),
				// R.drawable.empty_camera));
						R.drawable.empty_camera));

		// sensorTestBitmap = Bitmap.createScaledBitmap(sensorTestBitmap,
		// sensorTestForeground.getWidth(),
		// sensorTestForeground.getHeight(), false);
		augmentedCanvas = new Canvas(augmentedBitmap);
		augmentedCanvas.save();
		testDotImage = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
				mainActivity.getResources(), R.drawable.energy_00), 200, 200,
				false);

		/*
		 * ================================ SensorsView test area END
		 * ===============================
		 */
	}

	/**
	 * Initialisiert die Kamera, falls vorhanden und verbindet sie mit
	 * cameraSurface
	 * 
	 * @return true bei Erfolg
	 */
	public boolean initCamera() {
		if (mainActivity.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			try {
				camera = Camera.open();
				Camera.CameraInfo caminfo = new Camera.CameraInfo();
				Camera.getCameraInfo(0, caminfo);
					camera.setDisplayOrientation(caminfo.orientation);
				cameraSurface = new CameraSurfacePreview(mainActivity, camera);
				cameraParameters = camera.getParameters();
				cameraHorizontalViewAngle = cameraParameters
						.getHorizontalViewAngle();
				cameraParameters.getVerticalViewAngle();
				cameraReady = true;
				if (rotationVectorSensorReady)
					augmentedRealityReady = true;

				// FrameLayout previewFrame = (FrameLayout) mainActivity
				// .findViewById(R.id.mapCamera);
				// previewFrame.addView(cameraSurface);

				return true;
			} catch (Exception e) {
				Log.d("camera", "Camera can not be accessed");
				return false;
			}

		} else {
			return false;
		}
	}

	/**
	 * Initialisiert den RotationVectorSensor. Registriert einen Listener, der
	 * die Sensordaten bearbeitet und in getOrientationVectorRemapedValues
	 * speichert.
	 * 
	 * @return true bei Erfolg
	 */
	private boolean initRotationVectorSensor() {
		try {
			rotationVectorSensor = sensorManager
					.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
			if (rotationVectorSensor != null) {
				SensorEventListener rotationVectorListener = new SensorEventListener() {

					@Override
					public void onSensorChanged(SensorEvent event) {
						float[] Matrix = new float[9];
						SensorManager.getRotationMatrixFromVector(Matrix,
								event.values);
						SensorManager.getOrientation(Matrix,
								getOrientationVectorValues);

						float[] MatrixOut = new float[9];
						SensorManager.remapCoordinateSystem(Matrix,
								SensorManager.AXIS_X, SensorManager.AXIS_Z,
								MatrixOut);
						SensorManager.getOrientation(MatrixOut,
								getOrientationVectorRemapedValues);
						if (PlayActivity.active) {
							/**
							 * float angle = (float)
							 * (getOrientationVectorRemapedValues[0] // float angle
							 * = (float) (correctRotation()[0] / (2 * Math.PI) *
							 * 360); mainActivity.rotateMap(angle);
							 **/
							checkSwitchView();
                            if (cameraForeground != null)
							    cameraForeground.setImageBitmap(augmentedBitmap);
						}

						if (mainActivity
								.isVisibleScreen((RelativeLayout) mainActivity
										.findViewById(R.id.sensorsView))) {
							// updateSensorView(); // TODO: View for sensor data,
							// debug only
						}
					}

					@Override
					public void onAccuracyChanged(Sensor sensor, int accuracy) {
					}
				};

				sensorManager.registerListener(rotationVectorListener,
						rotationVectorSensor, SensorManager.SENSOR_DELAY_GAME);
				rotationVectorSensorReady = true;
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * private boolean initAccelerometer() { accelerometer = sensorManager
	 * .getDefaultSensor(Sensor.TYPE_ACCELEROMETER); if (accelerometer != null)
	 * { SensorEventListener accelerometerListener = new SensorEventListener() {
	 * 
	 * @Override public void onSensorChanged(SensorEvent event) {
	 *           accelerometerValues = event.values; }
	 * @Override public void onAccuracyChanged(Sensor sensor, int accuracy) { //
	 *           TODO Auto-generated method stub } };
	 *           sensorManager.registerListener(accelerometerListener,
	 *           accelerometer, SensorManager.SENSOR_DELAY_NORMAL); }
	 * 
	 *           return false; }
	 **/

	/**
	 * private boolean initMagneticFieldSensor() { magneticField = sensorManager
	 * .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD); if (magneticField != null)
	 * { SensorEventListener magneticFieldListener = new SensorEventListener() {
	 * 
	 * @Override public void onSensorChanged(SensorEvent event) {
	 *           magneticFieldValues = event.values; }
	 * @Override public void onAccuracyChanged(Sensor sensor, int accuracy) { }
	 *           }; sensorManager.registerListener(magneticFieldListener,
	 *           magneticField, SensorManager.SENSOR_DELAY_NORMAL); }
	 * 
	 *           return false; }
	 **/

	/**
	 * Berechnet den Winkel, von den das virtuelle Objekte von der Sichtlinie
	 * der Kamera horizontal abweicht.
	 * 
	 * @param player
	 *            Position des Spielers
	 * @param point
	 *            Position des virtuellen Objektes
	 * @return
	 */
	private float calcRotation(GPSPoint player, GPSPoint point) {
		double dLat = (point.getLatitude() - player.getLatitude())
				* GlobalConstants.CONST_LATITUDE;
		double dLong = (point.getLongitude() - player.getLongitude())
				* GlobalConstants.CONST_LONGITUDE;
		distance = Math.sqrt(dLat * dLat + dLong * dLong);

		if (distance <= GlobalConstants.NEAR)
			return 0;

		double arcSin = Math.asin(dLong / distance);
		double rotationOffset = getOrientationVectorRemapedValues[0]
				+ sensorRotationOffset;
		// double rotationOffset = correctRotation()[0] ;

		float ergebnis = 0;

		if (dLat >= 0) { // 1. und 2. Quadrant
			ergebnis = (float) (arcSin - rotationOffset);
		}
		if (dLat < 0 && dLong >= 0) { // 4. Quadrant
			// if (rotationOffset <= 0) {
			// rotationOffset = rotationOffset + Math.PI * 2;
			// }
			ergebnis = (float) (Math.PI - arcSin - rotationOffset);
		}
		if (dLat < 0 && dLong < 0) { // 3. Quadrant
			// if (rotationOffset > 0) {
			// rotationOffset = rotationOffset - Math.PI * 2;
			// }
			ergebnis = (float) (-Math.PI - arcSin - rotationOffset);
		}
		if (ergebnis > Math.PI)
			ergebnis = (float) (ergebnis - 2 * Math.PI);
		if (ergebnis < -Math.PI)
			ergebnis = (float) (ergebnis + 2 * Math.PI);
		return ergebnis;
	}

	/**
	 * Berechnet den Winkel, von den das virtuelle Objekte von der Sichtlinie
	 * der Kamera vertikal abweicht.
	 * 
	 * @param player
	 *            Position des Spielers
	 * @param point
	 *            Position des virtuellen Objektes
	 * @return
	 */
	private float calcHeightRotation(GPSPoint player, GPSPoint point) {
		float ergebnis = 0;
		double distance;
		double dLat = (point.getLatitude() - player.getLatitude())
				* GlobalConstants.CONST_LATITUDE;
		double dLong = (point.getLongitude() - player.getLongitude())
				* GlobalConstants.CONST_LONGITUDE;
		distance = Math.sqrt(dLat * dLat + dLong * dLong);
		ergebnis = (float) Math.atan(GlobalConstants.PLAYER_HEIGHT / distance);
		return ergebnis;
	}

	/**
	 * Aktualisiert den SensorView (Debugging Only)
	 */

	@SuppressWarnings("unused")
	private void updateSensorView() {
		// getOrientationVector0.setText("getOrientationVector0: "
		// + getOrientationVectorValues[0]);
		// getOrientationVector1.setText("getOrientationVector1: "
		// + getOrientationVectorValues[1]);
		// getOrientationVector2.setText("getOrientationVector2: "
		// + getOrientationVectorValues[2]);

		getOrientationRemaped0.setText("getOrientationRemaped0: "
				+ getOrientationVectorRemapedValues[0]);
		getOrientationRemaped1.setText("getOrientationRemaped1: "
				+ getOrientationVectorRemapedValues[1]);
		getOrientationRemaped2.setText("getOrientationRemaped2: "
				+ getOrientationVectorRemapedValues[2]);
		correctedRotation.setText("correctedRotation: " + correctRotation()[0]);

		// a.setLatitude(Float.parseFloat(playerLat.getText().toString()));
		// a.setLongitude(Float.parseFloat(playerLong.getText().toString()));
		// b.setLatitude(Float.parseFloat(targetLat.getText().toString()));
		// b.setLongitude(Float.parseFloat(targetLong.getText().toString()));

		calcRotation.setText("calcRotation: " + calcRotation(a, b));
		// calcRotation.setText("calcRotation: " + calcHeightRotation(a, b));
		distanceTextView.setText("distance: " + distance);
		locationLatView.setText(location.getLatitude() + "");
		locationLongView.setText(location.getLongitude() + "");

		redraw(); // TODO: use redraw() here only for sensortestView
		testDotImage = Bitmap.createScaledBitmap(testDotImage, 50, 50, false);

		double distanceFactor = calcDistanceFactor(b, a);
		if (distanceFactor <= 0)
			return;
		float horizontalRotation = calcRotation(a, b);
		if (horizontalRotation > GlobalConstants.SIGHT_ANGLE_HORIZONTAL
				|| horizontalRotation < -GlobalConstants.SIGHT_ANGLE_HORIZONTAL)
			return;
		float heightRotation;
		heightRotation = calcHeightRotation(a, b);
		// if (false)
		// heightRotation = heightRotation / 4; // no levitation in
		// sensortestView
		Bitmap scaledImage = Bitmap.createScaledBitmap(testDotImage,
				(int) (testDotImage.getWidth() * distanceFactor),
				(int) (testDotImage.getHeight() * distanceFactor), false);
		augmentedCanvas
				.drawBitmap(
						scaledImage,
						(augmentedCanvas.getWidth() * imageWidthOffset - scaledImage
								.getWidth())
								/ 2
								+ augmentedCanvas.getWidth()
								* horizontalRotation
								* GlobalConstants.CAMERA_ANGLE_FACTOR,
						(augmentedCanvas.getHeight() * imageHeightOffset - scaledImage
								.getHeight())
								/ 2
								+ augmentedCanvas.getHeight()
								* -1
								* (getOrientationVectorRemapedValues[1] - heightRotation),
						new Paint(Paint.FILTER_BITMAP_FLAG));

		// sensorTestCanvas.drawBitmap(testDotImage,
		// (sensorTestCanvas.getWidth() - testDotImage.getWidth()) / 2
		// + sensorTestCanvas.getWidth() * calcRotation(a, b)
		// * cameraAngleFactor,
		// (sensorTestCanvas.getHeight() - testDotImage.getHeight()) / 2
		// + sensorTestCanvas.getHeight() * -2
		// * getOrientationVectorRemapedValues[1], new Paint(
		// Paint.FILTER_BITMAP_FLAG));
		// drawOnOverlay(testDotImage, b, a, false);

		ImageView sensorTestForeground = (ImageView) mainActivity
				.findViewById(R.id.sensorTestForeground);

		sensorTestForeground.setScaleType(ScaleType.MATRIX);
		sensorTestForeground.setImageBitmap(augmentedBitmap);
	}

	/**
	 * (Debugging Only) Ver�ndert die horizontale Komponente der Werte des
	 * RotationVectorSensors in Abh�ngigkeit der vertikalen Komponente
	 * 
	 * @return die neue horizontale Rotation
	 */
	private float[] correctRotation() {
		float x = getOrientationVectorRemapedValues[0];
		float y = getOrientationVectorRemapedValues[1];
		float dy = Math.abs(1.5f - y);
		if (x < 0)
			dy = -dy;
		float[] values = new float[3];
		if (Math.abs(Math.abs(x) - 1.5) < 0.5) {
			values[0] = x - (0.3f * dy);
		} else if (Math.abs(Math.abs(x) - 1.5) < 1.0) {
			values[0] = x - (0.2f * dy);
		} else {
			values[0] = x;
		}
		values[1] = y;
		values[2] = getOrientationVectorRemapedValues[2];
		return values;
	}

	/**
	 * Zeichnet die �bergebene Bitmap. Berechnet die entsprechende Position auf
	 * dem Display durch die Position von target und der aktuellen location.
	 * 
	 * @param image
	 *            Bild, das gezeichnet werden soll
	 * @param target
	 *            Position des Objektes, das gezeichnet werden soll
	 * @param levitate
	 *            true, falls das Objekte schweben soll(f�r Geister), sonst
	 *            false
	 */
	public void drawOnOverlay(Bitmap image, GPSPoint target, boolean levitate) {
		drawOnOverlay(image, target, location, levitate, false);
	}

	/**
	 * Zeichnet die �bergebene Bitmap. Berechnet die entsprechende Position auf
	 * dem Display durch die Positionen von target und player.
	 * 
	 * @param image
	 *            Bild, das gezeichnet werden soll
	 * @param target
	 *            Position des Objektes, das gezeichnet werden soll
	 * @param player
	 *            Position des Spielers
	 * @param levitate
	 *            true, falls das Objekte schweben soll(f�r Geister), sonst
	 *            false
	 */
	public void drawOnOverlay(Bitmap image, GPSPoint target, GPSPoint player,
			boolean levitate, boolean hasMarker) {
		if (augmentedRealityReady) {
			double distanceFactor = calcDistanceFactor(target, player);
			float enemySizeFactor;
			if (hasMarker) { // has Marker => Enemy, has to be scaled down
				enemySizeFactor = GlobalConstants.ENEMY_SIZE_MULTIPLAYER;
			} else {
				enemySizeFactor = 1.0f;
			}
			if ((int) (image.getWidth() * enemySizeFactor * distanceFactor) <= 0
					|| (int) (image.getHeight() * enemySizeFactor * distanceFactor) <= 0)
				return;
			float horizontalRotation = calcRotation(player, target);
			if (horizontalRotation > GlobalConstants.SIGHT_ANGLE_HORIZONTAL
					|| horizontalRotation < -GlobalConstants.SIGHT_ANGLE_HORIZONTAL) {
				if (hasMarker) {
					drawMarker(horizontalRotation, distanceFactor / 2);
				}
				return;
			}
			float heightRotation;
			heightRotation = calcHeightRotation(player, target);
			if (levitate)
				heightRotation = heightRotation / 4;
			Bitmap scaledImage = Bitmap
					.createScaledBitmap(
							image,
							(int) (image.getWidth() * enemySizeFactor * distanceFactor),
							(int) (image.getHeight() * enemySizeFactor * distanceFactor),
							false);
			augmentedCanvas
					.drawBitmap(
							scaledImage,
							(augmentedCanvas.getWidth() * imageWidthOffset - scaledImage
									.getWidth())
									/ 2
									+ augmentedCanvas.getWidth()
									/ 2
									* horizontalRotation
									// * cameraAngleFactor
									/ (float) (getHorizontalViewAngle() / 2),
							(augmentedCanvas.getHeight() * imageHeightOffset - scaledImage
									.getHeight())
									/ 2
									+ augmentedCanvas.getHeight()
									/ 2
									* -1
									* (getOrientationVectorRemapedValues[1] - heightRotation)
									/ (float) (Math
											.toRadians(cameraHorizontalViewAngle) / 2),
							new Paint(Paint.FILTER_BITMAP_FLAG));
		}
	}

	/**
	 * Zeichnet einen Geistermarker am Bildschirmrand.
	 * 
	 * @param rotation
	 *            Winkel, von dem der Geist von der Sichtlinie der Kamera in
	 *            horizontaler Richtung abweicht.
	 * @param distanceFactor
	 *            Multiplikator der Markergr��e
	 */
	private void drawMarker(float rotation, double distanceFactor) {
		int distanceX = 0;
		int distanceY = 0;
		Bitmap marker;
		Bitmap scaledImage;
		if (rotation < 0 && rotation > -2.5) {
			marker = BitmapFactory.decodeResource(mainActivity.getResources(),
					R.drawable.marker_left);
			int mwidth = (int) (marker.getWidth() * distanceFactor);
			int mheight = (int) (marker.getHeight() * distanceFactor);
			if (!(mwidth == 0) && !(mheight == 0))
				scaledImage = Bitmap.createScaledBitmap(marker, mwidth,
						mheight, false);
			else
				scaledImage = null;
			distanceX = 0;
			distanceY = (int) (augmentedCanvas.getHeight() / 2 + (rotation + GlobalConstants.SIGHT_ANGLE_HORIZONTAL)
					/ -2.5 * augmentedCanvas.getHeight() / 2);
		} else if (rotation > 0 && rotation < 2.5) {

			marker = BitmapFactory.decodeResource(mainActivity.getResources(),
					R.drawable.marker_right);
			int mwidth = (int) (marker.getWidth() * distanceFactor);
			int mheight = (int) (marker.getHeight() * distanceFactor);
			if ((!(mwidth == 0)) && (!(mheight == 0))) {
				scaledImage = Bitmap.createScaledBitmap(marker, mwidth,
						mheight, false);
				distanceX = augmentedCanvas.getWidth() - scaledImage.getWidth();
				distanceY = (int) (augmentedCanvas.getHeight() / 2 + (rotation - GlobalConstants.SIGHT_ANGLE_HORIZONTAL)
						/ 2.5 * augmentedCanvas.getHeight() / 2);
			} else
				scaledImage = null;
		} else {
			marker = BitmapFactory.decodeResource(mainActivity.getResources(),
					R.drawable.marker_down);
			int mwidth = (int) (marker.getWidth() * distanceFactor);
			int mheight = (int) (marker.getHeight() * distanceFactor);
			if ((!(mwidth == 0)) && (!(mheight == 0)))
				scaledImage = Bitmap.createScaledBitmap(marker, mwidth,
						mheight, false);
			else
				scaledImage = null;
			if (rotation < 0) {
				distanceX = (int) ((rotation + 2.5) / (-Math.PI + 2.5) * (augmentedCanvas
						.getWidth() / 2 + -scaledImage.getWidth()));
			} else {
				// distanceX = (int) ( sensorTestCanvas.getWidth()/2 -
				// scaledImage.getWidth() - (rotation - 2.5) / (Math.PI-2.5) *
				// sensorTestCanvas.getWidth() / 4);
				distanceX = (int) (augmentedCanvas.getWidth() / 2
						- scaledImage.getWidth() + (Math.PI - rotation)
						/ (Math.PI - 2.5) * augmentedCanvas.getWidth() / 2);
			}
			distanceY = (int) (augmentedCanvas.getHeight() * imageHeightOffset - scaledImage
					.getHeight());
		}
		if (scaledImage != null)
			augmentedCanvas.drawBitmap(scaledImage, distanceX, distanceY,
					new Paint(Paint.FILTER_BITMAP_FLAG));
	}

	/**
	 * L�sst das Smatphone in Intervallen vibrieren. Die Dauer der Intervalle
	 * h�ngt von der Distanz zum n�chsten Geist ab.
	 * 
	 * @param player
	 *            Position des Spielers
	 * @param ghost
	 *            Position des Geistes
	 * @param maxDuration
	 *            Maximale Dauer eines Intervalles
	 */
	public void vibrateEnemy(GPSPoint player, ArrayList<Enemy> ghost,
			long maxDuration) {
		float factor = 0;
		for (int i = 0; i < ghost.size(); i++) {
			float currentFactor = calcDistanceFactor(
					ghost.get(i).getPosition(), player);
			if (factor < currentFactor)
				factor = currentFactor;
		}
		float threshold = calcDistanceFactor(new GPSPoint(0.00015, 0.0),
				new GPSPoint(0.0, 0.0));
		factor = factor - threshold;
		long[] pattern = new long[3];
		long duration = (long) (maxDuration * factor / (GlobalConstants.MAX_FAKTOR - threshold));
		pattern[0] = 0;
		pattern[1] = duration;
		pattern[2] = maxDuration - duration;

		vibration.vibrate(pattern, -1);
	}

	/**
	 * L�scht alle bisher gezeichneten Objekte auf dem Canvas.
	 */
	public void redraw() {
		if (augmentedRealityReady) {
			Paint paint = new Paint();
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
			Rect rect = new Rect(0, 0, augmentedCanvas.getWidth(),
					augmentedCanvas.getHeight());
			if (!bitmapChanged) { // TODO: find better place for this part
				if (cameraSurface.getWidth() > 0) {
					augmentedBitmap = Bitmap.createScaledBitmap(
							augmentedBitmap, cameraSurface.getWidth(),
							cameraSurface.getHeight(), false);
					augmentedCanvas = new Canvas(augmentedBitmap);
					augmentedCanvas.save();
					bitmapChanged = true;
				}
			}
			augmentedCanvas.drawRect(rect, paint);
		}
	}

	/**
	 * Ermittelt den Gr��enmultiplikator f�r ein Objekt.
	 * 
	 * @param target
	 *            Position des Objektes
	 * @param player
	 *            Position des Spielers
	 * @return Gr��enmultiplikator f�r das Objekt
	 */
	private float calcDistanceFactor(GPSPoint target, GPSPoint player) {
		double distance;
		double faktor;
		double dLat = (target.getLatitude() - player.getLatitude())
				* GlobalConstants.CONST_LATITUDE;
		double dLong = (target.getLongitude() - player.getLongitude())
				* GlobalConstants.CONST_LONGITUDE;
		distance = Math.sqrt(dLat * dLat + dLong * dLong);
		if (distance > GlobalConstants.FAR || distance < GlobalConstants.NEAR) {
			faktor = 0;
		} else {
			if (distance > GlobalConstants.NEAR
					&& distance <= GlobalConstants.FAR / 2) {
				faktor = (-GlobalConstants.MAX_FAKTOR / GlobalConstants.FAR)
						* 3 / 2 * distance + GlobalConstants.MAX_FAKTOR;
				;
			} else {
				faktor = (-GlobalConstants.MAX_FAKTOR / GlobalConstants.FAR)
						/ 2 * distance + GlobalConstants.MAX_FAKTOR / 2;
			}
		}
		return (float) faktor;
	}

	/**
	 * Wechselt die Ansicht, wenn das Smartphone gerade entsprechend gekippt
	 * wurde
	 */
	private void checkSwitchView() {
		if (switchViewEnabled) {
			if (Math.abs(getOrientationVectorRemapedValues[1]) > GlobalConstants.SWITCH_VIEW_THRESHOLD + 0.1
					&& currentViewType == true) {
				switchToMap();
			}
			if (Math.abs(getOrientationVectorRemapedValues[1]) < GlobalConstants.SWITCH_VIEW_THRESHOLD - 0.1
					&& currentViewType == false) {
				switchToAR();
			}
		}
	}

	/**
	 * Wechselt bei jedem Aufruf zwischen Augmented Reality Ansicht und
	 * Kartenansicht.
	 */
	public void switchView() { // TODO: stop drawing AR, when in MapView
		currentViewType = !currentViewType;
		if (currentViewType)
			switchToAR();
		else
			switchToMap();
	}

	/**
	 * Wechselt in die Augmented Reality Ansicht
	 */
	@SuppressLint("NewApi")
	public void switchToAR() {
		if (cameraEnabled && cameraReady) {
			currentViewType = true;
			playActivity.findViewById(R.id.mapCamera).setVisibility(
					View.VISIBLE);
			cameraForeground.setVisibility(View.VISIBLE);
			playActivity.findViewById(R.id.playground).setVisibility(
					View.INVISIBLE);

			// TODO: CARDBOARD
			if (PlayActivity.USE_CARDBOARD) {
				// HERE CARDBOARD STUFF
			}

			// playActivity.findViewById(R.id.zoomControlPlay).setVisibility(View.INVISIBLE);
		}
	}

	/**
	 * Wechselt in die Kartenansicht
	 */
	@SuppressLint("NewApi")
	public void switchToMap() {
		currentViewType = false;
		playActivity.findViewById(R.id.mapCamera).setVisibility(View.INVISIBLE);
		cameraForeground.setVisibility(View.INVISIBLE);
		playActivity.findViewById(R.id.playground).setVisibility(View.VISIBLE);
		// playActivity.findViewById(R.id.zoomControlPlay).setVisibility(View.VISIBLE);
	}

	public float[] getGetOrientationVectorValues() {
		return getOrientationVectorValues;
	}

	public float[] getGetOrientationVectorRemapedValues() {
		return getOrientationVectorRemapedValues;
	}

	private double getHorizontalViewAngle() {
		// double ratio = cs.width / cs.height;
		// int width = cameraSurface.getWidth();
		// int height = cameraSurface.getHeight();
		int width = augmentedCanvas.getWidth();
		int height = augmentedCanvas.getHeight();
		double ratio = (double) width / (double) height;
		double angleV = Math.toRadians(cameraHorizontalViewAngle); // switch
																	// axis
																	// because
																	// camera is
																	// turned
																	// 90�
		double angleH = 2 * Math.atan(ratio * Math.tan(angleV / 2));
		return angleH;
	}

	public Camera getCamera() {
		return camera;
	}

	public void releaseCamera() {
		if (camera != null) {
			camera.release();
			camera = null;
			cameraReady = false;
			augmentedRealityReady = false;
		}
	}

	public CameraSurfacePreview getCameraSurfacePreview() {
		return cameraSurface;
	}

	public void setLocation(GPSPoint loc) {
		location = loc;
	}

	public void setCameraForeground(ImageView camForeground) {
		cameraForeground = camForeground;
	}

	public boolean getAugmentedRealityReady() {
		return augmentedRealityReady;
	}

	public boolean getCameraReady() {
		return cameraReady;
	}

	public boolean getCameraEnabled() {
		return cameraEnabled;
	}

	public void setCameraEnabled(boolean enabled) {
		cameraEnabled = enabled;
		updateAugmentedRealityReady();
	}

	public void setSwitchViewEnabled(boolean switchView) {
		switchViewEnabled = switchView;
	}

	public static void setImageHeightOffset(float imageHeightOffset) {
		AugmentedReality.imageHeightOffset = imageHeightOffset;
	}

	public static void setImageWidthOffset(float imageWidthOffset) {
		AugmentedReality.imageWidthOffset = imageWidthOffset;
	}

	public boolean updateAugmentedRealityReady() {
		if (rotationVectorSensorReady && cameraReady && cameraEnabled) {
			augmentedRealityReady = true;
		} else {
			augmentedRealityReady = false;
		}
		return augmentedRealityReady;
	}

}
