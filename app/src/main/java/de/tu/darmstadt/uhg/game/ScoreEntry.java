package de.tu.darmstadt.uhg.game;

/**Repräsentiert einen einzelnen Spielstand
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
*/

public class ScoreEntry {
	
	String playerName;
	int points;
	long distance;
	long playtime;
	
	
	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}

	public long getPlaytime() {
		return playtime;
	}

	public void setPlaytime(long playtime) {
		this.playtime = playtime;
	}

	public String getPlayerName() {
		return playerName;
	}
	
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	public int getPoints() {
		return points;
	}
	
	public void setPoints(int points) {
		this.points = points;
	}
	
	public ScoreEntry(String playerName, int points, long distance, long playtime) {
		this.playerName = playerName;
		this.points = points;
		this.playtime = playtime;
		this.distance = distance;
	}
	
	
}
