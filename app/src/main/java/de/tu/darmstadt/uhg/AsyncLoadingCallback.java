package de.tu.darmstadt.uhg;

public interface AsyncLoadingCallback {

	public void loadingDone();
}
