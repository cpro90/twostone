package de.tu.darmstadt.uhg;

import android.graphics.Bitmap;

public class GlobalConstants {
	
	//Main Constants
	public static final int SELECT_PHOTO = 302; 
	public static final int REQUEST_ENABLE_BT = 303;
	public static final int EARTH_RADIUS = 6371000;
	public static final int MAX_ENEMY_COUNT = 5;
	public static final int CONST_LATITUDE = 111319; 
	public static final int CONST_LONGITUDE = 70386; 
	public static final int ACTIVITY_ONLINE = 1;
	public static final int ACTIVITY_EDITOR = 2;
	public static final int ACTIVITY_PLAY = 3;
	public static final int ACTIVITY_MAP = 4;
	public static final int ACTIVITY_GPS = 5;
	public static final int REFRESH_RATE = 8000;
	public static final int MIN_DATA_SEND_TIME = 500;
	public static final int GATT_SERVICE_HEART_RATE = 0x180D;
    public static final long BLUETOOTH_SCAN_PERIOD = 10000;
	public static final boolean MUCH_MEMORY = true;
	public static final boolean AUTO_DETECT_GHOSTS = false;
	public static final int PROFILE_PICTURE_COMPRESSION_RATE = 10;
	  
	
	//Drawer
	public static final int DRAWER_HOME = 1;
	public static final int DRAWER_PROFILE = 2;
	public static final int DRAWER_CHAT = 3;
	public static final int DRAWER_CHANGE_USER = 4;
	public static final int DRAWER_OPTIONS = 5;
	public static final int DRAWER_HELP = 6;
	public static final int DRAWER_STATISTICS = 7;
	public static final int DRAWER_RESTART = 8;
	public static final int DRAWER_CHANGE_LEVEL = 9;
	public static final int DRAWER_EXIT = 10;
	public static final int DRAWER_HIGHSCORE = 11;
	public static final int DRAWER_LEVEL_OVERVIEW = 12;
	
	//Play Activity
	public static final int LINE_COLOR = 0xAFAFAFAF;
	public static final int BACKGROUND_COLOR = 0xEFEFEFEF;
	public static final int LINE_WIDTH = 10;
	public static final float MAX_ZOOM = 19.0f;
	public static final float MIN_ZOOM = 14.0f;
	public static final float PLAYER_IMAGE_SIZE = 0.000065f;
	public static final float STONE_IMAGE_SIZE = 0.000025f;
	public static final float ENEMY_IMAGE_OFFSET = 0.000075f;
	public static final float IMAGE_SCALE_FACTOR = 1.5f;
	public static int REDRAW_TIME = 100;
	public static double MAP_DISTANCE_TO_START = 2500;
	
	//GPS Progressing
	public static final int SCALE = 2;
	public static final double TOLERANCEPOSITION = 0.0001;  
	public static final double TOLERANCEMOVEMENT = 0.00005; 
	public static final double GPS_INFINITE = 360000;
	
	//Level Editor
	public static final int MODE_CONNECT = 0;
	public static final int MODE_MOVE = 1;
	public static final int MODE_AUTO = 2;
	public static final boolean DRAW_POLYLINE = false;
	public static final boolean ENABLE_AUTOSAVE = true;
	public static final int MIN_ZOOM_LEVEL = 17;
	public static final int RESULT_PUBLISH_NEW_MAP = 20;
	public static final int MAX_WAYPOINTS = 64;
	public static final String RESULT_MAP_NAME = "de.tu.darmstadt.uhg.leveleditor.mapName";
	
	//Map Filter
	public static final String FILTER_MAP_ALL = "All";
	public static final String FILTER_MAP_LOCAL = "Local";
	public static final String FILTER_MAP_ONLINE = "Online";
	
	//Singleplayer
	public final static float ENEMY_SPAWN_SECURE_CIRCLE = 0.00025f;										    
	public final static int MAX_TRIES = 999;
	public final static int MAX_AVERAGE_COUNT = 100;
	public static final int ZOOM_NEAR = 1;
	public static final int ZOOM_NORMAL = 2;
	public static final int ZOOM_FAR = 3;
	public static final int ZOOM_ALL = 25;
	public static final int ENEMY_OFF = -1;
	public static final int ENEMY_FOOL = 0;
	public static final int ENEMY_EASY = 1;
	public static final int ENEMY_NORMAL = 2;
	public static final int ENEMY_GOOD = 3;
	public static final int ENEMY_EXTREME = 4;
	public static final int ENEMY_ABNORMAL = 5;
	public static final int SPEED_OFF = -1;
	public static final int SPEED_SNAIL = 10;
	public static final int SPEED_SLOW = 20;
	public static final int SPEED_WALKSPEED = 40;
	public static final int SPEED_MEDIUM = 60;
	public static final int SPEED_FAST = 80;
	public static final int SPEED_VERYFAST = 100;
	public static final int SPEED_TURBO = 120;
	
	//Enemy Characters
	public static final int CHARACTER_NOT_ASSIGNED = -1;
	public static final int CHARACTER_HOOKEY = 0; 
	public static final int CHARACTER_SPOOKEY = 1; 
	public static final int CHARACTER_ROOKEY = 2; 
	public static final int CHARACTER_LOKI = 3; 
	public static final int CHARACTER_BAZARGH = 4; 
	
	//Map
	public static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.ARGB_4444;
	public static final int GRASS = 0x00000000; 
	public static final int WAY = 0xFFFFFFFF; 
	public static final int OUTOFMAP = -1;
	public static final double COLLISION_TOLERANCE = 0.00001;
	
	//Player
	public static final double CALORIES_PER_METER = 0.08; // 100m = 8KCal
	public static final int MIN_WEIGHT = 40; // kg
	public static final int MIN_AGE = 5; // years
	public static final int MIN_HEIGHT = 90; // cm
	
	//Directions
	public static final int NORTH = 0;
	public static final int SOUTH = 1;
	public static final int WEST = 2;
	public static final int EAST = 3;
	public static final int TOLERANCE_DIRECTION = 0; //In Grad
	
	//URLs
	public static final String SERVER_HTTP_URL = "http://85.214.234.143:8013/";
	public static final String ADDRESS_GEO = "http://maps.googleapis.com/maps/api/geocode/";
	  
	//PHP Files on Server
	public static final String PHP_SUBMIT_MESSAGE_URL = "addChat.php";
	public static final String PHP_GET_MESSAGES_URL = "getChat.php";
	public static final String PHP_ADD_USER_URL = "addNewUserLoginData.php";
	public static final String PHP_GET_USER_URL = "verifyUserLoginData.php";
	public static final String PHP_USER_EXISTS_URL = "getUser.php";
	public static final String PHP_DELETE_USER_URL = "deleteUser.php";
	public static final String PHP_DELETE_USER_DATA_URL = "deleteUserData.php";
	public static final String PHP_UPDATE_USER_DATA_URL = "updateUserData.php";
	public static final String PHP_GET_USER_DATA_URL = "getUserData.php";
	public static final String PHP_UPLOAD_FILE_URL = "uploadMap.php";
	public static final String PHP_DOWNLOAD_FILE_URL = "downloadMap.php";
	public static final String PHP_GETALL_FILE_URL = "getAllMaps.php";
	public static final String PHP_MAPEXISTS_URL = "mapExists.php";
	public static final String PHP_MAPOWNER_URL = "isMapOwner.php";
	public static final String PHP_MAPDELETE_URL = "deleteMapFromServer.php";
	public static final String PHP_RATE_URL = "rate.php";
	public static final String PHP_DELETE_RATING_URL = "deleteRating.php";
	public static final String PHP_USER_RATING_URL = "getRating.php";
	public static final String PHP_USER_COMMENT_URL = "getMapComment.php";
	public static final String PHP_MAP_RATING_ALL_URL = "getMapRating.php";
	public static final String PHP_MAP_COMMENTS_ALL_URL = "getCommentsAll.php";
	public static final String PHP_MAP_DOWNLOADS_URL = "getDownloadsAll.php";
	public static final String PHP_GET_GLOBAL_SCORE_URL = "getGlobalScore.php";
	public static final String PHP_GET_PROFILE_PICTURE_URL = "getProfilePicture.php";
	public static final String PHP_ADD_PROFILE_PICTURE_URL = "addProfilePicture.php";
	public static final String PHP_LOG_GAMEDATA = "logGameData.php";
	public static final String PHP_GAMES_PLAYED = "gamesPlayed.php";
	
	//Security
	public static final String SALT = "THE$CAKE%IS(A)LIE!";
	public static final int RUN_ENCRYPTION = 23;
	
	//Augmented Reality
	public static final double NEAR = 1;
	public static final double FAR = 100;
	public static final double MAX_FAKTOR = 4;
	public static final float SIGHT_ANGLE_HORIZONTAL = 0.7f; 
	public static final float SWITCH_VIEW_THRESHOLD = 0.8f; 
	public static final float ENEMY_SIZE_MULTIPLAYER = 0.5f; 												
	public static final float CAMERA_ANGLE_FACTOR = 3.5f;
	public static final double PLAYER_HEIGHT = 1.8;
	
	//Intent Keys
	public static final String KEY_START_QUICKPLAY = "twostone_start_quickplay";
}
