package de.tu.darmstadt.uhg.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import de.tu.darmstadt.uhg.R;

public class ChatAdapter extends ArrayAdapter<String> {
	  private final Context context;
	  private final ArrayList<String> values;

	  public ChatAdapter(Context context, ArrayList<String> values) {
	    super(context, R.layout.highscore_list_item, values);
	    this.context = context;
	    this.values = values;
	  }

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.chat_list_text, parent, false);
	    TextView chatListTextData = (TextView) rowView.findViewById(R.id.chat_list_text_data);
	    TextView chatListTextName = (TextView) rowView.findViewById(R.id.chat_list_text_name);
	    TextView chatListTextMessage = (TextView) rowView.findViewById(R.id.chat_list_text_message);
	    chatListTextName.setText(values.get(position).split(" \\(")[0]);
	    if(values.get(position).split(", ").length > 1)
	    chatListTextData.setText((values.get(position).split(", ")[1].split("\\)")[0]));
//	    chatListTextMessage.setText(values.get(position).split("\n")[1]);
	    chatListTextMessage.setText(values.get(position));
	    	
	    

	    return rowView;
	  }

}
