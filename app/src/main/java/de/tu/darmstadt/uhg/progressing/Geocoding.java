package de.tu.darmstadt.uhg.progressing;
import android.util.Log;
import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.gps.GPSPoint;

public class Geocoding {
	    
	  /**Get Name of City*/
	  public static String getCityName(GPSPoint point) {
	
		 String ADDRESS2 = GlobalConstants.ADDRESS_GEO + "json?latlng=" + point.getLatitude() + "," + point.getLongitude() + "&sensor=true";
		 Log.d("UHG", "Fetching " + ADDRESS2);
		 
	     SQLQuery query = new SQLQuery();
		 if(!query.executeExternalSQL(ADDRESS2)) {
			 return "Unknown";
		 }
		 
		 String result = "";
		 result = query.querying();
		 
		 //Magic Stuff :)
		 String cutOff1 = result.substring(result.indexOf("long_name")+12);
		 String cutOff2 = cutOff1.substring(cutOff1.indexOf("long_name")+12);
		 String cutOff3 = cutOff2.substring(cutOff2.indexOf("long_name")+12);
		 String cutOff4 = cutOff3.substring(cutOff3.indexOf("\"")+1);
		 String cutOff5 = cutOff4.substring(0,cutOff4.indexOf("\""));
		 
		 return cutOff5;
	}
  
}
