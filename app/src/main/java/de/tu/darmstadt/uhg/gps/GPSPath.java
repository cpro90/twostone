package de.tu.darmstadt.uhg.gps;

import java.util.ArrayList;

/**Diese Klasse enth�lt Traversierungsmatrix einer Map
 * Ein GPSPath besteht aus GPSPathPoints (Knoten), die untereinander mit Kanten verbunden sind.
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
*/
public class GPSPath {

	/**Points enth�lt alle Weggeraden auf der Karte**/
	public ArrayList<GPSPathPoint> points;
	
	/**L�scht den Punkt an dem gegebenen Index */
	public void remove(int index) {
		if (points.size() > 0)
			points.remove(index);
	}
	
	
	/**Gibt den ersten GPSPathPoint zur�ck, auf den die Beschreibung passt**/
	public GPSPathPoint getByDescription(String input) {
		for (int i = 0; i < points.size(); i++) {
			if (points.get(i).getDescription() == input) return points.get(i);
		}
		return null;
	}
	
	/**
	 * Wie funktioniert diese Klasse/Methode?:
	 * Zuerst alle GPSPathPoints auf der Karte erstellen, diese haben alle jeweils eine GPS-Koordinate;
	 * Dann die Punkte verbinden, also leadsTo's einstellen, die comeFrom's werden automatisch miterzeugt;
	 * NUTZE die Methode: connect (point A, point B);
	 */
	public static void connect(GPSPathPoint a, GPSPathPoint b) {
		a.cameFromPoints.add(b);
		a.leadToPoints.add(b);
		b.cameFromPoints.add(a);
		b.leadToPoints.add(a);		
	}
	
	/**Wie connect(GPSPathPoint, GPSPathPoint), nur mit Descriptions**/
	public void connect(String descriptionA, String descriptionB) {
		GPSPathPoint a = getByDescription(descriptionA);
		GPSPathPoint b = getByDescription(descriptionB);
		a.cameFromPoints.add(b);
		a.leadToPoints.add(b);
		b.cameFromPoints.add(a);
		b.leadToPoints.add(a);		
	}

	/**Simple and fast connect between index-points using a one-way-flag**/
	public void connect(int a, int b, boolean oneWay) {
		points.get(a).leadToPoints.add(points.get(b));
		points.get(a).cameFromPoints.add(points.get(b));
		if (!oneWay) {
			points.get(b).cameFromPoints.add(points.get(a));
			points.get(b).leadToPoints.add(points.get(a));
		}
	}

	/**Simple and fast connect between index-points**/
	public void connect(int a, int b) {
		connect(a, b, false);
	}
	
	public int size () {
		return points.size();
	}
	

	public GPSPath() {
		this.points = new ArrayList<GPSPathPoint>();
	}

	public void addPoint(GPSPoint p) {
		points.add(new GPSPathPoint(p));
	}
	
	public void addPoint(GPSPoint p, String description, String startTag, String ghostIndex, String inactivePath) {
		if (!(ghostIndex.length() > 0)) ghostIndex = "-1";
		points.add(new GPSPathPoint(p, 
				description, 
				startTag.equalsIgnoreCase("true"), 
				Integer.parseInt(ghostIndex), 
				inactivePath.equalsIgnoreCase("true")));
	}

	public GPSPathPoint getPoint(int index) {
		return points.get(index);
	}
	
	public GPSPoint getPointPosition(int index) {
		return points.get(index).getPoint();
	}
	
}
