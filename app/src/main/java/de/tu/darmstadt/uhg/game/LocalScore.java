package de.tu.darmstadt.uhg.game;
import java.util.ArrayList;

public class LocalScore {
	ArrayList<String> mapName;
	ArrayList<Integer> mapScore;
	
	
	public ArrayList<String> getMapName() {
		return mapName;
	}

	public void setMapName(ArrayList<String> mapName) {
		this.mapName = mapName;
	}

	public ArrayList<Integer> getMapScore() {
		return mapScore;
	}

	public void setMapScore(ArrayList<Integer> mapScore) {
		this.mapScore = mapScore;
	}

	public LocalScore() {
		mapName = new ArrayList<String>();
		mapScore = new ArrayList<Integer>();
	}
	
	public int getScoreFromName(String name) {
		for (int i = 0; i < mapName.size(); i++) {
			if (mapName.get(i).equals(name)) {
				return mapScore.get(i);
			}
		}
		return 0;
	}
	
	
	public void setScore(String name, int score) {
		for (int i = 0; i < mapName.size(); i++) {
			if (mapName.get(i).equals(name)) {
				mapScore.set(i, score);
				return;
			}
		}
		mapName.add(name);
		mapScore.add(score);		
	}
	
	

}
