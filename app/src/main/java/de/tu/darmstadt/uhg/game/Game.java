package de.tu.darmstadt.uhg.game;

import java.util.ArrayList;
import java.util.Collections;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.FrameLayout;
import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.Level;
import de.tu.darmstadt.uhg.MainActivity;
import de.tu.darmstadt.uhg.Map;
import de.tu.darmstadt.uhg.Player;
import de.tu.darmstadt.uhg.R;
import de.tu.darmstadt.uhg.gps.GPSPathPoint;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.gps.GPSProgressing;

/**
 * Repr�sentiert ein Einzelspieler-PacStudent-Game. Mehrspieler erben von dieser
 * Klasse.
 * 
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
public class Game {

	boolean NEW_VIEW;
	Map map;
	Level level;
	Player player; // Player = PacMan
	ArrayList<Enemy> enemies;
	ArrayList<Stone> stones;
	int energyCountMultiplayerClient;
	Context context;
	ArrayList<Bitmap> imageEnemy;
	Bitmap imageEnergy;
	FrameLayout fmap;
	int points;
	int stoneCount;
	long timePlayed;
	String startDate;
	String endDate;
	private static double PLAYER_RADIUS = 0.000100;

	
	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public static int STONE_DISTANCE = 10;
	public static int MIN_STONE_DISTANCE = 10;

	public long getTimePlayed() {
		return timePlayed;
	}
	public void setTimePlayed(long l) {
		timePlayed = l;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	AugmentedReality augmentedReality; // TODO: AR erweiterung

	// TODO: BEGIN. Anpassung f�r Centralstation.
	int positionCounter = 0;
	ArrayList<GPSPoint> energyPositions = new ArrayList<GPSPoint>();
	// TODO: END.

	int difficulty;
	ArrayList<SimpleEnemy> simpleEnemies;
	public long pointsPerStone;

	// TODO: Dynamisch anpassen

	public void setAugmentedReality(AugmentedReality augment) { // TODO: AR
																// erweiterung
		augmentedReality = augment;
	}

	public void updateStoneDistance(int distance) {
		STONE_DISTANCE = (distance / 50);
		if (STONE_DISTANCE < MIN_STONE_DISTANCE)
			STONE_DISTANCE = MIN_STONE_DISTANCE;
	}

	public ArrayList<Stone> eatStones() {
		return stones;
	}

	public void setBread(ArrayList<Stone> bread) {
		this.stones = bread;
	}

	public ArrayList<Enemy> getEnemies() {
		return enemies;
	}

	public void setGhosts(ArrayList<Enemy> ghost) {
		this.enemies = ghost;
	}

	public int getBreadCountMultiplayerClient() {
		return energyCountMultiplayerClient;
	}

	public void setStoneCountMultiplayerClient(int stoneCountMultiplayerClient) {
		this.energyCountMultiplayerClient = stoneCountMultiplayerClient;
	}

	public int getStoneCount() {
		return stoneCount;
	}

	public void setStoneCount(int stoneCount) {
		this.stoneCount = stoneCount;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public ArrayList<Enemy> getEnemy() {
		return enemies;
	}

	public void setEnemy(ArrayList<Enemy> ghost) {
		this.enemies = ghost;
	}

	public long getPointsPerStone() {
		return pointsPerStone;
	}

	public void setPointsPerStone(long pointAmount) {
		this.pointsPerStone = pointAmount;
	}

	public Game(Map map, Player player, Context context,
			FrameLayout fmap, Level level) {
		NEW_VIEW = false;
		this.level = level;
		this.map = map;
		this.player = player;
		this.context = context;
		this.fmap = fmap;
		this.points = 0;
		this.simpleEnemies = level.getGhosts();
		this.difficulty = getDifficulty(simpleEnemies);
		this.pointsPerStone = difficulty; // Standard!

		updateStoneDistance(map.getMaxGhosts());
		generateEnemies();
		generateStonesOnPaths();
	}

	public Game(Map map, Player player, Context context, Level level) {
		NEW_VIEW = true;
		this.map = map;
		this.context = context;
		this.player = player;
		this.level = level;
		this.simpleEnemies = level.getGhosts();
		this.difficulty = getDifficulty(simpleEnemies);

		updateStoneDistance(map.getMaxGhosts());
		generateEnemies();
		generateStonesOnPaths();
	}

	public Game(Map map, Player player,
			ArrayList<Bitmap> imageGhost, Bitmap imageBread, Context context,
			FrameLayout fmap, Level level, AugmentedReality augment) {

		this.level = level;
		this.map = map;
		this.player = player;
		this.imageEnemy = imageGhost;
		this.imageEnergy = imageBread;
		this.context = context;
		this.fmap = fmap;
		this.points = 0;
		this.simpleEnemies = level.getGhosts();
		this.difficulty = getDifficulty(simpleEnemies);
		this.pointsPerStone = difficulty; // Standard!
		this.augmentedReality = augment;

		updateStoneDistance(map.getMaxGhosts());
		generateEnemies();
		generateStonesOnPaths();

	}

	/** Berechnet den Schwierigkeitsgrad des Levels **/
	public static int getDifficulty(ArrayList<SimpleEnemy> ghosts) {
		int temp = 0;
		for (int i = 0; i < ghosts.size(); i++) {
			temp += ghosts.get(i).getIntelligence() + 1;
			temp += ghosts.get(i).getSpeed() / 25;
		}
		return temp;
	}

	/** Gibt den errechneten Schwierigkeitsgrad zur�ck **/
	public int getDifficulty() {
		return this.difficulty;
	}

	/** Gibt zur�ck, wieviel Brot noch auf den Wegen liegt **/
	public int getRemainingStoneCount() {
		return stones.size();
	}

	/** Gibt zur�ck, wieviel Brot urspr�nglich vorhanden war **/
	public int getAllStonesCount() {
		return stoneCount;
	}

	public void updateAllEnemies() {
		for (int i = 0; i < simpleEnemies.size(); i++) {
			
			int doing = 1;
			if (enemies.get(i).getSpeed() == GlobalConstants.SPEED_SLOW)
				doing = 1;
			if (enemies.get(i).getSpeed() == GlobalConstants.SPEED_WALKSPEED)
				doing = 2;
			if (enemies.get(i).getSpeed() == GlobalConstants.SPEED_MEDIUM)
				doing = 2;
			if (enemies.get(i).getSpeed() == GlobalConstants.SPEED_FAST)
				doing = 3;
			
			for (int x = 0; x < doing; x++)
				enemies.get(i).walk();
			
		
			enemies.get(i).draw();
		}
	}

	public void animateAllEnemies() {
		for (int i = 0; i < simpleEnemies.size(); i++)
			enemies.get(i).animate();
	}

	/**
	 * Gibt den PathPoint zur�ck, auf dem der Geist mit der index-Nummer gesetzt
	 * werden soll
	 */
	private int getPathIndexByEnemyIndex(int ghostIndex) {
		for (int i = 0; i < map.getPath().size(); i++)
			if (map.getPath().getPoint(i).getGhostIndex() == ghostIndex)
				return i;
		return -1;
	}

	/** Erzeugt die Geister nach �bergebener Spezifikation **/
	private void generateEnemies() {
		this.enemies = new ArrayList<Enemy>();

		for (int i = 0; i < simpleEnemies.size(); i++) {
			if (i > map.getMaxGhosts() - 1)
				return;
			int rnd = (int) (Math.random() * map.getPath().size());

			// ENEMY POSITION != PLAYERS POSITION

			GPSPathPoint pPoint = map.getPath().getPoint(rnd);

			if (pPoint.getPoint().equalsWithTolerance(
					MainActivity.currentPosition,
					GlobalConstants.ENEMY_SPAWN_SECURE_CIRCLE)) {
				for (int tries = 1; tries < GlobalConstants.MAX_TRIES; tries++) {
					rnd = (int) (Math.random() * map.getPath().size());
					Log.d("UHG", "Enemy is within Players tolerance... Try "
							+ tries);
					pPoint = map.getPath().getPoint(rnd);
					if (!pPoint.getPoint().equalsWithTolerance(
							MainActivity.currentPosition,
							GlobalConstants.ENEMY_SPAWN_SECURE_CIRCLE))
						break;
				}
			}

			int index = getPathIndexByEnemyIndex(i + 1);
			if (index != -1)
				rnd = index;

			// Falls keine LeadToPoints existieren, auf Punkt stehen bleiben
			if (map.getPath().getPoint(rnd).getLeadToPoints().size() < 1) {
				map.getPath().getPoint(rnd).getLeadToPoints()
						.add(map.getPath().getPoint(rnd));
			}

			enemies.add(new Enemy(map, simpleEnemies.get(i).getIntelligence(),
					simpleEnemies.get(i).getSpeed() + i, // +i damit gleich
															// schnelle Geister
					simpleEnemies.get(i).getCharacter(), // nie dauerhaft
															// �berlappen!
					imageEnemy, pPoint, // zum
					map.getPath().getPoint(rnd).getLeadToPoints().get(0), // ersten
																			// ziel
																			// in
																			// der
																			// liste
																			// laufen
																			// TODO:
																			// zufall?!
					player, augmentedReality));
		}

	}

	private void addPoints(long amount) {
		points += amount;
	}

	/**
	 * Entfernt das Brot an der angegebenen Stelle
	 * 
	 * Returns true if bread is all eaten
	 **/
	public boolean eatStoneAt(GPSPoint position) {
		return eatStoneAt(position, true);
	}

	/**
	 * Entfernt das Brot an der angegebenen Stelle,
	 * 
	 * /**Wie eatBreadAt, nur mit Index, falls vorher schon gesucht wurde
	 **/
	public boolean eatStoneAt(GPSPoint position, boolean active, int pos) {
		stones.remove(pos);
		addPoints(pointsPerStone);
		MainActivity.sound = MediaPlayer.create(context, R.raw.eat);
		MainActivity.sound.start();

		return (stones.size() == 0);
	}

	/**
	 * Entfernt das Brot an der angegebenen Stelle,
	 * 
	 * @position die GPS Position
	 * @active aktives einsammeln oder tat des mitspielers Returns true if bread
	 *         is all eaten
	 **/

	public boolean eatStoneAt(GPSPoint position, boolean active) {
		for (int i = 0; i < stones.size(); i++) {
			if (stones.get(i).getPosition()
					.equalsWithTolerance(position, PLAYER_RADIUS)) {
				// TODO: BEGIN. Anpassung f�r Centralstation. Evtl sp�ter
				// ausarbeiten
				if (level.getMapName().equals("Centralstation")) {
					addSingleStone(energyPositions.get(positionCounter));
					positionCounter = (positionCounter + 1)
							% energyPositions.size();
				}
				// TODO: END
				stones.remove(i);
				addPoints(pointsPerStone);

				// Sound
				MainActivity.sound = MediaPlayer.create(context, R.raw.eat);
				MainActivity.sound.start();

				return (stones.size() == 0);

			}
		}
		return false;
	}

	/** Gets the indicator of the bread **/
	public int getStoneAt(GPSPoint position) {
		for (int i = 0; i < stones.size(); i++) {
			if (stones.get(i).getPosition()
					.equalsWithTolerance(position, PLAYER_RADIUS)) {
				return i;
			}
		}
		return -1;
	}

	/** Gibt true zur�ck, wenn ein Geist einen Spieler ber�hrt **/
	public boolean didEnemyDevoursPlayer(GPSPoint playerPos) {
		for (int i = 0; i < enemies.size(); i++) {
			// TODO: BEGIN. Anpassung f�r Centralstation.
			if (level.getMapName().equals("Centralstation")) {
				if (enemies.get(i).getPosition()
						.equalsWithTolerance(playerPos, PLAYER_RADIUS)) {
					return true;
				}
			} else
			// TODO: END
			if (enemies.get(i).getPosition()
					.equalsWithTolerance(playerPos, PLAYER_RADIUS / 2)) {
				return true;
			}
		}
		return false;
	}

	/** Gibt true zur�ck, wenn sich ein Geist im Radius 3*Player befindet **/
	public boolean enemyIsInNearOfPlayer(GPSPoint playerPos) {
		for (int i = 0; i < enemies.size(); i++) {
			// TODO: BEGIN. Anpassung f�r Centralstation.
			if (level.getMapName().equals("Centralstation")) {
				if (enemies.get(i).getPosition()
						.equalsWithTolerance(playerPos, PLAYER_RADIUS * 2)) {
					return true;
				}
			} else
			// TODO: END
			if (enemies.get(i).getPosition()
					.equalsWithTolerance(playerPos, PLAYER_RADIUS * 3)) {
				return true;
			}
		}
		return false;
	}

	/** Adds an single bread to map-board on defined position **/
	public void addSingleStone(GPSPoint position) {
		stones.add(new Stone(position, imageEnergy, map, augmentedReality));
		stoneCount += 1;
	}

	public void generateStonesOnPaths() {

		this.stones = new ArrayList<Stone>();

		ArrayList<GPSPathPoint> usedPoints = new ArrayList<GPSPathPoint>();

		for (int i = 0; i < map.getPath().size(); i++) {

			if (!map.getPath().getPoint(i).isInactivePath()) {
				GPSPoint pos = (map.getPath().getPoint(i).getPoint()).copy();
				usedPoints.add(map.getPath().getPoint(i));

				stones.add(new Stone(pos.copy(), imageEnergy, map,
						augmentedReality));

				int wayCount = map.getPath().getPoint(i).getLeadToPoints()
						.size();
				for (int k = 0; k < wayCount; k++) {

					if (!usedPoints.contains(map.getPath().getPoint(i)
							.getLeadToPoints().get(k).getPoint())) {

						GPSPathPoint cur = map.getPath().getPoint(i)
								.getLeadToPoints().get(k);

						if (!usedPoints.contains(cur)) {

							GPSPoint start = pos.copy();
							GPSPoint target = map.getPath().getPoint(i)
									.getLeadToPoints().get(k).getPoint();
							GPSPoint vector = target.copy();
							vector.substract(start);
							double diff = GPSProgressing.getWayDifference(
									start, target) / STONE_DISTANCE; // Distanz

							vector.scaleIt(1 / diff);

							for (int j = 1; j < diff; j++) {
								start.add(vector);
								stones.add(new Stone(start.copy(), imageEnergy,
										map, augmentedReality));
							}

						}
					}
				}

			}
		}
		stoneCount = stones.size();
	}

	/** Malt alle Brote auf die Karte, die noch nicht gefressen wurden **/
	public void drawAllRemainingBread() {
		sortBreadDescendingDistance();
		for (int i = 0; i < stones.size(); i++) {
			stones.get(i).draw();
		}
	}

	public void sortBreadDescendingDistance() {
		for (int i = 0; i < stones.size(); i++) {
			double dLat = (stones.get(i).getPosition().getLatitude() - player
					.getPosition().getLatitude())
					* GlobalConstants.CONST_LATITUDE;
			double dLong = (stones.get(i).getPosition().getLongitude() - player
					.getPosition().getLongitude())
					* GlobalConstants.CONST_LONGITUDE;
			stones.get(i).setDistance(Math.sqrt(dLat * dLat + dLong * dLong));
		}
		Collections.sort(stones);
	}

	/**
	 * Returns true, if player is on entrance private boolean
	 * checkIfPlayerIsOnEntrance () { return
	 * (GPSProgressing.checkPositionTolerance(player.getPosition(),
	 * startPosition)); }
	 **/
	public static int encodeSpeed(int speedCount) {
		if (speedCount == 1)
			return GlobalConstants.SPEED_SNAIL;
		if (speedCount == 2)
			return GlobalConstants.SPEED_SLOW;
		if (speedCount == 3)
			return GlobalConstants.SPEED_WALKSPEED;
		if (speedCount == 4)
			return GlobalConstants.SPEED_MEDIUM;
		if (speedCount == 5)
			return GlobalConstants.SPEED_FAST;
		return 0;
	}

	/**
	 * private Exception invalidMapException() { return new Exception(
	 * "Kartenfehler! Sie befinden sich nicht im Prinz-Georgs-Garten!"); }
	 * 
	 * private Exception invalidPlayerPosition() { return new Exception(
	 * "Player befindet sich nicht am Eingang des Prinz-Georg-Gartens!"); }
	 **/
	public static int encodeIntelligence(int intelligenceCount) {
		if (intelligenceCount == 1)
			return GlobalConstants.ENEMY_FOOL;
		if (intelligenceCount == 2)
			return GlobalConstants.ENEMY_EASY;
		if (intelligenceCount == 3)
			return GlobalConstants.ENEMY_NORMAL;
		if (intelligenceCount == 4)
			return GlobalConstants.ENEMY_GOOD;
		if (intelligenceCount == 5)
			return GlobalConstants.ENEMY_EXTREME;
		return 0;
	}


}