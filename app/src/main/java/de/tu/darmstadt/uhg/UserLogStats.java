package de.tu.darmstadt.uhg;

import de.tu.darmstadt.uhg.game.Game;
import de.tu.darmstadt.uhg.progressing.SQLQuery;
import de.tu.darmstadt.uhg.progressing.UserData;

public class UserLogStats {

	final public int GENDER_MALE = 0;
	final public int GENDER_FEMALE = 1;
	final public int GENDER_OTHER = 2;
	
	final public int GAMELOG_START = 0;
	final public int GAMELOG_QUIT = 1;
		
	String username;
	
	double distance;
	double playtime;
	String startDate;
	String endDate;
	int playedGames;
	
	private void loadUserLogStats() {
			
	}
		
	public void saveUserLogStats() {
		
	}
	
	public void init(UserData game) {
		this.username = game.getUsername();
		loadUserLogStats();
	}

	private long temp_playtime;
	private double temp_distance;
	public void logStartedGame (UserData game) {
		temp_playtime = game.getPlaytime();
		temp_distance = game.getDistance();
	}
	
	public void logQuitGame (UserData user, Game game) {
		String currentMap = game.getMap().getName();
		playedGames++;
		playtime = user.getPlaytime();
		startDate = game.getStartDate();
		endDate = game.getEndDate();
		distance = user.getDistance();
		
	  	SQLQuery query = new SQLQuery();
	   	query.addPair("username", username);
	   	query.addPair("map", currentMap);
	   	query.addPair("playtime", (playtime - temp_playtime) + "");
	   	query.addPair("distance", (int) Math.round((distance - temp_distance)) + "");
	   	query.addPair("startdate", startDate);
	   	query.addPair("enddate", endDate);
 	    query.executeSQL(GlobalConstants.PHP_GAMES_PLAYED);
	   	new AsyncLogGameData(query);
	}

}
