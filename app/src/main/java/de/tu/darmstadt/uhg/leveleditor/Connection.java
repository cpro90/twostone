package de.tu.darmstadt.uhg.leveleditor;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import de.tu.darmstadt.uhg.gps.GPSPath;
import de.tu.darmstadt.uhg.gps.GPSPathPoint;

/**
 * Klasse, die eine Verbindung von Markern darstellt.
 * Diese Klasse ist Bestandteil der Bachelorarbeit 
 * "Erzeugen von Spielfeldern f�r Standortbasierte Bewegungsspiele"
 * @author Chris Michel
 */
public class Connection {
	
	
	ArrayList<Connection> connectsTo;
	LatLng pos;
	MarkerOptions markerOptions;
	
	/**Transformiert einen GPSPath in eine ArrayList von Connections**/
	public static ArrayList<Connection> GPSPathToConnectionArray(GPSPath path) {
		ArrayList<Connection> ret = new ArrayList<Connection>();
		for (int i = 0; i < path.size(); i++) {
			GPSPathPoint p = path.getPoint(i);
			Connection c = new Connection(p.getPoint().toLatLng(), null);
			ret.add(c);
			for (int j = 0; j < p.getLeadToPoints().size(); j++) {
				Connection c2 = new Connection(p.getLeadToPoints().get(j).getPoint().toLatLng(), null);
				c.addConnection(c2);
			}
		}
		return ret;
	}

	public MarkerOptions getMarkerOptions() {
		return markerOptions;
	}
	public void setMarkerOptions(MarkerOptions markerOptions) {
		this.markerOptions = markerOptions;
	}
	/**Gibt alle Verbindungen zur�ck**/
	public ArrayList<Connection> getConnectsTo() {
		return connectsTo;
	}
	public void setConnectsTo(ArrayList<Connection> connectsTo) {
		this.connectsTo = connectsTo;
	}
	public LatLng getPos() {
		return pos;
	}
	public void setPos(LatLng pos) {
		this.pos = pos;
	}
	public Connection(LatLng pos, MarkerOptions markerOptions) {
		super();
		this.connectsTo = new ArrayList<Connection>();
		this.markerOptions = markerOptions;
		this.pos = pos;
	}

	/**F�gt eine neue Verbindung hinzu**/
	public void addConnection(Connection c) {
		connectsTo.add(c);
		if ((c != null) && (c.connectsTo != null))
			c.connectsTo.add(this);
	}	

	/**Entfernt eine Verbindung, falls vorhadnen**/
	public void removeConnection(Connection c) {
		if (c.connectsTo != null)
			c.connectsTo.remove(this);
		connectsTo.remove(c);
	}

	/**@returns true wenn eine Verbindung existiert**/
	public boolean hasConnectionTo(Connection c) {
		return connectsTo.contains(c);
	}
	
	/**Gibt die Anzahl der Verbindungen zur�ck**/
	public int connectionCount() {
		return connectsTo.size();
	}
	

}
