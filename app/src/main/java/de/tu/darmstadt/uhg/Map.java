package de.tu.darmstadt.uhg;

import java.util.ArrayList;

import de.tu.darmstadt.uhg.gps.GPSLocation;
import de.tu.darmstadt.uhg.gps.GPSPath;
import de.tu.darmstadt.uhg.gps.GPSPathPoint;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.widget.ImageView;

/**
 * Diese Klasse repr�sentiert eine Karte. Die Karte besteht aus einer
 * GPSLocation (mehr dazu, siehe Beschreibung in der Datei
 * de.tu.darmstadt.uhg.GPSLocation.java). Zur Location, die den Rand der Map
 * bestimmt wird hier noch eine Bitmap hinzugef�gt, die die Karte, wie sie auf
 * dem Bildschirm angezeigt wird, beinhaltet.
 * 
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 * 
 */
public class Map {

	GPSLocation location;
	Bitmap bitmap;
	Bitmap originalBitmap;
	Bitmap foreground;
	GPSPath path;
	Canvas canvas;
	int width;
	int height;
	int originalWidth;
	int originalHeight;
	int zoomLevel;
	int maxEnemies;
	long distance;
	int maxTime;

	public Map(GPSLocation location, Bitmap bitmap, Bitmap foreground) {
		this.location = location;
		this.bitmap = bitmap;
		this.originalBitmap = bitmap.copy(GlobalConstants.BITMAP_CONFIG, true);
		this.foreground = foreground;
		canvas = new Canvas(foreground);
		canvas.save();
		this.originalWidth = 0;
		this.originalHeight = 0;
		this.width = 0;
		this.height = 0;
		this.maxEnemies = GlobalConstants.MAX_ENEMY_COUNT;
		this.zoomLevel = GlobalConstants.ZOOM_NEAR; // Standard Zoom Level
	}
	
	public Map(GPSLocation location) {
		this.location = location;
		this.originalWidth = 0;
		this.originalHeight = 0;
		this.width = 0;
		this.height = 0;
		this.maxEnemies = GlobalConstants.MAX_ENEMY_COUNT;
		this.zoomLevel = GlobalConstants.ZOOM_NEAR; // Standard Zoom Level
	}

	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}
	
	/** Gibt die maximale Anzahl der Geister der Karte zur�ck **/
	public int getMaxGhosts() {
		return maxEnemies;
	}

	/** Setzt die maximale Anzahl an Geistern auf der Karte **/
	public void setMaxGhosts(int maxGhosts) {
		this.maxEnemies = maxGhosts;
	}

	/** Gibt die Zeit zur�ck, die maximal f�r das L�sen der Karte verf�gbar ist **/
	public int getMaxTime() {
		return maxTime;
	}

	/** Setzt die Zeit, die f�r das L�sen der Karte verf�gbar ist **/
	public void setMaxTime(int maxTime) {
		this.maxTime = maxTime;
	}

	/** Zoomt in die Karte rein/raus **/
	public void zoomMap(int zoomLevel, ImageView view) {
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(originalBitmap,
				getOriginalWidth() / zoomLevel,
				getOriginalHeight() / zoomLevel, false);
		setBitmap(scaledBitmap);
		setZoomLevel(zoomLevel);
		setWidth(getOriginalWidth() / zoomLevel);
		setHeight(getOriginalHeight() / zoomLevel);
		view.setImageBitmap(scaledBitmap);

	}

	/**
	 * Erstellt aus den Pfaden eine Custom Map mit gr�nem Hintergrund und wei�en
	 * Wegen, somit kann das eigene Leveldesign grafisch abstrakt dargestellt
	 * werden.
	 */
	public void designCustomBitmap() {

		Bitmap temp = bitmap.copy(GlobalConstants.BITMAP_CONFIG, true);
		Canvas canv = new Canvas(temp);

		Paint pathPainting = new Paint();
		pathPainting.setColor(Color.WHITE); // Wei�e Pfade
		pathPainting.setStyle(Style.FILL_AND_STROKE);
		pathPainting.setStrokeWidth(20);

		/**
		 * Paint backgroundPainting = new Paint();
		 * backgroundPainting.setColor(0x12FF20); //Gr�ner Hintergrund
		 * backgroundPainting.setStyle(Style.FILL_AND_STROKE); Rect rect=new
		 * Rect(0,0,originalWidth,originalHeight); canv.drawRect(rect,
		 * backgroundPainting);
		 **/

		// Alle Verbinden!
		for (int i = 0; i < path.size(); i++) {
			ArrayList<GPSPathPoint> p = path.getPoint(i).getLeadToPoints();
			for (int j = 0; j < p.size(); j++) {
				int startX = GPSPointToPixelPoint(path.getPoint(i).getPoint()).x;
				int startY = GPSPointToPixelPoint(path.getPoint(i).getPoint()).y;
				int stopX = GPSPointToPixelPoint(p.get(j).getPoint()).x;
				int stopY = GPSPointToPixelPoint(p.get(j).getPoint()).y;
				canv.drawLine(startX, startY, stopX, stopY, pathPainting);
			}
		}

		bitmap = temp.copy(GlobalConstants.BITMAP_CONFIG, true);
		this.originalBitmap = temp;

	}

	/**
	 * Setzt das Image auf die Position des Players, sozusagen abgleich mit dem
	 * Pfeil in der Mitte
	 **/
	public void updateImage(ImageView img, Player player, Point displayCenter) {
		Point p = GPSPointToPixelPoint(player.getPosition());
		img.scrollTo(-displayCenter.x + p.x, -displayCenter.y + p.y);
	}

	/**Returns the zoom level**/
	public int getZoomLevel() {
		return zoomLevel;
	}

	/**Sets the zoom level**/
	public void setZoomLevel(int zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

	/**Returns the original width of the bitmap**/
	public int getOriginalWidth() {
		return originalWidth;
	}

	/**Sets the original width of the bitmap**/
	public void setOriginalWidth(int originalWidth) {
		this.originalWidth = originalWidth;
	}

	/**Returns the original height of the bitmap**/
	public int getOriginalHeight() {
		return originalHeight;
	}

	/**Sets the original height of the bitmap**/
	public void setOriginalHeight(int originalHeight) {
		this.originalHeight = originalHeight;
	}

	/**Returns the GPS Path of the map**/
	public GPSPath getPath() {
		return path;
	}

	/**Sets the GPS Path of the map**/
	public void setPath(GPSPath path) {
		this.path = path;
	}

	/**Returns the current width of the bitmap**/
	public int getWidth() {
		return width;
	}

	/**Sets the current width of the bitmap**/
	public void setWidth(int width) {
		this.width = width;
	}

	/**Returns the current height of the bitmap**/
	public int getHeight() {
		return height;
	}

	/**Sets the current height of the bitmap**/
	public void setHeight(int height) {
		this.height = height;
	}

	/** Berechnet aus den GPSPunkten die Pixel der Position auf der Karte **/
	public Point GPSPointToPixelPoint(GPSPoint point) {
		// GPS-PUNKT ZU PIXEL UMRECHNEN
		double lat = this.location.getTopRightCoordinate().getLatitude()
				- point.getLatitude();
		double lon = this.location.getTopRightCoordinate().getLongitude()
				- point.getLongitude();

		// Wir haben jetzt die Top-Right-Koordinaten, wir ben�tigen aber
		// Top-Left f�r weitere Verfahren, also:
		lon = this.location.getFieldSize().getLongitude() - lon;

		// In Relation zur Fieldsize
		double y = lat / this.location.getFieldSize().getLatitude();
		double x = lon / this.location.getFieldSize().getLongitude();

		// Auf die Map gerechnet
		int mapX = (int) Math.round(x * this.getWidth());
		int mapY = (int) Math.round(y * this.getHeight());

		return new Point(mapX, mapY);
	}

	/** Berechnet aus den Pixeln die GPSPosition der Position auf der Karte **/
	public GPSPoint PixelPointToGPSPoint(Point point) {

		double distX = this.location.getTopRightCoordinate().getLatitude()
				- this.location.getBottomLeftCoordinate().getLatitude();
		double distY = this.location.getTopRightCoordinate().getLongitude()
				- this.location.getBottomLeftCoordinate().getLongitude();

		double posX = (double) point.x / this.getWidth();
		double posY = (double) point.y / this.getHeight();

		double absX = distX * posX
				+ this.location.getBottomLeftCoordinate().getLatitude();
		double absY = distY * posY
				+ this.location.getBottomLeftCoordinate().getLongitude();

		return new GPSPoint(absX, absY);
	}

	/** Draws an image on the specified position on the Map **/
	public void drawOver(Bitmap overlay, Point pos) {
		// Overlay zuerst dem Zoomfaktor anpassen
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(overlay,
				overlay.getWidth() / zoomLevel,
				overlay.getHeight() / zoomLevel, false);
		// Und dann auf die Karte zeichnen
		canvas.drawBitmap(scaledBitmap, pos.x - (scaledBitmap.getWidth() / 2),
				pos.y - (scaledBitmap.getHeight() / 2), new Paint(
						Paint.FILTER_BITMAP_FLAG));
	}

	/** Resets the maps drawing to default **/
	public void redraw() {
		Paint paint = new Paint();
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		Rect rect = new Rect(0, 0, originalWidth, originalHeight);
		canvas.drawRect(rect, paint);
	}

	/** Returns true, when the point is on the Map **/
	public boolean isOnMap(GPSPoint point) {

		Point p = GPSPointToPixelPoint(point);
		return ((p.x >= 0) && (p.y >= 0) && (p.x <= this.getWidth()) && (p.y <= this
				.getHeight()));

	}


	/**Returns the bottom left coordinate of the location**/
	public GPSPoint getFieldBottomLeftCoordinate() {
		return location.getBottomLeftCoordinate();
	}

	/**Sets the bottom left coordinate of the location**/
	public void setFieldBottomLeftCoordinate(GPSPoint fieldTopLeftCoordinate) {
		this.location.setBottomLeftCoordinate(fieldTopLeftCoordinate);
	}

	/**Returns the top right coordinate of the location**/
	public GPSPoint getFieldTopRightCoordinate() {
		return location.getTopRightCoordinate();
	}

	/**Sets the top right coordinate of the location**/
	public void setFieldTopRightCoordinate(GPSPoint fieldTopLeftCoordinate) {
		this.location.setTopRightCoordinate(fieldTopLeftCoordinate);
	}

	/**Returns the name of the location**/
	public String getName() {
		return location.getLocationName();
	}

	/**Sets the name of the location**/
	public void setName(String name) {
		this.location.setLocationName(name);
	}

	/**Returns the location**/
	public GPSLocation getLocation() {
		return location;
	}

	/**Sets the location**/
	public void setLocation(GPSLocation location) {
		this.location = location;
	}

	/**Returns the Bitmap**/
	public Bitmap getBitmap() {
		return bitmap;
	}

	/**Sets the Bitmap**/
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

}
