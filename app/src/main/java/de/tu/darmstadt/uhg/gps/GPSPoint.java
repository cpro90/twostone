package de.tu.darmstadt.uhg.gps;
import java.util.Scanner;

import com.google.android.gms.maps.model.LatLng;

import de.tu.darmstadt.uhg.GlobalConstants;


/**
<<<<<<< .mine
 * Diese Klasse repr�sentiert eine GPS Koordinate. Sie enth�lt den L�ngengrad (Longitude) und den
 * Breitengrad (Latitude). Zus�tzlich wird die Zeit aufgezeichnet, zu der der Punkt erstellt wurde,
 * damit sp�ter unter anderem die Geschwindigkeit des Spielers / der Spielerin, sowie Kalorien, etc.
 * berechnet werden k�nnen.
=======
 * Diese Klasse repr�sentiert eine GPS Koordinate. Sie enth�lt den L�ngengrad (Latitude) und den
 * Breitengrad (Longitude). Zus�tzlich wird die Zeit aufgezeichnet, zu der der Punkt erstellt wurde,
 * damit sp�ter unter anderem die Geschwindigkeit des Spielers / der Spielerin, sowie Kalorien, etc.
 * berechnet werden k�nnen.
>>>>>>> .r332
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
public class GPSPoint {
	
	double latitude;
	double longitude;
	double recordedTime;

	/**Returns true, if GPS Point is 0|0**/
	public boolean isIdentity() {
		return ((latitude == 0) && (longitude == 0));
	}
	public GPSPoint(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.recordedTime = (double) System.currentTimeMillis();
	}

	public GPSPoint(double latitude, double longitude, double recordedTime) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.recordedTime = recordedTime;
	}

	public GPSPoint(String string) {
		Scanner scan = new Scanner(string);
		scan.useDelimiter(",");
		this.latitude = Double.parseDouble(scan.next());
		this.longitude = Double.parseDouble(scan.next());
		scan.close();
	}

	public GPSPoint(GPSPoint position) {
		this.latitude = position.latitude;
		this.longitude = position.longitude;
		this.recordedTime = (double) System.currentTimeMillis();
	}
	
	public GPSPoint(LatLng position) {
		this.latitude = position.latitude;
		this.longitude = position.longitude;
		this.recordedTime = (double) System.currentTimeMillis();
	}
	
	public LatLng toLatLng() {
		return new LatLng(this.latitude, this.longitude);
	}
	
	/**Gibt den GPSPunkt als String zur�ck: x.xxxxx,y.yyyyy**/
	public String decode() {
		return (this.latitude + "," + this.longitude);
	}
	
	/**Adds two GPSPoints**/
	public void add (GPSPoint b) {
		this.latitude += b.getLatitude();
		this.longitude += b.getLongitude();
	}
	

	/**Substract two GPSPoints**/
	public void substract (GPSPoint b) {
		this.latitude -= b.getLatitude();
		this.longitude -= b.getLongitude();
	}
	
	/**Kopiert den GPS Punkt**/
	public GPSPoint copy () {
		return new GPSPoint(latitude, longitude, recordedTime);
	}
	
	/**Scales a GPSPoint**/
	public void scaleIt (double scale) {
		this.latitude *= scale;
		this.longitude *= scale;
	}
	
	/**Takes a GPSPoint and returns a new scaled one**/
	public GPSPoint scale (double step) {
		return new GPSPoint((this.latitude/step), (this.longitude/step));
	}

	/**Returns true when 2 points are equal**/
	public boolean equals (GPSPoint b) {
		return ((b.getLatitude() == latitude) && (b.getLongitude() == longitude));
	}

	/**Returns the absolute value of the vector GPSPoint**/
	public double getAbsolute () {
		return (Math.sqrt(latitude*latitude+longitude*longitude));
	}

	/**Returns true when 2 points are within a different tolerance**/
	public boolean equalsWithTolerance (GPSPoint b, double tolerance) {
		return ((Math.abs(b.getLatitude() - latitude) <= tolerance) && 
				(Math.abs(b.getLongitude() - longitude) <= tolerance));
	}
	
	/**Returns true, when two points are going to collide**/
	public boolean isWithPoint (GPSPoint b) {
		return ((Math.abs(b.getLatitude()-latitude) < GlobalConstants.COLLISION_TOLERANCE) &&
			(Math.abs(b.getLongitude()-longitude) < GlobalConstants.COLLISION_TOLERANCE)); 
	}
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getRecordedTime() {
		return recordedTime;
	}
	public void setRecordedTime(double recordedTime) {
		this.recordedTime = recordedTime;
	}
}
