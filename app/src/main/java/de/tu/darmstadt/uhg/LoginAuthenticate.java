package de.tu.darmstadt.uhg;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.tu.darmstadt.uhg.progressing.SQLQuery;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import java.security.*;

/**
 * Diese Klasse ist f�r einen sicheren und korrekten Loginablauf zust�ndig.
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
public class LoginAuthenticate {

	public static long regDate = 0;
		
	public static String encrypt(String password) {
		for (int i = 0; i < GlobalConstants.RUN_ENCRYPTION; i++)
			password = hash(password);
		return password;
	}
	
	/**Creates an SHA512-hash of an Input-String**/
	private static String hash(String password){
		password += GlobalConstants.SALT;
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("SHA-512");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();
      
            StringBuffer Hash = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
            {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                Hash.append(h);
            }
                  
            return Hash + "";
             
            } 
            catch (NoSuchAlgorithmException e) 
            {
            	e.printStackTrace();
            	return password;
            }
        
         
    }
	
	
	 /**Fragt Passwort und Nutzernamen-Kombination ab.
	  * Returns true when Passwort und Username mit den Daten des Servers �bereinstimmen
	  * 
	  * @returns true/false **/
   public static boolean authenticate(Context context, String username, String password, boolean hash) {
   	
   	if (hash) 
   		password = encrypt(password);
    	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
   	 StrictMode.setThreadPolicy(policy); 
   	
   	 SQLQuery query = new SQLQuery();
   	 query.addPair("username", username);
   	 query.addPair("password", password);
   	 
   	 String result = "";
		 JSONObject json_data;
		 
		 if (!query.executeSQL(GlobalConstants.PHP_GET_USER_URL)) return false;
		 
		 try {
			 	 BufferedReader reader = new BufferedReader(new InputStreamReader(query.getReceivedStream(),"iso-8859-1"),8);
			 	 StringBuilder sb = new StringBuilder();
			 	 String line = null;
			 	 while ((line = reader.readLine()) != null) {
			 		 sb.append(line + "n");
			 	 } 
			 	 query.getReceivedStream().close();
			 	 result=sb.toString();
			 	 }	catch(Exception e)	{
			 		 Log.e("log_tag", "Error converting result "+e.toString());
			 		 return false;
			 	 }
		 try {
			 	 JSONArray jArray = new JSONArray(result);
			 	 for(int i=0;i<jArray.length();i++){
				 	 json_data = jArray.getJSONObject(i);
				 	 result = (String) json_data.get("username");
				 	 Log.d("UHG", "TEST");
				 	 regDate = Long.parseLong((String) json_data.get("registerDate"));
				 	 Log.d("UHG", "regdate: " + regDate);
			 	 } 
			 }
		     catch(JSONException e){
			 		 Log.e("log_tag", "Error parsing data "+e.toString());
		 }

		 UserSettings.hashedPW = password;
		 return (result.equals(username));
	}
	 /** True, wenn Nutzer bereits existiert, sonst false
	  *  @returns true/false **/
  public static boolean userExists(Context context, String username) {
  	
   	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
  	 StrictMode.setThreadPolicy(policy); 
  	
  	 SQLQuery query = new SQLQuery();
  	 query.addPair("username", username);
  	 
  	 String result = "";
		 JSONObject json_data;
		 
		 if (!query.executeSQL(GlobalConstants.PHP_USER_EXISTS_URL)) return false;
		 
		 try {
			 	 BufferedReader reader = new BufferedReader(new InputStreamReader(query.getReceivedStream(),"iso-8859-1"),8);
			 	 StringBuilder sb = new StringBuilder();
			 	 String line = null;
			 	 while ((line = reader.readLine()) != null) {
			 		 sb.append(line + "n");
			 	 } 
			 	 query.getReceivedStream().close();
			 	 result=sb.toString();
			 	 }	catch(Exception e)	{
			 		 Log.e("log_tag", "Error converting result "+e.toString());
			 		 return false;
			 	 }
		 try {
			 	 JSONArray jArray = new JSONArray(result);
			 	 for(int i=0;i<jArray.length();i++){
				 	 json_data = jArray.getJSONObject(i);
				 	 result = (String) json_data.get("username");
			 	 } 
			 }
		     catch(JSONException e){
			 		 Log.e("log_tag", "Error parsing data "+e.toString());
		 }

		 return (result.equals(username));
	}
  
  /**F�gt einen neuen Nutzer der Datenbank hinzu**/
  public static void registerUser (String username, String password) {
  		password = encrypt(password);
	   	SQLQuery query = new SQLQuery();
	   	query.addPair("username", username);
	   	query.addPair("password", password);
 	    query.executeSQL(GlobalConstants.PHP_ADD_USER_URL);
  }

}
