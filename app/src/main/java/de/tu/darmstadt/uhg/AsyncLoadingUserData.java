package de.tu.darmstadt.uhg;

import android.os.AsyncTask;

public class AsyncLoadingUserData extends AsyncTask<Void, Void, Void>{
	
	AsyncLoadingCallback callback;
	
	
	public AsyncLoadingUserData(AsyncLoadingCallback callback) {
		this.callback = callback;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		UserSettings.userData.getUserData();
		return null;
	}
	
	protected void onPostExecute(Void result) {
		if(callback != null){
		callback.loadingDone();
		}
	}

}
