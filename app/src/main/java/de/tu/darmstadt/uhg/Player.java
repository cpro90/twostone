package de.tu.darmstadt.uhg;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ImageView;
import de.tu.darmstadt.uhg.gps.GPSPathPoint;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.gps.GPSProgressing;
import de.tu.darmstadt.uhg.progressing.UserData;

/**
 * Diese Klasse repr�sentiert den Spieler / die Spielerin. Er/Sie bekommt
 * experience bei gel�sten Maps, hat eine aktuelle, sowie vergangene Position,
 * einen Username, ist in der Kampagne auf einem bestimmten Level, hat eine
 * aktuelle Geschwindigkeit, einen bestimmten Weg insgesamt zur�ckgelegt, dabei
 * insgesamt Zeit verbraucht. Die boolean "initial" ist nur zum Erst-Starten als
 * flag gedacht, damit aus den Koordinaten nicht vom
 * 0-Meridian/�quator-Schnittpunkt innerhalb weniger Millisekunden eine
 * Verbindung zum aktuellen Punkt berechnet wird. CurrentSpeed w�re dann ca.
 * Lichtgeschwindigkeit :)
 * 
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 * 
 */
@SuppressWarnings("deprecation")
public class Player {

	String rank = ""; //Rankbezeichnung
	GPSPoint position; // Latitude, Longitude position of Player
	GPSPoint lastPosition; // Last position before current position
	String username; // Username on Server
	int campaignLevel; // Level in the Campaign
	double currentSpeed; // Current movement speed
	boolean initial; // Block before the first GPS Analysis
	GPSPathPoint pointA; // Ein Randpunkt
	GPSPathPoint pointB; // Ein Randpunkt


	public String getRank(UserData ud) {
		if (rank == "")
			rank = "Newbie";

		if (ud.getExp() > 100)
			rank = "Rookie";
		if (ud.getExp() > 1000)
			rank = "Sergeant";
		if (ud.getExp() > 5000)
			rank = "Lieutnant";
		if (ud.getExp() > 10000)
			rank = "Commander";
		if (ud.getExp() > 50000)
			rank = "Officer";
		if (ud.getExp() > 100000)
			rank = "General";
		if (ud.getExp() > 500000)
			rank = "Ruler";
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}


	/**Gibt zur�ck, ob der Player gerade initiiert wurde, d.h. dies sein erster Zug ist**/
	public boolean isInitial() {
		return initial;
	}

	/**Setzt den Initalwert des Players**/
	public void setInitial(boolean initial) {
		this.initial = initial;
	}
	

	/**
	 * L�scht den Player
	 * @param context
	 * @param player
	 */
	public Player deletePlayer(Context context) {
        
		String URL = context.getFilesDir().getPath().toString() + "/"
				+ this.getUsername() + ".plr";

		File file = new File(URL);
		file.delete();		
		
		URL = context.getFilesDir().getPath().toString() + "/"
				+ this.getUsername() + "_pic.plr";

		file = new File(URL);
		file.delete();		
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    	StrictMode.setThreadPolicy(policy); 
   
    	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("name", getUsername()));
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(GlobalConstants.SERVER_HTTP_URL + "deleteUser.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			//HttpResponse response = 
			httpclient.execute(httppost);
			//HttpEntity entity = response.getEntity();
			//InputStream is = entity.getContent();
		} catch(Exception e){
			Log.e("log_tag", "Cannot delete User. Error in http connection" + e.toString());
		}
		
		return new Player("Test");
        
	}

	/**Setzt ImageView.VISIBLE, wenn der Input true ist**/
	public static void decodeVisibility(ImageView view, int visible) {
			view.setVisibility(visible);
	}
	
	
	/** Default constructor **/
	public Player(int experience, GPSPoint position, String username,
			int campaignLevel) {
		this.position = position;
		this.lastPosition = position;
		this.username = username;
		this.campaignLevel = campaignLevel;
		this.initial = true;
	}

	/** Standard Settings **/
	public Player(String username) {
		this.position = new GPSPoint(0, 0);
		this.lastPosition = new GPSPoint(0, 0);
		this.username = username;
		this.campaignLevel = 1;
		this.initial = true;
	}

	/**
	 * Legt die beiden Punkte fest, zwischen denen sich der Spieler befindet
	 * 
	 * @param a
	 *            Punkt A
	 * @param b
	 *            Punkt B
	 */
	public void setBetweenPoints(GPSPathPoint a, GPSPathPoint b) {
		this.pointA = a;
		this.pointB = b;
	}

	/** Gibt einen Punkt zur�ck, in dessen N�he sich der Spieler befindet **/
	public GPSPathPoint getPointA() {
		return this.pointA;
	}

	/** Gibt einen Punkt zur�ck, in dessen N�he sich der Spieler befindet **/
	public GPSPathPoint getPointB() {
		return this.pointB;
	}

	
	/** Returns the position of the Player **/
	public GPSPoint getPosition() {
		return position;
	}
	
	/** Returns the last position of the Player **/
	public GPSPoint getLastPosition() {
		return lastPosition;
	}


	/** Player Attribute werden geupdated, z.B. gelaufene KM oder aktuelle Geschwindigkeit **/
	public void updatePlayerAttributes() {
		double cSpeed = GPSProgressing.getSpeed(this.position,
				this.lastPosition);

		this.currentSpeed = cSpeed;
		
	}

	/** Sets the position of the Player with accuracy **/
	public void setPosition(GPSPoint newPosition) {

		lastPosition = position.copy();
		position.setLatitude(newPosition.getLatitude());
		position.setLongitude(newPosition.getLongitude());
		position.setRecordedTime(newPosition.getRecordedTime());
		
		if (initial) {
			initial = false;
			lastPosition = position.copy();
		} else {
			//Kein Update von PlayerAttributes, wenn GPS Daten zum ersten Mal erfasst werden.
			updatePlayerAttributes();
		}
	}

	/** Returns the username of the Player **/
	public String getUsername() {
		return username;
	}

	/** Sets the username of the Player **/
	public void setUsername(String username) {
		this.username = username;
	}

	/**Returns the campaignLevel of the Player**/
	public int getCampaignLevel() {
		return campaignLevel;
	}

	/**Sets the campaignLevel of the Player**/
	public void setCampaignLevel(int campaignLevel) {
		this.campaignLevel = campaignLevel;
	}

}
