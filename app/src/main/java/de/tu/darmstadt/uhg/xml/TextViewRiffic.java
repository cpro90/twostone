package de.tu.darmstadt.uhg.xml;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewRiffic extends TextView {
	
	static Typeface riffic;

	public TextViewRiffic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypefaceRiffic();
    }

   public TextViewRiffic(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypefaceRiffic();
    }

   public TextViewRiffic(Context context) {
        super(context);
        setTypefaceRiffic();
   }
   
   private void setTypefaceRiffic(){
	   if(riffic == null) riffic = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "riffic.ttf");
	   setTypeface(riffic);
   }

}
