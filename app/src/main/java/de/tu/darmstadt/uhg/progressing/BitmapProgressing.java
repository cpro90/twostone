package de.tu.darmstadt.uhg.progressing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Environment;
import android.util.Log;

/**
 * Macht aus einer normalen Bitmap eine MutableBitmap
 * @source http://www.java2s.com/Code/Android/UI/SetImageBitmapforImageView.htm
 * Stand der Seite vom 08.07.2013
 *
 */
public class BitmapProgressing {
	
	/**Compress Bitmap**/
	public static Bitmap compress(Bitmap rawBitmap) {
		Canvas canvas;

	    int targetWidth = 400;
	    int targetHeight = 400;
	    
	    float relationW =  (float) rawBitmap.getWidth() /
	    		(float) rawBitmap.getHeight();
		float relationH =  (float) rawBitmap.getHeight() /
				(float) rawBitmap.getWidth();
		
		Log.d("UHG", "Relations: " + relationW + ", " + relationH);
		if ((relationW > 1) && (relationW < 1.5)) 
			targetHeight=(int) Math.round(targetHeight/relationW);
		
		if ((relationH > 1) && (relationH < 1.5)) 
			targetWidth=(int) Math.round(targetWidth/relationH);
		
	    Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
	                        targetHeight,Bitmap.Config.ARGB_8888);
	                        canvas = new Canvas(targetBitmap);
	                        
	    canvas.drawBitmap(rawBitmap, 
	                            new Rect(0, 0, rawBitmap.getWidth(),
	      rawBitmap.getHeight()), 
	                            new Rect(0, 0, targetWidth,
	      targetHeight), null);
	    return targetBitmap;
	}
	
	
	/**Crop image round**/
	 public static Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		 	Canvas canvas;
		    int targetWidth = 75;
		    int targetHeight = 75;
		    Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
		                        targetHeight,Bitmap.Config.ARGB_8888);
		                        canvas = new Canvas(targetBitmap);
		    Path path = new Path();
		    path.addCircle(((float) targetWidth - 1) / 2,
		    ((float) targetHeight - 1) / 2,
		    (Math.min(((float) targetWidth), 
		            ((float) targetHeight)) / 2),
		      Path.Direction.CCW);

		    canvas.clipPath(path);
		    Bitmap sourceBitmap = scaleBitmapImage;
		    canvas.drawBitmap(sourceBitmap, 
		                            new Rect(0, 0, sourceBitmap.getWidth(),
		      sourceBitmap.getHeight()), 
		                            new Rect(0, 0, targetWidth,
		      targetHeight), null);
		    return targetBitmap;
		   }
	 
	/**
	 * Konvertiert eine normale Bitmap zu einer mutablen (šnderbaren) Bitmap
	*/
	public static Bitmap convertToMutable(Bitmap imgIn) {
        try {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.tmp");
 
            //Open an RandomAccessFile
            //Make sure you have added uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"
            //into AndroidManifest.xml file
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
 
            // get the width and height of the source bitmap.
            int width = imgIn.getWidth();
            int height = imgIn.getHeight();
            Bitmap.Config type = imgIn.getConfig();
 
            //Copy the byte to the file
            //Assume source bitmap loaded using options.inPreferredConfig = Config.ARGB_8888;
            FileChannel channel = randomAccessFile.getChannel();
            MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, imgIn.getRowBytes()*height);
            imgIn.copyPixelsToBuffer(map);
            //recycle the source bitmap, this will be no longer used.
            imgIn.recycle();
            System.gc();// try to force the bytes from the imgIn to be released
 
            //Create a new bitmap to load the bitmap again. Probably the memory will be available.
            imgIn = Bitmap.createBitmap(width, height, type);
            map.position(0);
            //load it back from temporary
            imgIn.copyPixelsFromBuffer(map);
            //close the temporary file and channel , then delete that also
            channel.close();
            randomAccessFile.close();
 
            // delete the temp file
            file.delete();
 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        return imgIn;
    }
}
