package de.tu.darmstadt.uhg.progressing;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.LoginAuthenticate;
import de.tu.darmstadt.uhg.UserSettings;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;

public class UserData {
	
	String username;
	double distance;
	long playtime;
	String email;
	long startdate;
	long bestScore;
	long exp;
	Bitmap picture;
	long steps;
	
	public void addStep() {
		steps++;
	}
	
	public static Bitmap STANDARD_PICTURE = null;
	
	public Bitmap getPicture() {
		return picture;
	}

	public void setPicture(Bitmap picture) {
		this.picture = picture;
	}

	public long getBestScore() {
		return bestScore;
	}

	public void setBestScore(long bestScore) {
		this.bestScore = bestScore;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public long getPlaytime() {
		return playtime;
	}

	public void setPlaytime(long playtime) {
		this.playtime = playtime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getStartdate() {
		return startdate;
	}

	public long getSteps() {
		return steps;
	}

	public void setStartdate(long startdate) {
		this.startdate = startdate;
	}

	public long getExp() {
		return exp;
	}

	public void setExp(long exp) {
		this.exp = exp;
	}

	public UserData(String username) {
		super();
		this.username = username;
		steps = 0;
	}

	public void vanish() {
		distance = 0;
		playtime = 0;
	}

	public void addExp(long amount) {
		this.exp += amount;
	}

	public void addDistance(double amount) {
		this.distance += amount;
	}
	

	public void addTime(long amount) {
		this.playtime += amount;
	}
	
	
	private void getUserProfilePicture () {

	   	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	  	 StrictMode.setThreadPolicy(policy); 
	  	
	  	 SQLQuery query = new SQLQuery();
	  	 query.addPair("username", username);
	  	 
	  	 String result = "";
			 JSONObject json_data;
			 
			 if (!query.executeSQL(GlobalConstants.PHP_GET_PROFILE_PICTURE_URL)) {
		 		 picture=STANDARD_PICTURE;
				 return;
			 }
			 
			 try {
				 	 BufferedReader reader = new BufferedReader(new InputStreamReader(query.getReceivedStream(),"iso-8859-1"),8);
				 	 StringBuilder sb = new StringBuilder();
				 	 String line = null;
				 	 while ((line = reader.readLine()) != null) {
				 		 sb.append(line + "n");
				 	 } 
				 	 query.getReceivedStream().close();
				 	 result=sb.toString();
				 	 }	catch(Exception e)	{
				 		 Log.e("log_tag", "Error converting result "+e.toString());
				 		 picture=STANDARD_PICTURE;
				 		 return;
				 	 }
			 try {
				 	 JSONArray jArray = new JSONArray(result);
				 	 for(int i=0;i<jArray.length();i++){
					 	 json_data = jArray.getJSONObject(i);
					 	 try {
					 	 result = (String) json_data.get("picture");
					 	 
					 	 } catch (Exception e) {
					 		 picture=STANDARD_PICTURE;
					 		 return;
					 	 }
				 	 } 
				 }
			     catch(JSONException e){
				 		 Log.e("log_tag", "Error parsing data "+e.toString());
				 		 picture=STANDARD_PICTURE;
				 		 return ;
			 }

			 picture = stringToBitmap(result);

	}
	
	
	/**Holt sich die Nuterdaten, false falls keine existieren*/
	  public boolean getUserData() {
	  	
		     SQLQuery query = new SQLQuery();
			 query.addPair("username", username);
			 if(!query.executeSQL(GlobalConstants.PHP_GET_USER_DATA_URL)) {
				 return false;
			 }
			 
			 JSONObject json_data;
			 String result = "";
			 
			 result = query.querying();
			 try{
			 	 JSONArray jArray = new JSONArray(result);
			 	 for(int i = 0; i < jArray.length(); i++){
			 		//try {
					 	 json_data = jArray.getJSONObject(i);
					 	 UserSettings.height = Integer.parseInt(json_data.get("height") + "");
					 	UserSettings.weight = Integer.parseInt(json_data.get("weight") + "");
					 	UserSettings.age = Integer.parseInt(json_data.get("age") + "");
					 	 String gen = json_data.get("gender") + "";
					 	UserSettings.gender = gen.equalsIgnoreCase("m");
					 	 distance = Double.parseDouble(json_data.get("distance") + "");
					 	 playtime = Long.parseLong(json_data.get("playtime") + "");
					 	 email = json_data.get("email") + "";
					 	 startdate = Long.parseLong(json_data.get("startdate") + "");	
					 	 exp = Long.parseLong(json_data.get("exp") + "");	
					 	 bestScore = Long.parseLong(json_data.get("bestScore") + "");	
					 	 steps = Long.parseLong(json_data.get("steps") + "");	
			 		//} catch (Exception e) {
			 		//	 return false;
			 		//}
				 	
				 } 
			 } catch(JSONException e){
				 Log.e("log_tag", "Error parsing data "+e.toString());
			 }
			 getUserProfilePicture();
			 return true;
		}
	  

	private String bitmapToString(Bitmap bitmap){
		if (bitmap == null) {
			bitmap = STANDARD_PICTURE;
		}
	     ByteArrayOutputStream baos=new  ByteArrayOutputStream();
	     bitmap.compress(Bitmap.CompressFormat.JPEG, GlobalConstants.PROFILE_PICTURE_COMPRESSION_RATE, baos);
	     byte [] b=baos.toByteArray();
	     String temp=Base64.encodeToString(b, Base64.DEFAULT);
	     return temp;
	}
	
	public Bitmap stringToBitmap(String encodedString){
	     try{
	       byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
	       Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
	       return bitmap;
	     }catch(Exception e){
	       e.getMessage();
	       Log.d ("UHG", "Error encoding string to bitmap " + e.getMessage());
	       return null;
	     }
	}

	/**F�gt Daten des Nutzers der Datenbank hinzu**/
	  public void updateUserData () {
		  	//deleteUserData();
		  	SQLQuery query = new SQLQuery();
		   	query.addPair("username", username);
		   	query.addPair("height", UserSettings.height + "");
		   	query.addPair("weight", UserSettings.weight + "");
		   	query.addPair("distance", distance + "");
		   	query.addPair("playtime", playtime + "");
		   	query.addPair("email", email);
		   	query.addPair("startdate", startdate + "");
		   	query.addPair("bestScore", bestScore + "");
		   	query.addPair("exp", exp + "");
		   	query.addPair("steps", steps + "");
		   	if (UserSettings.gender == true)
		   		query.addPair("gender", "m");
		   	else
		   		query.addPair("gender", "f");
		   	query.addPair("age", UserSettings.age + "");
		   	query.addPair("id", UserSettings.GAME_ID + "");
	 	    query.executeSQL(GlobalConstants.PHP_UPDATE_USER_DATA_URL);
	 	    
	 	    SQLQuery query2 = new SQLQuery();
		   	query2.addPair("username", username);
		   	query2.addPair("picture", bitmapToString(picture));
	 	    query2.executeSQL(GlobalConstants.PHP_ADD_PROFILE_PICTURE_URL);
	  }

	  /**L�scht Daten des Nutzers der Datenbank hinzu**/
	  public void deleteUserData () {
		   	SQLQuery query = new SQLQuery();
		   	query.addPair("username", username);
	 	    query.executeSQL(GlobalConstants.PHP_DELETE_USER_DATA_URL);
	  }
	  
	  /**F�gt einen neuen Nutzer der Datenbank hinzu**/
	  public static void deleteUser (String username, String password) {
	  		password = LoginAuthenticate.encrypt(password);
		   	SQLQuery query = new SQLQuery();
		   	query.addPair("username", username);
		   	query.addPair("password", password);
	 	    query.executeSQL(GlobalConstants.PHP_DELETE_USER_URL);
	  }

	public static void setStandardPicture(Bitmap decodeResource) {
		STANDARD_PICTURE = decodeResource;		
	}
}
