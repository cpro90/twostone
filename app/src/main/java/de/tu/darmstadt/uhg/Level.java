package de.tu.darmstadt.uhg;

import java.util.ArrayList;

import de.tu.darmstadt.uhg.game.Game;
import de.tu.darmstadt.uhg.game.SimpleEnemy;

/**
 * Klasse repr�sentiert ein LevelSet zum Spielen
 * 
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 * 
 */
public class Level {

	String mapName; // Name der Karte, auf dem das Level gilt
	String levelName; // Name des Levels, bsp. "Kein Ausweg", "In der Klemme",
						// "Rush Hour", etc.
	ArrayList<SimpleEnemy> enemies; // Geister
	int minExp; // So viele EXP braucht man, um die Map zu spielen
	int exp; // So viele EXP bekommt man, wenn man im Spiel gewinnt
	int mustBeEaten; // Prozentual, wieviel Brot muss von der Karte gegessen
						// werden, damit
						// man gewinnt. 100 = 100%, 50 = 50%, 0 ist nicht
						// erlaubt!
	int difficulty; // Schwierigkeitsgrad der Map! Wird berechnet, muss nicht
					// angegeben werden!
	int maxTime; // Maximale Zeit, f�r die es Punkte gibt!

	public Level(String mapName, String levelName,
			ArrayList<SimpleEnemy> ghosts, int maxTime, int minExp, int exp,
			int mustBeEaten) {
		this.mapName = mapName;
		this.levelName = levelName;
		this.minExp = minExp;
		this.maxTime = maxTime;
		this.exp = exp;
		this.mustBeEaten = mustBeEaten;
		this.difficulty = Game.getDifficulty(ghosts);
		enemies = new ArrayList<SimpleEnemy>();
		for(int i = 0; i < ghosts.size(); i++){
			if(ghosts.get(i).getIntelligence() != GlobalConstants.ENEMY_OFF) enemies.add(ghosts.get(i));
		}
	}

	/** Gibt die maximale Zeit zur�ck **/
	public int getMaxTime() {
		return maxTime;
	}

	/** Setzt die maximale Zeit, die f�r das L�sen der Karte gilt **/
	public void setMaxTime(int maxTime) {
		this.maxTime = maxTime;
	}

	/** Gibt den Namen der Karte zur�ck **/
	public String getMapName() {
		return mapName;
	}

	/** Setzt den Namen der Karte **/
	public void setMap(String mapName) {
		this.mapName = mapName;
	}

	/** Gibt den Namen des Levels zur�ck **/
	public String getLevelName() {
		return levelName;
	}

	/** Setzt den Namen des Levels **/
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	/** Gibt die Geister im Level zur�ck **/
	public ArrayList<SimpleEnemy> getGhosts() {
		return enemies;
	}

	/** Setzt die Geister im Level **/
	public void setGhosts(ArrayList<SimpleEnemy> ghosts) {
		this.enemies = ghosts;
	}

	/**
	 * Gibt die Anzhal der Exp zur�ck, die f�r das Spielen der Karte ben�tigt
	 * werden
	 **/
	public int getMinExp() {
		return minExp;
	}

	/** Setzt die Anzahl der mindest erforderlichen EXP zum Spielen der Karte **/
	public void setMinExp(int minExp) {
		this.minExp = minExp;
	}

	/**
	 * Gibt die Erfahrungspunkte zur�ck, die man erh�lt, wenn man das Level
	 * spielt
	 **/
	public int getExp() {
		return exp;
	}

	/** Setzt die Erfahrungspunkte, die man erh�lt, wenn man das Level spielt **/
	public void setExp(int exp) {
		this.exp = exp;
	}

	/**
	 * Gibt die prozentuale Anzahl der Punkte zur�ck, die gesammelt werden
	 * m�ssen
	 **/
	public int getMustBeEaten() {
		return mustBeEaten;
	}

	/** Setzt die prozentuale Anzahl der Punkte, die gesammelt werden m�ssen **/
	public void setMustBeEaten(int mustBeEaten) {
		this.mustBeEaten = mustBeEaten;
	}

	/** Gibt die Schwierigkeit des Levels zur�ck **/
	public int getDifficulty() {
		return difficulty;
	}

	/** Setzt die Schwierigkeit des Levels **/
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

}
