package de.tu.darmstadt.uhg.dialogs;

import de.tu.darmstadt.uhg.R;
import android.app.AlertDialog;  
import android.content.Context;  
import android.content.DialogInterface;  
import android.content.DialogInterface.OnClickListener;  
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TableLayout;
import android.widget.TextView;
  
/** 
 * helper for Prompt-Dialog creation . NOT USED ANYMORE
 */  
public abstract class RatingDialog extends AlertDialog.Builder implements OnClickListener {  
 private final RatingBar ratingbar;  
 private final EditText comment;

 public RatingBar getRating() {
	return ratingbar;
}

 public EditText getComment() {
	return comment;
}

/** 
  * @param context
  */  
 public RatingDialog(Context context, int initial, double globalRate, String commi, String mapname) {  
  super(context); 
  setTitle(mapname);     
  
  ratingbar = new RatingBar(context);  
  ratingbar.setStepSize(1.0f);
  ratingbar.setNumStars(5);
  ratingbar.setId(1);
  ratingbar.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
  if ((initial > 0) && (initial < 6))
	  ratingbar.setRating(initial);
  else
	  ratingbar.setRating(0.0f);
  
  LayerDrawable stars = (LayerDrawable) ratingbar.getProgressDrawable();
  stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

  TextView text = new TextView(context);
  text.setText(context.getResources().getString(R.string.rateText));
  text.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

  TextView globalrating = new TextView(context);
  if (globalRate != 0) {
	  globalrating.setText(context.getResources().getString(R.string.globalRateText) + " " + ((float) Math.round(globalRate*10))/10); 
	  globalrating.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
  }

  TextView commentText = new TextView(context);
  commentText.setText(context.getResources().getString(R.string.leaveComment));
  commentText.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

  comment = new EditText(context);
  comment.setText(commi);
  
  TableLayout parent = new TableLayout(context);
  parent.setGravity(Gravity.CENTER);
  parent.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
          LayoutParams.MATCH_PARENT));
  
  if (globalRate != 0)
	  parent.addView(globalrating);
  parent.addView(text);
  parent.addView(ratingbar);
  parent.addView(commentText);
  parent.addView(comment);

  setView(parent);  
    
  setPositiveButton(context.getResources().getString(R.string.ok), this);  
  setNegativeButton(context.getResources().getString(R.string.cancel), this);  
 }  
  
 /** 
  * will be called when "cancel" pressed. 
  * closes the dialog. 
  * can be overridden. 
  * @param dialog 
  */  
 public void onCancelClicked(DialogInterface dialog) {  
  dialog.dismiss();  
 }  
  
 @Override  
 public void onClick(DialogInterface dialog, int which) {  
  if (which == DialogInterface.BUTTON_POSITIVE) {  
   if (onOkClicked(Math.round(ratingbar.getRating()), comment.getText().toString())) {  
    dialog.dismiss();  
   }  
  } else {  
   onCancelClicked(dialog);  
  }  
 }  
  
 /** 
  * called when "ok" pressed.
  * @return true, if the dialog should be closed. false, if not. 
  */  
 abstract public boolean onOkClicked(int rating, String commi);

}  
