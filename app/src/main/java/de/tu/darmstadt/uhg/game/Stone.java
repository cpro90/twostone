package de.tu.darmstadt.uhg.game;

import android.graphics.Bitmap;
import android.graphics.Point;
import de.tu.darmstadt.uhg.Map;
import de.tu.darmstadt.uhg.gps.GPSPoint;

/**
  * repräsentiert die Punkte, die eingesammelt werden.
  * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
**/
public class Stone implements Comparable<Stone>{
	
	GPSPoint position;
	Bitmap image;
	int STONE_SIZE = 16;
	Map map;
	double distance;
	
	public GPSPoint getPosition() {
		return position;
	}
		
	public void setPosition(GPSPoint position) {
		this.position = position;
	}
		
	public Bitmap getImage() {
		return image;
	}
	
	public void setImage(Bitmap image) {
		this.image = image;
	}
	
	public void setDistance(double distance){
		this.distance = distance;
	}
	
	public double getDistance(){
		return distance;
	}
	
	public Stone(GPSPoint position, Bitmap image, Map map, AugmentedReality augment) {
		super();
		this.position = position;
		this.image = image;
		this.map = map;
	}
	
	/**Draws Stones to the Map**/
	public void draw() {
		Point p = map.GPSPointToPixelPoint(this.position);
		map.drawOver(image, p);
	}

	@Override
	public int compareTo(Stone another) {
		if(this.distance > another.getDistance()){
			return -1;
		}
		else if(this.distance < another.getDistance()){
			return 1;
		}
		else return 0;
	}
	

}
