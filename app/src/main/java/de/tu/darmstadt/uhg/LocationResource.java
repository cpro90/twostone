package de.tu.darmstadt.uhg;

/**
 * Diese Klasse repräsentiert eine zu ladende Location, die die Informationen der zugehörigen
 * Dateien enthält, wie z.B. den Speicherort der XML oder des Images, sowie den Namen und
 * ob die Karte selbsterstellt wurde.
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 *
 */
public class LocationResource {
	String locationPath;
	String locationName;
	double distance;
	boolean selfMade;
	
	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public boolean isSelfMade() {
		return selfMade;
	}

	public void setSelfMade(boolean selfMade) {
		this.selfMade = selfMade;
	}

	public LocationResource(String locationPath, boolean selfMade) {
		this.locationPath = locationPath;
		this.locationName = locationPath.substring(0, locationPath.length()-4);
		this.selfMade = selfMade;
		this.distance = 0;
	}

	public LocationResource(String locationPath, String locationName, boolean selfMade) {
		this.locationPath = locationPath;
		this.locationName = locationName.substring(0, locationName.length()-4);
		this.selfMade = selfMade;
		this.distance = 0;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}


	public String getLocationPath() {
		return locationPath;
	}

	public void setLocationPath(String locationPath) {
		this.locationPath = locationPath;
	}

}
