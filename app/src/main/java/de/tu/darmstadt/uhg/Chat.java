package de.tu.darmstadt.uhg;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.tu.darmstadt.uhg.progressing.Geocoding;
import de.tu.darmstadt.uhg.progressing.SQLQuery;

import android.annotation.SuppressLint;
import android.util.Log;

/**
 * Klasse zum Chatten
 * @author Chris Michel
 *
 */
public class Chat {
		
	/**Sendet die Nachricht an den Chat-Server
	  * R�ckgabe ist true, wenn der Server sie entgegen nimmt**/
    @SuppressLint("SimpleDateFormat")
	public static boolean sendMessage(String username, String message) {
	   	
		 if (message.length() < 1) return true;
	
		 String cDT = DateFormat.getDateTimeInstance().format(new Date());
		 message = username + " (" + Geocoding.getCityName(MainActivity.currentPosition) + ", " + cDT + "): \n" + message; 
		
		 cDT = SQLQuery.convertDateTimeFormat(cDT);
		 SQLQuery query = new SQLQuery();
		 query.addPair("text", message);
		 query.addPair("lat", (int) Math.round(MainActivity.currentPosition.getLatitude()*10) + "");
		 query.addPair("lon", (int) Math.round(MainActivity.currentPosition.getLongitude()*10) + "");
		 return query.executeSQL(GlobalConstants.PHP_SUBMIT_MESSAGE_URL);
	}
   
  
   /**Holt bis zu 30 Nachrichten vom Chat-Server*/
  public static ArrayList<String> getMessages() {

		 ArrayList<String> results = new ArrayList<String>();
	     SQLQuery query = new SQLQuery();
		 query.addPair("lat", (int) Math.round(MainActivity.currentPosition.getLatitude()*10) + "");
		 query.addPair("lon", (int) Math.round(MainActivity.currentPosition.getLongitude()*10) + "");
		 if(!query.executeSQL(GlobalConstants.PHP_GET_MESSAGES_URL)) {
			 return results;
		 }
		 JSONObject json_data;
		 String result = "";
		 
		 result = query.querying();
		 try{
		 	 JSONArray jArray = new JSONArray(result);
		 	 for(int i = 0; i < jArray.length(); i++){
			 	 json_data = jArray.getJSONObject(i);
			 	 results.add(
				 json_data.get("message") + "");
			 } 
		 } catch(JSONException e){
			 Log.e("log_tag", "Error parsing data "+e.toString());
		 }
		 return results;
	}
  /** similar to getMessages(). But messages are listed in reverse order. Used to fill the chat ListView **/
  public static ArrayList<String> getInvertedMessages() {

		 ArrayList<String> results = new ArrayList<String>();
	     SQLQuery query = new SQLQuery();
		 query.addPair("lat", (int) Math.round(MainActivity.currentPosition.getLatitude()*10) + "");
		 query.addPair("lon", (int) Math.round(MainActivity.currentPosition.getLongitude()*10) + "");
		 if(!query.executeSQL(GlobalConstants.PHP_GET_MESSAGES_URL)) {
			 return results;
		 }
		 JSONObject json_data;
		 String result = "";
		 
		 result = query.querying();
		 try{
		 	 JSONArray jArray = new JSONArray(result);
		 	 for(int i = jArray.length() - 1; i >= 0; i--){
			 	 json_data = jArray.getJSONObject(i);
			 	 results.add(
				 json_data.get("message") + "");
			 } 
		 } catch(JSONException e){
			 Log.e("log_tag", "Error parsing data "+e.toString());
		 }
		 return results;
	}
  
 
}
