package de.tu.darmstadt.uhg;

import android.os.AsyncTask;
import de.tu.darmstadt.uhg.progressing.SQLQuery;

public class AsyncLogGameData extends AsyncTask<Void, Void, Void>{
	
	SQLQuery query;
	
	public AsyncLogGameData(SQLQuery query) {
		this.query = query;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		query.executeSQL(GlobalConstants.PHP_LOG_GAMEDATA);
		return null;
	}
	
	protected void onPostExecute(Void result) {
		
	}

}
