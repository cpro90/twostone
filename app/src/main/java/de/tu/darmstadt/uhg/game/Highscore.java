package de.tu.darmstadt.uhg.game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.progressing.SQLQuery;
import android.os.StrictMode;
import android.util.Log;

/**
 * Repr�sentiert die komplette Highscore.
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 *
 */
public class Highscore {
	
	
	ArrayList<ScoreEntry> scores;
	private static String SAVE_URL;

	//Zum auslesen der online Highscore
	JSONObject json_data;

    /**L�dt die aktuelle Global Score vom Server**/
    public ArrayList<ScoreEntry> getOnlineGlobalScore() {
    	return getOnlineHighscoreDataFromServer(GlobalConstants.PHP_GET_GLOBAL_SCORE_URL);
    }

    /**L�dt die aktuelle Highscore vom Server
     * NEU: Sortierung der Nutzerdaten nach gelaufener Strecke in Metern /25/01/2016 by Chris**/
    public ArrayList<ScoreEntry> getOnlineHighscoreDataFromServer(String kind) {
		 
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    	StrictMode.setThreadPolicy(policy); 
   

      	 SQLQuery query = new SQLQuery();    
		 ArrayList<ScoreEntry> results = new ArrayList<ScoreEntry>();
		 String result = "";
		 if (!query.executeSQL(kind)) {
	 		 return new ArrayList<ScoreEntry>();
		 }
		 
		 try{
			 	 BufferedReader reader = new BufferedReader(new InputStreamReader(query.getReceivedStream(),"iso-8859-1"),8);
			 	 StringBuilder sb = new StringBuilder();
			 	 String line = null;
			 	 while ((line = reader.readLine()) != null) {
			 	 sb.append(line + "n");
			 	 } 
			 	 query.getReceivedStream().close();
			 	 result=sb.toString();
			 	 }catch(Exception e){
			 	 Log.e("log_tag", "Error converting result "+e.toString());
		 		 return new ArrayList<ScoreEntry>();
			  }
		 try{
			 	 JSONArray jArray = new JSONArray(result);
			 	 for(int i=0;i<jArray.length();i++){
			 	 json_data = jArray.getJSONObject(i);
			 	 results.add(
			 			 new ScoreEntry((String) json_data.get("username"), 
			 					 (int) Math.round(Double.parseDouble((String) json_data.get("distance"))),0,0));
			 	 } 
			 	 }
			 	 catch(JSONException e){
			 		 Log.e("log_tag", "Error parsing data "+e.toString());
			 		 return new ArrayList<ScoreEntry>();
			 	 }
		 
		 return results;
	}

	
	public ArrayList<ScoreEntry> getScores() {
		return scores;
	}

	public void setScores(ArrayList<ScoreEntry> highscore) {
		this.scores = highscore;
	}

	public Highscore() {
		scores = new ArrayList<ScoreEntry>();
	}
	
	public void reloadOnlineHighscore() {
		scores = getOnlineHighscoreDataFromServer(GlobalConstants.PHP_GET_GLOBAL_SCORE_URL);		
	}

	/**Gibt den Listenersten zur�ck**/
	public ScoreEntry getBest() {
		return scores.get(0);
	}

	/**Gibt die Highscoregr��e zur�ck**/
	public int getEntries() {
		return scores.size();
	}
	
	
	/**Parst einen String und gibt die Score zur�ck
	 * Inputdaten:  playerName,score
	 */
	 public static ScoreEntry parseScore(String input) {
		for (int i = 0; i < input.length()-1; i++) {
			if (input.substring(i,i+1).equals("=")) { 
				return new ScoreEntry(input.substring(0,i), Integer.parseInt(input.substring(i+1)),0,0);
			}
		}
		return null;
	}
	
		
		public int getBestPositionFor(String username){
			for(int i = 0; i < scores.size(); i++){
				if(scores.get(i).getPlayerName().equals(username)){
					return i;
				}
			}
			return -1;
		}
		
		public int getBestPointsFor(String username){
			for(int i = 0; i < scores.size(); i++){
				if(scores.get(i).getPlayerName().equals(username)){
					return scores.get(i).getPoints();
				}
			}
			return -1;
		}


		/**Generiert eine Standard-Highscore
		 */
		public void generateStandardScore() {
			this.scores = new ArrayList<ScoreEntry>();
			try {
				FileOutputStream output = new FileOutputStream(new File(SAVE_URL));
				output.write(("Furious=10000,Fasty=7500,MrSpeed=5000,Rookie=2500,").getBytes());
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		
		//F�llt 0en auf
		public String fillInPoints(int points, int max) {
			String temp = points + "";
			for (int i = temp.length(); i < max; i++)
				temp = "0" + temp;
			return temp;
		}
		
		/**Gibt alle Eintr�ge als Liste zur�ck**/
		public ArrayList<String> getAllEntries() {
			ArrayList<String> temp = new ArrayList<String>();
			for (int i = 0; i < scores.size(); i++)
				temp.add(fillInPoints(scores.get(i).getPoints(), (scores.get(0).getPoints() + "").length()) + " " + scores.get(i).getPlayerName());
			return temp;
		}
		
		public ArrayList<String> ScoresToString(ArrayList<ScoreEntry> scores) {
			ArrayList<String> temp = new ArrayList<String>();
			for (int i = 0; i < scores.size(); i++)
				temp.add(fillInPoints(scores.get(i).getPoints(), (scores.get(0).getPoints() + "").length()) + " " + scores.get(i).getPlayerName());
			return temp;
		}

}
