package de.tu.darmstadt.uhg;

import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.gps.GPSProgressing;
import de.tu.darmstadt.uhg.leveleditor.ServerProgressing;
import de.tu.darmstadt.uhg.xml.ShortParsedFile;

public class AsyncLoadingQuickPlayOnlineMaps extends
		AsyncTask<Void, Void, ArrayList<ShortParsedFile>> {

	Context context;
	MainActivity mainActivity;
	ArrayList<String[]> mapsList;

	public AsyncLoadingQuickPlayOnlineMaps(Context context,
			ArrayList<String[]> localMapsList) {
		this.context = context;
		mainActivity = (MainActivity) context;
		if (localMapsList != null) {
			this.mapsList = localMapsList;
		} else {
			this.mapsList = new ArrayList<String[]>();
		}
	}

	@Override
	protected ArrayList<ShortParsedFile> doInBackground(Void... params) {
		return ServerProgressing.getAllOnlineMaps();
	}

	protected void onPostExecute(ArrayList<ShortParsedFile> result) {
		for (int i = 0; i < result.size(); i++) {
			if (MainActivity.levelExists(result.get(i).getTitle()))
				continue;
			String[] tempString = new String[6];

			if (MainActivity.GPSactivated) {
				GPSPoint markerPos = new GPSPoint(result.get(i).getPos());
				double dist = GPSProgressing.getWayDifference(
						MainActivity.currentPosition, markerPos);
				tempString[1] = Math.round(dist) + "";
			} else {
				tempString[1] = "-1";
			}

			tempString[0] = result.get(i).getTitle();
			tempString[2] = "Owner: " + result.get(i).getUser() + "\n"
					+ result.get(i).getDescription();
			tempString[3] = "true";
			tempString[4] = "n/a";

			mapsList = MainActivity.addSorted(mapsList, tempString, 1);
		}
		mainActivity.onQuickplayMapsAvailable(context, mapsList);
	}

}
