package de.tu.darmstadt.uhg.leveleditor;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.MainActivity;
import de.tu.darmstadt.uhg.R;
import de.tu.darmstadt.uhg.adapter.DrawerListItemArrayAdapter;
import de.tu.darmstadt.uhg.gps.GPSPath;
import de.tu.darmstadt.uhg.gps.GPSPathPoint;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.gps.GPSProgressing;
import de.tu.darmstadt.uhg.xml.ParsedFile;

/**
 * Klasse, die den Leveleditor repr�sentiert. Diese Klasse ist Bestandteil der
 * Bachelorarbeit
 * "Erzeugen von Spielfeldern f�r Standortbasierte Bewegungsspiele"
 * 
 * @author Chris Michel
 */
public class LevelEditorActivity extends FragmentActivity implements OnMapReadyCallback {
	private int moveWayPoint = GlobalConstants.MODE_AUTO;
	private String lvlTitle = "";
	private boolean once = true; 
	private String lvlDescription = "";
	public static int MIN_DISTANCE = 50;
	public static int MAX_DISTANCE = 2000; 
	private static LatLng changed_position = new LatLng(0, 0);
	private static  String DOWNLOAD_GPLAY_URL = "https://play.google.com/store/apps/details?id=com.google.android.gms&hl=de";
	private static GoogleMap googleMap;
	private static GPSPoint bottomLeft;
	private static GPSPoint topRight;
	private static GPSPoint marker;
	private static ArrayList<Connection> way;
	private static Marker clickedMarker;
	private static float zoomLevel;
	private static int counter;
	public static ParsedFile loadFileAtStartup;
	public boolean pause;
	private boolean largeDialog;
	private boolean amountDialog;
	private static boolean perspective;
	private boolean mapSatellite;
	private boolean modeManual;
	private static double distance = 0;
	private float tiltLevel = 0.0f;
	private Polyline playLine;
	private ListView drawerListView;
	private DrawerLayout drawerLayout;

	/** Addiert den Wert auf die Distanz **/
	private void addDistance(double value) {
		distance += value;
		TextView infoText = (TextView) findViewById(R.id.levelEditorInfo);
		infoText.setText("Distance: " + Math.round(distance) + " m / " + MAX_DISTANCE + " m");
	}

	/** L�dt ein Level in den Editor zum Bearbeiten **/
	public void loadLevel(ParsedFile levelFile) {

		lvlTitle = levelFile.getTitle();
		lvlDescription = levelFile.getDescription();

		bottomLeft = levelFile.getBottomLeft();
		topRight = levelFile.getTopRight();
		marker = bottomLeft;
		marker.add(topRight);
		marker.scale(0.5);
		counter = 0;
		way = new ArrayList<Connection>();

		// RUN1: Path auslesen und auf Karte zeichnen
		for (int i = 0; i < levelFile.getPath().size(); i++) {
			counter++;
			LatLng pos = new LatLng(levelFile.getPath().getPoint(i).getPoint()
					.getLatitude(), levelFile.getPath().getPoint(i).getPoint()
					.getLongitude());
			MarkerOptions marker = createNewMarker(pos);
			way.add(new Connection(new LatLng(pos.latitude, pos.longitude),
					marker));
		}

		// RUN2: Verbindungen setzen. O(n), avg: n * log(n)
		for (int i = 0; i < levelFile.getPath().size(); i++) {
			Connection cur = way.get(i);
			for (int j = 0; j < levelFile.getPath().getPoint(i)
					.getLeadToPoints().size(); j++) {
				int position = ParsedFile.getLeadingPointNr(
						levelFile.getPath(), levelFile.getPath().getPoint(i)
								.getLeadToPoints().get(j));
				Connection other = way.get(position);
				if (!cur.hasConnectionTo(other)) {
					cur.addConnection(other);
					addDistance(GPSProgressing.getWayDifference(new GPSPoint(
							cur.getPos()), new GPSPoint(other.getPos())));
				}
			}
		}

		redrawLayer();
		updateButtons();
	}

	/** Setzt die Kamera auf die aktuelle Position **/
	private void updateCamera(LatLng loc, float angleLevel) {
		if (googleMap == null)
			return;

		if (!perspective)
			angleLevel = 0;

		CameraPosition cameraPosition = null;

		if (GPSProgressing.getWayDifference(new GPSPoint(changed_position),
				new GPSPoint(loc)) > MIN_DISTANCE) {
			changed_position = loc;
			// Moved out of Radius. Update Camera
			cameraPosition = new CameraPosition.Builder().bearing(angleLevel)
					.zoom(zoomLevel)
					.target(googleMap.getCameraPosition().target)
					.tilt(tiltLevel).target(loc)// CHANGED 10.09.2015 --> Update
												// always to current Point
					.build();
		} else {
			cameraPosition = new CameraPosition.Builder().bearing(angleLevel)
					.zoom(zoomLevel)
					.target(googleMap.getCameraPosition().target)
					.tilt(tiltLevel).build();
		}

		CameraUpdate cameraUpdate = CameraUpdateFactory
				.newCameraPosition(cameraPosition);

		googleMap.moveCamera(cameraUpdate);
	}

	/** Updatet alle Buttons im Feld **/
	private void updateButtons() {
		Button finishCreating = (Button) findViewById(R.id.editor_button_finished);

		if (way.size() > 1){
			finishCreating.setEnabled(true);
			finishCreating.setAlpha(1.0f);
		}
		else{
			finishCreating.setEnabled(false);
			finishCreating.setAlpha(0.2f);
		}
	}

	public static LatLng intersection(LatLng a, LatLng b, LatLng c, LatLng d)
		    throws IllegalArgumentException
		{
		    // Wegen der Lesbarkeit
		    double x1 = a.latitude;
		    double x2 = a.longitude;
		    double x3 = c.latitude;
		    double x4 = c.longitude;
		    double y1 = b.latitude;
		    double y2 = b.longitude;
		    double y3 = d.latitude;
		    double y4 = d.longitude;

		    // Zaehler
		    double zx = (x1 * y2 - y1 * x2) - (x1 - x2) * (x3 * y4 - y3 * x4);
		    double zy = (x1 * y2 - y1 * x2) - (y1 - y2) * (x3 * y4 - y3 * x4);
		      
		    // Nenner
		    double n = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
		    
		    // Koordinaten des Schnittpunktes
		    double x = zx/n;
		    double y = zy/n;

		    // Vielleicht ist bei der Division duch n etwas schief gelaufen
		    if (x == Double.NaN || y == Double.NaN )
		    {
		        throw new IllegalArgumentException("Schnittpunkt nicht eindeutig."); 
		    }
		    
		    // Test ob der Schnittpunkt auf den angebenen Strecken liegt oder au�erhalb.
		    if
		    (
		        (x - x1) / (x2 - x1) > 1 ||
		        (x - x3) / (x4 - x3) > 1 ||
		        (y - y1) / (y2 - y1) > 1 ||
		        (y - y3) / (y4 - y3) > 1
		    )
		    {
		        throw new IllegalArgumentException("Schnittpunkt liegt au�erhalb.");
		    }

		    return new LatLng(x, y);
		}
	
	
	/** Erzeugt eine Verbindung zwischen c1 und c2 **/
	private void createConnection(Connection c1, Connection c2) {

		/**
		for (Connection c: way) {
			MarkerOptions a = c.getMarkerOptions();
			for (Connection y: c.getConnectsTo()) {
				MarkerOptions b = y.getMarkerOptions();
				try {
					LatLng schnitt = intersection(a.getPosition(), b.getPosition(), c1.getPos(), c2.getPos());

					addNewWayPoint(schnitt);
					
					return;
					
				} catch (IllegalArgumentException e) {
					
				}				
			}
		}	**/	
		
		c1.addConnection(c2);
		addDistance(GPSProgressing.getWayDifference(new GPSPoint(c1.getPos()),
				new GPSPoint(c2.getPos())));
		
	}

	/** Entfernt die Verbindung zwischen c1 und c2 **/
	private void removeConnection(Connection c1, Connection c2) {
		c1.removeConnection(c2);
		c2.removeConnection(c1);
		addDistance(-GPSProgressing.getWayDifference(new GPSPoint(c1.getPos()),
				new GPSPoint(c2.getPos())));
	}

	/** @returns true wenn eine Leveldatei mit diesem Titel bereits existiert **/
	private static boolean titleExists(String levelTitle) {
		String dataDir = context.getApplicationInfo().dataDir;
		File directory = new File(dataDir + "/komarcade/level");
		if (!directory.exists())
			directory.mkdirs();
		File outputFile = new File(directory, levelTitle + ".xml");
		return outputFile.exists();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		googleMap.clear();
		googleMap = null;
		System.gc();
	}

	private static Context context;

	@Override
	public void onMapReady(GoogleMap mapVar) {
		googleMap = mapVar;
		if (googleMap == null) {
			// Google Play Services veraltet oder nicht vorhanden! ABBRUCH!
			AlertDialog alertDialog = new AlertDialog.Builder(
					LevelEditorActivity.this).create();
			alertDialog.setTitle(getResources().getString(
					R.string.mapsTooOldTitle));
			alertDialog.setMessage(getResources()
					.getString(R.string.mapsTooOld));
			alertDialog.show();
			finish();
			return;
		}
		googleMap.getUiSettings().setCompassEnabled(false);
		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

		/** Begin new Layout Area **/

		String[] drawerStrings;

		final ImageButton drawerButton = (ImageButton) findViewById(R.id.editor_drawerButton);
		DrawerLayout.DrawerListener drawerListener;
		drawerStrings = getResources()
				.getStringArray(R.array.drawerListGeneral);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_leveleditor);
		drawerListView = (ListView) findViewById(R.id.drawerListView_leveleditor);

		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int width = displayMetrics.widthPixels;
		DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) drawerListView
				.getLayoutParams();
		params.width = (int) (width * 0.95f);
		drawerListView.setLayoutParams(params);

		drawerListView.setAdapter(new DrawerListItemArrayAdapter(this,
				drawerStrings));
		drawerListener = new DrawerLayout.DrawerListener() {

			@Override
			public void onDrawerStateChanged(int arg0) {
			}

			@Override
			public void onDrawerSlide(View arg0, float arg1) {
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onDrawerOpened(View arg0) {
				drawerButton.setImageDrawable(getResources().getDrawable(
						R.drawable.button_drawer_active));
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onDrawerClosed(View arg0) {
				drawerButton.setImageDrawable(getResources().getDrawable(
						R.drawable.button_drawer));
			}
		};
		drawerLayout.setDrawerListener(drawerListener);

		drawerButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!drawerLayout.isDrawerOpen(drawerListView)) {
					drawerLayout.openDrawer(drawerListView);
				}
			}
		});

		drawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				String screen = drawerListView.getItemAtPosition(position)
						.toString();
				if (screen.equalsIgnoreCase("level overview")) {
					setResult(GlobalConstants.DRAWER_LEVEL_OVERVIEW);
					finishLevelCreation();
				}
				if (screen.equalsIgnoreCase("help")) {
					setResult(GlobalConstants.DRAWER_HELP);
					finishLevelCreation();
				}
				if (screen.equalsIgnoreCase("options")) {
					setResult(GlobalConstants.DRAWER_OPTIONS);
					finishLevelCreation();
				}
				if (screen.equalsIgnoreCase("home")) {
					setResult(GlobalConstants.DRAWER_HOME);
					finishLevelCreation();
				}
				if (screen.equalsIgnoreCase("profile")) {
					setResult(GlobalConstants.DRAWER_PROFILE);
					finishLevelCreation();
				}
				if (screen.equalsIgnoreCase("chat")) {
					setResult(GlobalConstants.DRAWER_CHAT);
					finishLevelCreation();
				}
				if (screen.equalsIgnoreCase("statistics")) {
					setResult(GlobalConstants.DRAWER_STATISTICS);
					finishLevelCreation();
				}
				if (screen.equalsIgnoreCase("change user")) {
					setResult(GlobalConstants.DRAWER_CHANGE_USER);
					finishLevelCreation();
				}
				if (screen.equalsIgnoreCase("exit")) {
					setResult(GlobalConstants.DRAWER_EXIT);
					finishLevelCreation();
				}
				// drawerListView.setItemChecked(position, true);
				drawerLayout.closeDrawer(drawerListView);
			}
		});
		/** new Layout Area END **/

		pause = false;
		largeDialog = true;
		amountDialog = true;
		perspective = false;
		mapSatellite = false;
		modeManual = true;

		zoomLevel = 18.0f;
		moveWayPoint = GlobalConstants.MODE_CONNECT;
		counter = 0;
		way = new ArrayList<Connection>();
		if (loadFileAtStartup != null) {
			loadLevel(loadFileAtStartup);
			loadFileAtStartup = null;
		}
		updateButtons();
		checkAutosaveFile();

		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if ((resultCode != ConnectionResult.SUCCESS) || (googleMap == null)) {
			// Google Services nicht installiert
			AlertDialog.Builder builder = new AlertDialog.Builder(
					LevelEditorActivity.this);
			builder.setMessage(
					getResources().getString(R.string.installGoogleServices))
					.setTitle(
							getResources().getString(
									R.string.installGoogleServicesTitle));

			builder.setPositiveButton(getResources().getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							finishLevelCreation();
							Intent browser = new Intent(Intent.ACTION_VIEW, Uri
									.parse(DOWNLOAD_GPLAY_URL));
							startActivity(browser);
						}
					});

			builder.setNegativeButton(
					getResources().getString(R.string.cancel),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							finishLevelCreation();
						}
					});

			Dialog dialog = builder.create();
			dialog.show();

			return;
		}

		googleMap.setMyLocationEnabled(true);
		googleMap.getUiSettings().setZoomControlsEnabled(false);
		googleMap.getUiSettings().setMyLocationButtonEnabled(false);
		googleMap.getUiSettings().setRotateGesturesEnabled(false);
		googleMap.getUiSettings().setZoomGesturesEnabled(false);
		googleMap.getUiSettings().setScrollGesturesEnabled(false);
		googleMap.getUiSettings().setAllGesturesEnabled(false);

		googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

			@Override
			public void onMapClick(LatLng arg0) {
				// SIMPLE CLICK ON MAP+

				if ((moveWayPoint == GlobalConstants.MODE_MOVE) && (clickedMarker != null)) {
					Connection c = getConnectionByMarker(getMarkerFromSnippet(clickedMarker.getSnippet()));
					c.getMarkerOptions().position(arg0);
					c.pos = arg0;

					redrawLayer();
				}
			}
		});

		googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

			@Override
			public void onMapLongClick(LatLng arg0) {
				// LONG CLICK ON MAP
				addNewWayPoint(arg0);
			}
		});

		googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker arg0) {
				if ((moveWayPoint == GlobalConstants.MODE_CONNECT) && (clickedMarker != null)
						&& (clickedMarker != arg0)) {

					Connection c1 = getConnectionByMarker(getMarkerFromSnippet(arg0
							.getSnippet()));
					Connection c2 = getConnectionByMarker(getMarkerFromSnippet(clickedMarker
							.getSnippet()));

					// CHECK ob verbindung bereits existiert
					if (c1.hasConnectionTo(c2)) {
						removeConnection(c1, c2);
					} else {
						createConnection(c1, c2);
					}

					redrawLayer();
					clickedMarker = null;
				} else {
					setClickedMarker(arg0);
				}
				return false;
			}
		});

		updateCamera(
				new LatLng(MainActivity.currentPosition.getLatitude(), MainActivity.currentPosition.getLongitude()),
				MainActivity
						.getDirection());
		googleMap
				.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

					@Override
					public void onMyLocationChange(Location arg0) {
						if ((moveWayPoint == GlobalConstants.MODE_CONNECT) && (way.size() > 0))
							drawLineToPlayer(way.get(way.size() - 1)
									.getMarkerOptions());
						if (once) {
							once = false;
							updateCamera(
									new LatLng(arg0.getLatitude(), arg0
											.getLongitude()), MainActivity
											.getDirection());
						}
					}
				});

		ImageButton changeMode = (ImageButton) findViewById(R.id.changeMode);
		changeMode.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				ImageButton changeMode = (ImageButton) findViewById(R.id.changeMode);
				TextView changeModeText = (TextView) findViewById(R.id.editor_text_changeMode);
				if (modeManual) {
					//TODO: cange Mode
					modeManual = false;
					changeMode.setImageResource(R.drawable.button_automodus);
					changeModeText.setText(getResources().getString(R.string.autoMode));
				} else {
					//TODO: cange Mode
					modeManual = true;
					changeMode.setImageResource(R.drawable.button_manual);
					changeModeText.setText(getResources().getString(R.string.manualMode));
				}

			}
		});

		ImageButton changeMapStyle = (ImageButton) findViewById(R.id.changeMapStyle);
		changeMapStyle.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				ImageButton mStyle = (ImageButton) findViewById(R.id.changeMapStyle);
				TextView changeMapStyleText = (TextView) findViewById(R.id.editor_text_changeMapStyle);
				if (mapSatellite) {
					googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
					mapSatellite = false;
					mStyle.setImageResource(R.drawable.button_map_view);
					changeMapStyleText.setText(getResources().getString(R.string.mapView));
				} else {
					googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
					mapSatellite = true;
					mStyle.setImageResource(R.drawable.button_satellite_view);
					changeMapStyleText.setText(getResources().getString(R.string.satelliteView));
				}

			}
		});

		ImageButton changeMapTilt = (ImageButton) findViewById(R.id.changeMapTilt);
		changeMapTilt.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				ImageButton mStyle = (ImageButton) findViewById(R.id.changeMapTilt);
				TextView changeMapTiltText = (TextView) findViewById(R.id.editor_text_changeMapTilt);
				if (perspective) {
					tiltLevel = 0.0f;
					perspective = false;
					mStyle.setImageResource(R.drawable.button_perspective_2d);
					changeMapTiltText.setText(getResources().getString(R.string.perspective2d));
				} else {
					tiltLevel = 65.0f;
					perspective = true;
					mStyle.setImageResource(R.drawable.button_perspective_3d);
					changeMapTiltText.setText(getResources().getString(R.string.perspective3d));
				}

			}
		});

		ImageButton addToPath = (ImageButton) findViewById(R.id.addWayPoint);
		addToPath.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				addNewWayPoint(getCurrentPosition());
			}
		});

		ImageButton deleteFromPath = (ImageButton) findViewById(R.id.deleteWayPoint);
		deleteFromPath.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				deleteSelectedMarker();
			}
		});


		ImageButton moveFromPath = (ImageButton) findViewById(R.id.moveWayPoint);
		moveFromPath.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				changeMode();
			}
		});

		changeMode();
		changeMode();

		ImageButton editorSaveButtonClose = (ImageButton) findViewById(R.id.editor_save_button_close);
		editorSaveButtonClose.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				RelativeLayout saveDialog = (RelativeLayout) findViewById(R.id.editor_save_relative_dialog);
				saveDialog.setVisibility(View.INVISIBLE);
			}
		});

		Button editorSaveButtonCancel = (Button) findViewById(R.id.editor_save_button_cancel);
		editorSaveButtonCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				RelativeLayout saveDialog = (RelativeLayout) findViewById(R.id.editor_save_relative_dialog);
				saveDialog.setVisibility(View.INVISIBLE);
			}
		});

		Button editorButtonFinished = (Button) findViewById(R.id.editor_button_finished);
		editorButtonFinished.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				RelativeLayout saveDialog = (RelativeLayout) findViewById(R.id.editor_save_relative_dialog);
				saveDialog.setVisibility(View.VISIBLE);

//				if (lvlTitle.equalsIgnoreCase("autosave"))
//					editorSaveEditTitle.setText("");
//				else
//					editorSaveEditTitle.setText(lvlTitle);
			}
		});

		Button editorSaveButtonSave = (Button) findViewById(R.id.editor_save_button_save);
		editorSaveButtonSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (way.size() < 2)
					return; // Kein Level mit nur einem oder keinem Punkt!

				// Spiel pausieren, damit jetzt keine zus�tzlichen Waypoints
				// mehr gesetzt werden k�nnen
				if (!pause)
					pause = true;

				// Eckkoordinaten zusammensetzen und Marker berechnen
				bottomLeft = getMinimumGPSPoint();
				topRight = getMaximumGPSPoint();
				// Marker = Mittelpunkt der Karte
				marker = (GPSProgressing.getCenterPoint(bottomLeft, topRight));

				EditText editorSaveEditTitle = (EditText) findViewById(R.id.editor_save_edit_titel);
				EditText editorSaveEditDescription = (EditText) findViewById(R.id.editor_save_edit_description);

				lvlTitle = editorSaveEditTitle.getText().toString();

				if (lvlTitle.equalsIgnoreCase("autosave")) {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.cannotNameAutosave),
							Toast.LENGTH_SHORT).show();
					return;
				}

				if (MainActivity.containsIllegalLetter(lvlTitle)) {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.illegalLetterLevel),
							Toast.LENGTH_SHORT).show();
					return;
				}

				if ((lvlTitle.length() > 0) && (lvlTitle.substring(0,1).equalsIgnoreCase(" "))) {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.leadingSpace),
							Toast.LENGTH_SHORT).show();
					return;
				}

				if (lvlTitle.length() < 3) {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(
									R.string.titleTooShort),
							Toast.LENGTH_SHORT).show();
					return;
				}



				lvlDescription = editorSaveEditDescription.getText().toString();;

				if (!titleExists(lvlTitle))
					// Level does not exists => Create!
					createLevel(lvlTitle, lvlDescription);
				else {
					// Level already exists. Ask if overwrite
					AlertDialog.Builder builder = new AlertDialog.Builder(
							LevelEditorActivity.this);
					builder.setMessage(
							getResources().getString(
									R.string.overwriteLevel))
							.setTitle(
									getResources()
											.getString(
													R.string.overwriteLevelTitle));

					builder.setPositiveButton(
							getResources().getString(
									R.string.ok),
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialog,
										int id) {
									createLevel(lvlTitle,
											lvlDescription);
								}
							});

					builder.setNegativeButton(
							getResources().getString(
									R.string.cancel),
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialog,
										int id) {
									// DO NOTHING
								}
							});

					Dialog dialog = builder.create();
					dialog.show();
					return;

				};
				editorSaveEditDescription.setText(lvlDescription);
				return;
			}
		});

		View.OnClickListener zoomInListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (zoomLevel < googleMap.getMaxZoomLevel())
					zoomLevel += 1;
				updateCamera(googleMap.getCameraPosition().target,
						MainActivity.getDirection());
			}
		};

		View.OnClickListener zoomOutListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if ((zoomLevel > googleMap.getMinZoomLevel())
						&& (zoomLevel > GlobalConstants.MIN_ZOOM_LEVEL))
					zoomLevel -= 1;
				updateCamera(googleMap.getCameraPosition().target,
						MainActivity.getDirection());
			}
		};

		ImageButton zoomIn = (ImageButton) findViewById(R.id.zoomInButton);
		zoomIn.setOnClickListener(zoomInListener);
		ImageButton zoomIn2 = (ImageButton) findViewById(R.id.zoomInButton2);
		zoomIn2.setOnClickListener(zoomInListener);

		ImageButton zoomOut = (ImageButton) findViewById(R.id.zoomOutButton);
		zoomOut.setOnClickListener(zoomOutListener);
		ImageButton zoomOut2 = (ImageButton) findViewById(R.id.zoomOutButton2);
		zoomOut2.setOnClickListener(zoomOutListener);

		ImageButton redo = (ImageButton) findViewById(R.id.redo);
		redo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (way.size() > 0)
					delete(way.size() - 1);
			}
		});

		ImageButton pause = (ImageButton) findViewById(R.id.pause);
		pause.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				pausing();
			}
		});

		googleMap.getUiSettings().setScrollGesturesEnabled(true);
//		RelativeLayout r1 = (RelativeLayout) findViewById(R.id.editorMenu);
//		r1.setVisibility(RelativeLayout.VISIBLE);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		context = getApplicationContext();
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_level_editor);

		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.googleMap);
		mapFragment.getMapAsync(this);

	}

	private void changeMode() {	
		final TextView text = (TextView) findViewById(R.id.editor_text_moveWayPoint);
		text.setText(getResources().getString(R.string.autoConnect));
	
		clickedMarker = null;
		moveWayPoint++;
		if (moveWayPoint > 2) {
			moveWayPoint = 0;
		}
		ImageView movePoint = (ImageView) findViewById(R.id.moveWayPoint);

		// Info anzeigen, dass jetzt der Wegepunkt verschoben wird
		if (moveWayPoint == GlobalConstants.MODE_MOVE) {
			text.setText(getResources().getString(R.string.autoConnectMove));
			
			if (playLine != null)
				playLine.remove();
			movePoint.setImageResource(R.drawable.button_shift_pin);
		} else {
			if (moveWayPoint == GlobalConstants.MODE_CONNECT) {
				text.setText(getResources().getString(R.string.autoConnectManual));			
				movePoint
						.setImageResource(R.drawable.button_connect_waypoints);
			} else {				
				movePoint
						.setImageResource(R.drawable.button_single_pin);
			}
		}	

		Button finishCreating = (Button) findViewById(R.id.editor_button_finished);
		finishCreating.setEnabled(false);
		finishCreating.setAlpha(0.2f);		
	}
		
	

	/** Gibt die aktuelle Position auf der Google Maps Karte zur�ck **/
	protected LatLng getCurrentPosition() {
		double lat = googleMap.getMyLocation().getLatitude();
		double lon = googleMap.getMyLocation().getLongitude();
		return new LatLng(lat, lon);
	}

	/** Gibt den GPSPoint mit dem kleinsten Wert zur�ck **/
	protected GPSPoint getMinimumGPSPoint() {
		if (way.size() < 2)
			return new GPSPoint(0, 0);
		GPSPoint min = new GPSPoint(way.get(0).getPos());
		for (int i = 1; i < way.size(); i++) {
			Connection cur = way.get(i);
			if ((cur.getPos().latitude < min.getLatitude())
					&& (cur.getPos().longitude < min.getLongitude()))
				min = new GPSPoint(cur.getPos());
		}
		return min;
	}

	/** Gibt den GPSPoint mit dem gr��ten Wert zur�ck **/
	protected GPSPoint getMaximumGPSPoint() {
		if (way.size() < 2)
			return new GPSPoint(0, 0);
		GPSPoint max = new GPSPoint(way.get(0).getPos());
		for (int i = 1; i < way.size(); i++) {
			Connection cur = way.get(i);
			if ((cur.getPos().latitude > max.getLatitude())
					&& (cur.getPos().longitude > max.getLongitude()))
				max = new GPSPoint(cur.getPos());
		}
		return max;
	}

	/** Pausiert einen Modus **/
	protected void pausing() {
		this.pause = !pause;
	}

	/** Gibt die zum Marker zugeh�rige Verbindung zur�ck **/
	protected Connection getConnectionByMarker(MarkerOptions marker) {

		for (int i = 0; i < way.size(); i++) {
			if (way.get(i).getMarkerOptions().equals(marker))
				return way.get(i);
		}
		return null;
	}

	/** Gibt die Richtung des Vektors zwischen zwei GPSPunkten zur�ck **/
	public static double getAngleOfVector(GPSPoint pos1, GPSPoint pos2) {
		// CALCULATE THE DIRECTION VECTOR
		double dLat = pos1.getLatitude() - pos2.getLatitude();
		double dLon = pos1.getLongitude() - pos2.getLongitude();

		// CALCULATE THE NEW DIRECTION ANGLE
		// angle = tan-1(x, y) * 180/pi
		double angle = Math.atan2(dLon, dLat) * 180 / Math.PI;

		angle += 180;
		/**
		 * //MIRROR X AXIS if (pos2.getLatitude() < pos1.getLatitude()) angle =
		 * angle + 90;
		 * 
		 * //MIRROR Y AXIS if (pos2.getLongitude() < pos1.getLongitude()) angle
		 * = -angle;
		 **/

		return angle;
	}

	/** Setzt die Kamera auf die aktuelle Position **/
	public static void rotateCamera(float angleLevel) {
		if (!perspective)
			return;
		if (googleMap != null) {
			CameraPosition cameraPosition = CameraPosition
					.builder(googleMap.getCameraPosition()).bearing(angleLevel)
					.build();
			CameraUpdate cameraUpdate = CameraUpdateFactory
					.newCameraPosition(cameraPosition);
			googleMap.moveCamera(cameraUpdate);
		}
	}

	/**
	 * Algorithmus zur Berechnung der maximal zul�ssigen Geisteranzahl f�r eine
	 * Karte
	 **/
	protected static int calculateMaxEnemies() {

		int averageEdgesPerKnot;
		int knotsComplete = 0;
		double count = 0;

		for (int i = 0; i < way.size(); i++) {
			knotsComplete += way.get(i).getConnectsTo().size();
		}
		averageEdgesPerKnot = knotsComplete / way.size();

		count += Math.sqrt(way.size());
		count += Math.sqrt(distance / 100);
		count *= (double) (averageEdgesPerKnot / 2);

		if (count > way.size() / 2)
			count = way.size() / 2;
		if (count < 1)
			count = 1;
		if (count > GlobalConstants.MAX_ENEMY_COUNT)
			count = GlobalConstants.MAX_ENEMY_COUNT;

		/**
		 * Way with 36 WayPoints and 1.600m length = 6 + 4 = 10 Max Ghosts. Way
		 * with 4 WayPoints and 200m length = 2 Max Ghosts Way with 100
		 * WayPoints and 10.000m length = 20 Max Ghosts.
		 */

		return (int) Math.round(count);
	}

	/** Berechnet die Maximaldauer f�r das Bestehen der Karte **/
	protected static int calculateMaxTime() {
		return (int) Math.round(distance * 0.8);
	}

	/** Zur�ck-Button gedr�ckt **/
	@Override
	public void onBackPressed() {
		finishLevelCreation();
	}

	/** Gibt den Index einer Verbindung in der sortierten Wegepunkteliste zur�ck **/
	protected static int getPosition(Connection c) {
		for (int i = 0; i < way.size(); i++) {
			if (way.get(i).equals(c))
				return i;
		}
		return -1;
	}

	/**
	 * Erzeugt aus den Eingaben des Nutzers / der Nutzerin den fertigen GPSPath
	 * f�r das eigentliche Spiel
	 **/
	protected static GPSPath autoCreatePath() {
		GPSPath ret = new GPSPath();
		for (int i = 0; i < way.size(); i++) {
			Connection c = way.get(i);
			GPSPathPoint g = new GPSPathPoint(new GPSPoint(c.getPos()));
			ret.points.add(g);
		}
		for (int i = 0; i < way.size(); i++) {
			Connection c = way.get(i);
			for (int j = 0; j < c.getConnectsTo().size(); j++) {
				Connection to = c.getConnectsTo().get(j);
				int pos = getPosition(to);
				if (pos > i)
					ret.getPoint(i).addToLeadToPoints(ret.getPoint(pos));
			}
		}
		return ret;
	}

	/** Erstellt das Level und speichert dieses unter dem Titelnamen.xml **/
	protected void createLevel(String levelTitle, String lvlDescription) {
		GPSPath createdPath = autoCreatePath();

		ParsedFile newLevelFile = new ParsedFile(levelTitle, lvlDescription,
				bottomLeft, topRight, marker, calculateMaxEnemies(),
				Math.round(distance), calculateMaxTime(), createdPath);
		String xmlFile = newLevelFile.makeXMLContent();
		saveLevel(LevelEditorActivity.this, levelTitle, xmlFile);

		MainActivity.updateAllAvailableMaps(LevelEditorActivity.this);
		
		if(((CheckBox)findViewById(R.id.editor_save_check_publish)).isChecked()){
			Intent data = new Intent();
			data.putExtra(GlobalConstants.RESULT_MAP_NAME, levelTitle);
			setResult(GlobalConstants.RESULT_PUBLISH_NEW_MAP, data);
		}
		
		finishLevelCreation();
	}

	/** Erzeugt einen neuen Marker auf der Karte **/
	protected MarkerOptions createNewMarker(LatLng pos) {
		MarkerOptions marker = new MarkerOptions();
		marker.position(pos);
		marker.snippet(getResources().getString(R.string.point) + " "
				+ counter++);
		marker.anchor(0.5f, 0.9f);
		marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.way_marker));
		marker.title(getResources().getString(R.string.marked));
		return marker;
	}

	/** Erzeugt einen neuen Marker auf der Karte **/
	protected MarkerOptions createNewMarker(LatLng pos, String snippet) {
		MarkerOptions marker = new MarkerOptions();
		marker.position(pos);
		marker.snippet(snippet);
		marker.anchor(0.5f, 0.9f);
		marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.way_marker));
		marker.title(getResources().getString(R.string.marked));
		return marker;
	}

	/** F�gt einen neuen Wegepunkt hinzu **/
	protected void addNewWayPoint(LatLng pos) {
		MarkerOptions marker = createNewMarker(pos);
		way.add(new Connection(pos, marker));
		
		if (way.size() > 1) {
			// Connect to the last WayPoint if MoveWayPoint == 0;
			if (moveWayPoint == GlobalConstants.MODE_AUTO) {
				if (clickedMarker != null) {
					createConnection(way.get(way.size() - 1),
							getConnectionByMarker(getMarkerFromSnippet(clickedMarker.getSnippet())));
					clickedMarker = null;
				} else {
					createConnection(way.get(way.size() - 1),
							way.get(way.size() - 2));
				}
			}
		}
		redrawLayer();
		updateButtons();
		checkDimensions();
	}

	/**
	 * Pr�ft die Levelgr��e und die Anzahl der Wegepunkte. Gibt einen Hinweis
	 * zur�ck, falls es zu viele Wegepunkte werden oder das Level zu lang wird
	 **/
	protected void checkDimensions() {

		if ((way.size() > GlobalConstants.MAX_WAYPOINTS) || (distance > MAX_DISTANCE)) {
			String title;
			String msg;

			if (way.size() > GlobalConstants.MAX_WAYPOINTS) {
				if (!amountDialog)
					return;
				amountDialog = false;
				title = getResources()
						.getString(R.string.warningWayPointsTitle);
				msg = getResources().getString(R.string.warningWayPoints);
			} else {
				if (!largeDialog)
					return;
				largeDialog = false;
				title = getResources().getString(R.string.warningDistanceTitle);
				msg = getResources().getString(R.string.warningDistance);
			}

			AlertDialog.Builder builder = new AlertDialog.Builder(
					LevelEditorActivity.this);

			builder.setMessage(msg).setTitle(title);

			builder.setPositiveButton(getResources().getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
						}
					});

			Dialog dialog = builder.create();
			dialog.show();
		}

	}

	/** Entfernt eine Verbindung an der indizierten Stelle **/
	protected void delete(int markerPos) {
		Connection deletePoint = way.get(markerPos); // Dieser Punkt wird
														// hinterher gel�scht!

		while (deletePoint.getConnectsTo().size() > 0) {

			Connection neigbour = deletePoint.getConnectsTo().get(0);

			// Entfernung zur�ckrechnen
			addDistance(-GPSProgressing.getWayDifference(
					new GPSPoint(neigbour.getPos()),
					new GPSPoint(deletePoint.getPos())));

			deletePoint.removeConnection(neigbour);
		}

		way.remove(markerPos);

		// Das Spielfeld neu zeichnen
		redrawLayer();
		updateButtons();
	}

	/** L�scht den selektierten Marker und seine Verbindungen **/
	protected void deleteSelectedMarker() {
		for (int markerPos = 0; markerPos < way.size(); markerPos++) {

			// Durchsuche alle Marker und l�sche den selektierten!
			if (compareMarker(way.get(markerPos).getMarkerOptions(),
					clickedMarker)) {
				delete(markerPos);
				clickedMarker = null;
				break; // Punkt entfernt => Schleife beenden
			}
		}
	}

	/** @return true wenn zwei Marker identisch sind **/
	private boolean compareMarker(MarkerOptions markerOptions,
			Marker clickedMarker2) {
		if ((clickedMarker2 == null) || (markerOptions == null))
			return false;
		return (clickedMarker2.getSnippet().equalsIgnoreCase(markerOptions
				.getSnippet()));
	}

	/** Updated die neue Position des selektierten Markers **/
	protected void updateSelectedMarker(LatLng newPos) {
		for (int i = 0; i < way.size(); i++)
			if (compareMarker(way.get(i).getMarkerOptions(), clickedMarker)) {
				// DISTANZEN NEU BERECHNEN
				for (int k = 0; k < way.get(i).getConnectsTo().size(); k++) {
					addDistance(-GPSProgressing.getWayDifference(new GPSPoint(
							way.get(i).getConnectsTo().get(k).getPos()),
							new GPSPoint(way.get(i).getPos())));

					addDistance(+GPSProgressing.getWayDifference(new GPSPoint(
							way.get(i).getConnectsTo().get(k).getPos()),
							new GPSPoint(newPos)));
				}

				// PUNKT NEU SETZEN
				way.get(i).setPos(newPos);
				way.get(i).getMarkerOptions().position(newPos);

				break;
			}
		redrawLayer();
	}

	/** Beendet den Leveleditor und l�scht die Autosave-Datei **/
	public void finishLevelCreation() {
		distance = 0;
		deleteAutosaveFileIfExists();
		this.finish();
		System.gc();
	}

	/** Malt eine Linie zwischen zwei Markern auf die Karte **/
	public void drawLineOnMap(MarkerOptions from, MarkerOptions to) {
		PolylineOptions line = new PolylineOptions()
				.add(new LatLng(from.getPosition().latitude,
						from.getPosition().longitude),
						new LatLng(to.getPosition().latitude,
								to.getPosition().longitude)).width(10)
				.color(Color.RED);
		googleMap.addPolyline(line);
	}

	/** Malt eine Linie vom �bergebenen Marker zur Spielerposition **/
	public void drawLineToPlayer(MarkerOptions lastMarker) {
		if (GlobalConstants.DRAW_POLYLINE) {
			if (playLine != null)
				playLine.remove();

			PolylineOptions line = new PolylineOptions()
					.add(new LatLng(lastMarker.getPosition().latitude,
							lastMarker.getPosition().longitude),
							getCurrentPosition()).width(5).color(Color.BLUE);
			playLine = googleMap.addPolyline(line);
		}
	}

	/** Legt die Autosave-Datei an **/
	private static void autosave() {
		GPSPath createdPath = autoCreatePath();
		ParsedFile newLevelFile = new ParsedFile("autosave", "Autosaved File",
				new GPSPoint(0, 0), new GPSPoint(0, 0), new GPSPoint(0, 0), 0,
				0, 0, createdPath);
		String xmlFile = newLevelFile.makeXMLContent();
		saveLevel(context, "autosave", xmlFile);
	}

	/** @return true wenn eine Autosave-Datei existiert **/
	private static boolean existsAutosaveFile() {
		return titleExists("autosave");
	}

	/** L�scht die Autosave-Datei **/
	private static void deleteAutosaveFileIfExists() {
		if ((context == null) || (context.getApplicationInfo() == null))
			return;
		String dataDir = context.getApplicationInfo().dataDir;
		File directory = new File(dataDir + "/komarcade/level");
		File file = new File(directory, "autosave.xml");
		if (file.exists()) {
			if (file.delete()) 
				Log.d("UHG", "flushed!");
		    else 
				Log.d("UHG", "not flushed!");
		}
	}

	/**
	 * �berpr�ft, ob eine Autosave-Datei existiert und schl�gt dann vor, diese
	 * zu laden, falls sie existiert
	 **/
	private void checkAutosaveFile() {
		if (!existsAutosaveFile())
			return;
		AlertDialog.Builder builder = new AlertDialog.Builder(
				LevelEditorActivity.this);
		builder.setMessage(getResources().getString(R.string.autosaveInfo))
				.setTitle(getResources().getString(R.string.autosaveTitle));

		builder.setPositiveButton(getResources().getString(R.string.yes),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						LevelEditorActivity.loadFileAtStartup = MainActivity
								.loadLocation("autosave");
						loadLevel(loadFileAtStartup);
						loadFileAtStartup = null;
						updateButtons();
					}
				});

		builder.setNegativeButton(getResources().getString(R.string.discard),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						deleteAutosaveFileIfExists();
					}
				});

		Dialog dialog = builder.create();
		dialog.show();

	}

	/** Speichert das Spiellevel lokal **/
	public static void saveLevel(Context context, String title, String input) {
		try {
			String dataDir = context.getApplicationInfo().dataDir;
			File directory = new File(dataDir + "/komarcade/level");
			if (!directory.exists())
				directory.mkdirs();
			File outputFile = new File(directory, title + ".xml");
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}
			FileWriter output = new FileWriter(outputFile);
			output.write(input);
			output.close();

			if (title != "autosave") {
				Toast.makeText(context,
						context.getResources().getString(R.string.levelSaved),
						Toast.LENGTH_LONG).show();

				MainActivity.loadXMLLevelFiles(context);
				MainActivity.preloadLocations(context);
				LevelEditorActivity.deleteAutosaveFileIfExists();
			}

		} catch (IOException e) {
			e.printStackTrace();

			Toast.makeText(context,
					context.getResources().getString(R.string.levelSaveError),
					Toast.LENGTH_LONG).show();
		}
	}

	/** Updatet die Anzeige von Linien und Markern auf der Google Map Karte **/
	public void redrawLayer() {
		googleMap.clear();

		// Marker wieder setzen
		for (int i = 0; i < way.size(); i++) {
			LatLng pos = way.get(i).getMarkerOptions().getPosition();
			googleMap.addMarker(createNewMarker(pos, way.get(i)
					.getMarkerOptions().getSnippet()));
		}

		// Polylines wieder setzen
		for (int i = 0; i < way.size(); i++) {
			MarkerOptions markerStart = way.get(i).getMarkerOptions();
			for (int j = 0; j < way.get(i).getConnectsTo().size(); j++) {
				MarkerOptions markerEnd = way.get(i).getConnectsTo().get(j)
						.getMarkerOptions();
				drawLineOnMap(markerStart, markerEnd);
			}
		}

		if (GlobalConstants.ENABLE_AUTOSAVE)
			autosave();
	}

	/** Identifiziert einen marker anhand seiner Bezeichnung **/
	private MarkerOptions getMarkerFromSnippet(String string) {
		for (int i = 0; i < way.size(); i++) {
			if (way.get(i).getMarkerOptions().getSnippet()
					.equalsIgnoreCase(string))
				return way.get(i).getMarkerOptions();
		}
		return null;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.level_creator, menu);
		return true;
	}

	public static Marker getClickedMarker() {
		return clickedMarker;
	}

	public static void setClickedMarker(Marker clickedMarkerSel) {
		clickedMarker = clickedMarkerSel;
	}

}
