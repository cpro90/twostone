package de.tu.darmstadt.uhg;

import java.util.ArrayList;

import de.tu.darmstadt.uhg.leveleditor.ServerProgressing;
import de.tu.darmstadt.uhg.xml.ShortParsedFile;
import android.content.Context;
import android.os.AsyncTask;

public class AsyncLoadingOnlineMaps extends
		AsyncTask<Void, Void, ArrayList<ShortParsedFile>> {

	Context context;
	MainActivity mainActivity;
	ArrayList<String[]> localMapsList;

	public AsyncLoadingOnlineMaps(Context context,
			ArrayList<String[]> localMapsList) {
		this.context = context;
		mainActivity = (MainActivity) context;
		if (localMapsList != null) {
			this.localMapsList = localMapsList;
		} else {
			this.localMapsList = new ArrayList<String[]>();
		}
	}

	@Override
	protected ArrayList<ShortParsedFile> doInBackground(Void... params) {
		return ServerProgressing.getAllOnlineMaps();
	}

	protected void onPostExecute(ArrayList<ShortParsedFile> result) {
		MainActivity.updateOnlineAvailableMaps(context, result, localMapsList);
	}

}
