package de.tu.darmstadt.uhg;

import android.content.Context;
import android.os.AsyncTask;

public class AsyncUpdateUserData extends AsyncTask<Void, Void, Void>{
	
	Context context;
	MainActivity mainActivity;
	
	public AsyncUpdateUserData(Context context) {
		this.context = context;
		mainActivity = (MainActivity) context;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		UserSettings.userData.updateUserData();
		return null;
	}
	
	protected void onPostExecute(Void result) {
		
	}

}
