package de.tu.darmstadt.uhg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import android.content.Context;
import de.tu.darmstadt.uhg.leveleditor.LevelEditorActivity;
import de.tu.darmstadt.uhg.progressing.UserData;

public class UserSettings {

	public static Context context;

	public static int DATA_SEND_TIME = 5000;

	public static String playerName = "";
	public static UserData userData;
	public static String hashedPW = "";
	public static long GAME_ID;
	public static boolean autoLoginEnabled = true;
	public static boolean musicEnabled = true;
	public static boolean vibrationEnabled = true;
	public static boolean cameraEnabled = false;
	public static boolean switchViewEnabled = true;
	public static boolean rotationEnabled = true;
	public static boolean rotation_type_GPS = true;
	public static int weight = 0;
	public static int height = 0;
	public static int age = 0;
	public static boolean gender = false;

	/** Speichert die aktuellen Einstellungen **/
	public static void saveSettings() {
		String URL = context.getFilesDir().getPath().toString() + "/game.set";
		try {
			FileOutputStream output = new FileOutputStream(new File(URL));
			output.write((GAME_ID + ",").getBytes()); // Spielername speichern
			output.write((UserSettings.autoLoginEnabled + ",").getBytes()); 
			output.write((UserSettings.musicEnabled + ",").getBytes()); 
			output.write((UserSettings.vibrationEnabled + ",").getBytes());
			output.write((DATA_SEND_TIME - GlobalConstants.MIN_DATA_SEND_TIME + ",")
					.getBytes()); 
			output.write((0 + ",").getBytes());
			output.write((0 + ",").getBytes());
			output.write((LevelEditorActivity.MIN_DISTANCE + ",").getBytes());
			output.write((UserSettings.cameraEnabled + ",").getBytes());
			output.write((UserSettings.switchViewEnabled + ",").getBytes());
			output.write((hashedPW + ",").getBytes());
			output.write((UserSettings.rotationEnabled + ",").getBytes());
			output.write((UserSettings.rotation_type_GPS + ",").getBytes());
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void loadSettings() {
		String URL = context.getFilesDir().getPath().toString() + "/game.set";
		try {
			FileInputStream inputStream = new FileInputStream(new File(URL));
			BufferedReader buffer = new BufferedReader(new InputStreamReader(
					inputStream));
			Scanner scan = new Scanner(buffer.readLine());
			buffer.close();
			scan.useDelimiter(",");
			UserSettings.GAME_ID = Long.parseLong(scan.next());
			UserSettings.autoLoginEnabled = Boolean.parseBoolean(scan.next());
			UserSettings.musicEnabled = Boolean.parseBoolean(scan.next());
			UserSettings.vibrationEnabled = Boolean.parseBoolean(scan.next());
			int progress = Integer.parseInt(scan.next());

			// Here with scan.hasNext() to support older installed versions!
			if (scan.hasNext()) {
				Integer.parseInt(scan.next());
			}
			if (scan.hasNext()) {
				Integer.parseInt(scan.next());
			}
			if (scan.hasNext()) {
				Integer.parseInt(scan.next());
			}
			if (scan.hasNext()) {
				UserSettings.cameraEnabled = Boolean.parseBoolean(scan.next());
			}
			if (scan.hasNext()) {
				UserSettings.switchViewEnabled = Boolean.parseBoolean(scan
						.next());
			}
			if (scan.hasNext()) {
				UserSettings.hashedPW = scan.next();
			}
			if (scan.hasNext()) {
				UserSettings.rotationEnabled = Boolean.parseBoolean(scan
						.next());
			}
			if (scan.hasNext()) {
				UserSettings.rotation_type_GPS = Boolean.parseBoolean(scan
						.next());
			}
			UserSettings.DATA_SEND_TIME = progress
					+ GlobalConstants.MIN_DATA_SEND_TIME;

			saveSettings();
			scan.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean invalidLogin() {
		String pName = getLastPlayerLogin();
		if (pName.equals("")
				|| (UserSettings.hashedPW == null)
				|| (UserSettings.hashedPW == "")
				|| (!(LoginAuthenticate.authenticate(context, pName,
						UserSettings.hashedPW, false)))) {
			// updateUsernames();
			return true;
		}
		return false;
	}

	private static String getLastPlayerLogin() {
		playerName = "";

		String URL = context.getFilesDir().getPath().toString() + "/player.set";
		try {
			File file = new File(URL);
			if (!file.exists()) {
				return playerName;
			} else {
				FileInputStream inputStream = new FileInputStream(new File(URL));
				BufferedReader buffer = new BufferedReader(
						new InputStreamReader(inputStream));
				playerName = buffer.readLine();
				buffer.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		if (playerName == null)
			return "";
		return playerName;
	}

	public static void loadUserData(AsyncLoadingCallback callback) {
		loadUserData(getLastPlayerLogin(), callback);
	}

	public static void loadUserData(String userName, AsyncLoadingCallback callback) {
		if (userData == null) {
			userData = new UserData(userName);
		}
		new AsyncLoadingUserData(callback).execute();
	}

}
