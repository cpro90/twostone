package de.tu.darmstadt.uhg.xml;

public class ShortParsedFile {
	
	String title;
	String description;
	String pos;
	String user;
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPos() {
		return pos;
	}
	
	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public ShortParsedFile(String title, String description, String pos, String user) {
		this.title = title;
		this.description = description;
		this.pos = pos;
		this.user = user;
	}

	

}
