package de.tu.darmstadt.uhg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import de.tu.darmstadt.uhg.adapter.AvailableMapsArrayAdapter;
import de.tu.darmstadt.uhg.adapter.ChatAdapter;
import de.tu.darmstadt.uhg.adapter.DrawerListItemArrayAdapter;
import de.tu.darmstadt.uhg.adapter.EinstellungAdapter;
import de.tu.darmstadt.uhg.adapter.FilterSpinnerAdapter;
import de.tu.darmstadt.uhg.adapter.HighscoreArrayAdapter;
import de.tu.darmstadt.uhg.dialogs.PromptDialog;
import de.tu.darmstadt.uhg.game.AugmentedReality;
import de.tu.darmstadt.uhg.game.Highscore;
import de.tu.darmstadt.uhg.game.LocalScore;
import de.tu.darmstadt.uhg.game.PlayActivity;
import de.tu.darmstadt.uhg.game.Game;
import de.tu.darmstadt.uhg.game.SimpleEnemy;
import de.tu.darmstadt.uhg.gps.GPSLocation;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.gps.GPSProgressing;
import de.tu.darmstadt.uhg.leveleditor.LevelEditorActivity;
import de.tu.darmstadt.uhg.leveleditor.MapActivity;
import de.tu.darmstadt.uhg.leveleditor.ServerProgressing;
import de.tu.darmstadt.uhg.progressing.BitmapProgressing;
import de.tu.darmstadt.uhg.progressing.MapRating;
import de.tu.darmstadt.uhg.progressing.UserData;
import de.tu.darmstadt.uhg.xml.ParsedFile;
import de.tu.darmstadt.uhg.xml.ShortParsedFile;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * wird zuerst ausgef�hrt. Hier wird der Programmablauf gesteuert.
 * 
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 * @author Gerhard S�ckel yx68ijoq@stud.tu-darmstadt.de
 * 
 */
@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity {

	public String picPath;

	public static MainActivity mainActivity;
	public static MediaPlayer sound;
	public static MediaPlayer music;
	public static boolean scoreListDeleted = true;
	public static String dataDir;
	public static ListView availableMapsList;
	/** Variablen zum speichern der Koordinaten **/
	public static GPSPoint currentPosition = new GPSPoint(0, 0);
	double maxDifLatitude = 0;
	double maxDifLongitude = 0;
	boolean initial = true;
	static boolean loginSuccess = false; // True when a player is logged in
	static Map map;
	static Highscore highscore;
	public static Player player;
	static Menu mainMenu;
	private static long back_pressed; // last time the back button was pressed
	static ListView drawerListView;
	static DrawerLayout drawerLayout;
	static RelativeLayout theVisibleLayout;
	static boolean staticGPS = false;
	static boolean correctGPS = false;
	static double currentTime = 0;
	static ArrayList<ParsedFile> myParsedLocations;
	public static String choosenLocationFromList = "";
	public static String filterSelection = GlobalConstants.FILTER_MAP_LOCAL;
	private int quickplayTryCounter = 5; // Anzahl der versuche Quickplay zu starten

	static ArrayList<LocationResource> availableLocs;
	static GPSLocation currentLoc;
	static boolean flag_disclaimer = false;

	/** Opponents **/
	public static ArrayList<SimpleEnemy> myEnemies = new ArrayList<SimpleEnemy>();
	static ArrayList<String> scoreEntries = new ArrayList<String>();
	static ArrayList<String> scoreEntriesPGG = new ArrayList<String>();
	static ArrayList<String> scoreEntriesGBP = new ArrayList<String>();
	static ArrayList<String> scoreEntriesLUI = new ArrayList<String>();
	static ArrayList<String> scoreEntriesCS = new ArrayList<String>();
	static EinstellungAdapter opponentAdapter;
	static ArrayAdapter<String> highscoreAdapter;
	public static UserLogStats userLogStats;

	/** Locations **/
	static ArrayList<RelativeLayout> lastScreen = new ArrayList<RelativeLayout>();
	static boolean gpsTurnedOn = false;
	static double lastGPSactive;
	public static boolean GPSactivated = false;
	static GPSLocation emptyLocation = new GPSLocation("nowhere", new GPSPoint(
			0, 0), new GPSPoint(0, 0), false);

	/** Direction **/
	static float direction;
	static float directionGPS;
	static SensorManager sensorService;
	static SensorManager sensorStepCounterService;
	Sensor sensor; // TODO: auslagern
	Sensor stepCounterSensor;

	/** Game **/
	Game game;
	static boolean gameActive = false;
	static Bitmap foregroundMutable;
	public static AugmentedReality augmentedReality;

	public static Camera camera;

	/** Timer **/
	public static Timer t;
	public static TimerTask tTask;

	Timer allTasksTimer;
	TimerTask allTasks;

	/** For debugging **/
	static ArrayList<GPSPoint> averageLocation = new ArrayList<GPSPoint>();
	static int avgCount = 0;
	static int avgStaticCount = 0;
	static long lastDistance = 0;
	static double avgTolerance = 0.0000001;
	static GPSPoint lastAvg = new GPSPoint(0, 0);

	// ZoomLevel
	static boolean userScreenVisible = true;

	public static boolean establishedConnection;
	public static ArrayList<Bitmap> augmentedRealityImage;

	// LEVELS
	ArrayList<Level> level;

	public GPSPoint getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(GPSPoint currentPosition) {
		MainActivity.currentPosition = currentPosition;
	}

	/** Alles Ausf�hren, bevor das Spiel definitiv beendet wird! **/
	private void canFinish() {
		UserSettings.userData.updateUserData();
		saveSettings();
		if (camera != null) {// TODO: auslagern
			camera.release(); // release the camera for other applications
			camera = null;
		}
		finish();
		System.exit(0);

	}

	public void selectLevelByName(String levelName, Context context) {
		choosenLocationFromList = levelName;
		updateAllAvailableMaps(context);

		findViewById(R.id.mapsBottomPanel).setVisibility(View.VISIBLE);

		boolean local = levelExists(levelName);
		if (local) {
			findViewById(R.id.downloadMapSelectPanel).setVisibility(
					View.INVISIBLE);
			findViewById(R.id.mapsLocalButtonPanel).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.downloadMapSelectPanel).setVisibility(
					View.VISIBLE);
			findViewById(R.id.mapsLocalButtonPanel).setVisibility(
					View.INVISIBLE);
		}
	}

	public static double getLastLatitude() {
		return currentPosition.getLatitude();
	}

	public static double getLastLongitude() {
		return currentPosition.getLongitude();
	}

	/** Entfernt das Keyboard nach abgeschlossener Eingabe **/
	public void hideKeyboard(EditText edittext) {
		InputMethodManager inMethod = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inMethod.hideSoftInputFromWindow(edittext.getApplicationWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	/** Updated die Einstellungen **/
	public void updateSettings() {
		final ImageButton checkLogin = (ImageButton) findViewById(R.id.options_button_autologin);
		checkLogin
				.setImageResource(UserSettings.autoLoginEnabled ? R.drawable.button_options_on
						: R.drawable.button_options_off);
		final ImageButton einstellungen_music = (ImageButton) findViewById(R.id.options_button_music);
		einstellungen_music
				.setImageResource(UserSettings.musicEnabled ? R.drawable.button_options_on
						: R.drawable.button_options_off);
		final ImageButton einstellungen_vibration = (ImageButton) findViewById(R.id.options_button_vibration);
		einstellungen_vibration
				.setImageResource(UserSettings.vibrationEnabled ? R.drawable.button_options_on
						: R.drawable.button_options_off);
		final ImageButton einstellungen_camera = (ImageButton) findViewById(R.id.options_button_camera);
		einstellungen_camera
				.setImageResource(UserSettings.cameraEnabled ? R.drawable.button_options_on
						: R.drawable.button_options_off);
		final ImageButton einstellungen_switchView = (ImageButton) findViewById(R.id.options_button_switchAR);
		einstellungen_switchView
				.setImageResource(UserSettings.switchViewEnabled ? R.drawable.button_options_on
						: R.drawable.button_options_off);
		final ToggleButton einstellungen_rotation_type = (ToggleButton) findViewById(R.id.options_button_rotation_type);
		einstellungen_rotation_type
				.setChecked(UserSettings.rotation_type_GPS);
		final ImageButton einstellungen_rotation = (ImageButton) findViewById(R.id.options_button_rotation);
		einstellungen_rotation
				.setImageResource(UserSettings.rotationEnabled ? R.drawable.button_options_on
						: R.drawable.button_options_off);
		((LinearLayout)findViewById(R.id.options_linear_rotation_type_panel)).setVisibility(UserSettings.rotationEnabled? View.VISIBLE : View.GONE);
		final ImageButton einstellungen_gender = (ImageButton) findViewById(R.id.options_button_gender);
		einstellungen_gender
				.setImageResource(UserSettings.gender ? R.drawable.button_male
						: R.drawable.button_female);
		final TextView einstellungen_weight_text = (TextView) findViewById(R.id.options_text_weight_count);
		einstellungen_weight_text.setText(UserSettings.weight + "");
		final SeekBar einstellungen_weight_seekbar = (SeekBar) findViewById(R.id.options_seek_weight);
		einstellungen_weight_seekbar.setProgress(UserSettings.weight
				- GlobalConstants.MIN_WEIGHT);
		final TextView einstellungen_height_text = (TextView) findViewById(R.id.options_text_height_count);
		einstellungen_height_text.setText(UserSettings.height + "");
		final SeekBar einstellungen_height_seekbar = (SeekBar) findViewById(R.id.options_seek_height);
		einstellungen_height_seekbar.setProgress(UserSettings.height
				- GlobalConstants.MIN_HEIGHT);
		final TextView einstellungen_age_text = (TextView) findViewById(R.id.options_text_age_count);
		einstellungen_age_text.setText(UserSettings.age + "");
		final SeekBar einstellungen_age_seekbar = (SeekBar) findViewById(R.id.options_seek_age);
		einstellungen_age_seekbar.setProgress(UserSettings.age
				- GlobalConstants.MIN_AGE);
	}

	/** Updated das Profil und die Statistiken **/
	public void updateProfile() {
		if (UserSettings.userData == null)
			return;

		final TextView profil_points = (TextView) findViewById(R.id.profile_text_points);
		final TextView profil_name = (TextView) findViewById(R.id.profile_text_username);
		final TextView profil_speed = (TextView) findViewById(R.id.profile_text_speed);
		final TextView profil_time = (TextView) findViewById(R.id.profile_text_time);
		final TextView profil_distance = (TextView) findViewById(R.id.profile_text_distance);
		final ImageView profilPicture = (ImageView) findViewById(R.id.profile_picture);

		profilPicture.setImageBitmap(BitmapFactory.decodeResource(
				getResources(), R.drawable.ic_launcher));

		profilPicture.setImageBitmap(UserSettings.userData.getPicture());
		profil_name.setText(player.getUsername());
		profil_points.setText(UserSettings.userData.getExp() + "");

		long distTemp = Math.round(UserSettings.userData.getDistance());

		if (distTemp < 1000)
			profil_distance.setText(distTemp + " m");
		else
			profil_distance.setText(((double) (Math.round(distTemp / 10)))
					/ 100 + " km");

		profil_speed
				.setText(""
						+ ((int) Math
								.round((((double) distTemp) / ((double) UserSettings.userData
										.getPlaytime())) * 36) / 10) + " km/h");

		if (UserSettings.userData.getPlaytime() < 600) {
			profil_time.setText(Math.round(UserSettings.userData.getPlaytime())
					+ " " + getResources().getString(R.string.seconds));
		} else {
			if (UserSettings.userData.getPlaytime() < 3600) {
				profil_time.setText(Math.round(UserSettings.userData
						.getPlaytime() / 60) + " min");
			} else {
				profil_time.setText((((double) Math.round(UserSettings.userData
						.getPlaytime() / 360)) / 10) + " h");
			}
		}
	}

	public void logoutPlayer() {
		savePlayerLogin("");
		UserSettings.hashedPW = "";
		UserSettings.userData = null;
		userLogStats = null;
	}

	public void saveUserData() {
		if (UserSettings.userData != null) {
			// userData.updateUserData();
			new AsyncUpdateUserData(this).execute();
		}
	}

	// Small notification
	public void notificationProfile() {
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setMessage(getResources().getString(R.string.textProfileUpdate))
				.setTitle(getResources().getString(R.string.infoProfileUpdate));

		builder.setPositiveButton(getResources().getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					}
				});

		builder.show();
	}

	public void savePlayerLogin(String playername) {
		String URL = getFilesDir().getPath().toString() + "/player.set";
		try {
			FileOutputStream output = new FileOutputStream(new File(URL));
			output.write(playername.getBytes()); // Spielername speichern
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getLastPlayerLogin() {
		String playname = "";

		String URL = getFilesDir().getPath().toString() + "/player.set";
		try {
			File file = new File(URL);
			if (!file.exists()) {
				return playname;
			} else {
				FileInputStream inputStream = new FileInputStream(new File(URL));
				BufferedReader buffer = new BufferedReader(
						new InputStreamReader(inputStream));
				playname = buffer.readLine();
				buffer.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		if (playname == null)
			return "";
		return playname;
	}

	/** Speichert die aktuellen Einstellungen **/
	public void saveSettings() {
		String URL = getFilesDir().getPath().toString() + "/game.set";
		try {
			FileOutputStream output = new FileOutputStream(new File(URL));
			output.write((UserSettings.GAME_ID + ",").getBytes()); // Spielername
																	// speichern
			output.write((UserSettings.autoLoginEnabled + ",").getBytes()); // Spielername
			// speichern
			output.write((UserSettings.musicEnabled + ",").getBytes()); // Spielername
			// speichern
			output.write((UserSettings.vibrationEnabled + ",").getBytes()); // Spielername
			// speichern
			output.write((UserSettings.DATA_SEND_TIME
					- GlobalConstants.MIN_DATA_SEND_TIME + ",").getBytes()); // Spielername
			// speichern^
			output.write((0 + ",").getBytes());
			output.write((0 + ",").getBytes());
			output.write((LevelEditorActivity.MIN_DISTANCE + ",").getBytes());
			output.write((UserSettings.cameraEnabled + ",").getBytes());
			output.write((UserSettings.switchViewEnabled + ",").getBytes());
			output.write((UserSettings.hashedPW + ",").getBytes());
			output.write((UserSettings.rotationEnabled + ",").getBytes());
			output.write((UserSettings.rotation_type_GPS + ",").getBytes());
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Ausgabe bei Verbindungsfehler zum Server **/
	public void serverError() {
		Toast.makeText(MainActivity.this,
				getResources().getString(R.string.serverError),
				Toast.LENGTH_SHORT).show();
	}

	/**
	 * Entfernt z.B. geladene Bitmaps wieder, damit die VM mehr Speicher zur
	 * Verf�gung hat. TODO: Methode entfernen, wenn App stabil l�uft
	 * */
	public void getFreeMemory() {
		if (map != null) {
			map.bitmap = null;
			map.foreground = null;
			map.originalBitmap = null;
			map = null;
		}
		foregroundMutable = null;
		game = null;
		currentLoc = null;
		/**
		 * final ImageView myFullscreenMap = (ImageView)
		 * findViewById(R.id.MyFullscreenMap);
		 * myFullscreenMap.destroyDrawingCache();
		 **/
		System.gc();
	}

	public static LocalScore localScore;

	/** L�dt die letzten Scores */
	public void loadLocalScores() {
		String URL = getFilesDir().getPath().toString() + "/scores.set";
		try {
			localScore = new LocalScore();
			File file = new File(URL);
			if (!file.exists()) {
				try {
					FileOutputStream output = new FileOutputStream(
							new File(URL));
					output.write(("dummy,0,").getBytes());
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			FileInputStream inputStream = new FileInputStream(new File(URL));
			BufferedReader buffer = new BufferedReader(new InputStreamReader(
					inputStream));
			Scanner scan = new Scanner(buffer.readLine());
			buffer.close();
			scan.useDelimiter(",");
			while (scan.hasNext()) {
				localScore.setScore(scan.next(), Integer.parseInt(scan.next()));
			}
			scan.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveLocalScores() {
		String URL = getFilesDir().getPath().toString() + "/scores.set";
		try {
			FileOutputStream output = new FileOutputStream(new File(URL));
			for (int i = 0; i < localScore.getMapName().size(); i++) {
				output.write((localScore.getMapName().get(i) + ",").getBytes());
				output.write((localScore.getMapScore().get(i) + ",").getBytes());
			}
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** L�dt die letzten Einstellungen + Spielerliste */
	public void loadSettings() {
		UserSettings.context = this;
		loadLocalScores();
		String URL = getFilesDir().getPath().toString() + "/game.set";

		File file = new File(URL);
		if (!file.exists()) {
			// Hier FirstStart Menu! Spiel noch nie gespeichert!			
			 TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	         String IMEI =  tm.getDeviceId();    
	         UserSettings.GAME_ID = Long.parseLong(IMEI);			
			
			player = new Player("x");
			// DISCLAIMER
			showScreen((RelativeLayout) findViewById(R.id.disclaimerView),
					false);
		} else {
			UserSettings.loadSettings();
		}

		updateSwitchView(UserSettings.switchViewEnabled); // This does the magic
		// #Bugfix

		if (UserSettings.invalidLogin()) {
			showScreen((RelativeLayout) findViewById(R.id.loginView), false);
			return;
		}

		try{
		player = new Player(UserSettings.playerName);
		UserSettings.userData.setStartdate(LoginAuthenticate.regDate);
		userLogStats = new UserLogStats();
		userLogStats.init(UserSettings.userData);
		} catch(NullPointerException npe){
			Toast.makeText(this, "NullPointerException: userData == null", Toast.LENGTH_LONG).show();
			finish();
		}

		// 14.8.2016 no user data, due to privacy policy
//		if ((UserSettings.age == 0) || (UserSettings.height == 0)
//				|| (UserSettings.weight == 0))
//			notificationProfile();

		updateSettings();

		// updateProfile();

		if (UserSettings.autoLoginEnabled) {
			showScreen((RelativeLayout) findViewById(R.id.startViewNew), false);
			loginSuccess = true;
			Toast.makeText(
					MainActivity.this,
					getResources().getString(R.string.welcomeBack) + ", "
							+ player.getUsername(), Toast.LENGTH_LONG).show();
		} else {
			loginSuccess = false;
			showScreen((RelativeLayout) findViewById(R.id.loginView), false);
		}

	}

	/** Aktualisiert die aktuelle Highscore und speichert sie **/
	private void updateHighscore() {
		highscore.reloadOnlineHighscore();
		scoreEntries = highscore.getAllEntries();
		ListView hscoreGlobal = (ListView) findViewById(R.id.HighscoreListGlobal);
		highscoreAdapter = new HighscoreArrayAdapter(this, scoreEntries);
		hscoreGlobal.setAdapter(highscoreAdapter);
		highscoreAdapter.notifyDataSetChanged();

	}

	private static String lastMessage = "";

	/** L�dt alle aktuellen (die letzten 30) Nachrichten aus dem Chatverlauf **/
	private void updateMessages() {
		ArrayList<String> chatList = Chat.getInvertedMessages();
		// Change when lastMessage differs
		if ((chatList != null) && (chatList.size() > 0)) {
			if (!lastMessage.equalsIgnoreCase(chatList.get(chatList.size() - 1)
					.toString())) {
				lastMessage = chatList.get(chatList.size() - 1).toString();
				final ListView messageList = (ListView) findViewById(R.id.messageList);
				ChatAdapter msgs = new ChatAdapter(MainActivity.this, chatList);
				messageList.setAdapter(msgs);
				msgs.notifyDataSetChanged();
			}
		}
	}

	/** L�dt die registrierten User auf dem Handy **/
	private void updateUsernames() {
		/**
		 * final ListView usernameList = (ListView)
		 * findViewById(R.id.usernameList); ArrayAdapter<String> msgs = new
		 * ArrayAdapter<String>(MainActivity.this,
		 * android.R.layout.simple_list_item_1, registeredPlayers);
		 * usernameList.setAdapter(msgs); msgs.notifyDataSetChanged();
		 **/
	}

	private void newUserSubmit() {
		final Context c = this;
		PromptDialog dlg = new PromptDialog(c, R.string.username,
				R.string.usernameHint, false) {
			@Override
			public boolean onOkClicked(String input) {
				final String username = input;

				if (username.length() < 1) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.noUsername),
							Toast.LENGTH_SHORT).show();
					return false;
				}

				if (username.length() < 3) {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(R.string.usernameTooShort),
							Toast.LENGTH_SHORT).show();
					return false;
				}

				if (username.length() > 20) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.usernameTooLong),
							Toast.LENGTH_SHORT).show();
					return false;
				}

				if (containsIllegalLetter(username)) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.illegalLetter),
							Toast.LENGTH_SHORT).show();
					return false;
				}

				if (LoginAuthenticate.userExists(MainActivity.this, username)) {

					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.usernameExists),
							Toast.LENGTH_SHORT).show();
					return false;
				}

				// PW 1
				PromptDialog dlg2 = new PromptDialog(c, R.string.username,
						R.string.usernameHint, true) {
					@Override
					public boolean onOkClicked(String input) {
						final String password = input;
						if (input.length() < 4) {

							Toast.makeText(
									MainActivity.this,
									getResources().getString(
											R.string.passwordTooShort,
											Toast.LENGTH_SHORT),
									Toast.LENGTH_SHORT).show();

							return true;
						}

						// PW 2
						PromptDialog dlg3 = new PromptDialog(c,
								R.string.username, R.string.usernameHint, true) {
							@Override
							public boolean onOkClicked(String input) {
								if ((input.equals(password))) {
									LoginAuthenticate.registerUser(username,
											password);

									Toast.makeText(
											MainActivity.this,
											getResources().getString(
													R.string.accountSuccess,
													Toast.LENGTH_SHORT),
											Toast.LENGTH_SHORT).show();

									final EditText usernameInput = (EditText) findViewById(R.id.usernameEdit);
									final EditText passwordInput = (EditText) findViewById(R.id.passwordEdit);
									usernameInput.setText(username);
									passwordInput.setText(password);
								} else {

									Toast.makeText(
											MainActivity.this,
											getResources().getString(
													R.string.passwordWrong,
													Toast.LENGTH_SHORT),
											Toast.LENGTH_SHORT).show();
								}

								return true;
							}
						};
						dlg3.setTitle(getResources().getString(
								R.string.passwordTitle));
						dlg3.getInput().setText("");
						dlg3.setMessage(getResources().getString(
								R.string.passwordInsertAgain));
						dlg3.show();

						return true;
					}
				};
				dlg2.setTitle(getResources().getString(R.string.passwordTitle));
				dlg2.setMessage(getResources().getString(
						R.string.passwordInsert));
				dlg2.getInput().setText("");
				dlg2.show();

				return true;
			}
		};
		dlg.setTitle(getResources().getString(R.string.usernameTitle));
		dlg.setMessage(getResources().getString(R.string.usernameInsert));
		dlg.getInput().setText("");
		dlg.show();
	}

	/**
	 * wenn der Nutzer bereits auf dem Handy registriert ist steht "login" beim
	 * anmelden, ansonsten "registrieren"
	 **/
	private void checkUserRegistered() {
		/**
		 * final EditText username = (EditText) findViewById(R.id.usernameEdit);
		 * final Button loginLabel = (Button) findViewById(R.id.usernameLogin);
		 * if (registeredPlayers.contains(username.getText() + "")) {
		 * loginLabel.setText(getResources().getString(R.string.login)); } else
		 * { loginLabel.setText(getResources().getString(R.string.register)); }
		 **/
	}

	/**
	 * L�dt eine Location aus den gespeicherten Resourcen*
	 */
	public static ParsedFile loadLocation(String locName) {
		for (int i = 0; i < availableLocs.size(); i++)
			if (locName
					.equalsIgnoreCase(availableLocs.get(i).getLocationName())) {
				return myParsedLocations.get(i);
			}
		return null; // If no location is found!
		// Cannot be called, because of prepared Statements!
	}

	/** L�dt den Ladebildschirm **/
	public void accessLoadScreen() {
		Intent loadingIntent = new Intent(MainActivity.this, Splash.class);
		startActivity(loadingIntent);
	}

	/**
	 * Gibt den Datei-Speicher-Pfad einer Location zur�ck
	 */
	private LocationResource getLocationPath(String locName) {
		for (int i = 0; i < availableLocs.size(); i++)
			if (locName
					.equalsIgnoreCase(availableLocs.get(i).getLocationName())) {
				return availableLocs.get(i);
			}
		return null; // If no location is found!
	}

	/**
	 * L�dt alle Locations aus den gespeicherten Resourcen vor*
	 */
	public static void preloadLocations(Context context) {
		MapFragment.points.clear();
		myParsedLocations = new ArrayList<ParsedFile>();
		for (int i = 0; i < availableLocs.size(); i++) {
			myParsedLocations.add(new ParsedFile(availableLocs.get(i)
					.getLocationPath()));
		}
	}

	/** L�scht die aktuell ausgew�hlte Karte vom Server **/
	public void deleteCurrentMapFromServer() {
		// Wenn Nutzer berechtigt, dann L�sche karte, ansonsten Fehler!
		if (ServerProgressing.isMapOwner(player.getUsername(),
				choosenLocationFromList)) {

			AlertDialog.Builder builder = new AlertDialog.Builder(
					MainActivity.this);
			builder.setMessage(
					getResources().getString(R.string.deleteServerPrompt))
					.setTitle(
							getResources()
									.getString(R.string.deleteServerTitle));

			builder.setPositiveButton(getResources().getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							ServerProgressing.deleteMapFromServer(
									player.getUsername(),
									choosenLocationFromList);
							ParsedFile file = loadLocation(choosenLocationFromList);
							file.setOnline(false);
							saveXMLFile(file);
							updateAllAvailableMaps(MainActivity.this);
							Toast.makeText(
									MainActivity.this,
									getResources().getString(
											R.string.deleteServerSuccess),
									Toast.LENGTH_SHORT).show();
						}
					});

			builder.setNegativeButton(
					getResources().getString(R.string.cancel),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
						}
					});

			Dialog dialog = builder.create();
			dialog.show();

		} else {
			Toast.makeText(MainActivity.this,
					getResources().getString(R.string.deleteServerNotPossible),
					Toast.LENGTH_SHORT).show();
		}
	}

	/** L�dt die aktuell ausgew�hlte Karte auf den Server **/
	public void uploadCurrentMap() {
		ParsedFile file = loadLocation(choosenLocationFromList);
		if (file != null) {

			if (ServerProgressing.onlineMapExists(file.getTitle())) {
				// Karte ist bereits online
				Toast.makeText(MainActivity.this,
						getResources().getString(R.string.mapExists),
						Toast.LENGTH_SHORT).show();
				return;
			}

			file.setOnline(true);
			saveXMLFile(file);

			if (ServerProgressing.uploadMap(player.getUsername(), file)) {
				Toast.makeText(MainActivity.this,
						getResources().getString(R.string.uploadSuccess),
						Toast.LENGTH_SHORT).show();
				updateAllAvailableMaps(this);
			} else {
				Toast.makeText(MainActivity.this,
						getResources().getString(R.string.uploadFail),
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	/** XML Datei �berschreiben mit neuen Werten **/
	private void saveXMLFile(ParsedFile file) {
		if (file != null)
			file.save(this);
	}

	private GPSLocation getCurrentLocation() {
		return currentLoc;
	}

	/** true, wenn ein illegaler Buchstabe im input-String ist **/
	public static boolean containsIllegalLetter(String input) {
		return ((input.contains(",")) || (input.contains(";"))
				|| (input.contains("=")) || (input.contains("."))
				|| (input.contains("/")) || (input.contains("\"")) || (input.contains("'"))
				|| (input.contains("#")) || (input.contains("�"))
				|| (input.contains("<")) || (input.contains(">"))
				|| (input.contains("&")) || (input.contains("`"))
				|| (input.contains("\\")) || (input.contains("?"))
				|| (input.contains("�")) || (input.contains("�"))
				|| (input.contains("�")) || (input.contains("�"))
				|| (input.contains("�")) || (input.contains("�"))
				|| (input.contains("�")) || (input.contains("�"))
				|| (input.contains("!")) || (input.contains("~"))
				|| (input.contains(":")) || (input.contains("+")));
	}

	/**
	 * F�gt, basierend auf den n-ten Eintrag, ein Item sortiert einer ArrayList
	 * hinzu
	 **/
	public static ArrayList<String[]> addSorted(ArrayList<String[]> array,
			String[] item, int n) {
		double distance = Double.parseDouble(item[n]);
		for (int i = 0; i < array.size(); i++) {
			double current = Double.parseDouble(array.get(i)[n]);
			if (distance < current) {
				array.add(i, item);
				return array;
			}
		}
		array.add(item);
		return array;
	}

	/**
	 * Liest alle verf�gbaren Karten aus und gibt diese in der ListView incl.
	 * Entfernung wieder.
	 **/
	public static void updateAllAvailableMaps(Context context) {

		ArrayList<String[]> localMapsList = null;
		if (filterSelection.equals(GlobalConstants.FILTER_MAP_ALL)
				|| filterSelection.equals(GlobalConstants.FILTER_MAP_LOCAL)) {
			localMapsList = updateLocalAvailableMaps(context);
		}
		if (filterSelection.equals(GlobalConstants.FILTER_MAP_ALL)
				|| filterSelection.equals(GlobalConstants.FILTER_MAP_ONLINE)) {
			new AsyncLoadingOnlineMaps(context, localMapsList).execute();
		}

		if (filterSelection.equals(GlobalConstants.FILTER_MAP_LOCAL)) {
			// Only local Maps. Don't have to wait for server
			updateMapsAdapter(context, localMapsList);
		} else {
			// wait for online Maps server query to call
			// updateOnlineAvailableMaps
		}
	}

	public static ArrayList<String[]> updateLocalAvailableMaps(Context context) {
		// Iteration �ber alle LOKALEN Spielfelder
		if (filterSelection.equals(GlobalConstants.FILTER_MAP_ALL)
				|| filterSelection.equals(GlobalConstants.FILTER_MAP_LOCAL)) {
			ArrayList<String[]> tempArray = new ArrayList<String[]>();
			for (int i = 0; i < availableLocs.size(); i++) {
				// SKIP die Autosave-File
				if (availableLocs.get(i).getLocationName()
						.equalsIgnoreCase("autosave"))
					continue;

				// Die Liste wird mit 3 Eintr�gen gef�llt (Name, Entfernung,
				// Beschreibung)
				String[] tempString = new String[6];
				ParsedFile loadedLocation = loadLocation(availableLocs.get(i)
						.getLocationName());
				tempString[0] = availableLocs.get(i).getLocationName();

				if (GPSactivated) {
					// Wenn GPS aktiviert ist, berechne die Entfernung zum
					// entsprechenden Spielfeld
					GPSPoint markerPos = MapFragment
							.getMarkerPosition(availableLocs.get(i)
									.getLocationName());
					double dist = GPSProgressing.getWayDifference(
							currentPosition, markerPos);
					availableLocs.get(i).setDistance(dist);
					tempString[1] = Math.round(dist) + "";
				} else {
					// Ansonsten ist das Spielfeld "unendlich" weit weg: Wert <
					// 0
					tempString[1] = "-1";
				}
				tempString[2] = loadedLocation.getDescription();
				tempString[3] = loadedLocation.isOnline() + "";
				tempString[4] = loadedLocation.getDistance() + "";
				tempArray = addSorted(tempArray, tempString, 1);
			}
			return tempArray;
		}
		return null;
	}

	public static void updateOnlineAvailableMaps(Context context,
			ArrayList<ShortParsedFile> avlMaps, ArrayList<String[]> tempArray) {
		// Update MapFragment online maps
		MapFragment.onlineMaps = avlMaps;
		((MapFragment) mainActivity.getSupportFragmentManager()
				.findFragmentById(R.id.map_fragment)).refreshAll();
		// Iteration �ber alle ONLINE Spielfelder
		if (filterSelection.equals(GlobalConstants.FILTER_MAP_ALL)
				|| filterSelection.equals(GlobalConstants.FILTER_MAP_ONLINE)) {
			for (int i = 0; i < avlMaps.size(); i++) {
				if (MainActivity.levelExists(avlMaps.get(i).getTitle()))
					continue;
				String[] tempString = new String[6];

				if (MainActivity.GPSactivated) {
					GPSPoint markerPos = new GPSPoint(avlMaps.get(i).getPos());
					double dist = GPSProgressing.getWayDifference(
							MainActivity.currentPosition, markerPos);
					tempString[1] = Math.round(dist) + "";
				} else {
					tempString[1] = "-1";
				}

				tempString[0] = avlMaps.get(i).getTitle();
				tempString[2] = "Owner: " + avlMaps.get(i).getUser() + "\n"
						+ avlMaps.get(i).getDescription();
				tempString[3] = "true";
				tempString[4] = "n/a";

				tempArray = addSorted(tempArray, tempString, 1);
			}
			updateMapsAdapter(context, tempArray);
		}
	}

	public void onQuickplayMapsAvailable(Context context,
			ArrayList<String[]> tempArray) {
		int nearestMapIndex = -1;
		double minDistance = Double.MAX_VALUE;
		for (int i = 0; i < tempArray.size(); i++) {
			double dist = Double.parseDouble(tempArray.get(i)[1]);
			if (dist < minDistance) {
				minDistance = dist;
				nearestMapIndex = i;
			}
		}
		if (nearestMapIndex == -1 || minDistance > 500) {
			Toast.makeText(this, getResources().getString(R.string.noNearMaps),
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (!MainActivity.levelExists(tempArray.get(nearestMapIndex)[0])) {
			// Download Map
			new AsyncLoadingQuickplayDownloadMap(this,
					tempArray.get(nearestMapIndex)[0]).execute();
		} else {
			// Local Map
			onQuickPlayMapSelected(tempArray.get(nearestMapIndex)[0]);
		}

	}

	public void onQuickPlayMapSelected(String quickPlayMapTitle) {
		selectLevelByName(quickPlayMapTitle, this);
		playChosenLocation();
	}

	private static void updateMapsAdapter(Context context,
			ArrayList<String[]> tempArray) {
		AvailableMapsArrayAdapter adapter;
		if (availableMapsList.getAdapter() == null) {
			// Bisher keine Liste erzeugt: Neu erzeugen!
			adapter = new AvailableMapsArrayAdapter(context, tempArray,
					localScore);
			availableMapsList.setAdapter(adapter);
		} else {
			// Es gibt bereits eine Liste: Updaten!
			adapter = (AvailableMapsArrayAdapter) availableMapsList
					.getAdapter();
			adapter.clear();
			adapter.addAll(tempArray);
		}
	}

	/** View a new visible RelativeLayout **/
	public void showScreen(RelativeLayout layout, boolean enableBackFunction) {
		if (theVisibleLayout != null) {
			if (enableBackFunction)
				lastScreen.add(theVisibleLayout);
			theVisibleLayout.setVisibility(RelativeLayout.INVISIBLE);
		}
		if (layout.getParent().equals(findViewById(R.id.menuView))) {
			findViewById(R.id.menuView).setVisibility(RelativeLayout.VISIBLE);
		} else {
			findViewById(R.id.menuView).setVisibility(RelativeLayout.INVISIBLE);
		}
		layout.setVisibility(RelativeLayout.VISIBLE);
		theVisibleLayout = layout;

		// Men� Eintr�ge bearbeiten
		menuItemsUpdate();
		drawerItemsUpdate();
		if (layout.getId() == R.id.loginView)
			((DrawerLayout) findViewById(R.id.drawer_layout))
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		else
			((DrawerLayout) findViewById(R.id.drawer_layout))
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		if (layout == (RelativeLayout) findViewById(R.id.mapSelectView)) {
			updateAllAvailableMaps(this);
		}

		if (layout == (RelativeLayout) findViewById(R.id.highscoreView)) {
			scoreListDeleted = false;
			updateHighscore();
		} else {
			if (!scoreListDeleted)
				deleteScoreLists();
		}
	}

	/** L�scht die Listeintr�ge der Scores **/
	private void deleteScoreLists() {
		scoreListDeleted = true;
		ListView hscoreGlobal = (ListView) findViewById(R.id.HighscoreListGlobal);
		hscoreGlobal.setAdapter(null);
	}

	private void createAllTimer() {

		allTasksTimer = new Timer();
		allTasks = new TimerTask() {
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (isVisibleScreen((RelativeLayout) findViewById(R.id.chatView))) {
							updateMessages();
						}

						if (isVisibleScreen((RelativeLayout) findViewById(R.id.startViewNew))) {
							showWarning(!hasInternetConnectivity());
						}
					}
				});
			}
		};
		allTasksTimer.schedule(allTasks, 0, GlobalConstants.REFRESH_RATE);
	}

	private void stopGameTimer() {
		t.cancel();
	}

	/**
	 * Passt das Men� an den ge�ffneten Screen an, f�gt. z.B. im Profil die
	 * Funktion "Profil l�schen" hinzu.
	 */
	private void menuItemsUpdate() {
		if (mainMenu != null) {
			MenuItem deleteProfile = mainMenu.findItem(R.id.deleteProfile);
			deleteProfile
					.setVisible(isVisibleScreen((RelativeLayout) findViewById(R.id.profilView)));
			// MenuItem exitGame = mainMenu.findItem(R.id.exitGame);
			// exitGame.setVisible(isVisibleScreen((RelativeLayout)
			// findViewById(R.id.mapFullscreenView)));
			MenuItem logoff = mainMenu.findItem(R.id.logout);
			logoff.setVisible(!(isVisibleScreen((RelativeLayout) findViewById(R.id.loginView)))
					&& !(isVisibleScreen((RelativeLayout) findViewById(R.id.disclaimerView)))
					// && !(isVisibleScreen((RelativeLayout)
					// findViewById(R.id.mapFullscreenView)))
					&& !(isVisibleScreen((RelativeLayout) findViewById(R.id.gameOverView)))
					&& !(isVisibleScreen((RelativeLayout) findViewById(R.id.timeOverView)))
					&& !(isVisibleScreen((RelativeLayout) findViewById(R.id.gameWonView))));
		}
	}

	private void drawerItemsUpdate() {
		DrawerListItemArrayAdapter drawerListItemAdapter;
		String[] drawerListItems;
		if (theVisibleLayout.getId() == R.id.startViewNew)
			drawerListItems = getResources().getStringArray(
					R.array.drawerListHome);
		else if (theVisibleLayout.getId() == R.id.playground)
			drawerListItems = getResources().getStringArray(
					R.array.drawerListPause);
		else
			drawerListItems = getResources().getStringArray(
					R.array.drawerListGeneral);
		;
		drawerListItemAdapter = new DrawerListItemArrayAdapter(
				MainActivity.this, drawerListItems);
		drawerListView.setAdapter(drawerListItemAdapter);

	}

	/** Returns true if RelativeLayout is visible **/
	public boolean isVisibleScreen(RelativeLayout layout) {
		return (theVisibleLayout == layout);
	}

	/** Bei click on Map die ZoomControl anzeigen/ausblenden **/
	/**
	 * private void clickedOnMap() { // TODO: Make an options button visible!!!
	 * userScreenVisible = !userScreenVisible; ZoomControls zC = (ZoomControls)
	 * findViewById(R.id.zoomControl); if (userScreenVisible) {
	 * zC.setVisibility(FrameLayout.VISIBLE); } else {
	 * zC.setVisibility(FrameLayout.INVISIBLE); } }
	 **/

	/**
	 * Returns true if player is in GPSLocation ** private boolean
	 * checkLocation(GPSLocation field) { return
	 * ((player.getPosition().getLatitude() >= field
	 * .getBottomLeftCoordinate().getLatitude()) &&
	 * (player.getPosition().getLatitude() <= field
	 * .getTopRightCoordinate().getLatitude()) &&
	 * (player.getPosition().getLongitude() >= field
	 * .getBottomLeftCoordinate().getLongitude()) && (player
	 * .getPosition().getLongitude() <= field.getTopRightCoordinate()
	 * .getLongitude())); }
	 **/

	/** Spielabschluss **/
	public void finishGame(final Player player, final Game game) {

		stopGameTimer();
		deleteBackEntries(); 
		
		UserSettings.userData.addTime(game.getTimePlayed() / 1000);
		UserSettings.userData.addExp(game.getPoints());
		userLogStats.logQuitGame(UserSettings.userData,  game);
		
		saveUserData();
	}

	private SensorEventListener rotationListener = new SensorEventListener() {

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			/**
			 * Set direction 0 = North, 90 = East 180 = South 270 = West
			 */

			direction = event.values[0];

			LevelEditorActivity.rotateCamera(direction);
		}
	};

	/** Gibt die Richtung zur�ck, in die das Smartphone zeigt **/
	public static float getDirection() {
		return direction;
	}
	
	/** Gibt die Richtung zur�ck, in der sich das Smartphone bewegt **/
	public static float getDirectionGPS() {
		return directionGPS;
	}

	/** Zur�ck-Button gedr�ckt **/
	@Override
	public void onBackPressed() {
		if (((DrawerLayout) findViewById(R.id.drawer_layout))
				.isDrawerOpen((ListView) findViewById(R.id.drawerListView)))
			((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawers();
		else if (lastScreen.size() > 0) {
			showScreen(lastScreen.get(lastScreen.size() - 1), false);
			lastScreen.remove(lastScreen.size() - 1);
		} else if (back_pressed + 2000 > System.currentTimeMillis()) {
			exit();
		} else {
			Toast.makeText(getBaseContext(), R.string.backPressed,
					Toast.LENGTH_SHORT).show();
			back_pressed = System.currentTimeMillis();
		}
	}

	/** Alle gespeicherten Screens entfernen **/
	public void deleteBackEntries() {
		lastScreen.clear();
	}

	public void playBackgroundMusic() {
		if (music != null)
			music.stop();
		
		music = MediaPlayer.create(this, R.raw.music);
		if (UserSettings.musicEnabled)
			music.start();
		music.setLooping(true);
	}

	public void stopBackgroundMusic() {
		music.stop();
	}

	/** Startet die GPS Unterst�tzung **/
	public void initializeGPS() {
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		LocationListener locationListener = new LocationListener() {

			@Override
			public void onLocationChanged(Location location) {
				if (!GPSactivated)
					GPSactivated = true;

				// Setze die aktuelle Position auf die empfangenen GPS-Daten
				currentPosition = new GPSPoint(location.getLatitude(),
						location.getLongitude());
				directionGPS = location.getBearing();

				if (isVisibleScreen((RelativeLayout) findViewById(R.id.mapSelectView))) {
					updateAllAvailableMaps(MainActivity.this);
				}
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};

		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 5000, 0, locationListener);
			// Hier reicht es, wenn alle 5sek. die Location geupdated wird
		}
	}

	/**
	 * Update the Switch View Settings for the AR
	 * 
	 * @added by Chris
	 **/
	private void updateSwitchView(boolean enabled) {
		if (augmentedReality != null)
			augmentedReality.setSwitchViewEnabled(enabled);
	}

	private void startChat() {
		if (!currentPosition.isIdentity()) {
			showScreen((RelativeLayout) findViewById(R.id.chatView), true);
		} else {
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.gpsinactive),
					Toast.LENGTH_SHORT).show();
		}
	}

	private SensorEventListener stepCountSensorEventListener = new SensorEventListener() {

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {

		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			// mStepOffset = event.values[0];
			if (UserSettings.userData != null) {
				UserSettings.userData.addStep();
				Log.e("UHG", "Steps: " + UserSettings.userData.getSteps());
			}
		}
	};

	protected void onResume() {
		super.onResume();

		Log.d("UHG", "Resume");
		playBackgroundMusic();

		sensorStepCounterService.registerListener(stepCountSensorEventListener,
				stepCounterSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}

	protected void onPause() {
		super.onPause();

		Log.d("UHG", "Pause");
		if (UserSettings.musicEnabled)
			if (music!=null)
				music.stop();
		
		sensorStepCounterService
				.unregisterListener(stepCountSensorEventListener);
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mainActivity = this;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/** Begin new Layout Area **/
		String[] drawerStrings;

		UserData.setStandardPicture(BitmapFactory.decodeResource(
				getResources(), R.drawable.ic_launcher));

		final ImageButton drawerButton = (ImageButton) findViewById(R.id.drawerButton);
		DrawerListener drawerListener;
		drawerStrings = getResources().getStringArray(R.array.drawerListHome);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerListView = (ListView) findViewById(R.id.drawerListView);

		augmentedReality = new AugmentedReality(this);

		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int width = displayMetrics.widthPixels;
		DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) drawerListView
				.getLayoutParams();
		params.width = (int) (width * 0.95f);
		drawerListView.setLayoutParams(params);

		drawerListView.setAdapter(new DrawerListItemArrayAdapter(
				MainActivity.this, drawerStrings));
		drawerListener = new DrawerListener() {

			@Override
			public void onDrawerStateChanged(int arg0) {
			}

			@Override
			public void onDrawerSlide(View arg0, float arg1) {
			}

			@Override
			public void onDrawerOpened(View arg0) {
				drawerButton.setImageDrawable(getResources().getDrawable(
						R.drawable.button_drawer_active));
				drawerButton.setBackgroundResource(R.drawable.bg_icon_active);
			}

			@Override
			public void onDrawerClosed(View arg0) {
				drawerButton.setImageDrawable(getResources().getDrawable(
						R.drawable.button_drawer));
				drawerButton.setBackgroundResource(R.drawable.bg_icon);
			}
		};
		drawerLayout.setDrawerListener(drawerListener);

		drawerButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!drawerLayout.isDrawerOpen(drawerListView)) {
					drawerLayout.openDrawer(drawerListView);
				}
			}
		});

		drawerListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				String screen = drawerListView.getItemAtPosition(position)
						.toString();

				if (screen.equalsIgnoreCase("level overview")) {
					showScreen(
							(RelativeLayout) findViewById(R.id.mapSelectView),
							true);
				}
				if (screen.equalsIgnoreCase("create level")) {
					Intent levelEditorIntent = new Intent(MainActivity.this,
							LevelEditorActivity.class);
					startActivityForResult(levelEditorIntent,
							GlobalConstants.ACTIVITY_EDITOR);
				}
				if (screen.equalsIgnoreCase("help")) {
					showScreen((RelativeLayout) findViewById(R.id.helpView),
							true);
				}
				if (screen.equalsIgnoreCase("options")) {
					showScreen(
							(RelativeLayout) findViewById(R.id.einstellungView),
							true);
				}
				if (screen.equalsIgnoreCase("home")) {
					showScreen(
							(RelativeLayout) findViewById(R.id.startViewNew),
							true);
				}
				if (screen.equalsIgnoreCase("header")) { // Header = Profile
					updateProfile();
					showScreen((RelativeLayout) findViewById(R.id.profilView),
							true);
				}
				if (screen.equalsIgnoreCase("profile")) { // Header = Profile
					updateProfile();
					showScreen((RelativeLayout) findViewById(R.id.profilView),
							true);
				}
				if (screen.equalsIgnoreCase("highscore")) {
					showScreen(
							(RelativeLayout) findViewById(R.id.highscoreView),
							true);
				}
				if (screen.equalsIgnoreCase("chat")) {
					startChat();
				}
				if (screen.equalsIgnoreCase("change user")) {
					logOut();
				}
				if (screen.equalsIgnoreCase("about")) {
					showScreen((RelativeLayout) findViewById(R.id.aboutView),
							true);
				}
				if (screen.equalsIgnoreCase("exit")) {
					exit();
				}
				// drawerListView.setItemChecked(position, true);
				drawerLayout.closeDrawer(drawerListView);
			}
		});
		/** new Layout Area END **/

		final Button deleteProfile = (Button) findViewById(R.id.profile_button_delete);
		deleteProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				profileDeleter();
			}
		});

		availableMapsList = (ListView) findViewById(R.id.availableMapsList);

		// Stretch Highscore View to Screen Width
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		highscore = new Highscore();
		loadSettings();
		playBackgroundMusic();
		loadXMLLevelFiles(this);
		preloadLocations(this);
		initializeGPS();

		ListView opponentList = (ListView) findViewById(R.id.opponentList);
		ListView hscore = (ListView) findViewById(R.id.HighscoreListGlobal);
		// final ListView usernameList = (ListView)
		// findViewById(R.id.usernameList);
		final EditText usernameInput = (EditText) findViewById(R.id.usernameEdit);
		final EditText passwordInput = (EditText) findViewById(R.id.passwordEdit);

		usernameInput.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				checkUserRegistered();

			}

		});

		opponentAdapter = new EinstellungAdapter(this, myEnemies);
		highscoreAdapter = new HighscoreArrayAdapter(this, scoreEntries);

		opponentList.setAdapter(opponentAdapter);
		hscore.setAdapter(highscoreAdapter);

		sensorStepCounterService = (SensorManager) getSystemService(SENSOR_SERVICE);
		stepCounterSensor = sensorStepCounterService
				.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

		sensorService = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		// TODO: Orientation Sensor nur verwenden, falls kein AR vorhanden
		sensor = sensorService.getDefaultSensor(3); // 3 = Orientation Sensor
		createAllTimer();
		if (sensor != null) {
			sensorService.registerListener(rotationListener, sensor,
					SensorManager.SENSOR_DELAY_GAME);
		}

		// final ImageButton newGhost = (ImageButton)
		// findViewById(R.id.addNewOpponentButton);
		final ImageButton deleteAllEnemies = (ImageButton) findViewById(R.id.deleteAllOpponentButton);
		final ImageButton opponentButtonClose = (ImageButton) findViewById(R.id.opponent_button_close);
		final TextView opponentTextLevel = (TextView) findViewById(R.id.opponent_text_level);
		final Button disclaimerButtonAccept = (Button) findViewById(R.id.disclaimer_button_accept);
		final Button disclaimerButtonDecline = (Button) findViewById(R.id.disclaimer_button_decline);
		final Button quickplayButton = (Button) findViewById(R.id.button_quickplay);
		final Button singleplayerButton = (Button) findViewById(R.id.button_leveloverview);
		final Button levelEditButton = (Button) findViewById(R.id.button_leveledit);
		final Button optionsButton = (Button) findViewById(R.id.button_options);
		final Button profileButton = (Button) findViewById(R.id.button_profile);
		final Button highscoreButton = (Button) findViewById(R.id.button_highscore);
		final Button aboutButton = (Button) findViewById(R.id.button_about);
		final Button debugButton = (Button) findViewById(R.id.debugButton);
		final ImageButton chatButton = (ImageButton) findViewById(R.id.chatButton);
		final ImageButton hilfeButton = (ImageButton) findViewById(R.id.helpButton);
		final ImageButton helpButtonClose = (ImageButton) findViewById(R.id.help_button_close);
		final ImageButton helpButtonBack = (ImageButton) findViewById(R.id.help_button_back);
		final Button helpButtonThemeMain = (Button) findViewById(R.id.help_button_theme_main);
		final Button helpButtonThemeGameplay = (Button) findViewById(R.id.help_button_theme_gameplay);
		final Button helpButtonThemeEditor = (Button) findViewById(R.id.help_button_theme_editor);
		final ImageButton switchMapsView = (ImageButton) findViewById(R.id.switchMapsView);
		final Spinner filterMapsList = (Spinner) findViewById(R.id.filter_maps_list);
		final ImageButton ghostMapSelectButton = (ImageButton) findViewById(R.id.ghostMapSelectButton);
		final ImageButton mapStartModeButtonClose = (ImageButton) findViewById(R.id.map_start_mode_button_close);
		final Button mapStartModeButtonSingleplayer = (Button) findViewById(R.id.map_start_mode_button_singleplayer);
		final Button mapStartModeButtonMultiplayer = (Button) findViewById(R.id.map_start_mode_button_multiplayer);
		final Button loginButton = (Button) findViewById(R.id.usernameLogin);
		final Button register = (Button) findViewById(R.id.usernameRegister);
		final ImageButton playButton = (ImageButton) findViewById(R.id.playMapSelect);
		final ImageButton downloadMapButton = (ImageButton) findViewById(R.id.downloadMapSelect);
		final TextView message = (TextView) findViewById(R.id.message);
		final ImageButton chatButtonClose = (ImageButton) findViewById(R.id.chat_button_close);
		final ImageButton chatButtonSend = (ImageButton) findViewById(R.id.chat_button_send);
		final Button timeoverView = (Button) findViewById(R.id.button_timeover_home);
		final Button gameoverView = (Button) findViewById(R.id.button_gameover_home);
		final Button gamewonView = (Button) findViewById(R.id.button_gamewon_home);
		final ImageView profilePicture = (ImageView) findViewById(R.id.profile_picture);
		final ImageView profileReset = (ImageView) findViewById(R.id.profile_reset);
		final ImageButton checkLogin = (ImageButton) findViewById(R.id.options_button_autologin);
		final ImageButton einstellungen_music = (ImageButton) findViewById(R.id.options_button_music);
		final ImageButton map_settings = (ImageButton) findViewById(R.id.mapSettings);
		final ImageButton einstellungen_vibration = (ImageButton) findViewById(R.id.options_button_vibration);
		final ImageButton einstellungen_camera = (ImageButton) findViewById(R.id.options_button_camera);
		final ImageButton einstellungen_switchView = (ImageButton) findViewById(R.id.options_button_switchAR);
		final ImageButton einstellungen_rotation = (ImageButton) findViewById(R.id.options_button_rotation);
		final ToggleButton einstellungen_rotation_type = (ToggleButton) findViewById(R.id.options_button_rotation_type);
		final ImageButton einstellungen_gender = (ImageButton) findViewById(R.id.options_button_gender);
		final Button einstellungen_weight_change = (Button) findViewById(R.id.options_button_weight_change);
		final SeekBar einstellungen_weight_seekbar = (SeekBar) findViewById(R.id.options_seek_weight);
		final TextView einstellungen_weight_text = (TextView) findViewById(R.id.options_text_weight_count);
		final Button einstellungen_height_change = (Button) findViewById(R.id.options_button_height_change);
		final SeekBar einstellungen_height_seekbar = (SeekBar) findViewById(R.id.options_seek_height);
		final TextView einstellungen_height_text = (TextView) findViewById(R.id.options_text_height_count);
		final Button einstellungen_age_change = (Button) findViewById(R.id.options_button_age_change);
		final SeekBar einstellungen_age_seekbar = (SeekBar) findViewById(R.id.options_seek_age);
		final TextView einstellungen_age_text = (TextView) findViewById(R.id.options_text_age_count);
		final ListView availableMapsList = (ListView) findViewById(R.id.availableMapsList);
		final ImageButton opponentGoButton = (ImageButton) findViewById(R.id.opponentGoButton);

		map_settings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!choosenLocationFromList.equals("")) {
					inflateMapSettings(choosenLocationFromList);
				} else {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.noMapChoosen),
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		checkLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				UserSettings.autoLoginEnabled = !UserSettings.autoLoginEnabled;
				checkLogin
						.setImageResource(UserSettings.autoLoginEnabled ? R.drawable.button_options_on
								: R.drawable.button_options_off);
			}
		});

		einstellungen_music.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				UserSettings.musicEnabled = !UserSettings.musicEnabled;
				if (!UserSettings.musicEnabled)
					stopBackgroundMusic();
				else
					playBackgroundMusic();

				einstellungen_music
						.setImageResource(UserSettings.musicEnabled ? R.drawable.button_options_on
								: R.drawable.button_options_off);
			}
		});

		einstellungen_vibration.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				UserSettings.vibrationEnabled = !UserSettings.vibrationEnabled;
				einstellungen_vibration
						.setImageResource(UserSettings.vibrationEnabled ? R.drawable.button_options_on
								: R.drawable.button_options_off);
			}
		});

		einstellungen_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!UserSettings.cameraEnabled) {
					UserSettings.cameraEnabled = true;
					einstellungen_camera
							.setImageResource(R.drawable.button_options_on);

				} else {
					UserSettings.cameraEnabled = false;
					einstellungen_camera
							.setImageResource(R.drawable.button_options_off);

				}

			}
		});

		einstellungen_switchView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				UserSettings.switchViewEnabled = !UserSettings.switchViewEnabled;
				einstellungen_switchView
						.setImageResource(UserSettings.switchViewEnabled ? R.drawable.button_options_on
								: R.drawable.button_options_off);
				updateSwitchView(UserSettings.switchViewEnabled);
			}
		});
		
		einstellungen_rotation.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				UserSettings.rotationEnabled = !UserSettings.rotationEnabled;
				einstellungen_rotation
						.setImageResource(UserSettings.rotationEnabled ? R.drawable.button_options_on
								: R.drawable.button_options_off);
				((LinearLayout)findViewById(R.id.options_linear_rotation_type_panel)).setVisibility(UserSettings.rotationEnabled? View.VISIBLE : View.GONE);
			}
		});
		
		einstellungen_rotation_type.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				UserSettings.rotation_type_GPS = isChecked;
			}
		});

		einstellungen_gender.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				UserSettings.gender = !UserSettings.gender;
				einstellungen_gender
						.setImageResource(UserSettings.gender ? R.drawable.button_male
								: R.drawable.button_female);
				saveSettings();
				saveUserData();
			}
		});

		einstellungen_weight_change.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				findViewById(R.id.options_linear_weight_change_panel)
						.setVisibility(View.GONE);
				findViewById(R.id.options_linear_weight_seek_panel)
						.setVisibility(View.VISIBLE);
			}
		});

		einstellungen_weight_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						UserSettings.weight = seekBar.getProgress()
								+ GlobalConstants.MIN_WEIGHT;
						saveSettings();
						saveUserData();
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						einstellungen_weight_text
								.setText((progress + GlobalConstants.MIN_WEIGHT)
										+ "");
					}
				});

		einstellungen_height_change.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				findViewById(R.id.options_linear_height_change_panel)
						.setVisibility(View.GONE);
				findViewById(R.id.options_linear_height_seek_panel)
						.setVisibility(View.VISIBLE);
			}
		});

		einstellungen_height_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						UserSettings.height = seekBar.getProgress()
								+ GlobalConstants.MIN_HEIGHT;
						saveSettings();
						saveUserData();
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						einstellungen_height_text
								.setText((progress + GlobalConstants.MIN_HEIGHT)
										+ "");
					}
				});

		einstellungen_age_change.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				findViewById(R.id.options_linear_age_change_panel)
						.setVisibility(View.GONE);
				findViewById(R.id.options_linear_age_seek_panel).setVisibility(
						View.VISIBLE);
			}
		});

		einstellungen_age_seekbar
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						UserSettings.age = seekBar.getProgress()
								+ GlobalConstants.MIN_AGE;
						saveSettings();
						saveUserData();
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						einstellungen_age_text
								.setText((progress + GlobalConstants.MIN_AGE)
										+ "");
					}
				});

		disclaimerButtonAccept.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				flag_disclaimer = false;
				showScreen((RelativeLayout) findViewById(R.id.loginView), false);
			}
		});

		disclaimerButtonDecline.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				canFinish();
			}
		});

		// newGhost.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View arg0) {
		// if (myEnemies.size() < GlobalConstants.MAX_ENEMY_COUNT) {
		// // Hinzuf�gen von DummyGhosts
		// int speedCode = GlobalConstants.SPEED_SNAIL;
		// int intelligenceCode = GlobalConstants.ENEMY_FOOL;
		//
		//
		// String intelligence = opponentIntelligenceText.getText()
		// + "";
		// String speed = opponentSpeedText.getText() + "";
		// int speedCode = 0;
		// if (speed.equals(getResources().getString(R.string.snail)))
		// speedCode = GlobalConstants.SPEED_SNAIL;
		// if (speed.equals(getResources().getString(R.string.slow)))
		// speedCode = GlobalConstants.SPEED_SLOW;
		// if (speed.equals(getResources().getString(R.string.walk)))
		// speedCode = GlobalConstants.SPEED_WALKSPEED;
		// if (speed.equals(getResources().getString(R.string.medium)))
		// speedCode = GlobalConstants.SPEED_MEDIUM;
		// if (speed.equals(getResources().getString(R.string.fast)))
		// speedCode = GlobalConstants.SPEED_FAST;
		// int intelligenceCode = 0;
		// if (intelligence.equals(getResources().getString(
		// R.string.fool)))
		// intelligenceCode = GlobalConstants.ENEMY_FOOL;
		// if (intelligence.equals(getResources().getString(
		// R.string.easy)))
		// intelligenceCode = GlobalConstants.ENEMY_EASY;
		// if (intelligence.equals(getResources().getString(
		// R.string.normal)))
		// intelligenceCode = GlobalConstants.ENEMY_NORMAL;
		// if (intelligence.equals(getResources().getString(
		// R.string.good)))
		// intelligenceCode = GlobalConstants.ENEMY_GOOD;
		// if (intelligence.equals(getResources().getString(
		// R.string.extreme)))
		// intelligenceCode = GlobalConstants.ENEMY_EXTREME;
		//
		// myEnemies.add(new SimpleEnemy(intelligenceCode, speedCode));
		// opponentAdapter.notifyDataSetChanged();
		//
		// } else {
		// Toast.makeText(getApplicationContext(),
		// getResources().getString(R.string.maxOpponents),
		// Toast.LENGTH_SHORT).show();
		// }
		// }
		//
		// });

		opponentList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				opponentAdapter.onItemClicked(arg2);
			}

		});

		deleteAllEnemies.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				if (myEnemies.size() > 0) {
					myEnemies.clear();
					opponentAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(R.string.deletedOpponents),
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		opponentButtonClose.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				showScreen((RelativeLayout) findViewById(R.id.mapSelectView),
						false);
			}
		});

		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				newUserSubmit();
			}
		});

		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {

				if (!LoginAuthenticate.authenticate(MainActivity.this,
						usernameInput.getText().toString(), passwordInput
								.getText().toString(), true)) {
					// ERROR, PASSWORD / USERNAME COMBINATION IS WRONG
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(R.string.combinationWrong),
							Toast.LENGTH_SHORT).show();
					return;
				}

				player = new Player(usernameInput.getText().toString());

				drawerItemsUpdate();

				UserSettings.userData = new UserData(usernameInput.getText()
						.toString());
				userLogStats = new UserLogStats();
				userLogStats.init(UserSettings.userData);

				if (UserSettings.userData.getUserData()) {
					// SUCCESS
					UserSettings.userData
							.setStartdate(LoginAuthenticate.regDate);
				} else {
					// FEHLER BEIM FINDEN DER NUTZERDATEN
					Toast.makeText(
							getApplicationContext(),
							getResources()
									.getString(R.string.corruptedUserData),
							Toast.LENGTH_SHORT).show();

				}
				// updateProfile();

				showScreen((RelativeLayout) findViewById(R.id.startViewNew),
						true);
				loginSuccess = true;

				// Login Info
				Toast.makeText(
						getApplicationContext(),
						player.getUsername() + " "
								+ getResources().getString(R.string.loggedIn),
						Toast.LENGTH_SHORT).show();

				hideKeyboard(usernameInput);

				saveSettings();
				savePlayerLogin(player.getUsername());
			}
		});

		quickplayButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startQuickplay();
			}
		});

		singleplayerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showScreen((RelativeLayout) findViewById(R.id.mapSelectView),
						true);
			}
		});

		levelEditButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent levelEditorIntent = new Intent(MainActivity.this,
						LevelEditorActivity.class);
				startActivityForResult(levelEditorIntent,
						GlobalConstants.ACTIVITY_EDITOR);
			}
		});

		optionsButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showScreen((RelativeLayout) findViewById(R.id.einstellungView),
						true);
			}
		});

		profileButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				updateProfile();
				showScreen((RelativeLayout) findViewById(R.id.profilView), true);
			}
		});

		gameoverView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				getFreeMemory();
				showScreen((RelativeLayout) findViewById(R.id.startViewNew),
						false);
			}
		});

		timeoverView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				getFreeMemory();
				showScreen((RelativeLayout) findViewById(R.id.startViewNew),
						false);
			}
		});

		gamewonView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				getFreeMemory();
				showScreen((RelativeLayout) findViewById(R.id.startViewNew),
						false);
			}
		});

		switchMapsView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				RelativeLayout availableMapsOverviewPanel = (RelativeLayout) findViewById(R.id.availableMapsOverview_Panel);
				if (availableMapsOverviewPanel.getVisibility() == View.VISIBLE) {
					availableMapsOverviewPanel.setVisibility(View.INVISIBLE);
					switchMapsView
							.setImageResource(R.drawable.button_switch_maps_view);
				} else {
					((MapFragment) getSupportFragmentManager()
							.findFragmentById(R.id.map_fragment)).refreshAll();
					availableMapsOverviewPanel.setVisibility(View.VISIBLE);
					switchMapsView.setImageResource(R.drawable.button_map_view);
				}
			}
		});

		final String[] filterOptions = new String[] {
				GlobalConstants.FILTER_MAP_LOCAL,
				GlobalConstants.FILTER_MAP_ALL,
				GlobalConstants.FILTER_MAP_ONLINE };
		filterMapsList
				.setAdapter(new FilterSpinnerAdapter(this, filterOptions));
		filterMapsList.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				filterSelection = filterOptions[arg2];
				updateAllAvailableMaps(MainActivity.this);
				((MapFragment) getSupportFragmentManager().findFragmentById(
						R.id.map_fragment)).refreshAll();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		ghostMapSelectButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showScreen((RelativeLayout) findViewById(R.id.opponentView),
						true);
				opponentTextLevel.setText(choosenLocationFromList);
			}
		});

		mapStartModeButtonClose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				onBackPressed();
			}
		});

		highscoreButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// if (!highscore.connected) {
				// serverError();
				// return;
				// }
				showScreen((RelativeLayout) findViewById(R.id.highscoreView),
						true);
			}
		});

		aboutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showScreen((RelativeLayout) findViewById(R.id.aboutView), true);
			}
		});

		debugButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (checkInternetWarning())
					return;
				Intent mapIntent = new Intent(MainActivity.this,
						MapActivity.class);
				startActivityForResult(mapIntent, GlobalConstants.ACTIVITY_MAP);
			}
		});

		hilfeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				findViewById(R.id.help_relative_theme).setVisibility(
						View.INVISIBLE);
				findViewById(R.id.help_button_back).setVisibility(
						View.INVISIBLE);
				showScreen((RelativeLayout) findViewById(R.id.helpView), true);
			}
		});

		helpButtonClose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				findViewById(R.id.help_button_back).setVisibility(
						View.INVISIBLE);
				showScreen((RelativeLayout) findViewById(R.id.startViewNew),
						false);
			}
		});

		helpButtonBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				findViewById(R.id.help_button_back).setVisibility(
						View.INVISIBLE);
				findViewById(R.id.help_relative_theme).setVisibility(
						View.INVISIBLE);
			}
		});

		helpButtonThemeMain.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				findViewById(R.id.help_button_back).setVisibility(View.VISIBLE);
				findViewById(R.id.help_relative_theme).setVisibility(
						View.VISIBLE);
				((TextView) findViewById(R.id.help_text_theme_header))
						.setText(getResources().getString(R.string.helpMain));
				((ImageView) findViewById(R.id.help_image_picture))
						.setImageResource(R.drawable.help_main);
			}
		});

		helpButtonThemeGameplay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				findViewById(R.id.help_button_back).setVisibility(View.VISIBLE);
				findViewById(R.id.help_relative_theme).setVisibility(
						View.VISIBLE);
				((TextView) findViewById(R.id.help_text_theme_header))
						.setText(getResources()
								.getString(R.string.helpGameplay));
				((ImageView) findViewById(R.id.help_image_picture))
						.setImageResource(R.drawable.help_gameplay);
			}
		});

		helpButtonThemeEditor.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				findViewById(R.id.help_button_back).setVisibility(View.VISIBLE);
				findViewById(R.id.help_relative_theme).setVisibility(
						View.VISIBLE);
				((TextView) findViewById(R.id.help_text_theme_header))
						.setText(getResources().getString(R.string.helpEditor));
				((ImageView) findViewById(R.id.help_image_picture))
						.setImageResource(R.drawable.help_leveleditor);
			}
		});

		chatButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				updateMessages();
				startChat();
			}
		});

		message.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
				// If the event is a key-down event on the "enter" button
				if ((arg2.getAction() == KeyEvent.ACTION_DOWN)
						&& ((arg1 == KeyEvent.KEYCODE_ENTER) || (arg1 == KeyEvent.KEYCODE_MEDIA_NEXT))) {

					Chat.sendMessage(player.getUsername(), message.getText()
							+ "");
					message.setText("");
					updateMessages();

					return true;
				}
				return false;
			}
		});

		chatButtonClose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showScreen((RelativeLayout) findViewById(R.id.startViewNew),
						false);
			}
		});

		chatButtonSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Chat.sendMessage(player.getUsername(), message.getText() + "");
				message.setText("");
				updateMessages();
			}
		});

		profilePicture.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Intent imageReturnedIntent = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				imageReturnedIntent.setType("image/*");
				startActivityForResult(imageReturnedIntent,
						GlobalConstants.SELECT_PHOTO);
			}
		});

		profileReset.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				Bitmap b = BitmapFactory.decodeResource(
						MainActivity.this.getResources(),
						R.drawable.default_profile_pic);
				ImageView img = (ImageView) findViewById(R.id.profile_picture);
				img.setImageBitmap(b);
				drawerItemsUpdate();
				UserSettings.userData.setPicture(b);
				updateProfile();
				saveUserData();
			}
		});

		availableMapsList.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				TextView text = (TextView) arg1
						.findViewById(R.id.AvailableMapName);
				selectLevelByName(text.getText().toString(), MainActivity.this);
			}
		});

		availableMapsList
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int arg2, long arg3) {

						TextView text = (TextView) arg1
								.findViewById(R.id.AvailableMapName);
						if (levelExists(text.getText().toString())) {
							selectLevelByName(text.getText().toString(),
									MainActivity.this);
							inflateMapSettings(choosenLocationFromList);
						}
						return true;
					}
				});

		OnClickListener playButtonOnClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				playChosenLocation();
			}
		};

		mapStartModeButtonSingleplayer
				.setOnClickListener(playButtonOnClickListener);
		opponentGoButton.setOnClickListener(playButtonOnClickListener);

		playButton.setOnClickListener(playButtonOnClickListener);
		// Disabled due to missing multiplayer mode. Reactivate this part when
		// multiplayer is working.
		// playButton.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // CHECK IF NEAR
		// LocationResource locRes = getLocationPath(choosenLocationFromList);
		// if (locRes != null) {
		// if (locRes.getDistance() > 500) {
		// // TOO FAR AWAY
		// Toast.makeText(
		// MainActivity.this,
		// getResources()
		// .getString(R.string.mapTooFarAway),
		// Toast.LENGTH_SHORT).show();
		// return;
		// }
		// }
		//
		// ParsedFile getChoosenLoc;
		//
		// if (choosenLocationFromList == "") {
		// Toast.makeText(MainActivity.this,
		// getResources().getString(R.string.noMapChoosen),
		// Toast.LENGTH_SHORT).show();
		// return;
		// }
		// getChoosenLoc = loadLocation(choosenLocationFromList);
		//
		// currentLoc = new GPSLocation(getChoosenLoc);
		// Log.d("UHG", "dist: "
		// + getCurrentLocation().getContent().getDistance());
		//
		// if (opponentAdapter.getActiveEnemyCount() > getCurrentLocation()
		// .getContent().getMaxGhosts()) {
		// Toast.makeText(
		// MainActivity.this,
		// getResources().getString(R.string.tooManyGhosts)
		// + " "
		// + getCurrentLocation().getContent()
		// .getMaxGhosts(), Toast.LENGTH_SHORT)
		// .show();
		// return;
		// }
		// RelativeLayout mapStartOptionsViewPanel = (RelativeLayout)
		// findViewById(R.id.map_start_options_view_panel);
		// RelativeLayout mapStartModeRelativeMapPanel = (RelativeLayout)
		// mapStartOptionsViewPanel
		// .findViewById(R.id.map_start_mode_relative_map_panel);
		// AvailableMapsArrayAdapter mapsAdapter = (AvailableMapsArrayAdapter)
		// MainActivity.availableMapsList
		// .getAdapter();
		// mapStartModeRelativeMapPanel.removeAllViews();
		// mapStartModeRelativeMapPanel.addView(mapsAdapter.getView(
		// mapsAdapter.getPosition(choosenLocationFromList), null,
		// mapStartModeRelativeMapPanel),
		// new RelativeLayout.LayoutParams(
		// RelativeLayout.LayoutParams.MATCH_PARENT,
		// RelativeLayout.LayoutParams.WRAP_CONTENT));
		// showScreen(mapStartOptionsViewPanel, true);
		// }
		// });

		downloadMapButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						MainActivity.this);

				builder.setMessage(
						getResources().getString(R.string.downloadMap))
						.setTitle(
								getResources().getString(
										R.string.downloadMapTitle));

				builder.setPositiveButton(
						getResources().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								String content = ServerProgressing
										.downloadMap(choosenLocationFromList);
								LevelEditorActivity.saveLevel(
										MainActivity.this,
										choosenLocationFromList, content);
								selectLevelByName(choosenLocationFromList,
										MainActivity.this);
								((MapFragment) getSupportFragmentManager()
										.findFragmentById(R.id.map_fragment))
										.refreshAll();

							}
						});

				builder.setNegativeButton(
						getResources().getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// Do nothing
							}
						});

				Dialog dialog = builder.create();
				dialog.show();
			}
		});

		mapStartModeButtonMultiplayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(MainActivity.this,
						"Multiplayer is not yet available", Toast.LENGTH_SHORT)
						.show();
			}
		});

		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		LocationListener locationListener = new LocationListener() {

			public void onLocationChanged(Location location) {

				// Save Last Position
				currentPosition = new GPSPoint(location.getLatitude(),
						location.getLongitude());

				// TODO: GPS Data for SensorView
				if (isVisibleScreen((RelativeLayout) findViewById(R.id.sensorsView))) {
					augmentedReality.setLocation(new GPSPoint(location
							.getLatitude(), location.getLongitude()));
				}
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				/**
				 * Wird ausgef�hrt, wenn GPS Signal verschwindet oder TimeOut
				 * ist
				 **/
			}

			public void onProviderEnabled(String provider) {
				/** Handy GPS ist eingeschaltet **/
			}

			public void onProviderDisabled(String provider) {
				/** Handy GPS ist ausgeschaltet **/
			}

		};

		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		}

		onOnCreateFinished();

	}

	private void onOnCreateFinished() {
		Intent intent = getIntent();
		boolean isStartQuickplay = intent.getBooleanExtra(
				GlobalConstants.KEY_START_QUICKPLAY, false);
		if (isStartQuickplay && loginSuccess) {
			final Handler handler = new Handler();

			Runnable quickplayRunnable = new Runnable() {
				@Override
				public void run() {
					if (MainActivity.GPSactivated) {
						quickplayTryCounter = 5;
						startQuickplay();
					} else {
						if(quickplayTryCounter > 0){
							quickplayTryCounter--;
							Toast.makeText(MainActivity.this, getResources().getString(R.string.quickplayRetry), Toast.LENGTH_SHORT).show();
							handler.postDelayed(this, 3000);
						} else {
							Toast.makeText(MainActivity.this, getResources().getString(R.string.quickplayCanceled), Toast.LENGTH_LONG).show();
						}
					}
				}
			};
			handler.postDelayed(quickplayRunnable, 1000);
		}
	}

	protected void startQuickplay() {
		if (!MainActivity.GPSactivated) {
			Toast.makeText(MainActivity.this,
					getResources().getString(R.string.gpsinactive),
					Toast.LENGTH_SHORT).show();
			return;
		}
		ArrayList<String[]> localMapsList = null;
		localMapsList = updateLocalAvailableMaps(MainActivity.this);
		new AsyncLoadingQuickPlayOnlineMaps(MainActivity.this, localMapsList)
				.execute();
	}

	protected void playChosenLocation() {
		// CHECK IF NEAR
		LocationResource locRes = getLocationPath(choosenLocationFromList);
		if (locRes != null) {
			if (locRes.getDistance() > GlobalConstants.MAP_DISTANCE_TO_START) {
				// TOO FAR AWAY
				Toast.makeText(MainActivity.this,
						getResources().getString(R.string.mapTooFarAway),
						Toast.LENGTH_SHORT).show();
				return;
			}
		}

		ParsedFile getChoosenLoc;

		if (choosenLocationFromList == "") {
			Toast.makeText(MainActivity.this,
					getResources().getString(R.string.noMapChoosen),
					Toast.LENGTH_SHORT).show();
			return;
		}
		getChoosenLoc = loadLocation(choosenLocationFromList);

		currentLoc = new GPSLocation(getChoosenLoc);
		Log.d("UHG", "dist: " + getCurrentLocation().getContent().getDistance());

		if (opponentAdapter.getActiveEnemyCount() > getCurrentLocation()
				.getContent().getMaxGhosts()) {
			Toast.makeText(
					MainActivity.this,
					getResources().getString(R.string.tooManyGhosts) + " "
							+ getCurrentLocation().getContent().getMaxGhosts(),
					Toast.LENGTH_SHORT).show();
			return;
		}

		deleteBackEntries();

		map = new Map(getCurrentLocation());
		map.setPath(getCurrentLocation().getContent().getPath());
		map.setMaxGhosts(getCurrentLocation().getContent().getMaxGhosts());
		map.setDistance(getCurrentLocation().getContent().getDistance());
		map.setMaxTime(getCurrentLocation().getContent().getMaxTime());
		correctGPS = getCurrentLocation().getContent().isBoundOnWays();

		autoDetectGhosts();

		Level customLevel = new Level(getCurrentLocation().getLocationName(),
				"Custom Level", myEnemies, 1800, 0, 100, 100);

		try{
		game = new Game(map, player, getApplicationContext(), customLevel);
		} catch(IndexOutOfBoundsException ioobe){
			Toast.makeText(this, "IndexOutOfBoundsException", Toast.LENGTH_SHORT).show();
			finish();
		}

		lastDistance = Math.round(UserSettings.userData.getDistance());
		PlayActivity.setMap(map);
		PlayActivity.setGame(game);
		PlayActivity.setMain(MainActivity.this);

		userLogStats.logStartedGame(UserSettings.userData);

		// CHANGED 16/04/2015
		// Show Loadscreen instead to fetch GPS before starting
		Intent gpsIntent = new Intent(MainActivity.this,
				GPSLoadScreenActivity.class);
		startActivityForResult(gpsIntent, GlobalConstants.ACTIVITY_GPS);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == GlobalConstants.REQUEST_ENABLE_BT) {
			if (resultCode == Activity.RESULT_CANCELED) {
				// Bluetooth not enabled.
				finish();
				return;
			}
		}
		super.onActivityResult(requestCode, resultCode, data); // TODO: Is this
																// correct?

		switch (resultCode) {
		case GlobalConstants.DRAWER_HOME:
			showScreen((RelativeLayout) findViewById(R.id.startViewNew), true);
			break;
		case GlobalConstants.DRAWER_LEVEL_OVERVIEW:
			showScreen((RelativeLayout) findViewById(R.id.mapSelectView), true);
			break;
		case GlobalConstants.DRAWER_PROFILE:
			updateProfile();
			showScreen((RelativeLayout) findViewById(R.id.profilView), true);
			break;
		case GlobalConstants.DRAWER_HIGHSCORE:
			showScreen((RelativeLayout) findViewById(R.id.highscoreView), true);
			break;
		case GlobalConstants.DRAWER_CHAT:
			showScreen((RelativeLayout) findViewById(R.id.chatView), true);
			break;
		case GlobalConstants.DRAWER_CHANGE_USER:
			logOut();
			break;
		case GlobalConstants.DRAWER_OPTIONS:
			showScreen((RelativeLayout) findViewById(R.id.einstellungView),
					true);
			break;
		case GlobalConstants.DRAWER_HELP:
			showScreen((RelativeLayout) findViewById(R.id.helpView), true);
			break;
		case GlobalConstants.DRAWER_EXIT:
			exit();
			break;
		case GlobalConstants.DRAWER_CHANGE_LEVEL:
			showScreen((RelativeLayout) findViewById(R.id.mapSelectView), true);
			break;
		case GlobalConstants.DRAWER_RESTART:
			Intent playIntent = new Intent(MainActivity.this,
					PlayActivity.class);
			startActivityForResult(playIntent, GlobalConstants.ACTIVITY_PLAY);
			break;

		}
		if (requestCode == GlobalConstants.SELECT_PHOTO) {
			if (resultCode == RESULT_OK) {
				Uri selectedImage = data.getData();
				InputStream imageStream;
				try {
					imageStream = getContentResolver().openInputStream(
							selectedImage);
					Log.d("UHG", "Openend Picture at " + selectedImage);
					Bitmap compressed = BitmapProgressing
							.compress(BitmapFactory.decodeStream(imageStream));
					imageStream.close();

					ImageView img = (ImageView) findViewById(R.id.profile_picture);
					img.setImageBitmap(compressed);

					UserSettings.userData.setPicture(compressed);
					UserSettings.userData.updateUserData();

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			drawerItemsUpdate();
		}

		if (requestCode == GlobalConstants.ACTIVITY_EDITOR) {
			if (resultCode == GlobalConstants.RESULT_PUBLISH_NEW_MAP) {
				selectLevelByName(
						data.getStringExtra(GlobalConstants.RESULT_MAP_NAME),
						MainActivity.this);
				uploadCurrentMap();
			}
		}

	}

	public void deleteGhostAt(int position) {

		if (myEnemies.size() > 0 && myEnemies.get(position) != null) {
			myEnemies.remove(position);
			opponentAdapter.notifyDataSetChanged();
		} else {
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.deletedOpponents),
					Toast.LENGTH_SHORT).show();
		}
	}

	public Bitmap decodeUri(Uri uri) throws FileNotFoundException {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getContentResolver().openInputStream(uri),
				null, o);
		final int REQUIRED_SIZE = 140;

		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(
				getContentResolver().openInputStream(uri), null, o2);
	}

	/** Calculates ghosts for maps where no ghosts were choosen **/
	protected void autoDetectGhosts() {
		if (!GlobalConstants.AUTO_DETECT_GHOSTS)
			return;

		if (opponentAdapter.getActiveEnemyCount() < 1) {

			int max = map.getMaxGhosts();
			if (max > GlobalConstants.MAX_ENEMY_COUNT)
				max = GlobalConstants.MAX_ENEMY_COUNT;
			int intelligenceCount = 0;
			int speedCount = 1;
			if (max > 0) {
				for (int i = 1; i <= max; i++) {

					intelligenceCount++;
					if (intelligenceCount > speedCount) {
						intelligenceCount = 1;
						if (speedCount < 5)
							speedCount++;
					}

					opponentAdapter.activeGhost(
							Game.encodeIntelligence(intelligenceCount),
							Game.encodeSpeed(speedCount));

				}
			}
			opponentAdapter.notifyDataSetChanged();
		}
	}

	/** L�dt alle XMLLevel-Files aus den assets und dem eigenen Ordner **/
	public static void loadXMLLevelFiles(Context context) {// Alle Karten aus
															// dem
															// assets/maps-Verzeichnis
															// laden

		availableLocs = new ArrayList<LocationResource>();
		try {
			String[] fileList = context.getAssets().list("maps");
			for (String file : fileList) {
				availableLocs.add(new LocationResource(file, false));
			}
		} catch (IOException e1) {
			// Fehler kann per Definition nicht auftreten
		}

		// Alle Karten aus dem Eigenen Verzeichnis laden
		dataDir = context.getApplicationInfo().dataDir + "/komarcade/level";
		File dir = new File(dataDir);
		File[] fileList2 = dir.listFiles();
		if (fileList2 != null) {
			for (int i = 0; i < fileList2.length; i++) {
				availableLocs.add(new LocationResource(fileList2[i]
						.getAbsolutePath(), fileList2[i].getName().toString(),
						true));
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private void optionsMenuButtonCheck(ImageButton optionsMenu) {
		if ((Build.VERSION.SDK_INT <= 10)
				|| ((Build.VERSION.SDK_INT >= 14) && (ViewConfiguration
						.get(this).hasPermanentMenuKey()))) {
			optionsMenu.setVisibility(ImageButton.INVISIBLE);
		} else {
			optionsMenu.setVisibility(ImageButton.VISIBLE);
		}
	}

	/**
	 * @param visibility
	 *            zeigt die Warnungs-Info an (an/aus)
	 */
	public void showWarning(boolean visibility) {
		RelativeLayout warning = (RelativeLayout) findViewById(R.id.warningView);
		if (visibility)
			warning.setVisibility(RelativeLayout.VISIBLE);
		else
			warning.setVisibility(RelativeLayout.INVISIBLE);
	}

	/** Returns if device has internet connectivity **/
	public boolean hasInternetConnectivity() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return ((netInfo != null) && netInfo.isConnectedOrConnecting());
	}

	public boolean checkInternetWarning() {
		if (!hasInternetConnectivity()) {
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.noConnectivity),
					Toast.LENGTH_SHORT).show();
			return true;
		}
		return false;
	}

	private boolean profileDeleter() {
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setMessage(
				getResources().getString(R.string.deleteProfilePromt))
				.setTitle(getResources().getString(R.string.deleteProfile));

		builder.setPositiveButton(getResources().getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						final Context context = MainActivity.this;
						PromptDialog dlg = new PromptDialog(context,
								R.string.username, R.string.usernameHint, true) {
							@Override
							public boolean onOkClicked(String input) {
								if (LoginAuthenticate.authenticate(context,
										player.getUsername(), input, true)) {
									// PASSWORD CORRECT
									UserData.deleteUser(player.getUsername(),
											input);
									player = player
											.deletePlayer(MainActivity.this);
									saveSettings();
									updateUsernames();

									if (UserSettings.userData != null) {
										UserSettings.userData.deleteUserData();
									}
									UserSettings.userData = null;
									userLogStats = null;

									showScreen(
											(RelativeLayout) findViewById(R.id.loginView),
											false);
									Toast.makeText(
											MainActivity.this,
											getResources()
													.getString(
															R.string.deleteAccountSuccess,
															Toast.LENGTH_SHORT),
											Toast.LENGTH_SHORT).show();
								} else {
									// PASSWORD INCORRECT
									Toast.makeText(
											MainActivity.this,
											getResources().getString(
													R.string.passwordWrong,
													Toast.LENGTH_SHORT),
											Toast.LENGTH_SHORT).show();
								}

								return true;
							}
						};
						dlg.setTitle(getResources().getString(
								R.string.passwordTitle));
						dlg.getInput().setText("");
						dlg.setMessage(getResources().getString(
								R.string.passwordDelete));
						dlg.show();

					}
				});

		builder.setNegativeButton(getResources().getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// DO NOTHING!
					}
				});

		Dialog dialog = builder.create();
		dialog.show();
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		mainMenu = menu;
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		// Starte die OpenStreetMap
		MenuItem allAvailableMapsOverview = menu
				.findItem(R.id.openStreetMapOverview);
		allAvailableMapsOverview
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem arg0) {
						if (checkInternetWarning())
							return true;
						Intent mapIntent = new Intent(MainActivity.this,
								MapActivity.class);
						startActivityForResult(mapIntent,
								GlobalConstants.ACTIVITY_MAP);
						return true;
					}
				});

		// Starte die Credits
		MenuItem creditOverview = menu.findItem(R.id.creditOverview);
		creditOverview
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem arg0) {
						showScreen(
								(RelativeLayout) findViewById(R.id.aboutView),
								true);
						return true;
					}
				});

		// Starte die Activity des LevelEditors
		MenuItem levelEditorOverview = menu.findItem(R.id.levelEditorOverview);
		levelEditorOverview
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem arg0) {
						Intent levelEditorIntent = new Intent(
								MainActivity.this, LevelEditorActivity.class);
						startActivityForResult(levelEditorIntent,
								GlobalConstants.ACTIVITY_EDITOR);
						return true;
					}
				});

		MenuItem disclaimerOpen = menu.findItem(R.id.disclaimerOpen);
		disclaimerOpen
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem arg0) {
						final Button disclaimerButtonDecline = (Button) findViewById(R.id.disclaimer_button_decline);
						flag_disclaimer = true;
						disclaimerButtonDecline.setEnabled(false);
						showScreen(
								(RelativeLayout) findViewById(R.id.disclaimerView),
								true);
						return true;
					}
				});

		// ACCESS aufs Debug Men�
		MenuItem logout = menu.findItem(R.id.logout);
		logout.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem arg0) {
				return logOut();
			}
		});

		MenuItem exit = menu.findItem(R.id.exit);
		exit.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem arg0) {
				return exit();
			}
		});

		MenuItem resetStatistic = menu.findItem(R.id.resetStatistic);
		resetStatistic.setVisible(false);
		resetStatistic
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem arg0) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								MainActivity.this);
						builder.setMessage(
								getResources().getString(
										R.string.resetStatisticPromt))
								.setTitle(
										getResources().getString(
												R.string.resetStatistic));

						builder.setPositiveButton(
								getResources().getString(R.string.ok),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										UserSettings.userData.vanish();
										updateProfile();
									}
								});

						builder.setNegativeButton(
								getResources().getString(R.string.cancel),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// DO NOTHING!
									}
								});

						Dialog dialog = builder.create();
						dialog.show();
						return true;
					}
				});
		MenuItem exitGame = menu.findItem(R.id.exitGame);
		exitGame.setVisible(false);
		exitGame.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem arg0) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.exitGameByUser),
						Toast.LENGTH_SHORT).show();
				finishGame(player, game);
				showScreen((RelativeLayout) findViewById(R.id.gameOverView),
						false);
				return true;
			}
		});

		MenuItem deleteProfile = menu.findItem(R.id.deleteProfile);
		deleteProfile.setVisible(false);
		deleteProfile.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem arg0) {
				return profileDeleter();
			}
		});

		menuItemsUpdate();
		return true;
	}

	private void inflateMapSettings(String mapName) {

		showScreen((RelativeLayout) findViewById(R.id.map_options_view_panel),
				true);

		ImageButton mapOptionsButtonClose = (ImageButton) findViewById(R.id.map_options_button_close);
		Button mapOptionsButtonCancel = (Button) findViewById(R.id.map_options_button_cancel);
		Button mapOptionsButtonModify = (Button) findViewById(R.id.map_options_button_modify);
		Button mapOptionsButtonUpload = (Button) findViewById(R.id.map_options_button_upload);
		Button mapOptionsButtonDeleteServer = (Button) findViewById(R.id.map_options_button_delete_server);
		Button mapOptionsButtonDelete = (Button) findViewById(R.id.map_options_button_delete);
		Button mapOptionsButtonRate = (Button) findViewById(R.id.map_options_button_rate);

		boolean onlineMap = loadLocation(choosenLocationFromList).isOnline();

		boolean mapOwner;
		if (onlineMap)
			mapOwner = ServerProgressing.isMapOwner(player.getUsername(),
					choosenLocationFromList);
		else {
			mapOwner = true; // TODO: this is not completely correct. Real owner
								// might have removed map from server
		}

		Log.d("-------------------", "online: " + onlineMap + " owner: "
				+ mapOwner + " location: " + choosenLocationFromList);

		mapOptionsButtonModify.setVisibility(View.VISIBLE);
		mapOptionsButtonUpload.setVisibility(View.VISIBLE);
		mapOptionsButtonDeleteServer.setVisibility(View.VISIBLE);
		mapOptionsButtonDelete.setVisibility(View.VISIBLE);
		mapOptionsButtonRate.setVisibility(View.VISIBLE);

		if (!mapOwner) {
			mapOptionsButtonModify.setVisibility(View.GONE);
			mapOptionsButtonUpload.setVisibility(View.GONE);
			mapOptionsButtonDeleteServer.setVisibility(View.GONE);
		}

		if (!onlineMap) {
			mapOptionsButtonDeleteServer.setVisibility(View.GONE);
		} else {
			mapOptionsButtonUpload.setVisibility(View.GONE);
		}

		mapOptionsButtonClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		mapOptionsButtonCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		mapOptionsButtonModify.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				LevelEditorActivity.loadFileAtStartup = loadLocation(choosenLocationFromList);
				Intent levelEditorIntent = new Intent(MainActivity.this,
						LevelEditorActivity.class);
				startActivityForResult(levelEditorIntent,
						GlobalConstants.ACTIVITY_EDITOR);
			}
		});

		mapOptionsButtonUpload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkInternetWarning())
					return;
				else
					uploadCurrentMap();
				((MapFragment) getSupportFragmentManager().findFragmentById(
						R.id.map_fragment)).refreshAll();
				onBackPressed();
			}
		});

		mapOptionsButtonDeleteServer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkInternetWarning())
					return;
				if (loadLocation(choosenLocationFromList).isOnline())
					deleteCurrentMapFromServer();
				((MapFragment) getSupportFragmentManager().findFragmentById(
						R.id.map_fragment)).refreshAll();
				onBackPressed();
			}
		});

		mapOptionsButtonDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						MainActivity.this);

				builder.setMessage(
						getResources().getString(R.string.deleteLevel))
						.setTitle(
								getResources().getString(
										R.string.deleteLevelTitle));

				builder.setPositiveButton(
						getResources().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								LocationResource deleteLoc = getLocationPath(choosenLocationFromList);
								File file = new File(deleteLoc
										.getLocationPath());
								choosenLocationFromList = "";
								if (file.delete()) {
									loadXMLLevelFiles(getApplicationContext());
									preloadLocations(getApplicationContext());
									updateAllAvailableMaps(MainActivity.this);
									Toast.makeText(
											MainActivity.this,
											getResources().getString(
													R.string.successfulDelete),
											Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(
											MainActivity.this,
											getResources().getString(
													R.string.failedDelete),
											Toast.LENGTH_SHORT).show();
								}

							}
						});

				builder.setNegativeButton(
						getResources().getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// Do nothing
							}
						});

				Dialog dialog2 = builder.create();
				dialog2.show();

				((MapFragment) getSupportFragmentManager().findFragmentById(
						R.id.map_fragment)).refreshAll();

				onBackPressed();
			}
		});

		mapOptionsButtonRate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkInternetWarning())
					return;

				final int ownRating = MapRating
						.getUsersMapRating(choosenLocationFromList);
				double globalrating = MapRating.getMapPreferences(
						choosenLocationFromList).getTotalRating();
				// String comment = MapRating
				// .getUsersComment(choosenLocationFromList);

				((TextView) findViewById(R.id.map_rating_text_levelname))
						.setText(choosenLocationFromList);
				((TextView) findViewById(R.id.map_rating_text_average_rating))
						.setText("Average rating: " + globalrating);
				final RatingBar ratingBar = (RatingBar) findViewById(R.id.map_rating_ratingbar);
				if (ownRating > 0 && ownRating < 6)
					ratingBar.setRating(ownRating);
				else
					ratingBar.setRating(0);
				LayerDrawable stars = (LayerDrawable) ratingBar
						.getProgressDrawable();
				stars.getDrawable(2).setColorFilter(
						getResources().getColor(R.color.twostone_green),
						PorterDuff.Mode.SRC_ATOP);
				final EditText editComment = (EditText) findViewById(R.id.map_rating_edit_comment);
				Button ratingCancel = (Button) findViewById(R.id.map_rating_button_cancel);
				Button ratingOK = (Button) findViewById(R.id.map_rating_button_ok);
				ImageButton ratingClose = (ImageButton) findViewById(R.id.map_rating_button_close);

				showScreen(
						(RelativeLayout) findViewById(R.id.map_rating_view_panel),
						true);

				ratingCancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						onBackPressed();
					}
				});
				ratingClose.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						onBackPressed();
					}
				});
				ratingOK.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						int newRating = Math.round(ratingBar.getRating());
						if (newRating > 0) {
							MapRating.deleteMapRating(choosenLocationFromList,
									newRating);
							MapRating
									.rateMap(choosenLocationFromList,
											newRating, editComment.getText()
													.toString());
							((AvailableMapsArrayAdapter) MainActivity.availableMapsList
									.getAdapter()).invalidateRatings();
							onBackPressed();
						}
					}
				});

				// // OPEN RATE MENU
				// RatingDialog ratingdlg = new RatingDialog(
				// MainActivity.this, rating, globalrating, comment,
				// choosenLocationFromList) {
				// @Override
				// public boolean onOkClicked(int rating, String comment) {
				// // RATE!
				// if (rating > 0) {
				// MapRating.deleteMapRating(
				// choosenLocationFromList, rating);
				// MapRating.rateMap(choosenLocationFromList,
				// rating, comment);
				// }
				// return true;
				// }
				// };
				// ratingdlg.show();
			}
		});

		// CharSequence choose[];
		//
		// if (!onlineMap)
		// choose = new CharSequence[] {
		// getResources().getString(R.string.modify),
		// getResources().getString(R.string.upload),
		// getResources().getString(R.string.delete),
		// getResources().getString(R.string.cancel) };
		// else
		// choose = new CharSequence[] {
		// getResources().getString(R.string.modify),
		// getResources().getString(R.string.deleteServer),
		// getResources().getString(R.string.delete),
		// getResources().getString(R.string.rate),
		// getResources().getString(R.string.cancel) };
		//
		// AlertDialog.Builder builder = new
		// AlertDialog.Builder(MainActivity.this);
		// builder.setTitle(getResources().getString(R.string.chooseOption));
		// builder.setItems(choose, new DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// if (which == 0) {
		// LevelEditorActivity.loadFileAtStartup =
		// loadLocation(choosenLocationFromList);
		// Intent levelEditorIntent = new Intent(MainActivity.this,
		// LevelEditorActivity.class);
		// startActivityForResult(levelEditorIntent,
		// GlobalConstants.ACTIVITY_EDITOR);
		// }
		// if (which == 1) {
		// if (checkInternetWarning())
		// return;
		// if (loadLocation(choosenLocationFromList).isOnline())
		// deleteCurrentMapFromServer();
		// else
		// uploadCurrentMap();
		// }
		// if (which == 2) {
		// AlertDialog.Builder builder = new AlertDialog.Builder(
		// MainActivity.this);
		//
		// builder.setMessage(
		// getResources().getString(R.string.deleteLevel))
		// .setTitle(
		// getResources().getString(
		// R.string.deleteLevelTitle));
		//
		// builder.setPositiveButton(
		// getResources().getString(R.string.ok),
		// new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog,
		// int id) {
		// TextView text = (TextView) myView
		// .findViewById(R.id.AvailableMapName);
		// LocationResource deleteLoc = getLocationPath(text
		// .getText().toString());
		// File file = new File(deleteLoc
		// .getLocationPath());
		// choosenLocationFromList = "";
		// lastView = null;
		// if (file.delete()) {
		// loadXMLLevelFiles(getApplicationContext());
		// preloadLocations(getApplicationContext());
		// updateAllAvailableMaps(MainActivity.this);
		// Toast.makeText(
		// MainActivity.this,
		// getResources()
		// .getString(
		// R.string.successfulDelete),
		// Toast.LENGTH_SHORT).show();
		// } else {
		// Toast.makeText(
		// MainActivity.this,
		// getResources().getString(
		// R.string.failedDelete),
		// Toast.LENGTH_SHORT).show();
		// }
		//
		// }
		// });
		//
		// builder.setNegativeButton(
		// getResources().getString(R.string.cancel),
		// new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog,
		// int id) {
		// // Do nothing
		// }
		// });
		//
		// Dialog dialog2 = builder.create();
		// dialog2.show();
		// }
		//
		// if (onlineMap && (which == 3)) {
		// // RATING
		// if (checkInternetWarning())
		// return;
		//
		// int rating = MapRating
		// .getUsersMapRating(choosenLocationFromList);
		// double globalrating = MapRating
		// .getMapRating(choosenLocationFromList);
		// String comment = MapRating
		// .getUsersComment(choosenLocationFromList);
		//
		// // OPEN RATE MENU
		// RatingDialog ratingdlg = new RatingDialog(
		// MainActivity.this, rating, globalrating, comment,
		// choosenLocationFromList) {
		// @Override
		// public boolean onOkClicked(int rating, String comment) {
		// // RATE!
		// if (rating > 0) {
		// MapRating.deleteMapRating(
		// choosenLocationFromList, rating);
		// MapRating.rateMap(choosenLocationFromList,
		// rating, comment);
		// }
		// return true;
		// }
		// };
		// ratingdlg.show();
		//
		// }
		//
		// }
		// });
		// builder.show();

	}

	/** True, if levelName exits, else false **/
	public static boolean levelExists(String title) {
		if (availableLocs == null)
			return false;
		for (int i = 0; i < availableLocs.size(); i++)
			if (title.equalsIgnoreCase(availableLocs.get(i).getLocationName())) {
				return true;
			}
		return false; // If no location is found!
	}

	public boolean logOut() {
		// Aktuellen Spieler speichern

		updateUsernames();
		final EditText usernameInput = (EditText) findViewById(R.id.usernameEdit);
		usernameInput.setText(player.getUsername());
		saveSettings();
		logoutPlayer();
		showScreen((RelativeLayout) findViewById(R.id.loginView), false);
		loginSuccess = false;
		return true;
	}

	public boolean exit() {
		canFinish();
		return true;
	}
}
