package de.tu.darmstadt.uhg.game;

import java.util.ArrayList;
import android.graphics.Bitmap;
import de.tu.darmstadt.uhg.GlobalConstants;
import de.tu.darmstadt.uhg.Map;
import de.tu.darmstadt.uhg.Player;
import de.tu.darmstadt.uhg.gps.GPSPathPoint;
import de.tu.darmstadt.uhg.gps.GPSPoint;
import de.tu.darmstadt.uhg.gps.GPSProgressing;

/**
 * Repr�sentiert den Geist auf dem Spielfeld.
 * @author Chris Michel in74yjyq@stud.tu-darmstadt.de
 */
public class Enemy {
	GPSPoint position;
	GPSPoint direction;
	ArrayList<Bitmap> image;
	Player player;
	Map map;
	GPSPathPoint target;
	GPSPathPoint source;
	int speed;
	int animationCounter;
	float angle;
	double distance;
	double tolerance;
	int intelligence;
	int character;

	/**Algorithmus, sodass der Geist immer den n�chstbesten Weg zum Player nimmt, bemessen an Luftlinie**/
	private int followSimple(ArrayList<GPSPathPoint> potentialTargets) {
		int best = 0;
		double bestDistance = -1;
		for (int i = 0; i < potentialTargets.size(); i++) {
			double currentDistance = 
					GPSProgressing.getWayDifference(potentialTargets.get(i).getPoint(), player.getPosition());
			if ((bestDistance == -1) || (bestDistance > currentDistance)) {
				best = i;
				bestDistance = currentDistance;
			}
		}
		return best;
	}

	/**Algorithmus, sodass der Geist immer den n�chstbesten Weg zum Player nimmt //Beste Methode**/
	private int followExpert(ArrayList<GPSPathPoint> potentialTargets) {
		int best = 0;
		double bestDistance = -1;
		for (int i = 0; i < potentialTargets.size(); i++) {
			
			if ((player.getPointA() == null) || (player.getPointB() == null)) {
				return followSimple(potentialTargets);
			}
			
			double currentDistanceA = 
					GPSProgressing.getWayDifference(potentialTargets.get(i).getPoint(), player.getPointA().getPoint());
			double currentDistanceB = 
					GPSProgressing.getWayDifference(potentialTargets.get(i).getPoint(), player.getPointB().getPoint());

			if ((bestDistance == -1) || (bestDistance > currentDistanceA)) {
				best = i;
				bestDistance = currentDistanceA;
			}

			if (bestDistance > currentDistanceB) {
				best = i;
				bestDistance = currentDistanceB;
			}
			
		}
		return best;
	}
	
	/**Bewegt den Geist auf seiner Strecke**/
	void walk() {
		if (!position.equalsWithTolerance(target.getPoint(), tolerance)) {
			//Lets move the ghost towards his target
			position.add(direction);
		} else {
			//Set new Direction, because Ghost has reached his final destination
			//NEW (25.04.2014, added by Chris): Simple Ghost: Never go back where you came from unless there's no other way!
			
			int newTarget = 0;
			
			//GEIST SUCHT SICH IMMER ZUF�LLIGE WEGE
			if (intelligence >= GlobalConstants.ENEMY_FOOL)
			  newTarget = (int) (Math.random() * target.getLeadToPoints().size());
			
			//GEIST FOLGT PLAYER, WENN DIESER AN EINER BENACHBARTEN KREUZUNG IST
			if (intelligence >= GlobalConstants.ENEMY_EASY)
			  for (int i = 0; i < target.getLeadToPoints().size(); i++)
				  if (target.getLeadToPoints().get(i).getPoint().equalsWithTolerance(player.getPosition(), tolerance))
			          newTarget = i;
			

			//NEW PART: never send ghost back to where he came from before the normal mode
			if (target.getLeadToPoints().size() > 1)
				if (source.equals(target.getLeadToPoints().get(newTarget))) {
					//Target equals last source!!! Choose the next Target in the List if there is one, else index = 0;
					newTarget += 1;
					if (newTarget > target.getLeadToPoints().size()-1)
						newTarget = 0;
				}
					
			
			//GEIST FOLGT PLAYER, WENN DIESER AN EINER BENACHBARTEN KREUZUNG ODER IN DER N�HE IST (RADIUS 5*GEISTERBREITE)
			if (intelligence >= GlobalConstants.ENEMY_NORMAL)
			  for (int i = 0; i < target.getLeadToPoints().size(); i++)
				  if (target.getLeadToPoints().get(i).getPoint().equalsWithTolerance(player.getPosition(), tolerance*5))
			          newTarget = i;

			//GEIST NIMMT DEN WEG, DER AM EHSTEN ZUM PLAYER F�HRT
			if (intelligence >= GlobalConstants.ENEMY_GOOD)
				newTarget = this.followSimple(target.getLeadToPoints());

			//GEIST NIMMT DEN WEG, DER AM EHSTEN ZUM PLAYER F�HRT
			if (intelligence == GlobalConstants.ENEMY_EXTREME)
				newTarget = this.followExpert(target.getLeadToPoints());
						
		    GPSPathPoint temp = target.getLeadToPoints().get(newTarget); 
			source = target; 
			target = temp;
			position = source.getPoint().copy();
			calculateDirection();
		}
	}
	
	public GPSPoint getPosition() {
		return position;
	}
	
	public void setPosition(GPSPoint position) {
		this.position = position;
	}
	
	public ArrayList<Bitmap> getImage() {
		return image;
	}
	
	public void setImage(ArrayList<Bitmap> image) {
		this.image = image;
	}
	
	
	
	public int getIntelligence() {
		return intelligence;
	}

	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}

	public int getSpeed() {
		return speed;
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public int getCharacter() {
		return character;
	}
	
	public void setCharacter(int character) {
		this.character = character;
	}
	
	
	public Enemy(Map map, int intelligence, int speed, int character, ArrayList<Bitmap> ghostImage, GPSPathPoint source, GPSPathPoint target, Player player, AugmentedReality augment) {
		this.map = map;
		this.target = target;
		this.position = source.getPoint().copy();
		this.source = source;
		this.speed = speed;
		this.direction = new GPSPoint(0,0);
		this.intelligence = intelligence;
		this.player = player;
		this.angle = 0;
		this.image = ghostImage;
		this.animationCounter = 0;
		this.character = character;
		calculateDirection();
	}
	
	/**Berechnet f�r den Geist den Vektor, �ber den er das n�chste Ziel erreicht**/
	public void calculateDirection() {
		this.direction = target.getPoint().copy();
		direction.substract(source.getPoint().copy());
		this.distance = GPSProgressing.getWayDifference(source.getPoint(), target.getPoint()); 
		this.direction.scaleIt(((1/this.distance)*speed)/100);
		this.tolerance = this.direction.getAbsolute(); 
	}
	
	/**Geist-Animation... Hier: Rotation**/
	public void animate() {
		/**
		image.getImageMatrix().reset();
		Matrix matrix = new Matrix();
		angle += 10;
		if (angle > 360) angle -= 360;
		//matrix.postRotate(angle, image.getWidth()/2, image.getHeight()/2); 
		matrix.postRotate(angle, 0,0); 
	 	image.setImageMatrix(matrix);  	
	 	**/
	}

	/**Draws ghost do the map**/
	public void draw() {
		/**
		if (MainActivity.GOOGLE_MAP_PLAYING)
			return;
		Point p = map.GPSPointToPixelPoint(position);
		map.drawOver(image.get(animationCounter), p);
		animationCounter += 1;
		if (animationCounter >= image.size()) animationCounter = 0;**/
	}

}
