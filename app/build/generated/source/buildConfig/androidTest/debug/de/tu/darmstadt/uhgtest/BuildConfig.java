/**
 * Automatically generated file. DO NOT MODIFY
 */
package de.tu.darmstadt.uhgtest;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "de.tu.darmstadt.uhgtest";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
