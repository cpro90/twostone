/**
 * Automatically generated file. DO NOT MODIFY
 */
package de.tu.darmstadt.uhg;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "de.tu.darmstadt.uhg";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 24;
  public static final String VERSION_NAME = "2.8";
}
